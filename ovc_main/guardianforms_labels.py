from django.conf import settings

label_id = '%s beneficiary ID ' % settings.APP_NAME
label_person_id = 'Person_id '
label_first_name = 'First name '
label_surname = 'Surname '
label_other_names = 'Other name(s) '
label_sex = 'Sex '
label_nrc = 'NRC number '
label_district = 'District '
label_ward = 'Ward '
label_community = 'Community (CWAC or GDCLSU) '
label_mobile_phone = 'Mobile phone number '
label_email_address = 'Email address '
label_physical_address = 'Physical address '
label_workforce_member = 'Workforce member recorded on paper '
label_register_workforce_member = 'Register a workforce member'
label_register_org_unit = 'Register an organizational unit'


label_heading_paper_trail = 'Paper trail'
label_heading_beneficairy_id = '%s beneficiary ID' % settings.APP_NAME
label_heading_update_guardian_details = 'Update guardian details'
label_heading_guardian_identification = 'Guardian identification information'
label_heading_where_guardian_lives = 'Where the guardian lives'
label_location_change_date = 'Location change date '
label_heading_guadian_contact_details = 'Guardian contact details'

label_tooltip_other_name = 'Any other names which are either in official documents or are commonly used to refer to the person'
label_tooltip_date_of_birth = 'If unknown, put estimate'

label_person_recorded = 'Person recorded on paper'
label_paper_date = 'Date recorded on paper'
label_person_recorded_electronically = 'Person recorded electronically'
label_electronic_time = 'Date and time recorded electronically'
label_interface = 'Interface'

error_message_page_errors = "Guardian not saved - please correct the errors in red below"
error_message_nrc = "NRC number must be in the format 987654/32/1"
error_message_duplicate_nrc = "This NRC has already been associated with another user, workforce member or beneficiary in this system. You cannot register this NRC again."
error_message_duplicate_last_part = "is already in this registry."
error_message_duplicate_name = "There is already a person with the first name and surname you entered, are you sure you want to proceed?"
error_message_death_before_birth = "Date of death cannot be before date of birth"
error_message_death_before_last_beneficiary_date = "Date of death cannot be before previous record date of the beneficiary"
error_message_location_change_date = "Location change date cannot be before previous record dates for this guardian"
error_message_mobile_phone_number = "Phone number must be in the format: '+260977123456'. Up to 10 digits allowed."
error_message_sure_void = "Are you sure this person never existed (ie this record is a mistake or a duplicate record)?"


label_option_update_beneficiary_details = 'Update beneficiary details'
label_option_died = 'Deceased'
label_option_guardian_never_existed = 'Guardian never existed (record is a registration mistake or a duplicate)'


label_button_cancel = 'Cancel'
label_button_save = 'Save' 
label_button_update = 'Update'
label_button_make_changes = 'Make changes'
label_button_save_anyway = 'Save anyway'
label_button_yes = 'Yes'
label_button_no = 'No'