from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from ovc_main import workforceforms, workforce_registration, child_reg_forms, \
        organisation_unit_forms, guardianforms, organisation_registration, beneficiary_registration
import json
import decimal
from ovc_main.models import SetupGeorgraphy as locations, FormResWorkforce, RegPersonsOrgUnits,Forms
from ovc_main.organisation_registration import get_cwacs_in_ward

from django.template import RequestContext, loader
from django.shortcuts import render_to_response

from ovc_main.utils import fields_list_provider as field_provider, \
    validators as validate_helper, geo_location,lookup_field_dictionary as lookup_field, \
    auth_access_control_util

from django.contrib.auth.decorators import login_required
from datetime import datetime, date, time
from ovc_auth import user_manager #import get_user_role_geo_org
#from django.utils import simplejson
#Simport page_definitions
from django.conf import settings

from ovc_main.forms.services import ovc_assess_form as ovc_assess_frm, \
        residential_institution_form as res_inst_form,\
        basic_service_referral as basic_service_referral_form, \
        workforce_assessment_form, public_sensitisation_form, \
        workforce_training_form as workforce_training, child_in_conflict_with_law
from ovc_main.workforceforms_labels import label_new_workforce_title, label_update_workforce_title
from ovc_main.forms import forms_helper


# Create your views here.

def index(request):
    from django.core.paginator import Paginator

    org_types = field_provider.get_list_of_organisation_types()
    org_list = organisation_registration.get_org_list(getJSON=True)

    pages = Paginator(org_list, 25)
    pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': 1}
    is_capture_app = settings.IS_CAPTURE_SITE
    return render(request, 'ovc_main/organisation_search.html', {'orgs_list':pages.page(1), 'pageinfo':pageinfo,'org_types':org_types,'is_capture_app':is_capture_app})

@login_required(login_url='/')
def organisation_view(request):
    organisationid = request.GET['organisation_id']
    organisation = organisation_registration.load_org_from_id(organisationid)
    return render(request, 'ovc_main/organisation_view_readonly.html', {'organisation':organisation,})

@login_required(login_url='/')
def beneficiary_view(request):
    beneficiaryid = request.GET['beneficiary_id']
    beneficiary = beneficiary_registration.load_ben_from_id(beneficiaryid, True)

    print beneficiary.person_type

    if beneficiary.person_type == 'OVC':
        return render(request, 'ovc_main/ovc_child_view_readonly.html', {'beneficiary':beneficiary})
    else:
        return render(request, 'ovc_main/guardian_view_readonly.html', {'beneficiary':beneficiary})

def retrieve_geo_params(request, tmp):
    if 'districts' in request.POST:
        districts = request.POST.getlist('districts')
        tmp['districts'] = districts
    if 'wards' in request.POST:
        wards = request.POST.getlist('wards')
        tmp['wards'] = wards
    if 'communities' in request.POST:
        communities = request.POST.getlist('communities')
        tmp['communities'] = communities

    return tmp

@login_required(login_url='/')
def update_organisation(request):

    pagetitle = 'Update organisational unit'
    if request.GET:

        organisationid = request.GET['organisation_id']
        organisation = organisation_registration.load_org_from_id(organisationid)
        formfields = {'org_name': organisation.name,
                      'date_org_setup': organisation.date_operational,
                      'organisation_type': organisation.unit_type,
                      'legal_registration_type': organisation.legalregistrationtype,
                      'legal_registratiom_number': organisation.registrationnumber,
                      'land_phone_number': organisation.contact.land_phone_number,
                      'mobile_phone_number': organisation.contact.mobile_phone_number,
                      'email_address': organisation.contact.email_address,
                      'postal_address': organisation.contact.postal_address,
                      'physical_address': organisation.contact.physical_address,
                      'districts': organisation.locationserviceprovided['districts'],
                      'wards': organisation.locationserviceprovided['wards'],
                      'communities': organisation.locationserviceprovided['communities'],
                      'org_system_id': organisation.org_system_id,
                      'parent_unit': organisation.parent_organisation_id,
                      'org_never_existed': organisation.is_void,
                      'date_closed': organisation.date_closed}

        print 'wardX',organisation.locationserviceprovided['wards']

        print formfields, 'our fields we will be printing'
        form = organisation_unit_forms.OrganisationRegistrationForm(request.user, formfields, is_update_page = True, **organisation.locationserviceprovided)
        return render(request, 'ovc_main/new_organisation.html', {'form':form, 'organisation':organisation, 'page_title':pagetitle})

    if request.POST:
        print request.POST, 'this is what we actually set out'
        organisationid = request.POST['org_system_id']
        print organisationid, 'in our fresh pist, organisation id'
        organisation = organisation_registration.load_org_from_id(organisationid)

        form = organisation_unit_forms.OrganisationRegistrationForm(request.user, request.POST, is_update_page = True)

        fields_to_bepulled_from_server = auth_access_control_util.filter_fields_for_display(request.user, organisation_unit_forms.permission_type_fields, organisation_id=organisationid)
        form.full_clean()
        tmp = form.cleaned_data
        tmp = retrieve_geo_params(request, tmp)
        print form.errors.items(), 'errors'
        print fields_to_bepulled_from_server, 'fields to be considered'
        temp_passed = auth_access_control_util.custom_form_is_valid(form.errors.items(), fields_to_bepulled_from_server)

        if (form.is_valid() or temp_passed) and form.is_custom_validation():
            organisation_registration.update_organisation(tmp, request.user, changed_fields=form.changed_data, is_selective_update=True)
        #elif temp_passed  and form.is_custom_validation():
        #    organisation_registration.update_organisation(tmp, request.user, changed_fields=form.changed_data, is_selective_update=True)

        else:
            print 'all did not pass, in view of update page'
            form = organisation_unit_forms.OrganisationRegistrationForm(request.user, request.POST, is_update_page = True)
            return render(request, 'ovc_main/new_organisation.html', {'form':form, 'page_title':pagetitle,'organisation':organisation})
        return HttpResponseRedirect('/organisation/')


@login_required(login_url='/')
def new_organisation(request):
    form = organisation_unit_forms.OrganisationRegistrationForm(request.user)
    pagetitle = 'Register organisational unit'
    if request.POST:
        print request.POST, 'VALUES  IN POST'
        form = organisation_unit_forms.OrganisationRegistrationForm(request.user,request.POST)

        if form.is_valid() and form.is_custom_validation():
            tmp = form.cleaned_data
            tmp = retrieve_geo_params(request, tmp)
            organisation_registration.create_organisation(tmp, request.user)

            return HttpResponseRedirect('/organisation/')

    return render(request, 'ovc_main/new_organisation.html', {'form':form,'page_title':pagetitle})

@login_required(login_url='/')
def new_workforce(request):
    #print request.POST
    form = workforceforms.WorkforceRegistrationForm(request.user)
    is_capture_app = settings.IS_CAPTURE_SITE
    if request.POST:
        #raise Exception('because we can')
        form = workforceforms.WorkforceRegistrationForm(request.user, request.POST)

        if form.is_valid():
            tmp = form.cleaned_data
            tmp = retrieve_geo_params(request, tmp)
            wfc = workforce_registration.save_new_workforce(tmp, request.user)
            success = ''
            if wfc.direct_services:
                if wfc.direct_services == '1':
                    success = ' Workforce member saved successfully'
                else:
                    success = ' User saved successfully'
            #return HttpResponseRedirect('/workforce/wfcsearch/')
            from django.core.paginator import Paginator

            wfc_types = field_provider.get_Workforce_Type(search=False)
            wfc_list = workforce_registration.get_wfc_list(getJSON=True,user=request.user)
            pages = Paginator(wfc_list, 25)
            pageinfo = {'pageinfo':'pageinfo',
                        'pagescount':pages.num_pages,
                        'total_records':pages.count,
                        'pagenumber': 1,
                        'success':success}

            return render(request, 'ovc_main/workforce_search.html', {'wfc_list':pages.page(1), 'pageinfo':pageinfo,'wfc_types':wfc_types,'is_capture_app':is_capture_app,})

    pagetitle = label_new_workforce_title
    return render(request, 'ovc_main/new_workforce.html', {'form':form, 'page_title':pagetitle})


@login_required(login_url='/')
def workforce_index(request):
    from django.core.paginator import Paginator

    wfc_types = field_provider.get_Workforce_Type(search=False)
    wfc_list = workforce_registration.get_wfc_list(getJSON=True,user=request.user)

    pages = Paginator(wfc_list, 25)
    pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': 1}
    is_capture_app = settings.IS_CAPTURE_SITE
    return render(request, 'ovc_main/workforce_search.html', {'wfc_list':pages.page(1), 'pageinfo':pageinfo,'wfc_types':wfc_types,'user':request.user,'is_capture_app':is_capture_app})

@login_required(login_url='/')
def wfc_search_results(request):
    from django.core.paginator import Paginator

    searchtokenstring = request.GET['searchterm']

    itemsperpage = 25
    if 'itemsperpage' in request.GET:
        itemsperpage = request.GET['itemsperpage']

    wfc_type = None
    if 'wfctype' in request.GET:
        wfc_type = request.GET['wfctype']

    pagenumber = 1
    if 'pagenumber' in request.GET:
        pagenumber = request.GET['pagenumber']

    searchtokens = searchtokenstring.strip().split(' ')
    wfc_list = workforce_registration.get_wfc_list(cleantokens(searchtokens), getJSON=True, wfc_type=wfc_type, user = request.user)

    if int(itemsperpage) == -1:
        pages = Paginator(wfc_list, 0)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':1,
                'total_records':len(wfc_list),
                'pagenumber': 1}
        todisplay = wfc_list
    else:
        pages = Paginator(wfc_list,itemsperpage)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': pagenumber}
        todisplay = pages.page(pagenumber).object_list


    return HttpResponse(json.dumps((todisplay +[pageinfo] )))

@login_required(login_url='/')
def get_workforce_for_res_institution(request):
    user = request.user
    date_of_form = None
    form_id = None
    if 'date' in request.GET:
        date_of_form = request.GET['date']
    if 'form_id' in request.GET:
        form_id = request.GET['form_id']

    org_id = None
    if 'org_id' in request.GET:
        org_id = request.GET['org_id']
    #TODO Gift date filter
    wfc_list = workforce_registration.search_wfc_by_primary_org_type_and_date(primary_org_id = org_id,\
        primary_org_type=lookup_field.org_type_residential_children, by_date = date_of_form, user = user, getJSON=False)

    to_return = {}
    count = 1
    for wk_force in wfc_list:
        if wk_force.workforce_id and wk_force.workforce_id == lookup_field.empty_workforce_id:
            continue
        role_id = ''
        employment_status_id = ''
        if wk_force.id_int and form_id:
            if len(FormResWorkforce.objects.filter(workforce_id=wk_force.id_int, form_id=form_id)) > 0:
                model_res_workforce = FormResWorkforce.objects.filter(workforce_id=wk_force.id_int, form_id= form_id)[0]
                role_id = model_res_workforce.position_id
                employment_status_id = model_res_workforce.full_part_time_id
        workforce_status = {
            'workforce_id': wk_force.user_id,
            'workforce_name': wk_force.name,
            'role_id': role_id,
            'employment_status_id': employment_status_id
        }
        to_return[count] = workforce_status
        count = count + 1

    return HttpResponse(json.dumps(to_return))

@login_required(login_url='/')
def workforce_view(request):
    person_id = request.GET['person_id_int']
    user = request.user
    workforce = workforce_registration.load_wfc_from_id(person_id,user)

    sec = get_security_attributes(user,workforce)

    return render(request, 'ovc_main/workforce_view_readonly.html', {'workforce':workforce,'sec':sec})

def get_security_attributes(user=None,workforce=None):
    user_can_edit = 'None'
    user_can_view = 'None'
    user_can_view_cursor = 'None'
    user_can_assign_role = 'None'
    if user:
        if user.is_superuser:
            user_can_edit = 'text'
            user_can_view = 'table_row_clickable'
            user_can_view_cursor = 'cursor:pointer'
            user_can_assign_role = 'text'
        else:
            if user.has_perm('auth.wkf view'):
                user_can_view = 'table_row_clickable'
                user_can_view_cursor = 'cursor:pointer'

            ''' In lookup, show the 'file update' button depending on permissions '''
            ''' 1. any cross cutting 'file update' permission for workforce(15,16 or 17) or '''
            user_has_cross_cutn_perms = user.has_perm('auth.update wkf contact') or user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf special')

            ''' 2. user is related to the selected workforce member and has specific 'file update' permissions (5 or 6), or '''
            wfc_related_to_user = workforce_registration.wfc_and_user_related(workforce,user)
            user_has_specific_update_per = user.has_perm('auth.update wkf contact rel') or user.has_perm('auth.update wkf general rel')
            related_and_has_update_perms = wfc_related_to_user and user_has_specific_update_per

            ''' 3. the user is the selected workforce member and has permission 37 '''
            if user.reg_person:
                wfc_is_logged_in_user_with_perm = user.reg_person.pk == workforce.id_int and user.has_perm('auth.update wkf contact own')
                if user_has_cross_cutn_perms or related_and_has_update_perms or wfc_is_logged_in_user_with_perm:
                    user_can_edit = 'text'

            ''' if user has crosscutting permission 32 or 33, then they can allocate roles '''
            #print user.has_perm('auth.role management'),'user.has_perm(\'auth.role management\')'
            #print user.has_perm('auth.assign role non org spec'),'user.has_perm(\'auth.assign role non org spec\')'
            if user.has_perm('auth.role management') or user.has_perm('auth.assign role non org spec'):
                user_can_assign_role = 'text'

            ''' If they have org specific permissions 10 and 31, then they can view person based on some factors '''
            if user.has_perm('auth.role assign org rel'):
                wfc_related_to_user = workforce_registration.wfc_and_user_related(workforce,user)
                if wfc_related_to_user:
                    user_can_assign_role = 'text'

            if user.has_perm('auth.assign role org spec'):
                belongs_to_same_org = workforce_registration.user_and_wfc_same_org(workforce,user)
                if belongs_to_same_org:
                    user_can_assign_role = 'text'

    sec = {'user_can_edit':user_can_edit,'user_can_view':user_can_view,'user_can_view_cursor':user_can_view_cursor,'user_can_assign_role':user_can_assign_role,}
    return sec

@login_required(login_url='/')
def workforce_update(request):
    is_related_to_user = False
    is_logged_in_user = False
    user = request.user
    if request.GET:
        personid = request.GET['person_id']
        workforce = workforce_registration.load_wfc_from_id(personid,user)
        tmp_direct_services = '1'
        if workforce.workforce_id == lookup_field.empty_workforce_id:
            tmp_direct_services = '2'
        if workforce.date_of_birth:
            workforce.date_of_birth = workforce.date_of_birth.strftime(validate_helper.date_input_format)

        is_related_to_user = workforce_registration.wfc_and_user_related(workforce,user)
        if user.reg_person:
            if user.reg_person.pk == int(personid):
                is_logged_in_user = True

        formfields = {'nrc': workforce.national_id,
                      'wfc_system_id': personid,
                      'workforce_id': workforce.workforce_id,
                      'first_name': workforce.first_name,
                      'last_name': workforce.surname,
                      'other_names': workforce.other_names,
                      'sex': workforce.sex_id,
                      'date_of_birth': workforce.date_of_birth,
                      'workforce_type': workforce.person_type_id,
                      'man_number': workforce.man_number,
                      'ts_number': workforce.ts_number,
                      'sign_number': workforce.sign_number,
                      'direct_services': tmp_direct_services,
                      'org_units': workforce.org_units,
                      'districts': workforce.geo_location['districts'],
                      'wards': workforce.geo_location['wards'],
                      'communities': workforce.geo_location['communities'],
                      'designated_phone_number': workforce.contact.designated_phone_number,
                      'other_mobile_number': workforce.contact.other_mobile_number,
                      'email_address': workforce.contact.email_address,
                      'physical_address': workforce.contact.physical_address,
                      'steps_ovc_number': workforce.steps_ovc_number,
                      'wfc_system_id': workforce.id_int,
                      'org_data_hidden': workforce.org_data_hidden,
                      'is_related_to_user': is_related_to_user,
                      'is_logged_in_user': is_logged_in_user   ,
                      'primary_org_id': workforce.primary_org_id
                      }
        select_fields_radio = 'checked'
        pagetitle = label_update_workforce_title
        form = workforceforms.WorkforceRegistrationForm(request.user, formfields, is_update_page = True)
        return render(request, 'ovc_main/new_workforce.html', {'form':form, 'workforce':workforce, 'page_title':pagetitle,'select_fields_radio':select_fields_radio})

    if request.POST:
        #print request.POST,'request.POST'
        pagetitle = label_update_workforce_title
        tmp_current_workforce_id = request.POST['wfc_system_id']
        tmp_org_workforce_obj = workforce_registration.load_wfc_from_id(tmp_current_workforce_id,request.user)
        form = workforceforms.WorkforceRegistrationForm(request.user, request.POST, is_update_page = True)

        fields_to_bepulled_from_server = auth_access_control_util.filter_fields_for_display(request.user, workforceforms.fields_by_perms, owner_id=tmp_current_workforce_id, organisation_id=tmp_org_workforce_obj.primary_org_id)
        form.full_clean()
        temp_passed = auth_access_control_util.custom_form_is_valid(form.errors.items(), fields_to_bepulled_from_server)

        received_data = form.cleaned_data
        received_data = retrieve_geo_params(request, received_data)
        person_id = 0
        if received_data.has_key('wfc_system_id'):
            person_id = received_data['wfc_system_id']
        national_id = ''
        if received_data.has_key('nrc'):
            national_id = received_data['nrc']
        edit_mode_hidden = '1'
        if received_data.has_key('edit_mode_hidden'):
            edit_mode_hidden = received_data['edit_mode_hidden']
        #print form.errors.items(),'form.errors.items()'
        select_death_radio = ''
        select_fields_radio = 'checked'
        #If we are setting a user's date of death, only the date of death validation matters
        if edit_mode_hidden == '2' and not temp_passed:
            for field, value in form.errors.items():
                if field == 'date_of_death':
                    temp_passed = False
                    select_death_radio = 'checked'
                    select_fields_radio = ''
        #If we are deactivating user and custom validation fails, we don't really mind coz none of those fields matter anyway
        if edit_mode_hidden == '3' and not temp_passed:
            temp_passed = True

        if user.reg_person:
            if user.reg_person.pk == int(person_id):
                is_logged_in_user = True

        #Dont allow self deactivation and setting own date of death
        #if (edit_mode_hidden == '3' or edit_mode_hidden == '2') and is_logged_in_user:
            #temp_passed = False

        wfc = None
        #print temp_passed,'temp_passed'
        if form.is_valid() and False:  #making it fail at all times so we handle everything
            wfc = workforce_registration.update_workforce(received_data, person_id, request.user, changed_fields=form.changed_data)
        elif temp_passed:
            wfc = workforce_registration.update_workforce(received_data, person_id, request.user, changed_fields=form.changed_data, is_selective_update=True)
        else:
            wfc = workforce_registration.load_wfc_from_id(person_id,request.user)
            post = request.POST.copy()
            post['primary_org_id'] = wfc.primary_org_id
            form = workforceforms.WorkforceRegistrationForm(request.user, post, is_update_page = True)
            failed_save = True
            return render(request, 'ovc_main/new_workforce.html', {'form':form, 'workforce':wfc, 'page_title':pagetitle, 'select_death_radio':select_death_radio, 'select_fields_radio':select_fields_radio, 'failed_save':failed_save})

        success = ''
        #print wfc.direct_services,'wfc.direct_services'
        if wfc.direct_services:
            if wfc.workforce_id == lookup_field.empty_workforce_id:
                success = ' User updated successfully'
            else:
                success = ' Workforce updated successfully.'
        from django.core.paginator import Paginator

        wfc_types = field_provider.get_Workforce_Type(search=False)
        wfc_list = workforce_registration.get_wfc_list(getJSON=True,user=request.user)
        pages = Paginator(wfc_list, 25)
        pageinfo = {'pageinfo':'pageinfo',
                    'pagescount':pages.num_pages,
                    'total_records':pages.count,
                    'pagenumber': 1,
                    'success':success}
        return render(request, 'ovc_main/workforce_search.html', {'wfc_list':pages.page(1), 'pageinfo':pageinfo,'wfc_types':wfc_types})

@login_required(login_url='/')
def update_beneficiary(request):
    print 'outside get mode'
    if request.GET:
        personid = request.GET['beneficiary_id']
        beneficiary = beneficiary_registration.load_ben_from_id(personid)

        guardian_data = 'first_time'
        update_mode = 'Yes'
        g_chnge_date = None

        print g_chnge_date,

        for grd in beneficiary.guardian:
            g_chnge_date = grd.datelinked.strftime(validate_helper.date_input_format)

        today = datetime.now().date()#datetime.datetime.date()#.today()
        dpff = datetime.strptime(str(today), '%Y-%m-%d')

        date_paper_form = dpff.strftime(validate_helper.date_input_format)

        date_for_loc = None

        if beneficiary.location_change_date:
            date_for_loc = beneficiary.location_change_date.strftime(validate_helper.date_input_format)

        formfields = {
                      'nrc': beneficiary.national_id,
                      'pk_id': beneficiary.pk_id,
                      'beneficiary_id': beneficiary.beneficiary_id,
                      'first_name': beneficiary.first_name,
                      'last_name': beneficiary.last_name,
                      'other_name': beneficiary.other_name,
                      'sex': beneficiary.sex_id,
                      'date_of_birth': beneficiary.date_of_birth,
                      'birth_cert_num': beneficiary.birth_cert_num,
                      'STEPS_OVC_number': beneficiary.steps_ovc_number,
                      'nrc': beneficiary.national_id,
                      'guardians': beneficiary.guardian,
                      'mobile_phone_number': beneficiary.contact.mobile_phone_number,
                      'email_address': beneficiary.contact.email_address,
                      'physical_address': beneficiary.contact.physical_address,
                      'ben_district': beneficiary.residence.district,
                      'ben_ward': beneficiary.residence.ward,
                      'community': beneficiary.residence.community,
                      'res_id_hidden': beneficiary.residence.res_id,
                      'workforce_member': beneficiary.workforce_member,
                      #'date_paper_form_filled': beneficiary.date_paper_form_filled,
                      'guardians_hidden': guardian_data,
                      'guardian_change_date': date_paper_form,
                      'prev_guard_change_date': g_chnge_date,
                      'location_change_date': date_paper_form,
                      'date_of_death': date_paper_form,
                      'prev_loc_change_date': date_for_loc,
                      'date_paper_form_filled': date_paper_form,
                      'residential_institution': beneficiary.residence.residential_institution,
                      'is_update':update_mode,
                      }

        page_to_display = ''
        if beneficiary.person_type == 'OVC':
            pagetitle = 'Update OVC Child'
            form = child_reg_forms.ChildRegistrationForm(request.user,formfields, is_update_page = True)
            page_to_display = 'ovc_main/new_child.html'
        if beneficiary.person_type == 'OVC guardian':
            pagetitle = 'Update OVC Guardian'
            form = guardianforms.GuardianRegistrationForm(request.user,formfields, is_update_page = True)
            page_to_display = 'ovc_main/new_guardian.html'
        return render(request, page_to_display, {'form':form, 'beneficiary':beneficiary, 'page_title':pagetitle})

    if request.POST:
        form_for_id = child_reg_forms.ChildRegistrationForm(request.user,request.POST, is_update_page = True)

        person_id = form_for_id.data['pk_id']
        person_type = None

        if person_id:
            person_type = field_provider.get_person_type(person_id)

        pagetitle = 'Update Beneficiary'
        form = None

        permission_type_fields = None

        print 'our user',request.user
        if person_type == 'OVC':
            form = child_reg_forms.ChildRegistrationForm(request.user,request.POST, is_update_page = True)
            permission_type_fields = child_reg_forms.permission_type_fields
        else:
            form = guardianforms.GuardianRegistrationForm(request.user, request.POST, is_update_page = True)
            permission_type_fields = guardianforms.permission_type_fields

        fields_to_bepulled_from_server = auth_access_control_util.filter_fields_for_display(request.user, permission_type_fields)
        form.full_clean()
        tmp = form.cleaned_data
        tmp = retrieve_geo_params(request, tmp)
        print form.errors.items(), 'errors'
        print fields_to_bepulled_from_server, 'fields to be considered'
        temp_passed = auth_access_control_util.custom_form_is_valid(form.errors.items(), fields_to_bepulled_from_server)

        if (form.is_valid() or temp_passed):# and form.is_custom_validation():
            received_data = form.cleaned_data
            beneficiary_registration.update_ben(received_data, person_id, request.user, person_type, is_selective_update=True)
            #organisation_registration.update_organisation(tmp, request.user, changed_fields=form.changed_data, is_selective_update=True)

            from django.core.paginator import Paginator

            ben_list = []
            ben_types = field_provider.get_list_of_ben_types()
            ben_list = beneficiary_registration.load_beneficiaries(getJSON=True)

            success = None

            ben_id = field_provider.get_ben_id_from_pk(person_id)

            if ben_id:
                if person_type == 'OVC':
                    success = 'Child updated successfully with ID ' + ben_id
                else:
                    success = 'Guardian updated successfully with ID ' + ben_id
            else:
                if person_type == 'OVC':
                    success = 'Child updated successfully'
                else:
                    success = 'Guardian updated successfully'

            pages = Paginator(ben_list, 25)
            pageinfo = {'pageinfo':'pageinfo',
                        'pagescount':pages.num_pages,
                        'total_records':pages.count,
                        'pagenumber': 1,
                        'success': success
                        }

            #return HttpResponseRedirect("/beneficiary/benlists?searchterm=" + "" + "&pagenumber=" + pageinfo.pagescount + "&itemsperpage=" + 25 + "&bentype=" + "")
            #return HttpResponseRedirect('ovc_main/beneficiary_search.html')
            #print 'here 2'
            #render_to_response('my_template.html',my_data_dictionary,context_instance=RequestContext(request))
            #return render(request, 'ovc_main/beneficiary_search.html', {'bens_list':ben_list, 'pageinfo':pageinfo, 'ben_types':ben_types, })
            return HttpResponseRedirect('/beneficiary/ben_search/?&success=' + success)
        else:
            print form.errors
            if person_type == 'OVC':
                page_to_display = 'ovc_main/new_child.html'
            else:
                page_to_display = 'ovc_main/new_guardian.html'

            return render(request, page_to_display, {'form':form, 'page_title':pagetitle})
        print 'pageinfo', pageinfo
        #return HttpResponseRedirect('/beneficiary/ben_search/')


def listofwards(request):
    if request.method == 'GET':

        print request.GET

        district = request.GET['district_id']

        wardstoreturn = {}
        if district:

            constituencies =  locations.objects.filter(parent_area_id=district)
            sorted_constituencies = sorted(constituencies)
            for constituency in sorted_constituencies:
                for ward in locations.objects.filter(parent_area_id=constituency.area_id):
                    wardstoreturn[ward.area_id] = constituency.area_name+'-'+ward.area_name

        return HttpResponse(json.dumps(wardstoreturn))

def list_of_wards_in_district(request):
    dist_id = request.GET['district_id']
    wards = None
    wards = field_provider.get_list_children_from_parent_area_id(dist_id)

    return HttpResponse(json.dumps(list_of_wards))

def check_person_names(request):
    print 'got here'
    pk_id = request.GET['pk_id']
    first_name = request.GET['first_name']
    last_name = request.GET['last_name']
    found_person = field_provider.get_person_by_names(pk_id,first_name,last_name)
    return HttpResponse(json.dumps(found_person))

@login_required(login_url='/')
def check_form_titles(request):
    title = request.GET['title']
    form_id_int = request.GET['form_id']
    duplicate_found = False
    if title:
        m_form = Forms.objects.filter(form_title=title, is_void=False)
        if form_id_int:
            for form in m_form:
                if form.pk == int(form_id_int):
                    continue
                else:
                    dups_detected = True
                    break
        else:
            if len(m_form) > 0:
                duplicate_found = True
    return HttpResponse(json.dumps(duplicate_found))

def list_of_cwacs_in_ward(request):

    wardid = request.GET['ward_id']
    cwacs = {}

    if wardid:
        tmpcwacs = get_cwacs_in_ward(wardid)
        for cwac in tmpcwacs:
            cwacs[cwac.org_system_id] = '%s %s' % (cwac.org_id, cwac.name)

    return HttpResponse(json.dumps(cwacs))

@login_required(login_url='/')
def org_autocompletion_list(request):
    if request.method == 'GET':
        text = request.GET['org_name']
        user = request.user
        eligible_primary_orgs = auth_access_control_util.reg_assistant_orgs(user, subunit_limit=2)
        filter = []
        ''' Get the workforce ids for the eligible primary organisation ids '''
        for a,b in eligible_primary_orgs:
            filter.append(a)
        org_units_tpl = field_provider.get_list_of_organisations(orgname = text, user = user, search_ids=True)
        org_units = {}
        if user.is_superuser:
            org_units = tuple([(a, b, True) for a,b in org_units_tpl])
        else:
            org_units = tuple([(a, b, a in filter) for a,b in org_units_tpl])
        return HttpResponse(json.dumps(org_units))

@login_required(login_url='/')
def res_wfc_autocomplete(request):
    if request.method == 'GET':
        token = request.GET['wfc_name']
        user = request.user
        wfc_list = get_workforce_user_filled_res_form(user, token)

    return HttpResponse(json.dumps(wfc_list))

@login_required(login_url='/')
def all_workforce_autocomplete(request):
    ''' This method only returns the list of workforce members alone. Users are left out '''
    if request.method == 'GET':
        token = request.GET['wfc_name']
        user = request.user
        wfc_list = field_provider.get_list_of_workforce(workforce_name=token, user=user, workforce_only=True)

    return HttpResponse(json.dumps(wfc_list))

def get_workforce_user_filled_res_form(user, token=None):
    '''
    If user has data entry permission, this is Ajax control searches within registered
    workforce members and users, showing name, ZOMIS ID and NRC.
    If the user has standard logged in or inspector role, this is present to the
    current user.
    '''
    wfc_list = []
    if token:
        print 'Yes token'
        if user.has_perm('auth.enter frm high sensitive'):
            print 'Yes data entry'
            wfc_list = field_provider.get_list_of_workforce(workforce_name=token, user=user)
            print wfc_list,'wfc_list'
    if not wfc_list:
        print 'Not wfc_list'
        if not user.has_perm('auth.enter frm high sensitive') and (user.has_perm('auth.enter frm high sensitive own') or user.has_perm('auth.enter frm as inspector')):
            if user.reg_person:
                wfc_of_interest = [user.reg_person]
                wfc_list = tuple([(wfc.pk, wfc.workforce_id, wfc.national_id, wfc.first_name + ' ' + wfc.surname, True) for wfc in wfc_of_interest])
    return wfc_list

@login_required(login_url='/')
def res_child_autocomplete(request):
    '''
    Get child list for autocomplete control in section C of Residential institution Form
    If user has beneficiary lookup permission, allow searching by name, otherwise search by IDs only
    '''
    if request.method == 'GET':
        to_return = {}
        token = request.GET['child_name']
        user = request.user

        #search_by_id_only = False
        #if not user.has_perm('auth.ben lookup'):
        #    search_by_id_only = True
        #child_list = field_provider.get_beneficiaries(token, lookup_field.child_beneficiary_ovc, search_by_id_only, search_by_location = False)

        child_list = field_provider.search_ben_ovc(token, user)
        for child in child_list:
            if child.date_of_birth:
                child.date_of_birth = child.date_of_birth.strftime(validate_helper.date_input_format)
            if child.date_of_death:
                child.date_of_death = child.date_of_death.strftime(validate_helper.date_input_format)
        ''' Get prior date admitted and date left '''
        previous_dates = forms_helper.get_recent_child_date_admitted_left(child_list)

        to_return = tuple([(child.pk, child.beneficiary_id, child.birth_reg_id,\
                child.first_name + ' ' + child.surname, child.date_of_birth, \
                child.date_of_death, previous_dates[child.pk]['date_admitted'], \
                previous_dates[child.pk]['date_left'], field_provider.get_person_geo_location(child.pk)[0], \
                field_provider.get_person_geo_location(child.pk)[1])
                for child in child_list])

        return HttpResponse(json.dumps(to_return))

@login_required(login_url='/')
def public_sens_ben_autocomp(request):
    if request.method == 'GET':
        to_return = {}
        token = request.GET['ben_name']
        user = request.user

        '''
        Get beneficiary list for autocomplete control in section D of Public Sensitisation Form

        '''
        child_list = field_provider.search_ben_ovc(token, user, person_type = None)

        to_return = tuple([(child.pk, child.beneficiary_id, child.birth_reg_id,\
                child.national_id, child.first_name + ' ' + child.surname, \
                field_provider.get_description_for_item_id(child.sex_id)[0], field_provider.calculate_age(child.date_of_birth) )
                for child in child_list])
        print to_return,'to_return'
        return HttpResponse(json.dumps(to_return))

def ben_autocompletion_list(request):
    if request.method == 'GET':
        print request.GET
        text = request.GET['ben_name']
        ben_info = field_provider.get_list_of_beneficiaries(text, 'guardian')

        return HttpResponse(json.dumps(ben_info))

def get_guardian_contacts(request):
    if request.method == 'GET':
        print request.GET
        grd_id = request.GET['guardian_id']
        ben_info = field_provider.get_guardian_contacts(grd_id)
        print ben_info
        return HttpResponse(json.dumps(ben_info))

def ben_wfc_autocompletion_list(request):
    if request.method == 'GET':
        print 'wfc autocomp'
        print request.GET
        wfc_id = request.GET['wfc_id'] if request.GET.has_key('wfc_id') else None
        wfc_name = request.GET['wfc_name'] if request.GET.has_key('wfc_name') else None
        wfc_type = request.GET['wfc_type'] if request.GET.has_key('wfc_type') else None
        wfc_info = None
        if wfc_name:
            wfc_info = field_provider.get_list_of_beneficiaries(wfc_name, 'workforce',True, wfc_type)
        elif wfc_id:
            first_char = wfc_id[0];
            if first_char == 'W':
                wfc_info = field_provider.get_wfc_n_id_full_name(wfc_id)
            else:
                wfc_info = field_provider.get_wfc_n_id_full_name_int(wfc_id)

        return HttpResponse(json.dumps(wfc_info))

def ben_res_autocompletion_list(request):
    if request.method == 'GET':
        print request.GET
        text = request.GET['ins_name']
        ben_info = field_provider.get_list_of_residentials(text)

        print 'ben information'
        print ben_info

        return HttpResponse(json.dumps(ben_info))

def org_search_results(request):
    from django.core.paginator import Paginator

    print request.GET, 'checking what was passed'
    searchtokenstring = request.GET['searchterm']

    search_all = False
    if 'search_all' in request.GET:
        search_all = request.GET['search_all']
        if search_all.lower() == 'true':
            search_all = True
        else:
            search_all = False

    itemsperpage = 25
    if 'itemsperpage' in request.GET:
        itemsperpage = request.GET['itemsperpage']

    org_type = None
    if 'orgtype' in request.GET:
        org_type = request.GET['orgtype']

    pagenumber = 1
    if 'pagenumber' in request.GET:
        pagenumber = request.GET['pagenumber']

    searchtokens = searchtokenstring.strip().split(' ')
    org_list = organisation_registration.get_org_list(cleantokens(searchtokens), getJSON=True, org_type=org_type, search_all_orgs=search_all)

    print 'these are the items by page', itemsperpage

    if int(itemsperpage) == -1:
        pages = Paginator(org_list, 0)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':1,
                'total_records':len(org_list),
                'pagenumber': 1}
        todisplay = org_list
    else:
        pages = Paginator(org_list,itemsperpage)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': pagenumber}
        todisplay = pages.page(pagenumber).object_list

    print json.dumps((todisplay +[pageinfo] ))
    return HttpResponse(json.dumps((todisplay +[pageinfo] )))


def ben_search_results(request):
    from django.core.paginator import Paginator

    print request.GET, 'checking what was passed beneficiary'
    searchtokenstring = request.GET['searchterm']

    itemsperpage = 25
    if 'itemsperpage' in request.GET:
        itemsperpage = request.GET['itemsperpage']

    ben_type = None
    if 'bentype' in request.GET:
        ben_type = request.GET['bentype']

    pagenumber = 1
    if 'pagenumber' in request.GET:
        pagenumber = request.GET['pagenumber']

    searchtokens = searchtokenstring.strip().split(' ')

    ben_list = beneficiary_registration.load_beneficiaries(token = searchtokenstring, ben_type = ben_type, getJSON=True) #organisation_registration.get_org_list(cleantokens(searchtokens), getJSON=True, org_type=org_type)

    print 'these are the items per page', itemsperpage
    if int(itemsperpage) == -1:
        pages = Paginator(ben_list, 0)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':1,
                'total_records':len(ben_list),
                'pagenumber': 1}
        todisplay = ben_list
    else:
        pages = Paginator(ben_list,itemsperpage)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': pagenumber}
        todisplay = pages.page(pagenumber).object_list

        print 'page information:'
        print pageinfo
        print 'page toDisplay:'
        print todisplay
        print 'page json:'
        print json.dumps((todisplay +[pageinfo] ))
    return HttpResponse(json.dumps((todisplay +[pageinfo] )))

def cleantokens(tokens):
    '''
    get rid of of empty tokens
    method should be improved on
    '''
    toreturn = []
    for token in tokens:
        if token:
            toreturn.append(token.strip())
    if toreturn:
        return toreturn
    return None

@login_required(login_url='/')
def new_guardian_reg(request):
    print request.POST
    form = guardianforms.GuardianRegistrationForm(request.user)

    if request.POST:
        form = guardianforms.GuardianRegistrationForm(request.user,request.POST)

        if form.is_valid():
            print form.cleaned_data
            ben_id = beneficiary_registration.save_beneficiary(form.cleaned_data, request.user, 'guardian')

            from django.core.paginator import Paginator

            ben_list = []
            ben_types = field_provider.get_list_of_ben_types()
            ben_list = beneficiary_registration.load_beneficiaries(getJSON=True)

            success = None
            if ben_id:
                success = 'Guardian saved successfully with ID ' + ben_id
            else:
                success = 'Guardian saved successfully'

            pages = Paginator(ben_list, 25)
            pageinfo = {'pageinfo':'pageinfo',
                        'pagescount':pages.num_pages,
                        'total_records':pages.count,
                        'pagenumber': 1,
                        'success': success
                        }

            return HttpResponseRedirect('/beneficiary/ben_search/?&success=' + success)
            #return HttpResponseRedirect('/beneficiary/ben_search')

    return render(request, 'ovc_main/new_guardian.html', {'form':form})
@login_required(login_url='/')
def new_child_reg(request):
    print request.POST
    form = child_reg_forms.ChildRegistrationForm(request.user)

    if request.POST:
        form = child_reg_forms.ChildRegistrationForm(request.user,request.POST)

        if form.is_valid():
            if settings.DEBUG:
                print request.user.id
                print form.cleaned_data

            ben_id = beneficiary_registration.save_beneficiary(form.cleaned_data, request.user, 'ovc')

            from django.core.paginator import Paginator

            ben_list = []
            ben_types = field_provider.get_list_of_ben_types()
            ben_list = beneficiary_registration.load_beneficiaries(getJSON=True)

            success = None
            if ben_id:
                success = 'Child saved successfully with ID ' + ben_id
            else:
                success = 'Child saved successfully'

            pages = Paginator(ben_list, 25)
            pageinfo = {'pageinfo':'pageinfo',
                        'pagescount':pages.num_pages,
                        'total_records':pages.count,
                        'pagenumber': 1,
                        'success': success
                        }

            return HttpResponseRedirect('/beneficiary/ben_search/?&success=' + success)

            #return HttpResponseRedirect('/beneficiary/ben_search/')
        else: print form.errors

    return render(request, 'ovc_main/new_child.html', {'form':form})

def search_beneficiary(request):
    from django.core.paginator import Paginator

    ben_list = []
    ben_types = field_provider.get_list_of_ben_types()
    ben_list = beneficiary_registration.load_beneficiaries(getJSON=True)

    pages = Paginator(ben_list, 25)

    if 'success' in request.GET:
        success = request.GET['success']
        pageinfo = {'pageinfo':'pageinfo',
                    'pagescount':pages.num_pages,
                    'total_records':pages.count,
                    'pagenumber': 1,
                    'success':success}
    else:
        pageinfo = {'pageinfo':'pageinfo',
                    'pagescount':pages.num_pages,
                    'total_records':pages.count,
                    'pagenumber': 1}
    is_capture_app = settings.IS_CAPTURE_SITE
    return render(request, 'ovc_main/beneficiary_search.html', {'bens_list':pages.page(1), 'pageinfo':pageinfo, 'ben_types':ben_types, 'is_capture_app':is_capture_app})
#def process_page(page_fields):
#    page_def = page_definitions[page_fields]
#
#    try:
#        page_def(page_fields)
#    except:
#        return 'failed'
#
#    return 'success'

@login_required(login_url='/')
def forms_home_page(request):
    from django.core.paginator import Paginator

    user = request.user
    available_form_types = field_provider.get_form_types_for_workforce(user)
    forms = forms_helper.Load_form('all',True,user)
    pages = Paginator(forms, 25)

    if 'success' in request.GET:
        success = request.GET['success']
        pageinfo = {
                'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': 1,
                'success':success
                }
    else:
        pageinfo = {
                    'pageinfo':'pageinfo',
                    'pagescount':pages.num_pages,
                    'total_records':pages.count,
                    'pagenumber': 1
                    }

    return render(request, 'ovc_main/Forms/forms_home.html', {'form_types':available_form_types, 'pageinfo':pageinfo, 'forms':forms})

@login_required(login_url='/')
def user_can_add_form(request):
    print user_can_add_form,'user_can_add_form'
    can_add_form = True
    user = request.user
    form_type = request.GET['form_type']
    subject = request.GET['subject']
    to_return = {}
    if form_type:
        can_add_form = forms_helper.can_user_edit_form(form_type, user, subject)

    to_return = {'can_add_form': can_add_form,}
    return HttpResponse(json.dumps(to_return))

def get_subject(request):
    if request.method == 'GET':
        if request.GET.has_key('check_perms_wfc'):
            if request.user.is_superuser:
                return HttpResponse(json.dumps({'has_search_perm':True}))
            elif request.user.has_perm('auth.enter frm high sensitive'):
                return HttpResponse(json.dumps({'has_search_perm':True}))
            elif request.user.has_perm('auth.enter frm high sensitive own') and (request.user.reg_person.workforce_id != '' and request.user.reg_person.workforce_id != None):
                return HttpResponse(json.dumps({'has_search_perm':'self','wfc_pk':request.user.reg_person.pk,'wfc_text':"%s \t %s \t %s"%(request.user.reg_person.workforce_id,request.user.reg_person.national_id,request.user.reg_person.full_name)}))
            else:
                return HttpResponse(json.dumps({'has_search_perm':False}))

        token = request.GET['search_token']
        form_type = request.GET['form_type']
        search_info = field_provider.get_list_of_subjects(token,form_type)
        search_info = apply_security_subject_rules(form_type, request.user, search_info)
        return HttpResponse(json.dumps(search_info))

def apply_security_subject_rules(formtype, user, results):
    if formtype == 'FT3e':
        if user and user.has_perm('auth.enter frm high sensitive'):
            return results
        elif user and user.reg_person.workforce_id and user.has_perm('enter frm high sensitive own'):
            for subjectid, subjectworkforceid, subjectnationalid,subjectfullname in results:
                print 'stuff that is actually returned',subjectid, subjectworkforceid, subjectnationalid,subjectfullname
                if user.reg_person.pk == subjectid:
                    return (subjectid, subjectworkforceid, subjectnationalid,subjectfullname,)
        return tuple([('', '', '', 'No workforce results',)])
    return results


@login_required(login_url='/')
def load_form(request):
    subject = None
    form_type = None
    form_pk_id = None
    formfields = None
    user = request.user
    can_add_form = None
    other_params_dict = {}
    if request.GET:
        form_pk_id = request.GET['form_id'] if request.GET.has_key('form_id') else None
        if form_pk_id:
            #Update Mode
            form_object = forms_helper.Load_form(form_pk_id,user=user)
            form_type = form_object.form_type_id
            subject = form_object.form_subject_id
            formfields_questions = get_form_fields_with_values(form_object)
            formfields = get_custom_data_for_formfields(form_type,formfields_questions,form_object,user)
        else:
            #New mode
            subject = request.GET['subject'] if request.GET.has_key('subject') else None
            form_type = request.GET['form_type'] if request.GET.has_key('form_type') else None
            if form_type:
                can_add_form = forms_helper.can_user_add_form(form_type, user, subject)
                if not can_add_form:
                    success = 'You do not have permission to enter this form with the selected parameters'
                    return HttpResponseRedirect('/forms/forms_home/?&success=' + success)

            other_form_params = custom_form_request_attributes(form_type)

            if other_form_params:
                for param in other_form_params:
                    other_params_dict[param] = request.GET.get(param, None)

    if request.POST:
        returntype = request.POST.get('return_type', None)
        if settings.DEBUG:
            print 'POST in LOAD form', request.POST
            tmpfields = request.POST
            #print 'getitem', tmpfields['referrals_made']
            #print 'getlist', tmpfields.getlist('referrals_made', [])
            #raise
        if 'form_type' in request.POST:
            form_type = request.POST['form_type']
        if 'subject' in request.POST:
            subject = request.POST['subject']
        form = get_form_object(form_type,request.user, request.POST)
        if form.is_valid():
            print 'Is valid'
            tmpformid = forms_helper.save_form(form.cleaned_data, request.user, form_type)
            success = get_success_message(form_type)
            if returntype == 'json':
                return HttpResponse(json.dumps({'success':tmpformid}))
            return HttpResponseRedirect('/forms/forms_home/?&success=' + success)
        else:
            if settings.DEBUG:
                print form.errors, 'Errors'
            if returntype == 'json':
                return HttpResponse(json.dumps(form.errors))
            formfields = request.POST

    subject_object = get_subject_object(subject,form_type)
    red_url = get_form_type(form_type)
    form = get_form_object(form_type, request.user, formfields)
    params_to_pass = {'form':form,'subject':subject_object}
    params_to_pass = add_custom_data(params_to_pass, form_type, subject)
    if other_params_dict:
        params_to_pass.update(other_params_dict)
    return render(request, red_url, params_to_pass)
def add_custom_data(dict_params, form_type, subject):
    if form_type == 'FT3e':
        from ovc_main.utils import workforce_utils
        custom_dictionary = {
                             'workforce_orgs': json.dumps(workforce_utils.workforce_orgs_dict_for_display(subject))
                            }
        dict_params.update(custom_dictionary)

    return dict_params

def custom_form_request_attributes(formtype):
    parameters_dict =  {
                'FT3e': ['onloadformid'],
            }
    if formtype in parameters_dict:
        return parameters_dict[formtype]
    else:
        return None

def get_success_message(form_type):
    if form_type:
        return {
                'FT3e': None,
                'FT3d': 'OVC Assessment form saved successfully',
                'FT2d': 'Workforce training form saved successfully',
                'FT2c': 'Workforce member assessment form saved successfully',
                'FT1g': None,
                'FT1f': 'Residential form saved successfully',
                'FT1c': None,
                'FT1e': 'public sensitization form saved successfully',
                'FT3f': 'child in conflict with the law form saved successfully',
                }[form_type]

def get_custom_data_for_formfields(form_type,formfields_questions,form_object, user=None):
    if form_type:
        return {
                'FT3e': None,
                'FT3d': get_custom_field_data_for_ovc_assessment_form,
                'FT2d': get_custom_field_data_for_workforce_training_form,
                'FT2c': get_basic_form_without_custom,
                'FT1g': None,
                'FT1f': get_custom_field_data_for_residential_form,
                'FT1c': None,
                'FT1e': get_custom_field_data_for_public_sensitisation_form,
                'FT3f': get_custom_child_in_conflict_law,
                }[form_type](formfields_questions,form_object,user)

def get_custom_child_in_conflict_law(formfields,form_object,user=None):
    formfields['social_welfare_officer_managing_case'] = form_object.person_id_filled_paper#field_provider.get_wfc(form_object.person_id_filled_paper)
    formfields['wfc_id_hidden'] = form_object.wfc_id_hidden
    formfields['org_options'] = get_orgs_for_wfc(form_object.wfc_id_hidden,True)
    formfields['org_assessing_child'] = form_object.org_unit_id_filled_paper
    formfields['district'] = field_provider.get_district_for_ward(form_object.form_area_id)
    formfields['ward'] = form_object.form_area_id
    formfields['social_data_hidden'] = forms_helper.get_encounters_text_for_display(form_object.form_pk,lookup_field.ENCOUNTER_TYPE_ID_SOCIAL)
    formfields['court_data_hidden'] = forms_helper.get_encounters_text_for_display(form_object.form_pk,'other')

    print 'formfields', formfields
    return formfields

def get_custom_field_data_for_public_sensitisation_form(formfields,form_object,user):
    formfields['date_event_started'] =  form_object.date_began.strftime(validate_helper.date_input_format)
    formfields['date_event_ended'] = form_object.date_ended.strftime(validate_helper.date_input_format)
    if formfields.has_key('Q087'):
        if formfields['Q087']:
             formfields['duration_of_event'] = formfields['Q087']
             formfields['duration_units'] = '1' #Hours
    if formfields.has_key('Q086'):
        if formfields['Q086']:
             formfields['duration_of_event'] = formfields['Q086']
             formfields['duration_units'] = '2' #Days
    formfields['event_title'] = form_object.form_title

    if formfields.has_key('Q089'):
        if formfields['Q089']:
             geo_focus = formfields['Q089']
             if geo_focus == 124:
                m_district = geo_location.get_geo_ancestors_by_type(form_object.form_area_id, limit_level=lookup_field.geo_type_district_code)
                formfields['district'] = m_district.area_id
                formfields['ward'] = form_object.form_area_id
             if geo_focus == 123:
                formfields['district'] = str(form_object.form_area_id)

    form_id = form_object.form_pk
    formfields['org_contributions_data'] = forms_helper.get_organisation_contributions_json(form_id)
    formfields['beneficiary_data'] = forms_helper.get_beneficiary_status_json(form_id)

    return formfields

def get_custom_field_data_for_workforce_training_form(formfields,form_object,user):
    formfields['date_training_started'] =  form_object.date_began.strftime(validate_helper.date_input_format)
    formfields['date_training_ended'] = form_object.date_ended.strftime(validate_helper.date_input_format)
    if formfields.has_key('Q081'):
        if formfields['Q081']:
             formfields['duration_of_training'] = formfields['Q081']
             formfields['duration_units'] = '2' #Days
    if formfields.has_key('Q082'):
        if formfields['Q082']:
             formfields['duration_of_training'] = formfields['Q082']
             formfields['duration_units'] = '1' #Hours
    formfields['training_title'] = form_object.form_title
    m_district = geo_location.get_geo_ancestors_by_type(form_object.form_area_id, limit_level=lookup_field.geo_type_district_code)
    formfields['where_training_took_place_district'] = m_district.area_id
    formfields['where_training_took_place_ward'] = form_object.form_area_id

    form_id = form_object.form_pk
    formfields['org_contributions_data'] = forms_helper.get_organisation_contributions_json(form_id)
    formfields['workforce_member_data'] = forms_helper.get_workforce_status_json(form_id)

    return formfields

def get_custom_field_data_for_residential_form(formfields,form_object,user):
    formfields['date_of_form'] = form_object.date_began.strftime(validate_helper.date_input_format) if form_object.date_began else form_object.date_began
    person_filled_paper_id = form_object.person_id_filled_paper
    formfields['user_workforce_filled_form_hidden'] = form_object.wfc_id_hidden
    formfields['user_workforce_filled_form'] = person_filled_paper_id
    '''if str(person_filled_paper_id).isdigit():
        workforce_object = workforce_registration.load_wfc_from_id(person_filled_paper_id)
        if workforce_object:
            work_force_string = ''
            if workforce_object.workforce_id:
                work_force_string  = work_force_string + workforce_object.workforce_id + "\t"
            if workforce_object.national_id:
                work_force_string  = work_force_string + workforce_object.national_id + "\t"
            work_force_string  = work_force_string + workforce_object.first_name + ' ' + workforce_object.surname
            formfields['user_workforce_filled_form'] = work_force_string
            print work_force_string,'work_force_string'
    '''
    form_id = form_object.form_pk
    formfields['child_status_data'] = forms_helper.get_child_status_json(form_id)

    return formfields

def get_basic_form_without_custom(formfields,form_object,user=None):
    formfields['wfc_assessing_wfc'] = form_object.person_id_filled_paper#field_provider.get_wfc(form_object.person_id_filled_paper)
    formfields['wfc_id_hidden'] = form_object.wfc_id_hidden
    formfields['org_options'] = get_orgs_for_wfc(form_object.wfc_id_hidden,True)
    formfields['org_assessing_wfc'] = form_object.org_unit_id_filled_paper
    formfields['date_assessment_comp'] = form_object.date_began
    return formfields

def get_custom_field_data_for_ovc_assessment_form(formfields,form_object,user=None):
    formfields['wfc_assessing_child'] = form_object.person_id_filled_paper#field_provider.get_wfc(form_object.person_id_filled_paper)
    formfields['wfc_id_hidden'] = form_object.wfc_id_hidden
    formfields['org_options'] = get_orgs_for_wfc(form_object.wfc_id_hidden,True)
    formfields['org_assessing_child'] = form_object.org_unit_id_filled_paper
    formfields['date_assessment_comp'] = form_object.date_began
    formfields['adverse_cond'] = field_provider.get_adverse_conditions_for_form(form_object.form_pk,form_object.form_subject_id)
    formfields = forms_helper.load_csi_for_form_id(form_object.form_pk,formfields)

    print 'formfields', formfields
    return formfields

def get_form_fields_with_values(form):
    form_fields = {}
    form_fields['form_id'] = form.form_pk
    for question in form.questions:
        answer = get_answer(question)
        form_fields[str(question.question_code)] = answer
    return form_fields

def get_answer(question):
    answer = None
    answer_multi = []
    for answer in question.answers:
        if question.answer_type == lookup_field.ANSWER_TYPE_MULTI_SELECT:
            answer_multi.append(answer.answer_id)
        elif question.answer_type == lookup_field.ANSWER_TYPE_SINGLE_SELECT:
            answer = answer.answer_id
        else:
            answer = answer.answer

    if question.answer_type == lookup_field.ANSWER_TYPE_MULTI_SELECT:
        return answer_multi

    return answer


def get_form_object(form_type,user,formfields=None):
    if form_type:
        vartoreturn =  {
                'FT3e': basic_service_referral_form.BasicServiceReferralForm,
                'FT3d': ovc_assess_frm.OvcAssessmentForm,
                'FT2d': workforce_training.WorkforceTrainingForm,
                'FT2c': workforce_assessment_form.WorkforceAssessmentForm,
                'FT1g': None,
                'FT1f': res_inst_form.ResidentialInstitutionForm,
                'FT1c': None,
                'FT1e': public_sensitisation_form.PublicSensitisationForm,
                'FT3f': child_in_conflict_with_law.ChildInConflictLawForm,
                }[form_type](user,formfields)

        return vartoreturn

def get_form_type(form_type):
    if form_type:
        return {
                'FT3e': 'ovc_main/Forms/basic_service_referral_form.html',
                'FT3d': 'ovc_main/Forms/ovc_assessment.html',
                'FT2d': 'ovc_main/Forms/workforce_training.html',
                'FT2c': 'ovc_main/Forms/workforce_assessment.html',
                'FT1g': '#',
                'FT1f': 'ovc_main/Forms/residential_institution.html',
                'FT1c': '#',
                'FT1e': 'ovc_main/Forms/public_sensitisation.html',
                'FT3f': 'ovc_main/Forms/child_in_conflict_law.html',
                }[form_type]
    return None

def get_subject_object(subject,form_type):
    if form_type and subject:
        return {
                'FT3e': workforce_registration.load_wfc_from_id,
                'FT3d': beneficiary_registration.load_ben_from_id,
                'FT2d': laod_subject_placeholder,
                'FT2c': workforce_registration.load_wfc_from_id,
                'FT1g': laod_subject_placeholder,
                'FT1f': organisation_registration.load_org_from_id,
                'FT1c': laod_subject_placeholder,
                'FT1e': laod_subject_placeholder,
                'FT3f': beneficiary_registration.load_ben_from_id,
                }[form_type](subject, True, True)
    return None

def laod_subject_placeholder(subject, get_json, get_user):
    return ''

@login_required(login_url='/')
def get_wfc(request):
    wfc_id = request.GET['wfc_id'] if request.GET.has_key('wfc_id') else None
    get_perms = request.GET['get_perms'] if request.GET.has_key('get_perms') else None

    if get_perms:
        if request.user.is_superuser:
            to_return = {}
            to_return = {-1:'is_super_user'}
            perms = request.user.get_all_permissions()
            index = 1
            for perm in perms:
                to_return.update({index:perm})
                index += 1
            print to_return
            return HttpResponse(json.dumps(to_return))
        else:
            wfc_obj = field_provider.get_person_object_from_user_id(request.user.id)
            user_id_str = '%s /t %s /t %s'% (wfc_obj.workforce_id,wfc_obj.national_id, wfc_obj.full_name)
            to_return = {}
            to_return = {-2:wfc_obj.pk,-3:user_id_str}
            perms = request.user.get_all_permissions()
            index = 1
            for perm in perms:
                to_return.update({index:perm})
                index += 1
            return HttpResponse(json.dumps(to_return))
    elif wfc_id:
        orgs = get_orgs_for_wfc(wfc_id)
        return HttpResponse(json.dumps(orgs))

def get_orgs_for_wfc(wfc_id,to_tuple=False):
    orgs = {}
    wfc_object = workforce_registration.load_wfc_from_id(wfc_id,include_dead=True)

    if wfc_object:
        #Add the rest of the orgs
        if to_tuple:
            return tuple([('','-----')] + [(org.org_id_int, '%s - %s' % (org.org_id, org.org_name)) for org in wfc_object.org_units])

        #Add primary org first
        for org in wfc_object.org_units:
            if org.primary_org == 'Yes':
                orgs[-1] = org.org_id_int
        #Add the rest of the orgs
        for org in wfc_object.org_units:
            if org.org_id_int not in orgs:
                orgs[org.org_id_int] = '%s - %s' % (org.org_id, org.org_name)
    return orgs

def delete_form(request):
    form_id = request.GET['form_id']
    if form_id:
        forms_helper.delete_form(form_id)
        success = 'Form successfully deleted'
        return HttpResponseRedirect('/forms/forms_home/?&success=' + success)

from django.contrib.sites.models import get_current_site

def report_utils(request):
    set_name = request.GET['set_name'] if request.GET.has_key('set_name') else None
    set_id = request.GET['set_id'] if request.GET.has_key('set_id') else None
    refresh_set_names = request.GET['refresh_set_names'] if request.GET.has_key('refresh_set_names') else None
    orgs = request.GET['orgs'] if request.GET.has_key('orgs') else None
    s_id = request.GET['s_id'] if request.GET.has_key('s_id') else None
    set_delete = request.GET['set_delete'] if request.GET.has_key('set_delete') else None
    report_to_export = request.GET['report_to_export'] if request.GET.has_key('report_to_export') else None 
    sel_report = request.GET['sel_report'] if request.GET.has_key('sel_report') else None

    if set_name:
        set_id = field_provider.save_org_set(set_name,request.user.id)
        return HttpResponse(json.dumps({'set_id':set_id}))
    elif set_id:
        orgs = field_provider.get_orgs_for_set(set_id)
        print orgs
        return HttpResponse(json.dumps(orgs))
    elif refresh_set_names:
        sets = field_provider.get_orgs_for_set('set')
        return HttpResponse(json.dumps({'sets':sets}))
    elif orgs:
        print "set data",s_id,orgs
        orgs_save = orgs.split(',')
        saved = field_provider.save_org_set_orgs(s_id,orgs_save)
        return HttpResponse(json.dumps({'saved':saved}))
    elif set_delete:
        deleted = field_provider.delete_report_set(set_delete)
        return HttpResponse(json.dumps({'deleted':deleted}))
    elif report_to_export:
        exported = field_provider.export_report(report_to_export,report_id=sel_report)
        #str_ourl = os.path.join(settings.BASE_DIR, exported)
        ul = get_current_site(request).domain
        exp_str = str(exported)
        print 'look here','%s/%s'%(ul,exp_str)#settings.BASE_DIR#,exported, str_ourl
        #return HttpResponseRedirect('http://%s/%s'%(ul,exp_str))#os.path.join(settings.BASE_DIR, exported))
        
        return HttpResponse(json.dumps({'exported':'http://%s/%s'%(ul,exp_str)}))
        #return serve(request, os.path.basename(filepath), os.path.dirname(filepath))

    return None

def export_report(request):
    red_url = request.GET['red_url'] if request.GET.has_key('red_url') else None
    HttpResponseRedirect(red_url)

def reports_and_analysis(request):
    report_types = field_provider.get_list_of_report_types()
    orgs = field_provider.get_orgs_for_set('org')
    sets = field_provider.get_orgs_for_set('set')
    return render(request, 'ovc_main/reports_and analysis.html',{'report_types':report_types,'org_opts':orgs,'sets':sets})

def report_paramenters(request):
    report_id = request.GET['report_id'] if request.GET.has_key('report_id') else None
    filter = request.GET['filter'] if request.GET.has_key('filter') else None
    org_name = request.GET['org_name'] if request.GET.has_key('org_name') else None
    rep_id = request.GET['rep_id'] if request.GET.has_key('rep_id') else None
    question_id = request.GET['question_id'] if request.GET.has_key('question_id') else None
    report_params = None
        
    if report_id:
        report_params = field_provider.get_report_params(report_id,request.user)
    elif filter:
        report_params = field_provider.get_geo_filter_vals(filter,rep_id_v=rep_id)
    elif org_name:
        report_params = field_provider.get_org_units(org_name)
    elif question_id:
        report_params = forms_helper.get_question_answers(question_id)

    return HttpResponse(json.dumps(report_params))

def get_reported_by_options(request):
    '''
    Temporal solution: Gets the options for the Reported by value in Residential institution
    Permanent solution requires refactoring the FOrms home

    Report by institution is only available for selection if
        a) the logged in user has this institution as a parent unit or
        b) the logged in user has a data entry permission 1051.
    "Report by external inspector" is only availabe for selection
        a) if the logged in user has inspector permission 1053 or
        b) if the logged in user has a data entry permission 1051.
    '''
    to_return = []
    user = request.user
    org_id_int = request.GET['org_id_int']
    answer_set_ids_add_empty_list = {8:True}
    #(('', '-----'), (41, u'Report by institution'), (42, u'Inspection report by external inspector')) options
    options = field_provider.get_answer_set(answer_set_ids = answer_set_ids_add_empty_list)[8]
    for option in options:
        if option[0] == '':
            item = {'value': option[0],
                    'text': option[1]}
            to_return.append(item)
        if option[0] == 41:
            if user.has_perm('auth.enter frm high sensitive') or forms_helper.org_is_primary_parent(user, org_id_int):
                item = {'value': option[0],
                    'text': option[1]}
                to_return.append(item)

        if option[0] == 42:
            if user.has_perm('auth.enter frm as soc welf') or user.has_perm('auth.enter frm high sensitive'):
                item = {'value': option[0],
                    'text': option[1]}
                to_return.append(item)
    return HttpResponse(json.dumps(to_return))


def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    raise TypeError

def get_report(request):
    to_return_report = {}
    report_type = request.GET['report_type'] if request.GET.has_key('report_type') else None
    item_id = request.GET['item_id'] if request.GET.has_key('item_id') else None
    question = request.GET['question'] if request.GET.has_key('question') else None
    geo_area = request.GET['geo_area'] if request.GET.has_key('geo_area') else None
    org_unit = request.GET['org_unit'] if request.GET.has_key('org_unit') else None
    sub_units = request.GET['sub_units'] if request.GET.has_key('sub_units') else None
    from_date = request.GET['from_date'] if request.GET.has_key('from_date') else None
    to_date = request.GET['to_date'] if request.GET.has_key('to_date') else None
    from_date_c = request.GET['from_date_c'] if request.GET.has_key('from_date_c') else None
    to_date_c = request.GET['to_date_c'] if request.GET.has_key('to_date_c') else None
    answer = request.GET['answer'] if request.GET.has_key('answer') else None

    to_return_report = field_provider.get_report_data(
                                                      report_type,
                                                      item_id,
                                                      question,
                                                      geo_area,
                                                      org_unit,
                                                      sub_units,
                                                      from_date,
                                                      to_date,
                                                      from_date_c,
                                                      to_date_c,
                                                      answer
                                                      )

    return HttpResponse(json.dumps(to_return_report))#, default=decimal_default))

def form_search_results(request):
    from django.core.paginator import Paginator

    print request.GET, 'checking what was passed beneficiary'
    searchtokenstring = request.GET['searchterm']

    itemsperpage = 25
    if 'itemsperpage' in request.GET:
        itemsperpage = request.GET['itemsperpage']

    form_type = None
    if 'formtype' in request.GET:
        form_type = request.GET['formtype']

    search_date = None
    if 'searchdate' in request.GET:
        search_date = request.GET['searchdate']

    pagenumber = 1
    if 'pagenumber' in request.GET:
        pagenumber = request.GET['pagenumber']

    searchtokens = searchtokenstring.strip().split(' ')

    forms = forms_helper.Load_form('search',True,request.user,searchtokens,search_date,form_type)
    #pages = Paginator(forms, 25)
    #ben_list = beneficiary_registration.load_beneficiaries(token = searchtokenstring, ben_type = form_type, getJSON=True, search_date=search_date) #organisation_registration.get_org_list(cleantokens(searchtokens), getJSON=True, org_type=org_type)

    print 'these are the items per page', itemsperpage
    if int(itemsperpage) == -1:
        pages = Paginator(forms, 0)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':1,
                'total_records':len(forms),
                'pagenumber': 1}
        todisplay = forms
    else:
        pages = Paginator(forms,itemsperpage)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': pagenumber}
        todisplay = pages.page(pagenumber).object_list

        print 'page information:'
        print pageinfo
        print 'page toDisplay:'
        print todisplay
        print 'page json:'
        print json.dumps((todisplay +[pageinfo] ))
    return HttpResponse(json.dumps((todisplay +[pageinfo] )))

def view_help(request):
    return render_to_response(request, 'ovc_main/ZOMIS_help_html/HTML/index.html')
