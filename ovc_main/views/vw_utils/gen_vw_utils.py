from ovc_main.utils import fields_list_provider as field_provider, \
    validators as validate_helper
from ovc_main.forms import forms_helper

def format_bens_for_display(child_list, res_previous_admission_dates=None):
        for child in child_list:            
            if child.date_of_birth:
                child.date_of_birth = child.date_of_birth.strftime(validate_helper.date_input_format)
            if child.date_of_death:
                child.date_of_death = child.date_of_death.strftime(validate_helper.date_input_format)
        ''' Get prior date admitted and date left '''
        previous_dates = forms_helper.get_recent_child_date_admitted_left(child_list)

        to_return = tuple([(child.pk, child.beneficiary_id, child.birth_reg_id,\
                                child.first_name + ' ' + child.surname, child.date_of_birth, \
                                child.date_of_death, \
                                previous_dates[child.pk]['date_admitted'] if res_previous_admission_dates else '', \
                                previous_dates[child.pk]['date_left'] if res_previous_admission_dates else '', \
                                field_provider.get_person_geo_location(child.pk)[0], \
                                field_provider.get_person_geo_location(child.pk)[1]) 
                           for child in child_list])
        
        return to_return