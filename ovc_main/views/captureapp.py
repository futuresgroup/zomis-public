from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from ovc_main.capture_admin import capture_site, server_admin
import json

def server_add_capture(request):
    '''
    receive request to add new capture site,
    this will be added to queue for  an admin to 
    approve it
    '''
    if request.POST:
        form = capture_site.SetupCaptureSiteForm(request.POST)
        if form.is_valid():
            server_admin.record_capture_site(form._capture_site)
            return HttpResponse(json.dumps({'success':True}))
    else:
        return HttpResponse(json.dumps({'success':False}))
            


def setup_capture_site(request):
    '''
    At is at capture site level, this receives contents 
    of the capture site configuration information:
    1. capturesite name etc
    '''
    #add site to remote server and wait for server to
    #approve this capture site
    if request.POST:
        form = capture_site.SetupCaptureSiteForm(request.POST)
        if form.is_valid():
            capture_site.add_site_to_server(form._capture_site)
            return HttpResponse(json.dumps({'success':True}))
        else:
            return render(request, 'ovc_main/capture/setup_site.html', {'form':form})
    else:
        siteinfo = capture_site.local_capture_site_info()
        PENDING  = 1
        DECLINED = 3
        if siteinfo and siteinfo.approved:
            return HttpResponse('/home/')
        elif siteinfo and siteinfo.approval_status in [PENDING, DECLINED]:
            return render(request,'ovc_main/capture/capture_status.html', 
                          {'status': siteinfo.approval_status, 'siteinfo': siteinfo})
        else:
            form = capture_site.SetupCaptureSiteForm()
            return render(request, 'ovc_main/capture/setup_site.html', {'form':form})


def get_local_site(request):
    site = capture_site.local_site_info()
    return HttpResponse(json.dumps(site.__dict__))
