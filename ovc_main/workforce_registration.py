'''
Created on Sep 22, 2014

@author: glyoko

Save workforce data here
'''
from datetime import datetime
from models import RegPerson, RegPersonsContact, RegPersonsTypes, RegPersonsExternalIds, RegOrgUnit, RegPersonsOrgUnits, RegPersonsGeo, RegPersonsGdclsu, SetupGeorgraphy, RegOrgUnitGeography, RegPersonsWorkforceIds
from ovc_main.utils.idgenerator import workforce_id_generator
from ovc_main.utils import lookup_field_dictionary as fielddictionary, fields_list_provider as list_provider, audit_trail as logger,notification_manager as notifications
from ovc_main.utils.geo_location import matches_for_display, get_all_org_subunits_and_self
from django.db.models import Q
import operator
from django.core.exceptions import ValidationError
from ovc_auth.user_manager import allocate_registration_assistant_role, \
    allocate_standard_log_in,remove_role,get_or_create_user_from_model,get_user_role_geo_org, \
    remove_roles_in_geo, deactivate_user, delete_user
from ovc_auth.models import OVCUserRoleGeoOrg,AppUser
from ovc_main.utils import auth_access_control_util
import traceback
from django.db import transaction
from django.conf import settings
import json

__name__ = 'person'
roles_and_fields_to_show = {}

class GeoLocation:
    geo_id = None
    geo_name = None
    geo_parent_id = None
    geo_level = None
    
    def __init__(self, geo_id):
        self.geo_id = geo_id

        geo_location = SetupGeorgraphy.objects.get(pk=geo_id)
        
        self.geo_name = geo_location.area_name
        self.geo_parent_id = geo_location.parent_area_id
        self.geo_level = geo_location.area_type_id
        
    def geo_dict(self):
        return {self.geo_id: self.geo_name}
class Contact:
        designated_phone_number = None
        other_mobile_number = None
        email_address = None
        physical_address = None
        
        def __init__(self, designated_phone_number, other_mobile_number, contact_email, physical_address):
            self.designated_phone_number = designated_phone_number
            self.other_mobile_number = other_mobile_number
            self.email_address = contact_email
            self.physical_address = physical_address                         
            
        def __unicode__(self):
            return 'mobilephone:', self.designated_phone_number, 'othermobilenumber:',self.other_mobile_number, 'emailaddress:', self.email_address,'physicaladdress:', self.physical_address
        
        def get_contact_dict(self):
            return {fielddictionary.designated_phone_number: self.designated_phone_number, 
                    fielddictionary.other_mobile_number: self.other_mobile_number, 
                    fielddictionary.contact_physical_address: self.physical_address, 
                    fielddictionary.contact_email_address: self.email_address}
        
            


class WorkforceMember:
    id_int = None
    user_id = None
    workforce_id = None
    national_id =  None
    first_name = ''
    other_names = ''
    surname = ''
    name = None
    sex_id = None
    sex = None
    date_of_birth = None
    date_of_death = None
    steps_ovc_number = None
    man_number = None
    ts_number = None
    sign_number = None
    roles = None
    org_units = None
    primary_org_unit_name = None
    person_type = None
    person_type_id = None
    geo_location = None
    gdclsu_details = None
    contact = None
    registered_by_person_id = None
    direct_services = None

    def __init__(self, workforce_id, national_id, first_name,surname, other_names, sex_id, date_of_birth, steps_ovc_number,man_number,
                ts_number,sign_number,roles, org_units,primary_org_unit_name, person_type, gdclsu_details, contact,person_type_id, districts=None, 
                wards=None, communities=None,direct_services=None,edit_mode_hidden=None,workforce_type_change_date=None,parent_org_change_date=None,
                work_locations_change_date=None,date_of_death=None,org_data_hidden = None, primary_org_id=None, wards_string = None,
                org_units_string = None,communities_string = None):
        
        if workforce_id == fielddictionary.empty_workforce_id:
            self.user_id = national_id
        else:
            self.user_id = workforce_id
        self.workforce_id = workforce_id
        self.national_id = national_id
        self.first_name = first_name
        self.surname = surname
        self.other_names = other_names
        if first_name and surname:
            self.name = first_name + ' ' + surname
        self.sex_id = sex_id
        self.date_of_birth = date_of_birth
        #self.date_of_death = date_of_death
        self.steps_ovc_number = steps_ovc_number
        self.man_number = man_number
        self.ts_number = ts_number
        self.sign_number = sign_number
        #self.registered_by_person_id = registered_by_person_id
        self.roles = roles
        self.org_units = org_units
        self.primary_org_id = primary_org_id
        self.primary_org_unit_name = primary_org_unit_name
        self.person_type = person_type
        #self.geo_location = geo_location
        self.gdclsu_details = gdclsu_details       
        self.contact = contact
        self.person_type_id = person_type_id
        self.wards_string = wards_string
        self.org_units_string = org_units_string
        self.communities_string = communities_string
        self.geo_location = {}
        self.geo_location = {'districts':districts,
                             'wards': wards,
                             'communities':communities}
        
        self.direct_services = direct_services
        self.edit_mode_hidden = edit_mode_hidden
        self.workforce_type_change_date=workforce_type_change_date
        self.parent_org_change_date=parent_org_change_date
        self.work_locations_change_date=work_locations_change_date
        self.date_of_death=date_of_death
        self.org_data_hidden = org_data_hidden
        _distrcits_wards = []
        _communities = None
        if wards:
            _distrcits_wards += wards
        if districts:
            _distrcits_wards += districts
            
        if _distrcits_wards:
            if self.geo_location['communities']:
                _communities = self.geo_location['communities']
            else:
                _communities = []
            self.locations_for_display = matches_for_display(_distrcits_wards, _communities)
        else:
            self.locations_for_display = []
        
        self.locations_unique_readable = []
        
        for loc in _distrcits_wards:
            self.locations_unique_readable.append(GeoLocation(loc).geo_name)
        
        if _communities:
            for comm in communities:
                self.locations_unique_readable.append(RegOrgUnit.objects.get(pk=comm).org_unit_name)
        
    def __unicode__(self):
        return '%s %s'% (self.first_name, self.surname)
    
    def sex(self):
        self.sex = list_provider.get_description_for_item_id(self.sex_id)
        #self.sex = list_provider.get_item_desc_for_order_and_category(self.sex_id, fielddictionary.sex)
        if not self.sex:
            return ''
        return self.sex[0]
    
    def get_locations_for_display(self):
        return self.locations_for_display
    
class OrganisationUnit:
    def __init__(self, org_id_int, org_id, org_name, primary_org, hasRegAssistantRole='No'):
        self.org_id_int = org_id_int
        self.org_id = org_id
        self.org_name = org_name
        self.primary_org = primary_org
        self.is_reg_assistant = hasRegAssistantRole
        
    def __unicode__(self):
        return '%s (%s)'% (self.org_name, self.org_id)
        
    def __str__(self):
        return '%s (%s)'% (self.org_name, self.org_id)
        
@transaction.commit_on_success
def save_new_workforce(cleanpagefields, user):
    wfc  = create_workforce_object_from_page(cleanpagefields, user)
    try:
        if wfc:
            #Save RegPersons            
            reg_person = save_workforce(wfc)
            if wfc.direct_services:
                if wfc.direct_services == '1':
                    assign_wfc_unique_id(reg_person)
            #Save RegPersonsContact
            save_contact_details(wfc, reg_person)            
            #Save RegPersonsTypes
            save_persons_type(wfc, reg_person)  
            #Save RegPersonsExternalIds
            save_persons_external_ids(wfc, reg_person)            
            #RegPersonsOrgUnits 
            save_org_units(wfc, reg_person)
            #save RegPersonsGeo
            save_persons_geo_location(wfc.geo_location, reg_person)
            #Add WRA to SecPersonsRoles
            new_user, password = allocate_standard_log_in(reg_person)
            #save designated number to new user
            if wfc.contact:
                if wfc.contact.designated_phone_number:
                    new_user.designated_phone_number = wfc.contact.designated_phone_number
                    new_user.save()
            #success = notifications.send_notification(user_model=reg_person, new_user = new_user, message_type = notifications.MessageType.NEW_REGISTRATION, contact_type = notifications.ContactType.EMAIL)
            success = notifications.send_user_reg_info(person_model=reg_person, appuser = new_user, message={'Password': password})
            #Log save
            if reg_person.workforce_id: #workforce member has workforce ID
                logger.log(__name__,reg_person,fielddictionary.register_workforce_member,fielddictionary.web_interface_id,user=user)
            else:
                logger.log(__name__,reg_person,fielddictionary.register_user,fielddictionary.web_interface_id,user=user)
            return wfc
        else:
            raise Exception('Workforce is not valid')

    except Exception as e:
        traceback.print_exc()
        raise Exception('The workforce has some invalid data. Workforce Create failed.')
    
def save_org_units(wfc, reg_person):
    list_of_org_units = json.loads(wfc.org_units)
    orgs_to_save = []
    for key, org_data_str in list_of_org_units.items():
        if not org_data_str:
            continue
        org_unit_id = None
        primary_org_unit_yes_no= None
        is_assistant_in_org = False
        org_unit_id = org_data_str['org_unit_id'] 
        primary_org_unit_yes_no = org_data_str['primary_org']
        if str(org_data_str['reg_assistant']).lower().strip() == 'yes':
            is_assistant_in_org = True                
  
        org_model = RegOrgUnit.objects.get(org_unit_id_vis=org_unit_id,is_void=False)
        if is_assistant_in_org:
            allocate_registration_assistant_role(reg_person,org_model.pk)
        setup_list_yes_no_index = False

        #yes_no_tpl = list_provider.get_yes_no_index(primary_org_unit_yes_no)
        if primary_org_unit_yes_no == 'Yes':
            setup_list_yes_no_index = True
        org_model.primary = setup_list_yes_no_index
        orgs_to_save.append(org_model)
    if(len(RegPersonsOrgUnits.objects.filter(person = reg_person))>0):
        RegPersonsOrgUnits.objects.filter(person = reg_person).delete()
    for org in orgs_to_save:
        RegPersonsOrgUnits.objects.create(person = reg_person, parent_org_unit = org, primary = org.primary, date_linked = datetime.now(), date_delinked = None)

def save_persons_external_ids(wfc, reg_person):   
    if wfc.steps_ovc_number:
        RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.steps_ovc_caregiver, identifier = wfc.steps_ovc_number)
    if wfc.man_number:
        RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.govt_man_number, identifier = wfc.man_number)
    if wfc.ts_number:
        RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.teacher_service_id, identifier = wfc.ts_number)
    if wfc.sign_number:
        RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.police_sign_number, identifier = wfc.sign_number) 

def save_persons_type(wfc, reg_person):
    wfc_type_id = wfc.person_type
    if wfc_type_id:  
        RegPersonsTypes.objects.create(person = reg_person, person_type_id = wfc_type_id, date_began = datetime.now(), date_ended = None)

def save_contact_details(wfc,reg_person): 
    contact = wfc.contact
    if len(contact.designated_phone_number) == 10:
        contact.designated_phone_number = '+26'+contact.designated_phone_number
    if len(contact.other_mobile_number) == 10:
        contact.other_mobile_number = '+26'+contact.other_mobile_number
    if contact.designated_phone_number:
        RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_designated_mobile_phone, contact_detail = contact.designated_phone_number)
    if contact.other_mobile_number:
        RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_mobile_phone, contact_detail = contact.other_mobile_number)
    if contact.email_address:
        RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_email_address, contact_detail = contact.email_address)
    if contact.physical_address:
        RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_physical_address, contact_detail = contact.physical_address)
  
def save_workforce(wfc):
    '''
    if wfc.date_of_birth:
        wfc.date_of_birth = x
    if wfc.date_of_death:
        wfc.date_of_death = y
        
    	datetime.datetime.strptime('30-November-2012', '%d-%B-%Y').date()
    '''
    reg_person = RegPerson.objects.create(workforce_id=wfc.workforce_id,
                                    national_id=wfc.national_id,
                                    first_name=wfc.first_name,
                                    other_names = wfc.other_names,
                                    surname=wfc.surname,
                                    date_of_birth=wfc.date_of_birth,
                                    date_of_death=wfc.date_of_death,
                                    sex_id=wfc.sex_id
                                    )
    return reg_person        

def assign_wfc_unique_id(reg_person):
    uniqueid = workforce_id_generator(reg_person)
    reg_person.workforce_id = uniqueid
    reg_person.save()
    return uniqueid

def save_persons_geo_location(locations, mperson):
    try:
        for location in locations:
            if not locations[location]:
                continue
            if location=='communities':
                communities = locations[location]
                for community in communities:
                    mcommunity = RegOrgUnit.objects.get(pk=community,is_void=False)
                    RegPersonsGdclsu.objects.create(person=mperson, gdclsu=mcommunity,date_linked=datetime.now())
            else:
                for selected_location in locations[location]:
                    RegPersonsGeo.objects.create(person=mperson, area_id=selected_location,date_linked=datetime.now())
    except Exception as e:
        traceback.print_exc()
        raise Exception('failed to save to geo locations')
    

def create_workforce_object_from_page(cleanpagefields, user):
    
    #workforce_id = '' #Just skip it the first time. Saved once we have a unique pk
    workforce_id = None
    if cleanpagefields.has_key('workforce_id'):
        workforce_id = cleanpagefields['workforce_id']
        
    national_id = None
    if cleanpagefields.has_key('nrc'):
        national_id = cleanpagefields['nrc']
        
    edit_mode_hidden = None
    if cleanpagefields.has_key('edit_mode_hidden'):
        edit_mode_hidden = cleanpagefields['edit_mode_hidden']
        
    first_name = None
    if cleanpagefields.has_key('first_name'):
        first_name = cleanpagefields['first_name']
        
    other_names = None
    if cleanpagefields.has_key('other_names'):
        other_names = cleanpagefields['other_names']    

    surname = None
    if cleanpagefields.has_key('last_name'):
        surname = cleanpagefields['last_name'] 
        
    sex_id = None
    if cleanpagefields.has_key('sex'):
        sex_id = cleanpagefields['sex']   
        
    date_of_birth = None
    if cleanpagefields.has_key('date_of_birth'):
        date_of_birth = cleanpagefields['date_of_birth'] 
    
    designated_phone_number = None
    if cleanpagefields.has_key('designated_phone_number'):
        designated_phone_number = cleanpagefields['designated_phone_number'] 

    other_mobile_number = None
    if cleanpagefields.has_key('other_mobile_number'):
        other_mobile_number = cleanpagefields['other_mobile_number'] 

    email_address = None
    if cleanpagefields.has_key('email_address'):
        email_address = cleanpagefields['email_address'] 

    physical_address = None
    if cleanpagefields.has_key('physical_address'):
        physical_address = cleanpagefields['physical_address'] 
        
    contact = Contact(designated_phone_number, other_mobile_number, email_address, physical_address)
    
    workforce_type = None
    if cleanpagefields.has_key('workforce_type'):
        workforce_type = cleanpagefields['workforce_type'] 
    
    steps_ovc_number = None
    if cleanpagefields.has_key('steps_ovc_number'):
        steps_ovc_number = cleanpagefields['steps_ovc_number'] 
    
    man_number = None
    if cleanpagefields.has_key('man_number'):
        man_number = cleanpagefields['man_number'] 
        
    ts_number = None
    if cleanpagefields.has_key('ts_number'):
        ts_number = cleanpagefields['ts_number'] 
        
    sign_number = None
    if cleanpagefields.has_key('sign_number'):
        sign_number = cleanpagefields['sign_number'] 
    
    org_data_hidden = None
    if cleanpagefields.has_key('org_data_hidden'):
        org_data_hidden = cleanpagefields['org_data_hidden'] 
        
    direct_services = None
    if cleanpagefields.has_key('direct_services'):
        direct_services = cleanpagefields['direct_services'] 
        
    districts = None
    if cleanpagefields.has_key('districts'):
        if cleanpagefields['districts']:
            districts  = map(int, cleanpagefields['districts'])
    
    wards = None
    if cleanpagefields.has_key('wards'):
        if cleanpagefields['wards']:
            wards = map(int, cleanpagefields['wards'])
    
    communities = None
    if cleanpagefields.has_key('communities'):
        if cleanpagefields['communities']:
            communities = map(int, cleanpagefields['communities'])
    
    workforce_type_change_date = None
    if cleanpagefields.has_key('workforce_type_change_date'):
        workforce_type_change_date = cleanpagefields['workforce_type_change_date'] 

    parent_org_change_date = None
    if cleanpagefields.has_key('parent_org_change_date'):
        parent_org_change_date = cleanpagefields['parent_org_change_date'] 

    work_locations_change_date = None
    if cleanpagefields.has_key('work_locations_change_date'):
        work_locations_change_date = cleanpagefields['work_locations_change_date'] 

    date_of_death = None
    if cleanpagefields.has_key('date_of_death'):
        date_of_death = cleanpagefields['date_of_death']
    
    associated_org_units_list = None
    associated_org_units = None
    if org_data_hidden:
        associated_org_units = extract_org_unit_details(org_data_hidden)

    wfc = WorkforceMember(  workforce_id = workforce_id,
                            national_id =  national_id,
                            first_name = first_name,
                            surname = surname,
                            other_names = other_names,
                            sex_id = sex_id,
                            date_of_birth = date_of_birth,
                            steps_ovc_number = steps_ovc_number,
                            man_number = man_number,
                            ts_number = ts_number,
                            sign_number = sign_number,
                            roles = None,
                            org_units = org_data_hidden,
                            primary_org_unit_name = None,
                            person_type = workforce_type, 
                            #geo_location = None, 
                            gdclsu_details = None, 
                            contact = contact,
                            person_type_id = None,
                            districts=districts,
                            wards = wards,
                            communities=communities,
                            wards_string = None,
                            org_units_string = None,
                            communities_string = None,
                            direct_services = direct_services,
                            edit_mode_hidden = edit_mode_hidden,
                            workforce_type_change_date=workforce_type_change_date,
                            parent_org_change_date=parent_org_change_date,
                            work_locations_change_date=work_locations_change_date,
                            date_of_death=date_of_death
                        )  
    return wfc

def extract_org_unit_details(org_data_hidden):
    list_of_items = org_data_hidden.split(',,')
    return list_of_items
        


def get_wfc_list(tokens=None,getJSON=False, wfc_type=None, user = None, search_location = True, search_wfc_by_primary_org = True):
    wfcs = []
    matching_geo_org_ids = []
    modelwfcs = search_wfcs(tokens, wfc_type=wfc_type, search_location = search_location, search_by_primary_org = search_wfc_by_primary_org)
    for wfc in modelwfcs:
        try:
            wfc = load_wfc_from_id(wfc.pk,user)
            if getJSON:
                wfc = wfc_json(wfc,user)
                wfcs.append(wfc)
            else:
                wfcs.append(wfc)
        except Exception as e:
            traceback.print_exc()
            raise Exception('retrieving workforce failed')
    return wfcs

def wfc_json(workforce,user):
    user_can_edit = 'None'
    user_can_view = ''
    user_can_view_cursor = ''
    user_can_assign_role = 'None'
    is_capture_app = settings.IS_CAPTURE_SITE
    if user:
        if user.is_superuser:
            user_can_edit = 'text'
            user_can_view = 'table_row_clickable'
            user_can_view_cursor = 'cursor:pointer'
            user_can_assign_role = 'text'
        else:
            if user.has_perm('auth.wkf view'):
                user_can_view = 'table_row_clickable'
                user_can_view_cursor = 'cursor:pointer'

            ''' In lookup, show the 'file update' button depending on permissions '''
            ''' 1. any cross cutting 'file update' permission for workforce(15,16 or 17) or ''' 
            user_has_cross_cutn_perms = user.has_perm('auth.update wkf contact') or user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf special')

            ''' 2. user is related to the selected workforce member and has specific 'file update' permissions (5 or 6), or ''' 
            wfc_related_to_user = wfc_and_user_related(workforce,user)
            user_has_specific_update_per = user.has_perm('auth.update wkf contact rel') or user.has_perm('auth.update wkf general rel')
            related_and_has_update_perms = wfc_related_to_user and user_has_specific_update_per

            ''' 3. the user is the selected workforce member and has permission 37 ''' 
            if user.reg_person:
                wfc_is_logged_in_user_with_perm = user.reg_person.pk == workforce.id_int and user.has_perm('auth.update wkf contact own')
                if user_has_cross_cutn_perms or related_and_has_update_perms or wfc_is_logged_in_user_with_perm:
                    user_can_edit = 'text'

            ''' if user has crosscutting permission 32 or 33, then they can allocate roles '''
            if user.has_perm('auth.role management') or user.has_perm('auth.assign role non org spec'):
                user_can_assign_role = 'text'

            ''' If they have org specific permissions 10 and 31, then they can view person based on some factors '''
            if user.has_perm('auth.role assign org rel'):
                wfc_related_to_user = wfc_and_user_related(workforce,user)
                if wfc_related_to_user:
                    user_can_assign_role = 'text'

            if user.has_perm('auth.assign role org spec'):
                belongs_to_same_org = user_and_wfc_same_org(workforce,user)
                if belongs_to_same_org:
                    user_can_assign_role = 'text'
    #print is_capture_app,'is_capture_app'
    wfc = {'id_int': workforce.id_int,
                'user_id': workforce.user_id,
                'name': workforce.name,
                'person_type': workforce.person_type,
                'primary_org_unit_name': workforce.primary_org_unit_name,
                'location':'; '.join(workforce.locations_unique_readable),
                'user_can_edit':user_can_edit,
                'user_can_view':user_can_view,
                'user_can_view_cursor':user_can_view_cursor,
                'user_can_assign_role':user_can_assign_role,
                'is_capture_app':is_capture_app}
    return wfc

def search_wfcs(tokens, search_location=True, search_by_primary_org = False, wfc_type=False):
    result = set()
    q_list = []
    
    if tokens or wfc_type:
        try:
            if tokens:
                for token in tokens:
                    q_list.append(Q(first_name__icontains=token))
                    q_list.append(Q(surname__icontains=token.lower()))
                    q_list.append(Q(other_names__icontains=token.lower()))
                    q_list.append(Q(workforce_id__icontains=token))
            
            if wfc_type and tokens:
                q_list.append(Q(regpersonstypes__person_type_id__icontains=wfc_type))
                tmp_result = RegPerson.objects.filter(reduce(operator.or_, q_list), regpersonstypes__date_ended=None,  regpersonstypes__person_type_id__in=list_provider.get_Workforce_Type_as_list(),is_void=False,date_of_death=None)
            elif wfc_type and not tokens:
                tmp_result = RegPerson.objects.filter(regpersonstypes__person_type_id__contains=wfc_type, regpersonstypes__date_ended=None, regpersonstypes__person_type_id__in=list_provider.get_Workforce_Type_as_list(),is_void=False,date_of_death=None)
            elif tokens and not wfc_type:
                tmp_result = RegPerson.objects.filter(reduce(operator.or_, q_list), regpersonstypes__date_ended=None, regpersonstypes__person_type_id__in=list_provider.get_Workforce_Type_as_list(),is_void=False,date_of_death=None)
            elif not wfc_type and not tokens:
                tmp_result = RegPerson.objects.filter(regpersonstypes__person_type_id__in=list_provider.get_Workforce_Type_as_list(), regpersonstypes__date_ended=None, is_void=False,date_of_death=None)
   
            for person in tmp_result:
                result.add(person) 
            
            if search_location:
                org_ids = search_wfc_by_location(tokens)
                if org_ids:
                    orgstofetch = list(org_ids)
                    
                    if orgstofetch:
                        wfcs_by_geo = RegPerson.objects.filter(regpersonsorgunits__parent_org_unit_id__in=orgstofetch,is_void=False,date_of_death=None)
                    if wfcs_by_geo:
                        for wfc in wfcs_by_geo:
                            result.add(wfc)
            
            if search_by_primary_org:
                primary_org_ids = search_wfc_by_primary_org(tokens)
                if primary_org_ids:
                    orgstofetch = list(primary_org_ids)

                    if orgstofetch: 
                        wfcs_by_prim_org = RegPerson.objects.filter(regpersonsorgunits__parent_org_unit_id__in=orgstofetch, is_void=False,date_of_death=None)
                    if wfcs_by_prim_org:
                        for wfc in wfcs_by_prim_org:
                            result.add(wfc)
            
        except Exception as e:
            traceback.print_exc()
            raise Exception('workforce search failed')
    else:
        result = RegPerson.objects.filter(regpersonstypes__person_type_id__in=list_provider.get_Workforce_Type_as_list(), regpersonstypes__date_ended=None, is_void=False,date_of_death=None)
    return result

def search_wfc_by_primary_org(tokens):
    #print tokens,'tokens'
    org_ids = None
    search_condition = []
    if tokens:
        for term in tokens:
            #print term,'term'
            search_condition.append(Q(org_unit_name__icontains=term))

        orgs = RegOrgUnit.objects.filter(reduce(operator.or_, search_condition)).values_list('id', 'org_unit_name')
        #print orgs,'orgs'
        if orgs:
            idstosearch = []
            for id, unit_name in orgs:
                if id in idstosearch:
                    continue
                idstosearch.append(id)

            org_ids = RegPersonsOrgUnits.objects.filter(parent_org_unit_id__in=idstosearch, is_void=False).values_list('parent_org_unit', flat=True)

    return org_ids
    
def search_wfc_by_location(tokens):
    org_ids = None
    search_condition = []
    if tokens:
        for term in tokens:
            search_condition.append(Q(area_name__icontains=term))

        geos = SetupGeorgraphy.objects.filter(reduce(operator.or_, search_condition)).values_list('area_id', 'area_name')
        if geos:
            idstosearch = []
            for geo_id, geo_name in geos:
                if geo_id in idstosearch:
                    continue
                idstosearch.append(geo_id)
                childrenids = get_children_ids(geo_id)
                idstosearch = idstosearch + childrenids
            org_ids = RegOrgUnitGeography.objects.filter(area_id__in=idstosearch).values_list('org_unit', flat=True)
    return org_ids

def search_wfc_by_primary_org_type_and_date(primary_org_id=None, primary_org_type=None, by_date=None, user=None, getJSON = False):
    workforce_list = []
    if primary_org_id or primary_org_type:
        wfcs_tmp = list(RegPerson.objects.filter(regpersonsorgunits__primary = True, regpersonsorgunits__parent_org_unit__org_unit_id_vis=primary_org_id, \
            regpersonsorgunits__parent_org_unit__org_unit_type_id=primary_org_type, is_void=False,date_of_death=None))
        for item in wfcs_tmp:
            try:
                wfc = load_wfc_from_id(item.pk,user)
                if getJSON:
                    to_add = {'id_int': wfc.id_int,
                        'user_id': wfc.user_id,
                        'name': wfc.name,
                        'person_type': wfc.person_type,
                        }
                    workforce_list.append(to_add)
                else:
                    workforce_list.append(wfc)
            except Exception as e:
                traceback.print_exc()
                raise Exception('retrieving workforce failed')
    return workforce_list        
    
def get_children_ids(geoid, geoids=[]):
    geoids = [] + geoids
    children_ids = SetupGeorgraphy.objects.filter(parent_area_id=geoid).values_list('area_id', flat=True)
    if children_ids:
        for childid in children_ids:
            if childid in geoids:
                continue
            geoids.append(childid)
            get_children_ids(childid, geoids)
    return geoids
    
def load_wfc_from_id(wfc_pk,user=None,include_dead=False):
    print 'include_dead',include_dead
    if RegPerson.objects.filter(pk=wfc_pk,is_void=False).count() > 0:
        tmp_wfc = None
        if include_dead:
            tmp_wfc = RegPerson.objects.get(pk=wfc_pk,is_void=False)
        else:
            tmp_wfc = RegPerson.objects.get(pk=wfc_pk,is_void=False,date_of_death=None)
            
        if tmp_wfc:
            if tmp_wfc.workforce_id == '':
                tmp_wfc.workforce_id = fielddictionary.empty_workforce_id
        roles = None
        org_units = []
        person_type = None
        geos = None
        wards=None
        districts = None
        communities = None
        contact = None 
        person_type_id = None
        primary_org_unit_name = ''
        primary_org_id = ''
        if RegPersonsOrgUnits.objects.filter(person=tmp_wfc,date_delinked=None,is_void=False).count() > 0:
            try:
                tmp_org_unit = RegPersonsOrgUnits.objects.get(person=tmp_wfc,primary=True,date_delinked=None,is_void=False)
                if tmp_org_unit:
                    primary_org_unit_name = tmp_org_unit.parent_org_unit.org_unit_name
                    primary_org_id = tmp_org_unit.parent_org_unit.pk
            except:
                primary_org_unit_name = None
                primary_org_id = None
            tmp_org_units = RegPersonsOrgUnits.objects.filter(person=tmp_wfc,is_void=False,date_delinked=None)
            if tmp_org_units:
                for org in tmp_org_units:
                    
                    org_model = org.parent_org_unit
                    tmp_yes_no = org.primary#list_provider.get_item_desc_for_order_and_category(, fielddictionary.gen_YesNo)
                    yes_no_value = ''
                    if tmp_yes_no:                        
                        yes_no_value = 'Yes'
                    else:
                        yes_no_value = 'No'
                    wfc_user = None
                    has_reg_assisstant_role = "No"
                    if tmp_wfc:
                        if len(AppUser.objects.filter(reg_person=tmp_wfc))==1:
                            #print tmp_wfc.workforce_id,'Crashing workforce id'
                            #print tmp_wfc.first_name, 'first_name'
                            #print tmp_wfc.surname,'surname'
                            wfc_user = AppUser.objects.get(reg_person=tmp_wfc)
                            if wfc_user:
                                role_geos = get_user_role_geo_org(wfc_user)
                                for roles_geo in  role_geos:
                                    if roles_geo.org_unit:
                                        if roles_geo.org_unit.pk == org_model.pk and roles_geo.group.group_name=='Registration assistant':
                                            has_reg_assisstant_role="Yes"
                    
                    org_unit = OrganisationUnit(
                                    org_id_int = org_model.pk,
                                    org_id = org_model.org_unit_id_vis,                                    
                                    org_name = org_model.org_unit_name,
                                    primary_org = yes_no_value,
                                    hasRegAssistantRole = has_reg_assisstant_role
                                    )
                    
                    org_units.append(org_unit)
            
        if RegPersonsTypes.objects.filter(person=tmp_wfc,is_void=False,date_ended=None).count() > 0:
            person_type = RegPersonsTypes.objects.get(person=tmp_wfc,is_void=False,date_ended=None)
        person_type_desc=''
        if person_type:
            person_type_id = person_type.person_type_id
            wfc_type_tpl = list_provider.get_description_for_item_id(person_type.person_type_id)
            if(len(wfc_type_tpl) > 0):
                person_type_desc = wfc_type_tpl[0]
            
        #RegPersonsExternalIds
        man_id = ''
        ovc_id = ''
        ts_id = ''
        sing_id = ''
        if RegPersonsExternalIds.objects.filter(person=tmp_wfc,is_void=False).count() > 0:
            tmp_man_id = [(l.identifier) for l in RegPersonsExternalIds.objects.filter(person=tmp_wfc, identifier_type_id = fielddictionary.govt_man_number,is_void=False)]
            if(len(tmp_man_id) > 0):
                man_id = tmp_man_id[0]
                
            tmp_ovc_id = [(l.identifier) for l in RegPersonsExternalIds.objects.filter(person=tmp_wfc, identifier_type_id = fielddictionary.steps_ovc_caregiver,is_void=False)]
            if(len(tmp_ovc_id) > 0):
                ovc_id = tmp_ovc_id[0]
                
            tmp_ts_id = [(l.identifier) for l in RegPersonsExternalIds.objects.filter(person=tmp_wfc, identifier_type_id = fielddictionary.teacher_service_id,is_void=False)]
            if(len(tmp_ts_id) > 0):
                ts_id = tmp_ts_id[0]
                
            tmp_sign_id = [(l.identifier) for l in RegPersonsExternalIds.objects.filter(person=tmp_wfc, identifier_type_id = fielddictionary.police_sign_number,is_void=False)]
            if(len(tmp_sign_id) > 0):
                sing_id = tmp_sign_id[0]
                #sing_id = x[0]
        
        designated_phone = ''
        other_mobile_number = ''
        email_address = ''
        physical_address = ''
        if RegPersonsContact.objects.filter(person=tmp_wfc,is_void=False).count() > 0:
            designated_phone_tpl = [(l.contact_detail) for l in RegPersonsContact.objects.filter(person=tmp_wfc, contact_detail_type_id = fielddictionary.contact_designated_mobile_phone,is_void=False)]
            if(len(designated_phone_tpl) > 0):
                designated_phone = designated_phone_tpl[0]
                
            mobile_phone_tpl = [(l.contact_detail) for l in RegPersonsContact.objects.filter(person=tmp_wfc, contact_detail_type_id = fielddictionary.contact_mobile_phone,is_void=False)]
            if(len(mobile_phone_tpl) > 0):
                other_mobile_number = mobile_phone_tpl[0]
             
            email_address_tpl = [(l.contact_detail) for l in RegPersonsContact.objects.filter(person=tmp_wfc, contact_detail_type_id = fielddictionary.contact_email_address,is_void=False)]
            if(len(email_address_tpl) > 0):
                email_address = email_address_tpl[0]
                
            physical_address_tpl = [(l.contact_detail) for l in RegPersonsContact.objects.filter(person=tmp_wfc, contact_detail_type_id = fielddictionary.contact_physical_address,is_void=False)]
            if(len(physical_address_tpl) > 0):
                physical_address = physical_address_tpl[0]
                
        contact = Contact(designated_phone, other_mobile_number, email_address, physical_address)
        
        geos = {}
        if RegPersonsGeo.objects.filter(person=tmp_wfc,is_void=False,date_delinked=None).count() > 0:
            m_wfc_geo = RegPersonsGeo.objects.filter(person=tmp_wfc,is_void=False,date_delinked=None)
            
            for geo in m_wfc_geo:
                areainfo = SetupGeorgraphy.objects.get(area_id=geo.area_id)
                if areainfo.area_type_id in geos:
                    geos[areainfo.area_type_id].append(areainfo.area_id)
                else:
                    geos[areainfo.area_type_id] = [areainfo.area_id]
        if geos and fielddictionary.geo_type_district_code in geos:
            districts = geos[fielddictionary.geo_type_district_code]
        if geos and fielddictionary.geo_type_ward_code in geos:
            wards = geos[fielddictionary.geo_type_ward_code ]
        
        communties = {}
        if RegPersonsGdclsu.objects.filter(person=tmp_wfc,is_void=False,date_delinked=None).count() > 0:
            communities = RegPersonsGdclsu.objects.filter(person=tmp_wfc,is_void=False,date_delinked=None).values_list('gdclsu_id', flat=True)
        
        org_data_hidden = reconstruct_org_text(org_units)
        
        
        wfc = WorkforceMember(  
                        workforce_id = tmp_wfc.workforce_id,
                        national_id =  tmp_wfc.national_id,
                        first_name = tmp_wfc.first_name,
                        surname = tmp_wfc.surname,
                        other_names = tmp_wfc.other_names,
                        sex_id = tmp_wfc.sex_id,
                        date_of_birth = tmp_wfc.date_of_birth,
                        date_of_death = tmp_wfc.date_of_death,
                        steps_ovc_number = ovc_id,
                        man_number = man_id,
                        ts_number = ts_id,
                        sign_number = sing_id,
                        roles = None,
                        org_units =  org_units,
                        primary_org_unit_name = primary_org_unit_name,
                        person_type = person_type_desc, 
                        gdclsu_details = None, 
                        contact = contact,
                        person_type_id = person_type_id,
                        districts=districts,
                        wards = wards,
                        wards_string = get_obj_strings(wards,None,'ward'),
                        org_units_string = get_obj_strings(org_units,primary_org_unit_name,'org'),
                        communities_string = get_obj_strings(communities,None,'community'),
                        communities= communities,
                        direct_services = '',
                        edit_mode_hidden = '',
                        workforce_type_change_date=None,
                        parent_org_change_date=None,
                        work_locations_change_date=None,
                        org_data_hidden = org_data_hidden,
                        primary_org_id = primary_org_id
                    )
        wfc.id_int = tmp_wfc.pk

        return wfc
    
    else:
        print 'Workforce with the ID passsed does not exists'
        #raise Exception('Workforce with the ID passsed does not exists')

def get_obj_strings(objs,primary_org_unit_name, type):
    to_return_org = None
    if objs:
        for obj in objs:
            string_name = get_string(obj,type)
            if string_name == primary_org_unit_name:
                continue
            if to_return_org:
                to_return_org = '%s, %s'% (to_return_org,string_name)
            else:
                to_return_org = string_name
            
    return to_return_org
            
def get_string(obj,type):
    print 'type',type
    #print 'obj', obj
    to_return_string = None
    if type == 'org':
        to_return_string = obj.org_name
    elif type == 'community':
        to_return_string = RegOrgUnit.objects.get(pk=obj).org_unit_name if RegOrgUnit.objects.filter(pk=obj).count() == 1 else None
    elif type == 'ward':
        to_return_string = SetupGeorgraphy.objects.get(pk=obj).area_name if SetupGeorgraphy.objects.filter(pk=obj).count() == 1 else None
    return to_return_string

    '''
    return {
                'org': obj.org_name,
                'community': obj.org_name,
                'ward': SetupGeorgraphy.objects.get(pk=obj).area_name if SetupGeorgraphy.objects.filter(pk=obj) == 1 else None,
                }[type] if obj else None
    '''
    

#egregious hack: will need refactoring but works for now
def reconstruct_org_text(org_units):
    to_return = {}
    if org_units:
        count = 1
        for org in org_units:
            org_data = {}
            org_data['org_unit_name'] = org.org_name
            org_data['org_unit_id'] = org.org_id
            org_data['primary_org'] = org.primary_org
            org_data['reg_assistant'] = ''

            to_return[count] = org_data
            count = count + 1
    return json.dumps(to_return)

@transaction.commit_on_success
def update_workforce(cleanpagefields, workforce_model_id_int, user, changed_fields=None, is_selective_update=False):
    wfc  = create_workforce_object_from_page(cleanpagefields, user)
    wfc.id_int = workforce_model_id_int
    
    if is_selective_update:
        saved_wfc = load_wfc_from_id(wfc.id_int, user)
        is_related_to_user = False
        is_logged_in_user = False
        is_related_to_user = wfc_and_user_related(saved_wfc,user)
        if user.reg_person:
            if user.reg_person.pk == int(workforce_model_id_int):
                is_logged_in_user = True
        wfc_permission_fields = get_workforce_permission_fields(user=user,nrc=saved_wfc.national_id,workforce_related_to_user=is_related_to_user,workforce_is_logged_in_user=is_logged_in_user)
        fieldstoupdate = auth_access_control_util.fields_to_show(user, wfc_permission_fields['Workforce Update'])
        #print wfc.national_id, 'our national id in workforce object'
        wfc = auth_access_control_util.update_object(object_to_update=saved_wfc, objects_with_updates=wfc, fields_to_update=fieldstoupdate)
        #print wfc.national_id, 'our national id in workforce object after'
    try:
        if wfc:
            reg_person = get_workforce(wfc,user,is_related_to_user,is_logged_in_user)
            if wfc.direct_services and wfc.workforce_id == fielddictionary.empty_workforce_id:
                if wfc.direct_services == '1':
                    assign_wfc_unique_id(reg_person)
            update_user(reg_person, wfc)
            if wfc.edit_mode_hidden == '1':
                #Save RegPersonsContact
                update_contact_details(wfc,reg_person,user,is_related_to_user,is_logged_in_user)  
                #Save RegPersonsTypes
                update_persons_type(wfc,reg_person,user,is_related_to_user,is_logged_in_user)  
                #Save RegPersonsExternalIds
                update_persons_external_ids(wfc, reg_person,user,is_related_to_user,is_logged_in_user) 
                #RegPersonsOrgUnits 
                update_org_units(wfc, reg_person,user,is_related_to_user,is_logged_in_user)
                #RegPersonsGeo
                update_geo_location(wfc,reg_person,user,is_related_to_user,is_logged_in_user)
            elif wfc.edit_mode_hidden == '2':
                record_persons_death(wfc,reg_person,user,is_related_to_user,is_logged_in_user)
                if len(AppUser.objects.filter(reg_person = reg_person)) > 0:
                    reg_person_user = AppUser.objects.filter(reg_person = reg_person)[0]
                    deactivate_user(reg_person_user)
            elif wfc.edit_mode_hidden == '3':
                record_persons_deactivation(wfc,reg_person,user,is_related_to_user,is_logged_in_user)
                special_case_void_designated_mobile_number(wfc,reg_person,user,is_related_to_user,is_logged_in_user)
                if len(AppUser.objects.filter(reg_person = reg_person)) > 0:
                    reg_person_user = AppUser.objects.filter(reg_person = reg_person)[0]
                    delete_user(reg_person_user)
            #Log save
            if reg_person.workforce_id == fielddictionary.empty_workforce_id: #workforce member has no workforce ID
                logger.log(__name__,reg_person,fielddictionary.update_user,fielddictionary.web_interface_id,user=user)
            else:
                logger.log(__name__,reg_person,fielddictionary.update_workforce_member,fielddictionary.web_interface_id,user=user)
            return wfc
        else:
            raise Exception('workforce is a nonetype')

    except Exception as e:
        traceback.print_exc()
        #raise Exception('Workforce has some invalid data. Workforce update failed.')
    
def update_user(person_model, workforce_object):
    try:
        if person_model.workforce_id == fielddictionary.empty_workforce_id:
            person_model.workforce_id = ''
        user = get_or_create_user_from_model(person_model)
        user.national_id = person_model.national_id
        user.first_name = person_model.first_name  
        user.last_name = person_model.surname
        if not (person_model.workforce_id == fielddictionary.empty_workforce_id or person_model.workforce_id == ''):
            user.workforce_id = person_model.workforce_id
        if workforce_object.contact:
            user.designated_phone_number = workforce_object.contact.designated_phone_number
        user.save()
    except Exception as e:
        traceback.print_exc()
        raise Exception('Updating user failed')            

def record_persons_death(wfc,reg_person,user=None,workforce_related_to_user=False,workforce_is_logged_in_user=False):
    if user.has_perm('auth.update wkf general') or (user.has_perm('auth.update wkf general rel') and workforce_related_to_user):
        reg_person.record_death(wfc.date_of_death)
    
def record_persons_deactivation(wfc,reg_person,user=None,workforce_related_to_user=False,workforce_is_logged_in_user=False):
    if user.has_perm('auth.update wkf general') or (user.has_perm('auth.update wkf general rel') and workforce_related_to_user):
        reg_person.make_void()
    
def update_org_units(wfc, reg_person,user=None,workforce_related_to_user=False,workforce_is_logged_in_user=False):
    if user.has_perm('auth.update wkf general') or (user.has_perm('auth.update wkf general rel') and workforce_related_to_user):
        list_of_org_units = json.loads(wfc.org_units)
        orgs_to_save = []
        orgs_to_save_ids = []
        for key, org_data_str in list_of_org_units.items():
            if not org_data_str:
                continue
            org_unit_id = None
            primary_org_unit_yes_no= None
            org_unit_id = org_data_str['org_unit_id'] 
            primary_org_unit_yes_no = org_data_str['primary_org']

            org_model = RegOrgUnit.objects.get(org_unit_id_vis=org_unit_id)
            setup_list_yes_no_index = False
            #yes_no_tpl = list_provider.get_yes_no_index(primary_org_unit_yes_no)
            if primary_org_unit_yes_no == 'Yes':
                setup_list_yes_no_index = True
            org_model.primary = setup_list_yes_no_index
            orgs_to_save.append(org_model)
            orgs_to_save_ids.append(org_model.pk)
        existing_org={}
        if(len(RegPersonsOrgUnits.objects.filter(person = reg_person,date_delinked=None,is_void=False))>0):
            models = RegPersonsOrgUnits.objects.filter(person = reg_person,date_delinked=None,is_void=False)
            for model in models:
                existing_org[model.parent_org_unit_id] = model
        for org in orgs_to_save:
            #if org not in db, save
            if org.pk not in existing_org.keys():
                RegPersonsOrgUnits.objects.create(person = reg_person, parent_org_unit = org, primary = org.primary, date_linked = wfc.parent_org_change_date, date_delinked = None)
            #if org unit in db
            elif org.pk in existing_org.keys():
                #if modified
                existing_person_org_model = existing_org[org.pk]
                if existing_person_org_model.primary != org.primary:
                    #deprecate db model 
                    model_org_unit = RegPersonsOrgUnits.objects.get(parent_org_unit_id=org.pk, person = reg_person, is_void=False)
                    model_org_unit.make_void(wfc.parent_org_change_date)
                    #then save new model
                    RegPersonsOrgUnits.objects.create(person = reg_person, parent_org_unit = org, primary = org.primary, date_linked = wfc.parent_org_change_date, date_delinked = None)

        for kvp in existing_org.items():
            org = kvp[1]
            #if id not in to_save
            if org.parent_org_unit_id not in orgs_to_save_ids:
                #deprecate
                model_org_unit = RegPersonsOrgUnits.objects.get(parent_org_unit_id=org.parent_org_unit_id, person = reg_person,date_delinked=None, is_void=False)
                model_org_unit.make_void(wfc.parent_org_change_date)
                #remove roles
                user = get_or_create_user_from_model(reg_person)
                user_group=''
                if len(OVCUserRoleGeoOrg.objects.filter(user=user, org_unit_id__pk=org.parent_org_unit_id))>0:
                    user_group = OVCUserRoleGeoOrg.objects.get(user=user, org_unit_id__pk=org.parent_org_unit_id).group
                if user_group:
                    remove_role(user, user_group, org.parent_org_unit_id)

def update_persons_external_ids(wfc, reg_person,user=None,workforce_related_to_user=False,workforce_is_logged_in_user=False):  
    model_external_ids = None
    if user.has_perm('auth.update wkf general') or (user.has_perm('auth.update wkf general rel') and workforce_related_to_user):
        if (len(RegPersonsExternalIds.objects.filter(person_id=reg_person,identifier_type_id = fielddictionary.steps_ovc_caregiver,is_void=False)) > 0):
            model_external_ids = RegPersonsExternalIds.objects.get(person_id=reg_person,identifier_type_id = fielddictionary.steps_ovc_caregiver,is_void=False)
            if model_external_ids.identifier != wfc.steps_ovc_number:
                if wfc.steps_ovc_number:
                    model_external_ids.make_void()
                    RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.steps_ovc_caregiver, identifier = wfc.steps_ovc_number)
                if not wfc.steps_ovc_number:
                    model_external_ids.make_void()
        else:
            if wfc.steps_ovc_number:
                RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.steps_ovc_caregiver, identifier = wfc.steps_ovc_number)

        model_external_ids = None
        if (len(RegPersonsExternalIds.objects.filter(person_id=reg_person,identifier_type_id = fielddictionary.govt_man_number,is_void=False)) > 0):
            model_external_ids = RegPersonsExternalIds.objects.get(person_id=reg_person,identifier_type_id = fielddictionary.govt_man_number,is_void=False)
            if model_external_ids.identifier != wfc.man_number:
                if wfc.man_number:
                    model_external_ids.make_void()
                    RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.govt_man_number, identifier = wfc.man_number)
                if not wfc.man_number:
                    model_external_ids.make_void()
        else:
            if wfc.man_number:
                RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.govt_man_number, identifier = wfc.man_number)

        model_external_ids = None
        if (len(RegPersonsExternalIds.objects.filter(person_id=reg_person,identifier_type_id = fielddictionary.teacher_service_id,is_void=False)) > 0):
            model_external_ids = RegPersonsExternalIds.objects.get(person_id=reg_person,identifier_type_id = fielddictionary.teacher_service_id,is_void=False)
            if model_external_ids.identifier != wfc.ts_number:
                if wfc.ts_number:
                    model_external_ids.make_void()
                    RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.teacher_service_id, identifier = wfc.ts_number)
                if not wfc.ts_number:
                    model_external_ids.make_void()
        else:
            if wfc.ts_number:
                RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.teacher_service_id, identifier = wfc.ts_number)

        model_external_ids = None
        if (len(RegPersonsExternalIds.objects.filter(person_id=reg_person,identifier_type_id = fielddictionary.police_sign_number,is_void=False)) > 0):
            model_external_ids = RegPersonsExternalIds.objects.get(person_id=reg_person,identifier_type_id = fielddictionary.police_sign_number,is_void=False)
            if model_external_ids.identifier != wfc.sign_number:
                if wfc.sign_number:
                    model_external_ids.make_void()
                    RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.police_sign_number, identifier = wfc.sign_number)
                if not wfc.sign_number:
                    model_external_ids.make_void()
        else:
            if wfc.sign_number:
                RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.police_sign_number, identifier = wfc.sign_number)

def update_persons_type(wfc,reg_person,user=None,workforce_related_to_user=False,workforce_is_logged_in_user=False):
    wfc_type_id = wfc.person_type
    if user.has_perm('auth.update wkf general') or (user.has_perm('auth.update wkf general rel') and workforce_related_to_user):
        if wfc_type_id:  
            try:
                model_person_type = RegPersonsTypes.objects.get(person_id=reg_person, date_ended = None, is_void=False)
                if model_person_type.person_type_id != wfc_type_id:
                    model_person_type.make_void(wfc.workforce_type_change_date)
                    RegPersonsTypes.objects.create(person = reg_person, person_type_id = wfc_type_id, date_began = wfc.workforce_type_change_date, date_ended = None)
            except Exception as e:
                traceback.print_exc()
                raise Exception('Updating workforce type failed.')
        
def get_workforce(wfc,user=None,workforce_related_to_user=False,workforce_is_logged_in_user=False):
    try:
        model_person = RegPerson.objects.get(pk=wfc.id_int,is_void=False,date_of_death=None)
        if user.has_perm('auth.update wkf special'):
            if not wfc.workforce_id == fielddictionary.empty_workforce_id:
                model_person.workforce_id = wfc.workforce_id
            #Only people with special permissions can update exisitng nrc
            if model_person.national_id:
                model_person.national_id = wfc.national_id  
            model_person.first_name = wfc.first_name
            model_person.other_names = wfc.other_names
            model_person.surname = wfc.surname
            model_person.date_of_birth = wfc.date_of_birth
            model_person.sex_id = wfc.sex_id 
        #if NRC is blank, only user with general permissions can add one
        if not model_person.national_id:
            if user.has_perm('auth.update wkf general') or (user.has_perm('auth.update wkf general rel') and workforce_related_to_user):
                model_person.national_id = wfc.national_id 

        model_person.save()
    
    except Exception as e:
        traceback.print_exc()
        raise Exception('Workforce general details update Failed')
    return model_person

def special_case_void_designated_mobile_number(wfc,reg_person,user,workforce_related_to_user,workforce_is_logged_in_user):
    model_contact = None
    if RegPersonsContact.objects.filter(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_designated_mobile_phone,is_void=False).count() > 0:
        model_contact = RegPersonsContact.objects.get(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_designated_mobile_phone,is_void=False)
        if wfc.contact.designated_phone_number and model_contact:
            model_contact.make_void()


def update_contact_details(wfc,reg_person,user,workforce_related_to_user,workforce_is_logged_in_user): 
    contact = wfc.contact
    if user.has_perm('auth.update wkf contact') or (user.has_perm('auth.update wkf contact rel') and workforce_related_to_user) or (user.has_perm('auth.update wkf contact own') and workforce_is_logged_in_user):
        if len(contact.designated_phone_number) == 10:
            contact.designated_phone_number = '+26'+contact.designated_phone_number
        if len(contact.other_mobile_number) == 10:
            contact.other_mobile_number = '+26'+contact.other_mobile_number

        model_contact = None
        if (len(RegPersonsContact.objects.filter(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_designated_mobile_phone,is_void=False)) > 0):
            model_contact = RegPersonsContact.objects.get(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_designated_mobile_phone,is_void=False)
            if model_contact.contact_detail != contact.designated_phone_number:
                if contact.designated_phone_number:
                    model_contact.make_void()
                    RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_designated_mobile_phone, contact_detail = contact.designated_phone_number)
                elif not contact.designated_phone_number:
                    model_contact.make_void()
        else:
            if contact.designated_phone_number:
                RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_designated_mobile_phone, contact_detail = contact.designated_phone_number)

        model_contact = None
        if (len(RegPersonsContact.objects.filter(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_mobile_phone,is_void=False)) > 0):
            model_contact = RegPersonsContact.objects.get(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_mobile_phone,is_void=False)
            if model_contact.contact_detail != contact.other_mobile_number:
                if contact.other_mobile_number:
                    model_contact.make_void()
                    RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_mobile_phone, contact_detail = contact.other_mobile_number)
                elif not contact.other_mobile_number:
                    model_contact.make_void()
        else:
            if contact.other_mobile_number:
                RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_mobile_phone, contact_detail = contact.other_mobile_number)

        model_contact = None
        if (len(RegPersonsContact.objects.filter(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_email_address,is_void=False)) > 0):
            model_contact = RegPersonsContact.objects.get(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_email_address,is_void=False)
            if model_contact.contact_detail != contact.email_address:
                if contact.email_address:
                    model_contact.make_void()
                    RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_email_address, contact_detail = contact.email_address, is_void = False)
                if not contact.email_address:
                    model_contact.make_void()
        else:
            if contact.email_address:
                RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_email_address, contact_detail = contact.email_address, is_void = False)

        model_contact = None
        if (len(RegPersonsContact.objects.filter(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_physical_address,is_void=False)) > 0):
            model_contact = RegPersonsContact.objects.get(person_id=reg_person,contact_detail_type_id = fielddictionary.contact_physical_address,is_void=False)
            if model_contact.contact_detail != contact.physical_address:
                if contact.physical_address:
                    model_contact.make_void()
                    RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_physical_address, contact_detail = contact.physical_address)
                if not contact.physical_address:
                    model_contact.make_void()
        else:
            if contact.physical_address:
                RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_physical_address, contact_detail = contact.physical_address)
    
def update_geo_location(wfc, mperson,user=None,workforce_related_to_user=False,workforce_is_logged_in_user=False):
    locations = wfc.geo_location
    tolocationstoupdate = []
    if user.has_perm('auth.update wkf general') or (user.has_perm('auth.update wkf general rel') and workforce_related_to_user):
        try:
            #print locations,'locations received'
            for location_level in locations:#get location types
                #print locations[location_level],location_level
                if not locations[location_level]:
                    if 'districts' == location_level:
                        #All districts emptied
                        if (RegPersonsGdclsu.objects.filter(person=mperson, is_void=False)).count > 0:
                            gdclsus = RegPersonsGdclsu.objects.filter(person=mperson, is_void=False)
                            for gl in gdclsus:
                                gl.make_void(date_delinked=wfc.work_locations_change_date, is_void=True)
                        #RegPersonsGdclsu.objects.filter(person=mperson, is_void=False).update(date_delinked=wfc.work_locations_change_date, is_void=True)
                        if (RegPersonsGeo.objects.filter(person=mperson, is_void=False)).count > 0:
                            geos = RegPersonsGeo.objects.filter(person=mperson, is_void=False)
                            for geo in gdclsus:
                                geo.make_void(date_delinked=wfc.work_locations_change_date, is_void=True)
                        #RegPersonsGeo.objects.filter(person=mperson, is_void=False).update(is_void=True, date_delinked=wfc.work_locations_change_date)
                    if 'wards' == location_level:
                        pass
                        #All wards removed
                    if 'communities' == location_level:
                        if (RegPersonsGdclsu.objects.filter(person=mperson, is_void=False)).count > 0:
                            gdclsus = RegPersonsGdclsu.objects.filter(person=mperson, is_void=False)
                            for gl in gdclsus:
                                gl.make_void(date_delinked=wfc.work_locations_change_date, is_void=True)
                        #RegPersonsGdclsu.objects.filter(person=mperson, is_void=False).update(date_delinked=wfc.work_locations_change_date, is_void=True)
                        #All communities removed
                    continue
                if 'communities' == location_level:
                    if (RegPersonsGdclsu.objects.filter(person=mperson, is_void=False).exclude(gdclsu_id__in=locations[location_level])).count > 0:
                        gdclsus = RegPersonsGdclsu.objects.filter(person=mperson, is_void=False).exclude(gdclsu_id__in=locations[location_level])
                        for gl in gdclsus:
                            gl.make_void(date_delinked=wfc.work_locations_change_date, is_void=True)
                    #RegPersonsGdclsu.objects.filter(person=mperson, is_void=False).exclude(gdclsu_id__in=locations[location_level]).update(date_delinked=wfc.work_locations_change_date, is_void=True)
                    communities = locations[location_level]
                    for community in communities:
                        gdclsu_model,created = RegPersonsGdclsu.objects.get_or_create(person=mperson, gdclsu_id=community,is_void=False)
                        if created:
                            gdclsu_model.date_linked=wfc.work_locations_change_date
                            gdclsu_model.save()
                    continue
                tolocationstoupdate += locations[location_level]
            
            if (RegPersonsGeo.objects.filter(person=mperson, is_void=False).exclude(area_id__in=tolocationstoupdate)).count > 0:
                geos = RegPersonsGeo.objects.filter(person=mperson, is_void=False).exclude(area_id__in=tolocationstoupdate)
                for geo in geos:
                    geo.make_void(date_delinked=wfc.work_locations_change_date, is_void=True)
            #RegPersonsGeo.objects.filter(person=mperson, is_void=False).exclude(area_id__in=tolocationstoupdate).update(is_void=True, date_delinked=wfc.work_locations_change_date)

            for locationid in tolocationstoupdate:#get specifict locations
                geo_model,created = RegPersonsGeo.objects.get_or_create(person=mperson, area_id=locationid,is_void=False)
                if created:
                    geo_model.date_linked=wfc.work_locations_change_date
                    geo_model.save()
            if tolocationstoupdate:
                try:
                    remove_roles_in_geo(user, preserve_area_list=tolocationstoupdate)
                except Exception as e:
                    traceback.print_exc()
        except Exception as e:
            traceback.print_exc()
            raise Exception('failed to update to geo locations')



def user_and_wfc_same_org(workforce,user):
    to_return = False
    workforce_primary_org_id = workforce.primary_org_id
    orgids_where_user_is_reg_assistant = getOrgIdsWhereIsRegAssistant(user)
    orgids_where_user_is_reg_assistant,'orgids_where_user_is_reg_assistant'
    if workforce_primary_org_id in orgids_where_user_is_reg_assistant:
        to_return = True
    return to_return
    
    
def wfc_and_user_related(workforce,user):
    is_related = False
    workforce_parent_org_ids = [org.org_id_int for org in workforce.org_units]
    orgids_where_user_is_reg_assistant = auth_access_control_util.reg_assistant_orgs(user, subunit_limit=2)#getOrgIdsWhereIsRegAssistant(user)
    if orgids_where_user_is_reg_assistant.count > 0:
        for org_id, org_name in orgids_where_user_is_reg_assistant:
            if len(RegOrgUnit.objects.filter(org_unit_id_vis=org_id)) > 0:
                parent_org = RegOrgUnit.objects.filter(org_unit_id_vis=org_id)[0].pk
                sub_units = get_all_org_subunits_and_self([parent_org], limit=2)
                for item in workforce_parent_org_ids:                
                    if item in sub_units:
                        is_related = True
                        break
    return is_related

def getOrgIdsWhereIsRegAssistant(user=None):
    toReturn = []
    regPerson = user.reg_person  
    if regPerson:
        wfc_from_user = load_wfc_from_id(regPerson.pk,user)
        if wfc_from_user.org_units:
            for org in wfc_from_user.org_units:
                if org.is_reg_assistant:
                    toReturn.append(org.org_id_int)
        
    else:
        print 'User does not have a valid reg person entity'
    return toReturn


def get_workforce_permission_fields(user=None,nrc=None,workforce_related_to_user=None,workforce_is_logged_in_user=None): 
    #nrc field
    nrc_special = ''
    nrc_general = ''
    #contact fields
    contact = ''
    #general fields
    workforce_type = ''
    man_number = ''
    ts_number = ''
    sign_number = ''
    steps_ovc_number = ''
    direct_services = ''
    org_unit = ''
    primary_parent_org_unit = ''
    parent_org_change_date = ''
    workforce_type_change_date = ''
    work_locations_change_date = ''
    #special fields
    first_name = ''
    last_name = ''
    other_names = ''
    sex = ''
    date_of_birth = ''
    date_of_death = ''
    geo_location = ''
    
    if user:
        #Handle NRC special case
        if user.has_perm('auth.update wkf special'):
            first_name = 'first_name'
            last_name = 'surname'
            other_names = 'other_names'
            sex = 'sex_id'
            date_of_birth = 'date_of_birth'
        if nrc:
            nrc_special = 'national_id'
            nrc_general = ''
            
        elif not nrc:
            nrc_special = ''
            nrc_general = 'national_id'
        #contact fields permissions
        if user.has_perm('auth.update wkf contact'):
            contact = 'contact'            
        elif user.has_perm('auth.update wkf contact rel'):
            if workforce_related_to_user:
                contact = 'contact'                
        elif user.has_perm('auth.update wkf contact own'):
            if workforce_is_logged_in_user:
                contact = 'contact'
        
        #general fields permissions
        if user.has_perm('auth.update wkf general'):
            workforce_type = 'person_type'
            man_number = 'man_number'
            ts_number = 'ts_number'
            sign_number = 'sign_number'
            steps_ovc_number = 'steps_ovc_number'
            #direct_services = 'direct_services'
            org_unit = 'org_units'
            #primary_parent_org_unit = 'primary_parent_org_unit'
            parent_org_change_date = 'parent_org_change_date'
            workforce_type_change_date = 'workforce_type_change_date'
            work_locations_change_date = 'work_locations_change_date'
            date_of_death = 'date_of_death'
            geo_location = 'geo_location'
            
        elif user.has_perm('auth.update wkf general rel'):
            if workforce_related_to_user:
                workforce_type = 'person_type'
                man_number = 'man_number'
                ts_number = 'ts_number'
                sign_number = 'sign_number'
                steps_ovc_number = 'steps_ovc_number'
                #direct_services = 'direct_services'
                org_unit = 'org_units'
                #primary_parent_org_unit = 'primary_parent_org_unit'
                parent_org_change_date = 'parent_org_change_date'
                workforce_type_change_date = 'workforce_type_change_date'
                work_locations_change_date = 'work_locations_change_date'
                date_of_death = 'date_of_death'
                geo_location = 'geo_location'
        
        roles_and_fields_to_show['Workforce Update'] ={ 'auth.update wkf special':[first_name, last_name, other_names, sex, date_of_birth,nrc_special,'edit_mode_hidden','workforce_id','direct_services'],
            'auth.update wkf contact':[contact,'edit_mode_hidden','workforce_id','direct_services'],
            'auth.update wkf contact own':[contact,'edit_mode_hidden','workforce_id','direct_services'],
            'auth.update wkf contact rel':[contact,'edit_mode_hidden','workforce_id','direct_services'],
            'auth.update wkf general':[workforce_type, man_number, ts_number, sign_number,steps_ovc_number,direct_services,org_unit,primary_parent_org_unit,workforce_type_change_date,work_locations_change_date,nrc_general,date_of_death,parent_org_change_date,geo_location,'edit_mode_hidden','workforce_id','direct_services'],
            'auth.update wkf general rel':[workforce_type, man_number, ts_number, sign_number,steps_ovc_number,direct_services,org_unit,primary_parent_org_unit,workforce_type_change_date,work_locations_change_date,nrc_general,date_of_death,parent_org_change_date,geo_location,'edit_mode_hidden','workforce_id','direct_services']
            }
    return roles_and_fields_to_show
