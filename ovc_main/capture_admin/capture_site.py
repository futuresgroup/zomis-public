from ovc_main.models import AdminCaptureSites
from django import forms
import datetime
import requests
from django.conf import settings
from OVC_IMS.celery_app import app
from celery import Celery
from celery.result import AsyncResult
import celery.states
import json
from rest_framework import status

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, \
    Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText

'''
The code in here runs at the capture site. Server side functions are defined
in the ./server_admin.py
'''
class CaptureSite():
    def __init__(self, *args, **kwargs): 
        if args:
            #TODO
            pass           
        if kwargs:
            self.capture_site_name = kwargs['capture_site_name'] if 'capture_site_name' in kwargs else None
            self.site_admin_appuser_id = kwargs['admin_app_user_id'] if 'admin_appuser_id' in kwargs else None
            self.org_unit_vis_id = kwargs['org_unit_vis_id'] if 'org_unit_vis_id' in kwargs else None
            self.mac_address = kwargs['mac_address'] if 'mac_address' in kwargs else None
            self.date_installed = kwargs['date_installed'] if 'date_installed' in kwargs else None
            self.datetime_installed = kwargs['datetime_installed'] if 'datetime_installed' in kwargs else None
            self.approved = kwargs['approved'] if 'approved' in kwargs else None
            self.approval_status = kwargs['approval_status'] if 'approval_status' in kwargs else None
            self.datetime_approved = kwargs['datetime_approved'] if 'datetime_approved' in kwargs else None
            self.server_error_message = kwargs['server_error_message'] if 'server_error_message' in kwargs else None
            self.capture_site_id = kwargs['capture_site_id'] if 'capture_site_id' in kwargs else None
            self.password_for_site_admin = kwargs['password_for_site_admin'] if 'password_for_site_admin' in kwargs else None
            self.remote_server_contacted = kwargs['remote_server_contacted'] if 'remote_server_contacted' in kwargs else None
        
            
class SetupCaptureSiteForm(forms.Form):
    org_unit_vis_id = forms.CharField(label="Owning organisational unit ZOMIS ID")
    site_admin_appuser_id = forms.CharField(label="NRC/Workforce ID for user carrying out this installation")
    password_for_site_admin = forms.CharField(label="Password for user carrying out this installation", widget=forms.PasswordInput())
    capture_site_name = forms.CharField(label="Name of capture site")
    mac_address = forms.CharField(widget=forms.HiddenInput(), required=False)
    date_installed = forms.CharField(widget=forms.HiddenInput(), required=False)
    datetime_installed = forms.CharField(widget=forms.HiddenInput(), required=False)
    
    approved = forms.CharField(widget=forms.HiddenInput(), required=False)
    approval_status = forms.CharField(widget=forms.HiddenInput(), required=False)
    datetime_approved = forms.CharField(widget=forms.HiddenInput(), required=False)
    server_error_message = forms.CharField(widget=forms.HiddenInput(), required=False)
    capture_site_id = forms.CharField(widget=forms.HiddenInput(), required=False)
    remote_server_contacted = forms.CharField(widget=forms.HiddenInput(), required=False)
    
    _capture_site = None
    
    def __init__(self, *args, **kwargs):
        super(SetupCaptureSiteForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = '/capture/initialsitesetup/'
        self.helper.layout = Layout(
                                    Div(
                                        Div(
                                            css_class = 'panel-heading'
                                        ),
                                        Div(
                                             HTML('<p/>')
                                            ,'org_unit_vis_id'
                                            ,'site_admin_appuser_id'
                                            ,'password_for_site_admin'
                                            ,'capture_site_name'
                                            ,'mac_address'
                                            ,'date_installed'
                                            ,'datetime_installed'
                                            ,'approved'
                                            ,'approval_status'
                                            ,'datetime_approved'
                                            ,'server_error_message'
                                            ,'capture_site_id'
                                            ),
                                        css_class='panel panel-grey'
                                        ),
                                    Div(

                                        #Submit('save', 'Save changes'),
                                        #Button('cancel', 'Cancel'),
                                        HTML("<Button type=\"submit\" style=\"margin-right:4px\" class=\" mandatory btn btn-primary\"><i class=\"fa fa-wrench\"></i> Setup</Button>"),
                                        style='margin-left:38%'
                                       )
                                    )
    
    def clean(self):
        cleaned_data = super(SetupCaptureSiteForm, self).clean()
        
        org_unit_vis_id = self.cleaned_data.get('org_unit_vis_id')
        site_admin_appuser_id = self.cleaned_data.get('site_admin_appuser_id')
        capture_site_name = self.cleaned_data.get('capture_site_name')
        mac_address = self.cleaned_data.get('mac_address')
        date_installed = self.cleaned_data.get('date_installed')
        datetime_installed = self.cleaned_data.get('datetime_installed')
        
        if org_unit_vis_id and site_admin_appuser_id and capture_site_name:
            '''
            self._capture_site = CaptureSite(sitename=capture_site_name, site_admin_appuser_id=site_admin_appuser_id,
                                        installer_appuser_id=installer_appuser_id, org_unit_vis_id=org_unit_vis_id,
                                        mac_address, date_installed=date_installed, datetime_installed=datetime_installed)
            '''
            self._capture_site = CaptureSite(**cleaned_data)
            return cleaned_data
        else:
            raise forms.ValidationError('Ensure that you have entered all the fields')
    

def approve_capture_site(capturesiteid):    
    '''
    at capture site, receive values from the server side and update
    local admincapturesites model. 
    #TODO:
    There should really be different
    models for the capture site and the server site
    '''
    AdminCaptureSites.objects.filter(id=capturesiteid).update(approved=True, approval_status=2)

@app.task(serializer='json', bind=True)
def register_site_on_remote_server(capturesite):
    while not is_connected():
        print "no connecting to server"
    server_url = settings.CAPTURE_SITE_URLS['server_url']
    req = requests.post(server_url, headers = {'content-type': 'application/json'}, data = json.dumps(capturesite.__dict__))
    if status.is_success(req.status_code):
        capturesite.remote_server_contacted = True
        capturesite.save()
        return True
    else:
        return False
    
def add_site_to_server(capturesite):
    capturesite = add_local_site(capturesite)
    #post stuff to server at this point after recording at localsite
    if capturesite:
        register_site_on_remote_server.delay(capturesite)

def local_site_info():
    siteinfo = None
    siterecords = AdminCaptureSites.objects.all()
    if len(siterecords) > 0:
        site = siterecords[0]
        siteinfo = {}
        siteinfo['approval_status'] = site.approval_status
        siteinfo['server_error_message'] = site.server_error_message
        capturesite = CaptureSite(site.__dict__)
    
    return capturesite
    

def add_local_site(capturesite):
    from uuid import getnode
    
    c = capturesite
    
    nowdatetime = datetime.datetime.now()
    macaddress = getnode() #this may not be an actual mac_address, but useful anyhow
    c.mac_address = macaddress
    c.date_installed = datetime.datetime.now().date()
    c.datetime_installed = nowdatetime
    try:
        AdminCaptureSites.objects.create(capture_site_name=c.capture_site_name,
                                         date_installed=nowdatetime.date(),
                                         datetime_installed=nowdatetime,
                                         mac_address = macaddress,
                                         org_unit_vis_id = c.org_unit_vis_id,
                                         installer_appuser_id = c.installer_appuser_id,
                                         site_admin_appuser_id = c.site_installer_appuser_id)
    except Exception as e:
        print e
        raise Exception(e)
    return c
    
def update_local_capture_site(capturesiteid):
    '''
    '''
    AdminCaptureSites.objects.filter(id=capturesiteid).update(approved=False, approval_status=3)
    
def local_capture_site_info():
    csite = None
    sites = AdminCaptureSites.objects.all()
    if sites:
        site = sites[0]
        csite = CaptureSite(site.__dict__)
    return csite
        

def is_connected():
    import socket
    REMOTE_SERVER = "zomis.mcdmch.gov.zm"
    try:
        # see if we can resolve the host name -- tells us if there is
        # a DNS listening
        host = socket.gethostbyname(REMOTE_SERVER)
        # connect to the host -- tells us if the host is actually
        # reachable
        s = socket.create_connection((host, 80), 2)
        return True
    except:
        pass
    return False
    