from ovc_main.models import AdminCaptureSites


def get_capture_sites():
    return AdminCaptureSites.objects.all()
    
def get_capture_site(capturesiteid):
    try:
        return AdminCaptureSites.objects.get(id=capturesiteid)
    except:
        return None


def record_capture_site(capturesite):
    c = capturesite
    site = AdminCaptureSites.objects.create(capture_site_name=c.capture_site_name,
                                         date_installed=c.date_installed,
                                         datetime_installed=c.datetime_installed,
                                         mac_address = c.macaddress,
                                         org_unit_vis_id = c.org_unit_vis_id,
                                         installer_appuser_id = c.installer_appuser_id,
                                         site_admin_appuser_id = c.site_installer_appuser_id)
    site.capture_site_id = site.id
    site.save()
    return True
    
def approve_site(capturesiteid, appuser):
    try:
        AdminCaptureSites.objects.filter(id=capturesiteid).update(approval_status=2,
                                                                  approved=True,
                                                                  approved_by_appuser_id=appuser.id)
    except Exception as e:
        print e
        raise Exception(e)
    
    return True
    