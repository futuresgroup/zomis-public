'''
Created on Oct 6, 2014

@author: pkaumba
'''
from datetime import datetime
from datetime import date, time
from models import CoreAdverseConditions, RegPerson, RegPersonsContact, RegPersonsTypes, FormResChildren, SetupGeorgraphy, RegOrgUnit
from models import RegPersonsExternalIds, RegPersonsGuardians, RegPersonsGeo, RegPersonsGdclsu, RegPersonsOrgUnits, SetupList
from models.audit_trail import RegPersonsAuditTrail
from ovc_main.utils.idgenerator import beneficiary_id_generator
from ovc_main.utils import lookup_field_dictionary as fielddictionary, fields_list_provider as list_provider, audit_trail as logger
import organisation_registration as org_reg
import child_reg_forms
import datetime
from workforce_registration import OrganisationUnit
from django.db import transaction
from ovc_main.utils import auth_access_control_util
from django.conf import settings
from ovc_auth.models import AppUser




permission_type_fields = {  'rel_org':
                                {        
                                'auth.update ben contact by org':['contact'],
                                'auth.update ben general by org':['steps_ovc_number','residential_institution','ben_district','ben_ward','community',
                                                                  'date_of_death','location_change_date','prev_loc_change_date','guardian_change_date','prev_guard_change_date','guardian','notes_relationship','guardians_hidden', 'edit_mode_hidden', 'pk_id']
                                },
        
                            'other':
                                {
                                 'auth.update ben special':['first_name', 'last_name', 'other_name', 'sex_id', 'date_of_birth','birth_cert_num','workforce_member','date_paper_form_filled','national_id','edit_mode_hidden', 'pk_id'],
                                 'auth.update ben contact by org':['contact', 'edit_mode_hidden','workforce_member','date_paper_form_filled', 'pk_id'],
                                'auth.update ben general by org':['steps_ovc_number','residential_institution','residence','audit_trail'
                                                                  'date_of_death','location_change_date','workforce_member','date_paper_form_filled','prev_loc_change_date','guardian_change_date','prev_guard_change_date','guardian','notes_relationship', 'associated_guardians','guardians_hidden', 'edit_mode_hidden', 'pk_id']
                                }
                          }

__name__ = 'person'
something_saved = False

class AuditTrail:
    Person_recorded_on_paper = None
    Paper_date = None
    Person_recorded_electronically = None    
    Electronic_date_time = None    
    Interface = None
    
    def __init__(self,Person_recorded_on_paper , Paper_date, Person_recorded_electronically, Electronic_date_time, Interface):
            self.Person_recorded_on_paper = Person_recorded_on_paper
            self.Paper_date = Paper_date
            self.Person_recorded_electronically = Person_recorded_electronically
            self.Electronic_date_time = Electronic_date_time
            self.Interface = Interface        

class Guardian:
    #Child's guardians/primary caregivers
    no_guardian = None
    guardian = None
    notes_relationship = None
    guard_name = None
    datelinked = None
    
    def __init__(self, no_guardian, guardian, notes_relationship, guard_name, datelinked):
            self.datelinked = datelinked
            self.no_guardian = no_guardian
            self.guardian = guardian
            self.notes_relationship = notes_relationship
            self.guard_name = guard_name
            
    def __unicode__(self):
        return 'noguardian:', self.no_guardian, 'guardian:',self.guardian, 'ward:', self.notes_relationship
        
    def get_contact_dict(self):
        return {#fielddictionary.contact_physical_address: self.residential_institution,
                fielddictionary.guard: self.no_guardian, 
                fielddictionary.guardian: self.guardian, 
                fielddictionary.notes_relationship: self.notes_relationship,}
    
class Contact:
    #Child's contact details
    mobile_phone_number = None
    email_address = None
    physical_address = None
        
    def __init__(self, mobile_phone_number, email_address, physical_address):
        self.mobile_phone_number = mobile_phone_number
        self.email_address = email_address
        self.physical_address = physical_address                         
            
    def __unicode__(self):
        return 'landline:', self.landline_phone_number, 'mobile:',self.mobile_phone_number, 'emailaddress:', self.email_address, 'postaladdress', self.postal_address,'physicaladdress:', self.physical_address
        
    def get_contact_dict(self):
        return {fielddictionary.contact_landline_phone: self.designated_phone_number, 
                fielddictionary.contact_mobile_phone: self.other_mobile_number, 
                fielddictionary.contact_physical_address: self.physical_address,
                fielddictionary.contact_post_address: self.postal_address, 
                fielddictionary.contact_email_address: self.email_address}
    
class Residence:
    #Where the child lives
    residential_institution = None
    district = None
    ward = None
    community = None
    res_id = None
    
    def __init__(self, residential_institution, district, ward, community, res_id):
            self.residential_institution = residential_institution
            self.district = district
            self.ward = ward
            self.community = community
            self.res_id = res_id
    
    def __unicode__(self):
        return 'institution:', self.residential_institution, 'district:',self.district, 'ward:',self.ward , 'community', self.community
        
    def get_contact_dict(self):
        return {fielddictionary.geo_type_residential_institution: self.residential_institution,
                fielddictionary.geo_type_district_code: self.district, 
                fielddictionary.geo_type_ward_code: self.ward, 
                fielddictionary.geo_type_community: self.community,}
    

class OvcChild:
    #Child Identification information
    pk_id = None
    beneficiary_id = None
    first_name = None 
    last_name  = None
    other_name  = None
    name = None
    sex_id  = None
    date_of_birth = None 
    date_of_death = None
    birth_cert_num  = None
    steps_ovc_number  = None
    national_id  = None
    guardian = None
    contact = None
    adverse_cond = []
    residence = None
    guardian_hidden = None
    associated_guardians = None
    person_type = None
    sex = None
    edit_mode_hidden = None
    location_change_date = None
    guardian_change_date = None
    date_paper_form_filled = None
    workforce_member = None
    audit_trail = None


    
    def __init__(self,pk_id,beneficiary_id,first_name,last_name, other_name, sex_id, date_of_birth, date_of_death, steps_ovc_number,national_id, guardian, contact, adverse_cond,residence, birth_cert_num, associated_guardians, person_type, sex, edit_mode_hidden, location_change_date, guardian_change_date, date_paper_form_filled, workforce_member, audit_trail):
                    
        if not beneficiary_id:
            self.id = national_id
        else:
            self.id = beneficiary_id
        self.beneficiary_id = beneficiary_id
        self.national_id = national_id
        self.first_name = first_name
        self.last_name = last_name
        self.other_name = other_name
        
        #If both names exist, concatenate them, if not, use only the first name or only the last name
        if first_name:
            self.name = first_name
            if last_name:
                self.name = self.name + ' ' + last_name
        elif last_name:
            self.name = last_name
        
        self.sex_id = sex_id
        self.date_of_birth = date_of_birth
        self.date_of_death = date_of_death
        self.birth_cert_num = birth_cert_num
        self.steps_ovc_number = steps_ovc_number   
        self.contact = contact
        self.residence = residence
        self.adverse_cond = adverse_cond
        self.guardian = guardian
        self.associated_guardians = associated_guardians
        self.person_type = person_type
        self.sex = sex
        self.pk_id = pk_id
        self.edit_mode_hidden = edit_mode_hidden
        self.location_change_date = location_change_date
        self.guardian_change_date = guardian_change_date
        self.date_paper_form_filled = date_paper_form_filled
        self.workforce_member = workforce_member
        self.audit_trail = audit_trail
        
    def __unicode__(self):
        return self.first_name + ' ' + self.last_name

@transaction.commit_on_success
def save_beneficiary(cleanpagefields, user, page_source,return_pk=False):
 
    is_guardian = False
    
    if page_source == 'guardian':
         is_guardian = True
    else:
        is_guardian = False
         
    #create beneficiary object from page fields
    ben  = create_benficiary_object_from_page(cleanpagefields, user, is_guardian)
    print 'Saving beneficiary...'
    reg_person = save_ben(ben)
    print 'assigning unique ids'
    assign_ben_unique_id(reg_person)
    ben.beneficiary_id = reg_person.beneficiary_id
    print 'Saving contacts...'
    save_contact = {'save_mobile': True,'save_email': True,'save_address': True}
    save_contact_details(ben,reg_person,save_contact)
    print 'Saving where the beneficiary lives'
    save_location = {'save_gdclsu': True,'save_ward': True, 'save_res':True }
    save_beneficiary_location(ben,reg_person, is_guardian, save_location)
    print 'Saving child\'s person type...'
    save_persons_type(reg_person, is_guardian)
    #End the execution here if saving guardian and not OVC child
    if is_guardian:
        print 'log audit to trail'
        save_audit_trail(ben,user,reg_person,fielddictionary.register_beneficiary)
        return ben.beneficiary_id
    
    print 'Saving external Ids...' 
    save_persons_external_ids(ben, reg_person)
    print 'Saving adverse conditions...' 
    save_adverse_conditions(ben,reg_person)
    print 'Saving ovc child\'s guardians'
    save_ben_guardians(ben, reg_person)
    print 'log audit to trail'
    save_audit_trail(ben,user,reg_person,fielddictionary.register_beneficiary)
    
    if return_pk:
        return ben.pk_id
    'return ben.beneficiary_id',ben.beneficiary_id
    return ben.beneficiary_id
    
       
def save_ben(ben):
    try:
        reg_person = RegPerson.objects.create(beneficiary_id=ben.beneficiary_id,
                                        national_id=ben.national_id,
                                        first_name=ben.first_name,
                                        other_names = ben.other_name,
                                        surname=ben.last_name,
                                        date_of_birth=ben.date_of_birth,
                                        date_of_death=ben.date_of_death,
                                        sex_id=ben.sex_id,
                                        birth_reg_id=ben.birth_cert_num,
                                        is_void=False
                                        )
        global something_saved
        something_saved = True
        return reg_person
    except Exception as e:
        print e     
    
def save_contact_details(ben,reg_person,save_contacts=None): 
    contact = ben.contact
    global something_saved
    if save_contacts:
        try:
            if save_contacts['save_mobile'] == True:
                if contact.mobile_phone_number:
                    RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_mobile_phone, contact_detail = contact.mobile_phone_number, is_void=False)
                    something_saved = True
            
            if save_contacts['save_email'] == True:
                if contact.email_address:
                    RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_email_address, contact_detail = contact.email_address, is_void=False)
                    something_saved = True
                    
            if save_contacts['save_address'] == True:
                if contact.physical_address:
                    RegPersonsContact.objects.create(person = reg_person, contact_detail_type_id = fielddictionary.contact_physical_address, contact_detail = contact.physical_address, is_void=False)
                    something_saved = True
        except Exception as e:
            print e

def assign_ben_unique_id(reg_person):
    print 'assigning unique beneficiary ID'
    uniqueid = beneficiary_id_generator(reg_person)
    reg_person.beneficiary_id = uniqueid
    reg_person.save()
    return uniqueid

def save_persons_external_ids(ben, reg_person):   
    if ben.steps_ovc_number:
        RegPersonsExternalIds.objects.create(person=reg_person, identifier_type_id = fielddictionary.steps_ovc_caregiver, identifier = ben.steps_ovc_number, is_void=False)
        global something_saved
        something_saved = True
        
def get_adverse_conditions(cleanpagefields):
    return cleanpagefields['adverse_cond']


def save_adverse_conditions(ben,reg_person):
    adverse_conditions = ben.adverse_cond
    for adverse_condition in adverse_conditions:
        CoreAdverseConditions.objects.create(beneficiary_person_id = reg_person.pk,
                                             adverse_condition_id = adverse_condition,
                                             is_void=False,
                                             sms_id = None,
                                             form_id = None
                                             )
        global something_saved
        something_saved = True
        
def save_persons_type(reg_person, is_guardian):
    person_type = None
    global something_saved
    if is_guardian:
        person_type = fielddictionary.child_guardian_guardian 
    else:
        person_type = fielddictionary.child_beneficiary_ovc
    
    RegPersonsTypes.objects.create(person = reg_person, person_type_id = person_type, date_began = datetime.datetime.now().date(), date_ended = None,  is_void=False)
    something_saved = True
        
def save_ben_guardians(ben, reg_person):
    list_of_guardians = ben.associated_guardians
    if list_of_guardians:
        for ben_data_str in list_of_guardians:
            if not ben_data_str:
                continue
            
            ben_details_list = ben_data_str.split(',')
            
            if not ben_details_list:
                continue
            guard_unit_id = list_provider.get_person_id(ben_details_list[1])
            guard_details = ben_details_list[3]
            
            save_guarian = False
            save_guarian = is_guardian_already_saved(guard_unit_id, reg_person)
            
            if save_guarian:
                RegPersonsGuardians.objects.create(
                                                    child_person = reg_person,
                                                    guardian_person_id = guard_unit_id,
                                                    relationship_notes = guard_details,
                                                    date_linked = datetime.datetime.now().date(),
                                                    date_delinked = None,
                                                    is_void=False
                                                   )
                global something_saved
                something_saved = True



def is_guardian_already_saved(guard_unit_id, reg_person):
    if RegPersonsGuardians.objects.filter(child_person = reg_person,guardian_person_id = guard_unit_id, date_delinked = None,is_void=False).count() > 0:
        return False
    
    return True

def save_beneficiary_location(ben,reg_person, is_guardian, save_location = None):
    residential = ben.residence
    global something_saved
    print 'resdint', residential.res_id
    if save_location.has_key('save_res'):
        if save_location['save_res'] == True:
            if is_guardian == False:
                if residential.res_id:
                    FormResChildren.objects.create(
                                                   child_person_id = ben.pk_id,
                                                   institution_id = int(residential.res_id)                                  
                                                   )
                    #something_saved = True
    
    if save_location.has_key('save_gdclsu'):
        if save_location['save_gdclsu'] == True:
            if residential.community:
                if residential.community > 0:
                    if RegOrgUnit.objects.filter(pk = residential.community):
                        org = RegOrgUnit.objects.get(pk = residential.community, is_void=False)
                        if org:
                            RegPersonsGdclsu.objects.create(person=reg_person, gdclsu=org, date_linked=ben.location_change_date, is_void=False)
                            something_saved = True
    
    if save_location.has_key('save_ward'):
        if save_location['save_ward'] == True:
            if residential.ward:
                RegPersonsGeo.objects.create(person=reg_person, area_id=residential.ward,date_linked=ben.location_change_date, is_void=False)  
                something_saved = True
    

def create_benficiary_object_from_page(cleanpagefields, user, is_guardian):
    
    beneficiary_id = None
    if cleanpagefields.has_key('beneficiary_id'):
        beneficiary_id = cleanpagefields['beneficiary_id']
    
    pk_id = None
    if cleanpagefields.has_key('pk_id'):
        pk_id = cleanpagefields['pk_id']
    
    first_name = None 
    if cleanpagefields.has_key('first_name'):
        first_name = cleanpagefields['first_name']
                                      
    last_name  = None
    if cleanpagefields.has_key('last_name'):
        last_name = cleanpagefields['last_name']
        
    other_name  = None
    if cleanpagefields.has_key('other_name'):
        other_name = cleanpagefields['other_name']
        
    sex_id  = None
    sex = None
    if cleanpagefields.has_key('sex'):
        sex_id = cleanpagefields['sex']
        sex = list_provider.get_item_desc_from_id(sex_id)
        
    date_of_birth = None
    if cleanpagefields.has_key('date_of_birth'):
        date_of_birth = cleanpagefields['date_of_birth']
        
    birth_cert_num  = None
    if cleanpagefields.has_key('birth_cert_num'):
        birth_cert_num = cleanpagefields['birth_cert_num']
        
    steps_ovc_number  = None
    if cleanpagefields.has_key('STEPS_OVC_number'):
        steps_ovc_number = cleanpagefields['STEPS_OVC_number']
     
    date_of_death  = None
    if cleanpagefields.has_key('date_of_death'):
        date_of_death = cleanpagefields['date_of_death']
        
    national_id  = None
    if cleanpagefields.has_key('nrc'):
        national_id = cleanpagefields['nrc']
        
    mobile_phone_number = None
    if cleanpagefields.has_key('mobile_phone_number'):
        mobile_phone_number = cleanpagefields['mobile_phone_number']
        
    email_address = None
    if cleanpagefields.has_key('email_address'):
        email_address = cleanpagefields['email_address']
            
    physical_address = None
    if cleanpagefields.has_key('physical_address'):
        physical_address = cleanpagefields['physical_address']
    
    contact = Contact(mobile_phone_number,email_address,physical_address)
    
    residential_institution = None
    if cleanpagefields.has_key('residential_institution'):
        residential_institution = cleanpagefields['residential_institution']
        
    district = None
    if cleanpagefields.has_key('ben_district'):
        district = cleanpagefields['ben_district']
        
    ward = None
    if cleanpagefields.has_key('ben_ward'):
        ward = cleanpagefields['ben_ward']
        
    community = None
    if cleanpagefields.has_key('community'):
        community = cleanpagefields['community']
    
    res_id = None
    if cleanpagefields.has_key('res_id_hidden'):
        res_id = cleanpagefields['res_id_hidden']
    
    residence = Residence(residential_institution,district,ward,community, res_id)
    
    edit_mode_hidden = None
    if cleanpagefields.has_key('edit_mode_hidden'):
        edit_mode_hidden = cleanpagefields['edit_mode_hidden']
    
    location_change_date = None
    if cleanpagefields.has_key('location_change_date'):
        location_change_date = cleanpagefields['location_change_date']
    if location_change_date == None:
        location_change_date = datetime.datetime.now().date()
    
        
    guardian_change_date = None
    if cleanpagefields.has_key('guardian_change_date'):
        guardian_change_date = cleanpagefields['guardian_change_date']
        
    workforce_member = None
    if cleanpagefields.has_key('workforce_member'):
        workforce_member = cleanpagefields['workforce_member']
       
    date_paper_form_filled = None
    if cleanpagefields.has_key('date_paper_form_filled'):
        date_paper_form_filled = cleanpagefields['date_paper_form_filled']
    
    if is_guardian:
        return OvcChild( 
                    pk_id = pk_id,
                    beneficiary_id = beneficiary_id,
                    first_name = first_name,
                    last_name  = last_name,
                    other_name  = other_name,
                    sex_id  = sex_id,
                    sex = sex,
                    date_of_birth = date_of_birth, 
                    date_of_death = date_of_death,
                    birth_cert_num  = birth_cert_num,
                    steps_ovc_number  = steps_ovc_number,
                    national_id  = national_id,
                    guardian = None,
                    contact = contact,
                    adverse_cond = None,
                    residence = residence,
                    associated_guardians = None,
                    person_type = None,
                    edit_mode_hidden = edit_mode_hidden,
                    location_change_date = location_change_date,
                    guardian_change_date = guardian_change_date,
                    workforce_member = workforce_member,
                    date_paper_form_filled = date_paper_form_filled,
                    audit_trail = None
                )
    
    adverse_cond = []
    if cleanpagefields.has_key('adverse_cond'):
        adverse_cond = cleanpagefields['adverse_cond']
    
    no_guardian = None
    if cleanpagefields.has_key('no_guardian'):
        no_guardian = cleanpagefields['no_guardian']
        
    guard = None
    if cleanpagefields.has_key('guardian'):
        guard = cleanpagefields['guardian']
        
    notes_relationship = None
    if cleanpagefields.has_key('notes_relationship'):
        notes_relationship = cleanpagefields['notes_relationship']
        
    guard_name = None
        
    guardian = Guardian(no_guardian,guard,notes_relationship,guard_name, None)
    
    guardian_hidden = None
    if cleanpagefields.has_key('guardians_hidden'):
        guardian_hidden = cleanpagefields['guardians_hidden']
    
    associated_guardians = None
    
    if guardian_hidden == 'first_time':
        associated_guardians = None
    elif guardian_hidden:
        associated_guardians = extract_guardian_details(guardian_hidden)
    
    ben = OvcChild( 
                    pk_id = None,
                    beneficiary_id = beneficiary_id,
                    first_name = first_name,
                    last_name  = last_name,
                    other_name  = other_name,
                    sex_id  = sex_id,
                    sex = sex,
                    date_of_birth = date_of_birth, 
                    date_of_death = date_of_death,
                    birth_cert_num  = birth_cert_num,
                    steps_ovc_number  = steps_ovc_number,
                    national_id  = national_id,
                    guardian = guardian,
                    contact = contact,
                    adverse_cond = adverse_cond,
                    residence = residence,
                    associated_guardians = associated_guardians,
                    person_type = None,
                    edit_mode_hidden = edit_mode_hidden,
                    location_change_date = location_change_date,
                    guardian_change_date = guardian_change_date,
                    workforce_member = workforce_member,
                    date_paper_form_filled = date_paper_form_filled,
                    audit_trail = None
                )
    
    return ben

def extract_guardian_details(org_data_hidden):
    list_of_items = None
    if org_data_hidden:
        list_of_items = org_data_hidden.split('#')
    
    return list_of_items 

def load_beneficiaries(token = None, ben_type = None, getJSON=False):
    bens = []
    modelben = list_provider.get_beneficiaries(ben_token = token, person_type = ben_type)
      
    for ben in modelben:
        #try:
        beneficiary = load_ben_from_id(ben.pk, False)
        if getJSON:
            beneficiary = ben_json(beneficiary)
            bens.append(beneficiary)
        else:
            bens.append(beneficiary)
        #except Exception as e:
         #   print e
          #  raise Exception('retrieving beneficiary failed')
        
    return bens

def ben_json(beneficiary):
    
    residence = beneficiary.residence
    is_capture_app = settings.IS_CAPTURE_SITE
    district = None
    ward = None
    community = None
    
    if residence:
        if residence.district:
            district = get_geo_name(residence.district)
        if residence.ward:
            ward = get_geo_name(residence.ward)
        if residence.community:
            community = get_cwac_name(residence.community)
            
    
    dob = datetime.datetime.strptime(str(beneficiary.date_of_birth), '%Y-%m-%d')
    
    ben = {'pk_id': beneficiary.pk_id,
           'beneficiary_id': beneficiary.beneficiary_id,
                 'name': beneficiary.name,
                'date_of_birth': dob.strftime('%d-%B-%Y'),
                'sex': beneficiary.sex,
                'person_type': beneficiary.person_type,
                'district': district,
                'ward': ward,
                'community': community,
                'is_capture_app':is_capture_app}
    return ben

def get_geo_name(a_id):
    child_area = SetupGeorgraphy.objects.get(area_id=a_id)
    
    if child_area.parent_area_id:
        parent_area = SetupGeorgraphy.objects.get(area_id=child_area.parent_area_id)
        return parent_area.area_name + ' - ' + child_area.area_name 
    else:
        return child_area.area_name
    
def get_cwac_name(a_id):
    if RegOrgUnit.objects.filter(pk=a_id, date_closed=None, is_void = False).count() > 0:
        child_area = RegOrgUnit.objects.get(pk=a_id, date_closed=None, is_void = False)
        if child_area:
            return child_area.org_unit_id_vis + ' - ' + child_area.org_unit_name

   
def load_ben_from_id(ben_pk, get_values = False,get_dead_bens=False):
    print 'ben_pk',ben_pk,get_values
    if RegPerson.objects.filter(pk=ben_pk).count() == 1:
        tmp_ben = RegPerson.objects.get(pk=ben_pk)
        bene = tmp_ben;
        if tmp_ben:
            if tmp_ben.beneficiary_id == '':
                if tmp_ben.national_id:
                    tmp_ben.beneficiary_id = tmp_ben.national_id
                elif tmp_ben.birth_reg_id:
                    tmp_ben.beneficiary_id = tmp_ben.birth_reg_id
                else:
                    tmp_ben.beneficiary_id = fielddictionary.empty_workforce_id
                        
        org_units = []
        person_type = None
        geos = None
        ward=None
        district = None
        community = None
        contact = None 
        person_type_id = None
        sex = None
        
        if RegPersonsOrgUnits.objects.filter(person=tmp_ben).count() > 0:
            tmp_org_units = RegPersonsOrgUnits.objects.filter(person=tmp_ben)
            if tmp_org_units:
                for org in tmp_org_units:
                    
                    org_model = org.parent_org_unit_id
                    tmp_yes_no = list_provider.get_item_desc_for_order_and_category(org.primary, fielddictionary.gen_YesNo)
                    yes_no_value = ''
                    if tmp_yes_no:
                        yes_no_value = tmp_yes_no[0]
                    org_unit = OrganisationUnit(
                                    org_id = org_model.org_unit_id_vis,
                                    org_name = org_model.org_unit_name,
                                    primary_org = yes_no_value
                                    )
                    
                    org_units.append(org_unit)
            
        if RegPersonsTypes.objects.filter(person=tmp_ben, is_void=False).count() > 0:
            person_type = RegPersonsTypes.objects.get(person=tmp_ben, is_void=False)
        person_type_desc=''
        if person_type:
            person_type_desc = list_provider.get_item_desc_from_id(person_type.person_type_id)
                        
        #RegPersonsExternalIds
        ovc_id = None
        count_persons = RegPersonsExternalIds.objects.filter(person=tmp_ben, identifier_type_id = fielddictionary.steps_ovc_caregiver, is_void=False).count()
        if count_persons > 0:
            tmp_ovc_id = None
            
            if count_persons > 1:
                fil_ext_ids = RegPersonsExternalIds.objects.filter(person=tmp_ben, identifier_type_id = fielddictionary.steps_ovc_caregiver, is_void=False)
                for filter in fil_ext_ids:
                    tmp_ovc_id = filter
            else:
                tmp_ovc_id = RegPersonsExternalIds.objects.get(person=tmp_ben, identifier_type_id = fielddictionary.steps_ovc_caregiver, is_void=False)
                
            if tmp_ovc_id:
                ovc_id = tmp_ovc_id.identifier
                 
        mobile_phone_number = ''
        email_address = ''
        physical_address = ''
        
        if RegPersonsContact.objects.filter(person=tmp_ben, is_void=False).count() > 0:
            designated_phone_tpl = [(l.contact_detail) for l in RegPersonsContact.objects.filter(person=tmp_ben, contact_detail_type_id = fielddictionary.contact_designated_mobile_phone, is_void=False)]
            if(len(designated_phone_tpl) > 0):
                designated_phone = designated_phone_tpl[0]
                
            mobile_phone_tpl = [(l.contact_detail) for l in RegPersonsContact.objects.filter(person=tmp_ben, contact_detail_type_id = fielddictionary.contact_mobile_phone, is_void=False)]
            if(len(mobile_phone_tpl) > 0):
                mobile_phone_number = mobile_phone_tpl[0]
             
            email_address_tpl = [(l.contact_detail) for l in RegPersonsContact.objects.filter(person=tmp_ben, contact_detail_type_id = fielddictionary.contact_email_address, is_void=False)]
            if(len(email_address_tpl) > 0):
                email_address = email_address_tpl[0]
                
            physical_address_tpl = [(l.contact_detail) for l in RegPersonsContact.objects.filter(person=tmp_ben, contact_detail_type_id = fielddictionary.contact_physical_address, is_void=False)]
            if(len(physical_address_tpl) > 0):
                physical_address = physical_address_tpl[0]
        
        contact = Contact(mobile_phone_number,email_address,physical_address)
        
        dist = None
        wrd = None
        comm = None
        loc_date_linked = None
        geos = {}
        if RegPersonsGeo.objects.filter(person=tmp_ben, date_delinked = None, is_void=False).count() > 0:
            m_ben_geo = RegPersonsGeo.objects.filter(person=tmp_ben, date_delinked = None, is_void=False)
            
            for geo in m_ben_geo:
                areainfo = SetupGeorgraphy.objects.get(area_id=geo.area_id)
                loc_date_linked = geo.date_linked
                if areainfo.area_type_id == fielddictionary.geo_type_ward_code:
                    ward = areainfo.area_id
                    wrd = areainfo.area_id
                    
                    parent = areainfo.parent_area_id
                    parent_geo = SetupGeorgraphy.objects.get(area_id=parent)
                    while (fielddictionary.geo_type_district_code != parent_geo.area_type_id):
                        parent_geo = SetupGeorgraphy.objects.get(area_id=parent_geo.parent_area_id)
                    
                    parent_geo.area_type_id = fielddictionary.geo_type_district_code    
                    district = parent_geo.area_id
                    dist = parent_geo.area_id
                    
                    #Todo: Use dictionary to get both name and id
                    #districts = {}
                    
                    #districts[parent_geo.area_id] = parent_geo.area_name
        
        communties = None
        if RegPersonsGdclsu.objects.filter(person=tmp_ben, date_delinked=None, is_void=False).count() > 0:
            communities = RegPersonsGdclsu.objects.filter(person=tmp_ben, date_delinked = None , is_void=False)
            
            glu = None
            for com in communities:
                glu = com
            if glu:
                loc_date_linked = glu.date_linked
                if RegOrgUnit.objects.filter(pk = glu.gdclsu_id, is_void=False).count() > 0:
                    org_unit = RegOrgUnit.objects.get(pk = glu.gdclsu_id, is_void=False)
                    community = org_unit.pk
                    comm = org_unit.pk
            
        if get_values:
            if district:
                district = get_geo_name(district)
            if ward:
                ward = get_geo_name(ward)
            if community:
                community = get_cwac_name(community)
            
            
        res_ins = FormResChildren.objects.filter(child_person_id = tmp_ben.pk)
        resdnt = None
        if res_ins:
            for res in res_ins:
                resdnt = int(res.institution_id)
        
        regOrg = None
        if resdnt:
            regOrg = RegOrgUnit.objects.filter(pk=int(resdnt), is_void=False)
        
        org = None
        if regOrg:
            for r_org in regOrg:
                org = r_org
        
        
        res_inst = None
        res_id = None
        if org:
            res_inst = org.org_unit_id_vis + ' ' + org.org_unit_name + ',' + str(org.pk) + ','+ str(dist) +','+ str(wrd) +','+ str(comm)
            res_id = org.pk
        
        
        residence = Residence(res_inst,district,ward,community, res_id)
        
        sex = list_provider.get_item_desc_from_id(tmp_ben.sex_id)
        
        guardian = []
        
        if RegPersonsGuardians.objects.filter(child_person=bene, is_void=False).count() > 0:
            guardi = RegPersonsGuardians.objects.filter(child_person=bene, is_void=False)
            
            for grd in guardi:
                print 'g_p_id:',grd.guardian_person_id
                if RegPerson.objects.filter(pk=grd.guardian_person_id, is_void=False).count() == 1:
                    person = RegPerson.objects.get(pk=grd.guardian_person_id, is_void=False)
                    guard_pers_id_str = list_provider.get_person_id(grd.guardian_person_id,True)
                    guardian.append(Guardian(no_guardian='',guardian=guard_pers_id_str,notes_relationship=grd.relationship_notes, guard_name = person.full_name, datelinked = grd.date_linked))
                else:
                    print 'something went wrong when loading guardians in beneficiary_registraion.load_ben_from_id, found more than one guardian'
        
        workforce_member = None
        date_paper_form_filled = None
        
        audit_trail = []
        
        audit = RegPersonsAuditTrail.objects.filter(person_id=bene.pk)
        for aud in audit:
            person_rec_paper = get_person_name_id(aud.person_id_recorded_paper)
            person_rec_elec = ''
            if AppUser.objects.filter(pk=aud.app_user.pk).count() == 1:
                app_user = AppUser.objects.get(pk=aud.app_user.pk)
                if app_user.is_superuser:
                    person_rec_elec = 'Super User'
                else:
                    person_rec_elec = '%s, %s'% (app_user.reg_person.workforce_id,app_user.reg_person.full_name)
            
            interface = get_interface_name(aud.interface_id)
                        
            audit_trail.append(AuditTrail(
                                          Person_recorded_on_paper = person_rec_paper,
                                          Paper_date=aud.date_recorded_paper, 
                                          Person_recorded_electronically=person_rec_elec, 
                                          Electronic_date_time = aud.timestamp_modified, 
                                          Interface = interface
                                          )
                               )
        
        ben = OvcChild( 
                        pk_id = tmp_ben.pk,
                        beneficiary_id = tmp_ben.beneficiary_id,
                        first_name = tmp_ben.first_name,
                        last_name  = tmp_ben.surname,
                        other_name  = tmp_ben.other_names,
                        sex_id  = tmp_ben.sex_id,
                        sex = sex,
                        date_of_birth = tmp_ben.date_of_birth,#tmp_ben.date_of_birth,
                        date_of_death = tmp_ben.date_of_death,
                        birth_cert_num  = tmp_ben.birth_reg_id,
                        steps_ovc_number  = ovc_id,
                        national_id  = tmp_ben.national_id,
                        person_type = person_type_desc,
                        guardian = guardian,
                        contact = contact,
                        adverse_cond = None,
                        residence = residence,
                        associated_guardians = None,
                        edit_mode_hidden = None,
                        location_change_date = loc_date_linked,
                        guardian_change_date = None,
                        workforce_member = workforce_member,
                        date_paper_form_filled = date_paper_form_filled,
                        audit_trail = audit_trail
                      )
        ben.id_int = tmp_ben.pk
        
        return ben    
            
    else:
        raise Exception('Beneficiary with the ID passsed does not exists')

def get_interface_name(interface_id):
    interf = None
    if interface_id:
        if SetupList.objects.filter(item_id=interface_id).count()>0:
            interf = SetupList.objects.get(item_id=interface_id).item_description
    return interf
    
def get_person_name_id(pers_id):
    workforce_member = None
    if RegPerson.objects.filter(pk=pers_id, is_void=False).count()>0:
        reg_person = RegPerson.objects.get( pk=pers_id, is_void=False)
        if reg_person:
            if reg_person.workforce_id and reg_person.full_name:
                workforce_member = reg_person.workforce_id + ', ' + reg_person.full_name
            elif reg_person.beneficiary_id and reg_person.full_name:
                workforce_member = reg_person.beneficiary_id + ', ' + reg_person.full_name
            elif reg_person.beneficiary_id:
                workforce_member = reg_person.beneficiary_id
            elif reg_person.workforce_id:
                workforce_member = reg_person.workforce_id
            elif reg_person.full_name:
                workforce_member = reg_person.full_name
                
    return workforce_member

@transaction.commit_on_success 
def update_ben(cleanpagefields, person_id_pk, user, page_source,is_selective_update=None):
    is_guardian = False
    if page_source == 'guardian' or page_source == 'OVC guardian':
         is_guardian = True
    else:
        is_guardian = False
    
    #create beneficiary object from page fields
    ben  = create_benficiary_object_from_page(cleanpagefields, user, is_guardian)
    print 'dob before', ben.date_of_birth
    ben.pk_id = person_id_pk
    if is_selective_update:
        saved_ben = load_ben_from_id(ben.pk_id)
        delta_fields_to_update = auth_access_control_util.filter_fields_for_display(user, permission_type_fields)
        ben = auth_access_control_util.update_object(object_to_update=saved_ben, objects_with_updates=ben, fields_to_update=delta_fields_to_update)
    
    old_reg_person = RegPerson.objects.get(pk=ben.pk_id,is_void=False)
    
    if old_reg_person:
        if ben.edit_mode_hidden == '1':
            print 'Updating beneficiary...'
            if void_and_save_old_person(old_reg_person, ben):
                ben.beneficiary_id = old_reg_person.beneficiary_id
                reg_person = old_reg_person
            else:
                reg_person = old_reg_person
            print 'something saved after ben', something_saved
        
            print 'Updating contacts...'
            save_contacts = void_old_Contacts(old_reg_person, ben)
            if save_contacts:
                save_contact_details(ben,reg_person, save_contacts)
            print 'something saved after contact', something_saved
            
            print 'Updating where the beneficiary lives'
            save_location = void_old_location(ben,old_reg_person)
            if save_location:
                save_beneficiary_location(ben,reg_person, False, save_location)#For now we pass in false as so that we don't update the residential institution bit
            print 'something saved after location', something_saved
                     
            '''#update on person type makes no sense
            print 'Updating child\'s person type...'
            if void_old_person_type(old_reg_person, is_guardian):
                save_persons_type(reg_person, is_guardian)
            '''
                
        elif ben.edit_mode_hidden == '2':
            print 'death'
            record_persons_death(ben,old_reg_person)
            
        elif ben.edit_mode_hidden == '3':
            print 'deactivation'
            record_persons_deactivation(ben,old_reg_person)
        
        print 'something saved after death and deactivation', something_saved  
        #End the execution here if saving guardian and not OVC child
        if is_guardian:
            print 'log audit to trail'
            save_audit_trail(ben,user,old_reg_person,fielddictionary.update_beneficiary)
            return
        
    if old_reg_person:
        if ben.edit_mode_hidden == '1':
            print 'Updating external Ids...'  
            if void_old_ex_ids(old_reg_person, ben):
                save_persons_external_ids(ben, old_reg_person)
            print 'something saved after ext id', something_saved
            
            print 'Updating ovc child\'s guardians'
            if void_old_guardians(old_reg_person, ben):
                save_ben_guardians(ben, old_reg_person)
            print 'something saved after guardians', something_saved
            
    print 'log audit to trail'
    save_audit_trail(ben,user,old_reg_person,fielddictionary.update_beneficiary)
    
def get_wfc_pk_from_wfc_id(wfc_id):
    if wfc_id:
        reg_ps = RegPerson.objects.filter(workforce_id=wfc_id, is_void=False)
        reg_p = None
        for reg_s in reg_ps:
            if reg_s:
                reg_p = reg_s            
        if reg_p:
            return reg_p.pk
        else:
            return None
    else:
        return None
    
            
def get_workforce_id(workforce_member):
    if workforce_member:
        wfc_mem = workforce_member.split(',')
        if wfc_mem:
            if wfc_mem[0]:
                return wfc_mem[0]
    return None

def void_old_person_type(old_reg_person, is_guardian):
    person_type = None
    
    try:
        if is_guardian:
            person_type = fielddictionary.child_guardian_guardian 
        else:
            person_type = fielddictionary.child_beneficiary_ovc
            
        if RegPersonsTypes.objects.filter(person = old_reg_person, person_type_id = person_type, is_void=False).count() > 0:
            person_type = RegPersonsTypes.objects.get(person = old_reg_person, person_type_id = person_type, is_void=False)
            person_type.is_void=True
            person_type.save() 
            
            return True
            
    except Exception as e:
            print e
            raise Exception('void on beneficiary type failed')
    
    return False
        

def record_persons_death(ben,reg_person):
    try:
        reg_person.date_of_death = ben.date_of_death
        reg_person.save()
        global something_saved
        something_saved = True
    except Exception as e:
            print e
            raise Exception('record on beneficiary death failed')
    
    return True

def record_persons_deactivation(wfc,reg_person):
    try:
        reg_person.is_void=True
        reg_person.save()
        global something_saved
        somthing_saved = True
        
    except Exception as e:
            print e
            raise Exception('record on beneficiary deactivation failed')
    
    return True
    
    
def void_and_save_old_person(old_reg_person,ben):
    try:
        global something_saved
        
        if old_reg_person.national_id != ben.national_id:
            old_reg_person.national_id=ben.national_id
            something_saved = True
        
        if old_reg_person.first_name != ben.first_name:
            old_reg_person.first_name=ben.first_name
            something_saved = True
            
        if old_reg_person.other_names != ben.other_name:
            old_reg_person.other_names = ben.other_name
            something_saved = True
            
        if old_reg_person.surname != ben.last_name:
            old_reg_person.surname=ben.last_name
            something_saved = True
        
        if old_reg_person.date_of_birth != ben.date_of_birth:
            old_reg_person.date_of_birth=ben.date_of_birth
            something_saved = True
        
        if old_reg_person.date_of_death != ben.date_of_death:
            old_reg_person.date_of_death=ben.date_of_death
            something_saved = True
            
        if old_reg_person.sex_id != ben.sex_id:
            old_reg_person.sex_id=ben.sex_id
            something_saved = True
            
        if old_reg_person.birth_reg_id != ben.birth_cert_num:
            old_reg_person.birth_reg_id=ben.birth_cert_num
            something_saved = True
        
        if something_saved:
            old_reg_person.save()
            return True
            
    except Exception as e:
            print e
            raise Exception('void on beneficiary failed')
    
    return False

def void_old_Contacts(old_reg_person, ben):
    
    save_contact = {'save_mobile': False,'save_email': False,'save_address': False}
    
    try: 
        if RegPersonsContact.objects.filter(person = old_reg_person, contact_detail_type_id = fielddictionary.contact_mobile_phone, is_void=False).count()>0:
            mobile_rec = RegPersonsContact.objects.filter(person = old_reg_person, contact_detail_type_id = fielddictionary.contact_mobile_phone, is_void=False)
            for mobile in mobile_rec:
                if ben.contact.mobile_phone_number == mobile.contact_detail:
                    continue
                else:
                    mobile.is_void=True
                    mobile.save()
                    if save_contact.has_key('save_mobile'):
                        save_contact['save_mobile'] = True
        else:
            if save_contact.has_key('save_mobile'):
                    save_contact['save_mobile'] = True
                
        if RegPersonsContact.objects.filter(person = old_reg_person, contact_detail_type_id = fielddictionary.contact_email_address, is_void=False).count()>0:
            email_rec = RegPersonsContact.objects.filter(person = old_reg_person, contact_detail_type_id = fielddictionary.contact_email_address, is_void=False)
            for email in email_rec:
                if ben.contact.email_address == email.contact_detail:
                    continue
                else:
                    email.is_void=True
                    email.save()
                    if save_contact.has_key('save_email'):
                        save_contact['save_email'] = True
        else:
            if save_contact.has_key('save_email'):
                    save_contact['save_email'] = True
                
        if RegPersonsContact.objects.filter(person = old_reg_person, contact_detail_type_id = fielddictionary.contact_physical_address, is_void=False).count()>0:
            address_rec = RegPersonsContact.objects.filter(person = old_reg_person, contact_detail_type_id = fielddictionary.contact_physical_address, is_void=False)
            for address in address_rec:
                if ben.contact.physical_address == address.contact_detail:
                    continue
                else:
                    address.is_void=True
                    address.save()
                    if save_contact.has_key('save_address'):
                        save_contact['save_address'] = True
        else:
            if save_contact.has_key('save_address'):
                    save_contact['save_address'] = True
                    
        print 'save_contact: ', save_contact
                
    except Exception as e:
            print e
            raise Exception('void on contacts failed')
    
    return save_contact

def void_old_location(ben,old_reg_person):    
    save_location = {'save_gdclsu': False,'save_ward': False, 'save_res':False}
    ward_voided = False
    cwac_voided = False
    save_res_ins = True
         
    try:
        if RegPersonsGdclsu.objects.filter(person=old_reg_person, is_void=False).count() > 0:
            gdclsu_rec = RegPersonsGdclsu.objects.filter(person=old_reg_person, is_void=False)
            for gdclsu in gdclsu_rec:
                if int(gdclsu.gdclsu_id) == int(ben.residence.community):
                    continue
                else:
                    gdclsu.is_void=True
                    gdclsu.date_delinked=ben.location_change_date
                    gdclsu.save()
                    cwac_voided = True
                    
        elif ben.residence.community:
            cwac_voided = True
            
        if RegPersonsGeo.objects.filter(person=old_reg_person, is_void=False).count() > 0:
            geo_rec = RegPersonsGeo.objects.filter(person=old_reg_person, is_void=False)
            for geo in geo_rec:
                if int(geo.area_id) == int(ben.residence.ward):
                    continue
                else:
                    geo.is_void=True
                    geo.date_delinked=ben.location_change_date
                    geo.save()
                    ward_voided = True
                
        elif ben.residence.community:
            ward_voided = True
        
        if FormResChildren.objects.filter(child_person_id = ben.pk_id).count() > 0:
            resd_ins = FormResChildren.objects.filter(child_person_id = ben.pk_id)
            for resd_in in resd_ins:
                if resd_in.institution_id == int(ben.residence.res_id):
                    save_res_ins = False
                
        if save_location.has_key('save_gdclsu'):
            save_location['save_gdclsu'] = cwac_voided
            
        if save_location.has_key('save_ward'):
            save_location['save_ward'] = ward_voided
        
        if save_location.has_key('save_res'):
            save_location['save_res'] = save_res_ins
             
        print 'save location:', save_location
        
    except Exception as e:
        print e
        raise Exception('void on location failed')
    
    return save_location
    
def void_old_ex_ids(old_reg_person, ben):
    made_void = False
    
    try:
        if RegPersonsExternalIds.objects.filter(person=old_reg_person, identifier_type_id = fielddictionary.steps_ovc_caregiver, is_void=False).count() > 0:
            steps_ovc_recs = RegPersonsExternalIds.objects.filter(person=old_reg_person, identifier_type_id = fielddictionary.steps_ovc_caregiver, is_void=False)
            for steps_ovc_rec in steps_ovc_recs:
                if steps_ovc_rec.identifier == ben.steps_ovc_number:
                    made_void = False
                else:
                    steps_ovc_rec.is_void=True
                    steps_ovc_rec.save()
                    made_void = True
        else:
            made_void = True
            
    except Exception as e:
        print e
        raise Exception('void on external ids failed')
    
    return made_void

def void_old_guardians(old_reg_person, ben):
    guardian_voided = False
    try:
        guardians = RegPersonsGuardians.objects.filter(
                                            child_person = old_reg_person,
                                            date_delinked = None,
                                            is_void=False
                                           )
        if guardians:
            for guard in guardians:
                if void_guardian(ben, guard.guardian_person_id):
                    guard.date_delinked = ben.guardian_change_date
                    guard.is_void=True
                    guard.save()
                    guardian_voided = True
                else:
                    continue
        else:
            guardian_voided = True
                
    except Exception as e:
        print e
        raise Exception('void on guardians ids failed')
    
    return guardian_voided

def void_guardian(ben, val_to_void):
    list_of_guardians = ben.associated_guardians
    if list_of_guardians:
        for ben_data_str in list_of_guardians:
            if not ben_data_str:
                continue
            
            ben_details_list = ben_data_str.split(',')
            if not ben_details_list:
                continue
            
            guard_unit_id = list_provider.get_person_id(ben_details_list[1])
            guard_details = ben_details_list[3]
            
            if guard_unit_id == val_to_void:
                return False
            
    return True
    
def save_audit_trail(ben,user,reg_person,trans_type):
    if something_saved:
        wfc_id = get_workforce_id(ben.workforce_member)
        wfc_pk = None
        if wfc_id:
            wfc_pk = wfc_id if wfc_id.isnumeric() else None   #get_wfc_pk_from_wfc_id(wfc_id)
        wfc_elec_pk = get_wfc_pk_from_wfc_id(user.workforce_id)
        logger.log(__name__,reg_person,trans_type,fielddictionary.web_interface_id, ben.date_paper_form_filled, wfc_pk, user)
        global something_saved
        something_saved = False

    