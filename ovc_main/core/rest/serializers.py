'''
    @glyoko This module contains classes that are used for serialising data prior to
    an upload/download operation, checking validity and saving of uploaded/downloaded 
    data.
'''

from django.forms import widgets
from rest_framework import serializers
from ovc_main.models import *
from ovc_auth.models import *
from django.contrib.auth.models import Group, Permission
from django.db import transaction
from django.core.serializers.json import Serializer
import json
import psycopg2
from django.conf import settings
import traceback

'''
    REST SERIALISERS
    
    This section contains the serialisers built using the django rest framework.
    Each of them have an associated django model.
'''
class RegPersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPerson
        fields = (  
                    'id',
                    'beneficiary_id',
                    'workforce_id',
                    'birth_reg_id',
                    'national_id',
                    'first_name',
                    'other_names',
                    'surname',
                    'date_of_birth',
                    'date_of_death',
                    'sex_id',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        person = None
        if len(RegPerson.objects.filter(pk=id)) > 0:
            person = RegPerson.objects.get(pk=id)
        else:
            person = RegPerson()
        person.id=id
        person.beneficiary_id=validated_data['beneficiary_id'] if validated_data['beneficiary_id'] else None
        person.workforce_id=validated_data['workforce_id'] if validated_data['workforce_id'] else None
        person.birth_reg_id=validated_data['birth_reg_id'] if validated_data['birth_reg_id'] else None
        person.national_id=validated_data['national_id'] if validated_data['national_id'] else None
        person.first_name=validated_data['first_name'] 
        person.other_names=validated_data['other_names'] if validated_data['other_names'] else None
        person.surname=validated_data['surname'] 
        person.date_of_birth=validated_data['date_of_birth'] 
        person.date_of_death=validated_data['date_of_death'] if validated_data['date_of_death'] else None
        person.sex_id=validated_data['sex_id'] 
        person.is_void=validated_data['is_void'] 
        
        if person:
            person.save()
        return person

class RegPersonsGeoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPersonsGeo
        fields = (  
                    'id',
                    'person',
                    'area_id',
                    'date_linked',
                    'date_delinked',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        person_geo = None
        if len(RegPersonsGeo.objects.filter(pk=id)) > 0:
            person_geo = RegPersonsGeo.objects.get(pk=id)
        else:
            person_geo = RegPersonsGeo()
        person_geo.id=id
        person_geo.person=validated_data['person'] 
        person_geo.area_id=validated_data['area_id']
        person_geo.date_linked=validated_data['date_linked'] if validated_data['date_linked'] else None
        person_geo.date_delinked=validated_data['date_delinked'] if validated_data['date_delinked'] else None
        person_geo.is_void=validated_data['is_void']
        
        if person_geo:
            person_geo.save()
        return person_geo

class RegPersonsGdclsuSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPersonsGdclsu
        fields = (  
                    'id',
                    'person',
                    'gdclsu',
                    'date_linked',
                    'date_delinked',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        person_gdclsu = None
        if len(RegPersonsGdclsu.objects.filter(pk=id)) > 0:
            person_gdclsu = RegPersonsGdclsu.objects.get(pk=id)
        else:
            person_gdclsu = RegPersonsGdclsu()
        person_gdclsu.id=id
        person_gdclsu.person=validated_data['person'] 
        person_gdclsu.gdclsu=validated_data['gdclsu']
        person_gdclsu.date_linked=validated_data['date_linked'] if validated_data['date_linked'] else None
        person_gdclsu.date_delinked=validated_data['date_delinked'] if validated_data['date_delinked'] else None
        person_gdclsu.is_void=validated_data['is_void'] 
        
        if person_gdclsu:
            person_gdclsu.save()
        return person_gdclsu
                  
class RegPersonsOrgUnitsSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPersonsOrgUnits
        fields = (  
                    'id',
                    'person',
                    'parent_org_unit',
                    'primary',
                    'date_linked',
                    'date_delinked',
                    'is_void'
                  )

    def save(self, validated_data, id):
        person_orgs = None
        if len(RegPersonsOrgUnits.objects.filter(pk=id)) > 0:
            person_orgs = RegPersonsOrgUnits.objects.get(pk=id)
        else:
            person_orgs = RegPersonsOrgUnits()
        person_orgs.id=id
        person_orgs.person=validated_data['person'] 
        person_orgs.parent_org_unit=validated_data['parent_org_unit'] 
        person_orgs.primary=validated_data['primary'] 
        person_orgs.date_linked=validated_data['date_linked'] if validated_data['date_linked'] else None
        person_orgs.date_delinked=validated_data['date_delinked'] if validated_data['date_delinked'] else None
        person_orgs.is_void=validated_data['is_void']
        
        if person_orgs:
            person_orgs.save()
        return person_orgs
    
class RegPersonsTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPersonsTypes
        fields = (  
                    'id',
                    'person',
                    'person_type_id',
                    'date_began',
                    'date_ended',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        person_types = None
        if len(RegPersonsTypes.objects.filter(pk=id)) > 0:
            person_types = RegPersonsTypes.objects.get(pk=id)
        else:
            person_types = RegPersonsTypes()
        person_types.id=id
        person_types.person=validated_data['person'] 
        person_types.person_type_id=validated_data['person_type_id'] 
        person_types.date_began=validated_data['date_began'] if validated_data['date_began'] else None
        person_types.date_ended=validated_data['date_ended'] if validated_data['date_ended'] else None
        person_types.is_void=validated_data['is_void']
        
        if person_types:
            person_types.save()
        return person_types
                  
class RegOrgUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegOrgUnit
        fields = (  
                    'id',
                    'org_unit_id_vis',
                    'org_unit_name',
                    'org_unit_type_id',
                    'is_gdclsu',
                    'date_operational',
                    'date_closed',
                    'parent_org_unit_id',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        org_unit = None
        if len(RegOrgUnit.objects.filter(pk=id)) > 0:
            org_unit = RegOrgUnit.objects.get(pk=id)
        else:
            org_unit = RegOrgUnit()
        org_unit.id=id
        org_unit.org_unit_id_vis=validated_data['org_unit_id_vis'] 
        org_unit.org_unit_name=validated_data['org_unit_name'] 
        org_unit.org_unit_type_id=validated_data['org_unit_type_id'] 
        org_unit.is_gdclsu=validated_data['is_gdclsu'] 
        org_unit.date_operational=validated_data['date_operational'] if validated_data['date_operational'] else None
        org_unit.date_closed=validated_data['date_closed'] if validated_data['date_closed'] else None
        org_unit.parent_org_unit_id=validated_data['parent_org_unit_id'] if validated_data['parent_org_unit_id'] else None
        org_unit.is_void=validated_data['is_void']
        
        if org_unit:
            org_unit.save()
        return org_unit
    
class RegOrgUnitGeoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegOrgUnitGeography
        fields = (  
                    'id',
                    'org_unit',
                    'area_id',
                    'date_linked',
                    'date_delinked',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        org_unit_geo = None
        if len(RegOrgUnitGeography.objects.filter(pk=id)) > 0:
            org_unit_geo = RegOrgUnitGeography.objects.get(pk=id)
        else:
            org_unit_geo = RegOrgUnitGeography()
        org_unit_geo.id=id
        org_unit_geo.org_unit=validated_data['org_unit'] 
        org_unit_geo.area_id=validated_data['area_id'] 
        org_unit_geo.date_linked=validated_data['date_linked'] if validated_data['date_linked'] else None
        org_unit_geo.date_delinked=validated_data['date_delinked'] if validated_data['date_delinked'] else None
        org_unit_geo.is_void=validated_data['is_void']
        
        if org_unit_geo:
            org_unit_geo.save()
        return org_unit_geo
    
class SetupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SetupList
        fields = (  
                    'id',
                    'item_id',
                    'item_description',
                    'item_description_short',
                    'item_category',
                    'the_order',
                    'user_configurable',
                    'sms_keyword',
                    'field_name',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        setup_list = None
        if len(SetupList.objects.filter(pk=id)) > 0:
            setup_list = SetupList.objects.get(pk=id)
        else:
            setup_list = SetupList()
        setup_list.id=id
        setup_list.item_id=validated_data['item_id'] 
        setup_list.item_description=validated_data['item_description'] 
        setup_list.item_description_short=validated_data['item_description_short'] if validated_data['item_description_short'] else None
        setup_list.item_category=validated_data['item_category'] if validated_data['item_category'] else None
        setup_list.the_order=validated_data['the_order'] if validated_data['the_order'] else None
        setup_list.user_configurable=validated_data['user_configurable']
        setup_list.sms_keyword=validated_data['sms_keyword'] 
        setup_list.field_name=validated_data['field_name'] if validated_data['field_name'] else None
        setup_list.is_void=validated_data['is_void']
        
        if setup_list:
            setup_list.save()
        return setup_list

class SetupGeoSerializer(serializers.ModelSerializer):
    class Meta:
        model = SetupGeorgraphy
        fields = (  
                    'id',
                    'area_id',
                    'area_type_id',
                    'area_name',
                    'parent_area_id',
                    'area_name_abbr',
                    'timestamp_created',
                    'timestamp_updated',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        setup_geo = None
        if len(SetupGeorgraphy.objects.filter(pk=id)) > 0:
            setup_geo = SetupGeorgraphy.objects.get(pk=id)
        else:
            setup_geo = SetupGeorgraphy()
        setup_geo.id=id
        setup_geo.area_id=validated_data['area_id'] 
        setup_geo.area_type_id=validated_data['area_type_id'] 
        setup_geo.area_name=validated_data['area_name'] 
        setup_geo.parent_area_id=validated_data['parent_area_id'] if validated_data['parent_area_id'] else None
        setup_geo.area_name_abbr=validated_data['area_name_abbr'] if validated_data['area_name_abbr'] else None
        setup_geo.is_void=validated_data['is_void']
        
        if setup_geo:
            setup_geo.save()
        return setup_geo

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUser
        fields = (  
                    'id',
                    'reg_person',
                    'workforce_id',
                    'password',
                    'national_id',
                    'first_name',
                    'last_name',
                    'designated_phone_number',
                    'is_staff',
                    'is_active',
                    'date_joined',
                    'timestamp_created',
                    'timestamp_updated',
                    'password_changed_timestamp'
                  )
                  
    def save(self, validated_data, id):
        '''
        Saves the downloaded users in the capture application
        '''
        user = None
        if len(AppUser.objects.filter(pk=id)) > 0:
            user = AppUser.objects.get(pk=id)
        else:
            user = AppUser()
        user.id=id
        user.reg_person=validated_data['reg_person'] if validated_data['reg_person'] else None
        user.workforce_id=validated_data['workforce_id'] #if validated_data['workforce_id'] else None
        user.national_id=validated_data['national_id'] #if validated_data['national_id'] else None
        user.password=validated_data['password'] #if validated_data['password'] else None
        user.first_name=validated_data['first_name'] #if validated_data['first_name'] else None
        user.last_name=validated_data['last_name'] #if validated_data['last_name'] else None
        user.designated_phone_number=validated_data['designated_phone_number'] #if validated_data['designated_phone_number'] else None
        user.is_staff=validated_data['is_staff'] 
        user.is_active=validated_data['is_active'] 

        if user:
            user.save()
        return user
    
    @transaction.commit_on_success
    def is_valid(self, id):
        try:
            if len(AppUser.objects.filter(pk=id)) > 0:
                AppUser.objects.filter(pk=id).delete()
            return super(UsersSerializer,self).is_valid()
        except Exception as e:
            traceback.print_exc()
            raise Exception('Cannot validate the user')

class ListQuestionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListQuestions
        fields = (  
                    'id',
                    'question_text',
                    'question_code',
                    'form_type_id',
                    'answer_type_id',
                    'answer_set_id',
                    'the_order',
                    'timestamp_modified',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        question = None
        if len(ListQuestions.objects.filter(pk=id)) > 0:
            question = ListQuestions.objects.get(pk=id)
        else:
            question = ListQuestions()
        question.id=id
        question.question_text=validated_data['question_text'] if validated_data['question_text'] else None
        question.question_code=validated_data['question_code'] 
        question.form_type_id=validated_data['form_type_id'] if validated_data['form_type_id'] else None
        question.answer_type_id=validated_data['answer_type_id'] if validated_data['answer_type_id'] else None
        question.answer_set_id=validated_data['answer_set_id'] if validated_data['answer_set_id'] else None
        question.the_order=validated_data['the_order'] if validated_data['the_order'] else None
        question.is_void=validated_data['is_void']
        
        if question:
            question.save()
        return question

    
class ListAnswersSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListAnswers
        fields = (  
                    'id',
                    'answer_set_id',
                    'answer',
                    'the_order',
                    'timestamp_modified',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        answer = None
        if len(ListAnswers.objects.filter(pk=id)) > 0:
            answer = ListAnswers.objects.get(pk=id)
        else:
            answer = ListAnswers()
        answer.id=id
        answer.answer_set_id=validated_data['answer_set_id'] if validated_data['answer_set_id'] else None
        answer.answer=validated_data['answer'] if validated_data['answer'] else None
        answer.the_order=validated_data['the_order'] if validated_data['the_order'] else None
        answer.is_void=validated_data['is_void']
        
        if answer:
            answer.save()
        return answer
  
class FormsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Forms
        fields = (  
                    'form_guid',
                    'form_title',
                    'form_type_id',
                    'form_subject_id',
                    'form_area_id',
                    'date_began',
                    'date_ended',
                    'date_filled_paper',
                    'person_id_filled_paper',
                    'org_unit_id_filled_paper',
                    'capture_site_id',
                    'timestamp_created',
                    'user_id_created',
                    'timestamp_updated',
                    'user_id_updated',
                    'is_void'
                  )

    def save(self, validated_data, form_guid, parent_model_class):
        form = None
        if Forms.objects.filter(form_guid=form_guid).count() > 0:
            form = Forms.objects.get(form_guid=form_guid)
        else:
            form = Forms()
        form.form_guid=validated_data['form_guid'] 
        form.form_title=validated_data['form_title'] if validated_data['form_title'] else None
        form.form_type_id=validated_data['form_type_id'] if validated_data['form_type_id'] else None
        form.form_subject_id=validated_data['form_subject_id'] if validated_data['form_subject_id'] else None
        form.form_area_id=validated_data['form_area_id'] if validated_data['form_area_id'] else None
        form.date_began=validated_data['date_began'] if validated_data['date_began'] else None
        form.date_ended=validated_data['date_ended'] if validated_data['date_ended'] else None
        form.date_filled_paper=validated_data['date_filled_paper'] if validated_data['date_filled_paper'] else None
        form.person_id_filled_paper=validated_data['person_id_filled_paper'] if validated_data['person_id_filled_paper'] else None
        form.org_unit_id_filled_paper=validated_data['org_unit_id_filled_paper'] if validated_data['org_unit_id_filled_paper'] else None
        form.capture_site_id=validated_data['capture_site_id'] if validated_data['capture_site_id'] else None
        form.timestamp_created=validated_data['timestamp_created'] if validated_data['timestamp_created'] else None
        form.user_id_created=validated_data['user_id_created'] if validated_data['user_id_created'] else None
        form.timestamp_updated=validated_data['timestamp_updated'] if validated_data['timestamp_updated'] else None
        form.user_id_updated=validated_data['user_id_updated'] if validated_data['user_id_updated'] else None
        form.is_void=validated_data['is_void']
        
        if form:
            form.save()
        return form
    
class FormGenAnswersSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormGenAnswers
        fields = (  
                    #'form',
                    'question',
                    'answer'
                  )
                  
    def save(self, validated_data, form_guid, parent_model_class):
        m_parent = None
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
        if m_parent:
            gen_ans = FormGenAnswers()
            gen_ans.form=m_parent
            gen_ans.question=validated_data['question'] 
            gen_ans.answer=validated_data['answer'] if validated_data['answer'] else None
            gen_ans.save()
        return gen_ans
                  
class FormGenTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormGenText
        fields = (  
                    #'form',
                    'question',
                    'answer_text'
                  )
                  
    def save(self, validated_data, form_guid, parent_model_class):
        m_parent = None
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
        if m_parent:
            gen_text = FormGenText()
            gen_text.form=m_parent
            gen_text.question=validated_data['question'] 
            gen_text.answer_text=validated_data['answer_text'] if validated_data['answer_text'] else None
            gen_text.save()
        return gen_text
                  
class FormGenNumSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormGenNumeric
        fields = (  
                    #'form',
                    'question',
                    'answer'
                  )

    def save(self, validated_data, form_guid, parent_model_class):
        m_parent = None
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
        if m_parent:
            gen_num = FormGenNumeric()
            gen_num.form=m_parent
            gen_num.question=validated_data['question'] 
            gen_num.answer=validated_data['answer'] if validated_data['question'] else None
            gen_num.save()
        return gen_num
    
class FormCsiSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormCsi
        fields = (  
                    #'form',
                    'domain_id',
                    'score',
                    'observations'
                  )
    def save(self, validated_data, form_guid, parent_model_class):
        m_parent = None
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
        if m_parent:
            form_csi = FormCsi()
            form_csi.form=m_parent
            form_csi.domain_id=validated_data['domain_id'] if validated_data['domain_id'] else None
            form_csi.score=validated_data['score'] if validated_data['score'] else None
            form_csi.observations=validated_data['observations'] if validated_data['observations'] else None
            form_csi.save()
        return form_csi
    
class FormPersonPartSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormPersonParticipation
        fields = (  
                    #'form',
                    'workforce_or_beneficiary_id',
                    'participation_level_id'
                  )
    def save(self, validated_data, form_guid, parent_model_class):
        m_parent = None
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
        if m_parent:
            form_person_part = FormPersonParticipation()
            form_person_part.form=m_parent
            form_person_part.workforce_or_beneficiary_id=validated_data['workforce_or_beneficiary_id'] 
            form_person_part.participation_level_id=validated_data['participation_level_id'] if validated_data['participation_level_id'] else None
            form_person_part.save()
        return form_person_part    

class FormOrgUnitContribSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormOrgUnitContributions
        fields = (  
                    #'form',
                    'org_unit_id',
                    'contribution_id'
                  )

    def save(self, validated_data, form_guid, parent_model_class):
        m_parent = None
        try:
            if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
                m_parent = parent_model_class.objects.get(form_guid=form_guid)
            if m_parent:
                org_unit_contrib = FormOrgUnitContributions()
                org_unit_contrib.form=m_parent
                org_unit_contrib.org_unit_id=validated_data['org_unit_id'] 
                org_unit_contrib.contribution_id=validated_data['contribution_id'] 
                org_unit_contrib.save()
        except Exception as e:
            traceback.print_exc()
            raise e
        return org_unit_contrib
       

class FormResChildSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormResChildren
        fields = (  
                    #'form',
                    'child_person_id',
                    'institution_id',
                    'residential_status_id',
                    'date_admitted',
                    'date_left',
                    'sms_id'
                  )
                  
    def save(self, validated_data, form_guid, parent_model_class):
        m_parent = None
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
        if m_parent:
            res_child = FormResChildren()
            res_child.form=m_parent
            res_child.child_person_id=validated_data['child_person_id'] if validated_data['child_person_id'] else None
            res_child.institution_id=validated_data['institution_id'] if validated_data['institution_id'] else None
            res_child.residential_status_id=validated_data['residential_status_id'] if validated_data['residential_status_id'] else None
            res_child.date_admitted=validated_data['date_admitted'] if validated_data['date_admitted'] else None
            res_child.date_left=validated_data['date_left'] if validated_data['date_left'] else None
            res_child.sms_id=validated_data['sms_id'] if validated_data['sms_id'] else None
            res_child.save()
        return res_child 

class FormResWfcSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormResWorkforce
        fields = (  
                    #'form',
                    'workforce_id',
                    'institution_id',
                    'position_id',
                    'full_part_time_id'
                  )
    def save(self, validated_data, form_guid, parent_model_class):
        m_parent = None
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
        if m_parent:
            res_wfc = FormResWorkforce()
            res_wfc.form=m_parent
            res_wfc.workforce_id=validated_data['workforce_id'] if validated_data['workforce_id'] else None
            res_wfc.institution_id=validated_data['institution_id'] if validated_data['institution_id'] else None
            res_wfc.position_id=validated_data['position_id'] if validated_data['position_id'] else None
            res_wfc.full_part_time_id=validated_data['full_part_time_id'] if validated_data['full_part_time_id'] else None
            res_wfc.save()
        return res_wfc     

class PermsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = (  
                    'id',
                    'name',
                    'content_type',
                    'codename'
                  )
                  
    def save(self, validated_data, id):
        role = None
        if len(Permission.objects.filter(pk=id)) > 0:
            role = Permission.objects.get(pk=id)
        else:
            role = Permission()
        role.pk = id
        role.name=validated_data['name'] 
        role.content_type=validated_data['content_type'] 
        role.codename=validated_data['codename'] 
        
        if role:
            role.save()
        return role
    
    @transaction.commit_on_success
    def is_valid(self, id):
        try:
            toReturn = True
            if len(Permission.objects.filter(pk=id)) > 0:
                m_perm = Permission.objects.get(pk=id)
                Permission.objects.filter(pk=id).delete()
                toReturn = super(PermsSerializer,self).is_valid()
                m_perm.save()
            else:
                toReturn = super(PermsSerializer,self).is_valid()
            return toReturn
        except Exception as e:
            traceback.print_exc()
            raise Exception('Cannot validate the permission')

class PermsDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OVCPermission
        fields = (  
                    'id',
                    'permission_description',
                    'permission_set',
                    'permission_type',
                    'restricted_to_self',
                    'restricted_to_org_unit',
                    'restricted_to_geo'
                  )
                  
    def save(self, validated_data, id):
        ovc_perm = None
        if len(OVCPermission.objects.filter(pk=id)) > 0:
            ovc_perm = OVCPermission.objects.get(pk=id)
        else:
            ovc_perm = OVCPermission()
        ovc_perm.pk=id
        ovc_perm.permission_description=validated_data['permission_description'] 
        ovc_perm.permission_set=validated_data['permission_set'] 
        ovc_perm.permission_type=validated_data['permission_type'] 
        ovc_perm.restricted_to_self=validated_data['restricted_to_self'] 
        ovc_perm.restricted_to_org_unit=validated_data['restricted_to_org_unit'] 
        ovc_perm.restricted_to_geo=validated_data['restricted_to_geo'] 
        if len(Permission.objects.filter(pk=id)) > 0:
            parent_perm = Permission.objects.get(pk=id)
            ovc_perm.content_type = parent_perm.content_type
            ovc_perm.codename = parent_perm.codename
        
        if ovc_perm:
            ovc_perm.save()
        return ovc_perm
    
    @transaction.commit_on_success
    def is_valid(self, id):
        try:
            toReturn = True
            if len(OVCPermission.objects.filter(pk=id)) > 0:
                m_perm_detail = OVCPermission.objects.get(pk=id)
                OVCPermission.objects.filter(pk=id).delete()
                toReturn = super(PermsDetailSerializer,self).is_valid()
                m_perm_detail.save()
            else:
                toReturn = super(PermsDetailSerializer,self).is_valid()
            return toReturn
        except Exception as e:
            traceback.print_exc()
            raise Exception('Cannot validate the perm details')

class RolesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = (  
                    'id',
                    'name'
                  )
                  
    def save(self, validated_data, id):
        role = None
        if len(Group.objects.filter(pk=id)) > 0:
            role = Group.objects.get(pk=id)
        else:
            role = Group()
        role.pk = id
        role.name=validated_data['name'] 
        
        if role:
            role.save()
        return role
    
    @transaction.commit_on_success
    def is_valid(self, id):
        try:
            if len(Group.objects.filter(pk=id)) > 0:
                Group.objects.filter(pk=id).delete()
            return super(RolesSerializer,self).is_valid()
        except Exception as e:
            traceback.print_exc()
            raise Exception('Cannot validate the group')
    
class RolesDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OVCRole
        fields = (  
                    'id',
                    'group_id',
                    'group_name',
                    'group_description',
                    'restricted_to_org_unit',
                    'restricted_to_geo',
                    'automatic'
                  )
                  
    def save(self, validated_data, id):
        role = None
        if len(OVCRole.objects.filter(pk=id)) > 0:
            role = OVCRole.objects.get(pk=id)
        else:
            role = OVCRole()
        role.pk = id
        role.group_id=validated_data['group_id'] 
        role.group_name=validated_data['group_name'] 
        role.name = validated_data['group_name'] 
        role.group_description=validated_data['group_description'] 
        role.restricted_to_org_unit=validated_data['restricted_to_org_unit'] 
        role.restricted_to_geo=validated_data['restricted_to_geo'] 
        role.automatic=validated_data['automatic'] 
        
        if role:
            role.save()
        return role
    
    
    @transaction.commit_on_success
    def is_valid(self, id):
        try:
            if len(OVCRole.objects.filter(pk=id)) > 0:
                OVCRole.objects.filter(pk=id).delete()
            return super(RolesDetailSerializer,self).is_valid()
        except Exception as e:
            traceback.print_exc()
            raise Exception('Cannot validate the group details')
  
class OVCUserRoleGeoOrgSerialiser(serializers.ModelSerializer):
    class Meta:
        model = OVCUserRoleGeoOrg
        fields = (  
                    'id',
                    'user',
                    'group',
                    'org_unit',
                    'area',
                    'void'
                  )
                  
    def save(self, validated_data, id):
        user_role_geo_org = None
        if len(OVCUserRoleGeoOrg.objects.filter(pk=id)) > 0:
            user_role_geo_org = OVCUserRoleGeoOrg.objects.get(pk=id)
        else:
            user_role_geo_org = OVCUserRoleGeoOrg()
        user_role_geo_org.id = id
        user_role_geo_org.user=validated_data['user'] 
        user_role_geo_org.group=validated_data['group'] 
        user_role_geo_org.org_unit=validated_data['org_unit'] if validated_data['org_unit'] else None
        user_role_geo_org.area=validated_data['area'] 
        user_role_geo_org.void=validated_data['void'] 
        
        if user_role_geo_org:
            user_role_geo_org.save()
        return user_role_geo_org

class CoreEncountersSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoreEncounters
        fields = (  
                    'form_id',
                    'workforce_person',
                    'beneficiary_person',
                    'encounter_date',
                    'org_unit_id',
                    'area_id',
                    'encounter_type_id',
                    'sms_id'
                  )
    def save(self, validated_data, form_guid, parent_model_class):
        '''
        a) if there is already a record on the server for the same beneficiary/adverse 
        condition combination which does not have a form_id, add the form_id to the 
        record.  
        b) If there is already a record on the server for the same beneficiary/adverse 
        condition combination which has a different form_id, do not upload.
        '''
        m_parent = None
        m_existing_rec = None
        workforce_person = validated_data['workforce_person']
        beneficiary_person = validated_data['beneficiary_person']
        encounter_date = validated_data['encounter_date']
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
            if len(CoreEncounters.objects.filter(workforce_person = workforce_person,\
                                beneficiary_person = beneficiary_person,\
                                encounter_date = encounter_date)) > 0:
                m_existing_rec = CoreEncounters.objects.filter(workforce_person = workforce_person,\
                                        beneficiary_person = beneficiary_person,\
                                        encounter_date = encounter_date)[0]
                    
        if m_parent:
            if m_existing_rec:
                if not m_existing_rec.form_id:
                    m_existing_rec.form_id = m_parent.pk
                    m_existing_rec.save()
                    return m_existing_rec  
                if not (m_existing_rec.form_id == m_parent.pk):                    
                    return   
            else:
                core_encounters = CoreEncounters()
                core_encounters.form_id=m_parent.pk
                core_encounters.workforce_person=workforce_person 
                core_encounters.beneficiary_person=beneficiary_person 
                core_encounters.encounter_date=encounter_date 
                core_encounters.org_unit_id=validated_data['org_unit_id'] 
                core_encounters.area_id=validated_data['area_id'] 
                core_encounters.encounter_type_id=validated_data['encounter_type_id'] 
                core_encounters.sms_id=validated_data['sms_id'] if validated_data['sms_id'] else None
                core_encounters.save()
        return core_encounters  
    
class CoreServicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoreServices
        fields = (  
                    #'form',
                    'workforce_person',
                    'beneficiary_person',
                    'encounter_date',
                    'core_item_id'
                  )
    def save(self, validated_data, form_guid, parent_model_class):
        m_form = None
        m_core_encounter = None
        core_item_id = validated_data['core_item_id'] 
        workforce_person = validated_data['workforce_person'] 
        beneficiary_person = validated_data['beneficiary_person'] 
        encounter_date = validated_data['encounter_date'] 
        
        if Forms.objects.filter(form_guid=form_guid).count() > 0:
            m_form = Forms.objects.filter(form_guid=form_guid)[0]
            if len(parent_model_class.objects.filter(workforce_person = workforce_person,\
                                beneficiary_person = beneficiary_person,\
                                encounter_date = encounter_date)) > 0:
                m_core_encounter = parent_model_class.objects.filter(workforce_person = workforce_person,\
                                    beneficiary_person = beneficiary_person,\
                                    encounter_date = encounter_date)[0]
        if m_core_encounter:
            core_services = CoreServices()
            core_services.core_item_id=core_item_id
            core_services.workforce_person=workforce_person
            core_services.beneficiary_person= beneficiary_person
            core_services.encounter_date=encounter_date
            core_services.form_id = m_core_encounter.form_id
            core_services.save()
        return core_services 
    
class CoreAdverseConditionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoreAdverseConditions
        fields = (  
                    'form_id',
                    'beneficiary_person',
                    'adverse_condition_id',
                    'sms_id',
                    'is_void',
                  )
    def save(self, validated_data, form_guid, parent_model_class):
        ''' 
        a) if there is already a record on the server for the same beneficiary/adverse condition 
        combination which does not have a form_id, add the form_id to the record.  
        b) If there is already a record on the server for the same beneficiary/adverse condition 
        combination which has a different form_id, do not upload (save).
        '''
        m_parent = None
        m_existing_rec = None
        adverse_condition_id = validated_data['adverse_condition_id'] 
        beneficiary_person = validated_data['beneficiary_person'] 
        if parent_model_class.objects.filter(form_guid=form_guid).count() > 0:
            m_parent = parent_model_class.objects.get(form_guid=form_guid)
            if len(CoreAdverseConditions.objects.filter(adverse_condition_id = adverse_condition_id,\
                                beneficiary_person = beneficiary_person)) > 0:
                m_existing_rec = CoreAdverseConditions.objects.filter(adverse_condition_id = adverse_condition_id,\
                                        beneficiary_person = beneficiary_person)[0]
        if m_parent:
            if m_existing_rec:
                if not m_existing_rec.form_id:
                    m_existing_rec.form_id = m_parent.pk
                    m_existing_rec.save()
                    return m_existing_rec  
                if not (m_existing_rec.form_id == m_parent.pk):                    
                    return  
            else:
                core_adverse = CoreAdverseConditions()
                core_adverse.form_id=m_parent.pk
                core_adverse.beneficiary_person=beneficiary_person
                core_adverse.adverse_condition_id=adverse_condition_id
                core_adverse.sms_id=validated_data['sms_id'] if validated_data['sms_id'] else None
                core_adverse.is_void=validated_data['is_void']
                core_adverse.save()
        return core_adverse  
    
'''
    CUSTOM SERIALISERS
    
    This section contains the custom seriliser classes for data elements that do
    not have djnago models associated with them. The custom serialisers use SQL
    to retrieve and save data to the tables
'''

class UserRolesSerialiser(Serializer):
    def __init__(self, data=None):
        self.user_groups = []
        self.user_role_data = data
        self.data = []
        
    def set_data_items(self):
        all_users = AppUser.objects.all()
        for user in all_users:
            if user.is_superuser:
                continue
            if not user.has_perm('auth.log in capture site'):
                continue
            try:
                groups = get_roles(user)
                mapped_object = {
                    'workforce_id': user.workforce_id,
                    'national_id': user.national_id,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'groups': groups,
                }
                self.user_groups.append(mapped_object)
            except Exception as e:
                traceback.print_exc()
                raise e
        self.data = self.user_groups
    
    def is_valid(self):
        return True
        
    def save(self):
        try:
            if self.user_role_data:
                for item in self.user_role_data:
                    workforce_id = item['workforce_id'] if item['workforce_id'] else None
                    national_id = item['national_id'] if item['national_id'] else None
                    first_name = item['first_name'] if item['first_name'] else None
                    last_name = item['last_name'] if item['last_name'] else None
                    groups = item['groups'] if item['groups'] else None
                    user = None
                    if workforce_id and national_id:
                        user = AppUser.objects.get(workforce_id=workforce_id, \
                            national_id=national_id, first_name=first_name, last_name=last_name)
                    if workforce_id and not national_id:
                        user = AppUser.objects.get(workforce_id=workforce_id, \
                            first_name=first_name, last_name=last_name)
                    if not workforce_id and national_id:
                        user = AppUser.objects.get(national_id=national_id, \
                            first_name=first_name, last_name=last_name)
                    if user:
                        user_groups = user.groups.all()
                        for group_id in groups:
                            ovc_role = None
                            if len(OVCRole.objects.filter(group_id=group_id)) >0:
                                ovc_role = OVCRole.objects.filter(group_id=group_id)[0]
                                group = OVCRole.objects.get(id=ovc_role.pk)
                                if group not in user_groups:
                                    #print 'Saving'
                                    group.user_set.add(user)
                                else:
                                    print 'Already exisits'
        except Exception as e:
            traceback.print_exc()
            raise e
            

def get_roles(user):
    to_return = []
    roles = user.groups.all()
    for role in roles:
        to_return.append(role.ovcrole.group_id)
    return to_return

class RolePermissionsSerialiser(Serializer):
    def __init__(self, data=None):
        self.role_permissions = []
        self.role_perms_data = data
        self.data = []
        
    def set_data_items(self):
        try:
            connection = psycopg2.connect(host=settings.DATABASES['default']['HOST'], database=settings.DATABASES['default']['NAME'],\
                    user=settings.DATABASES['default']['USER'], password=settings.DATABASES['default']['PASSWORD'])
            cursor = connection.cursor()
            cursor.execute('select id,group_id,permission_id from auth_group_permissions')
            for row in cursor:            
                    mapped_object = {
                        'id': row[0],
                        'group_id': row[1],
                        'permission_id': row[2],
                    }
                    self.role_permissions.append(mapped_object)
            self.data = self.role_permissions
        except Exception as e:
            traceback.print_exc()
            raise e
    
    def is_valid(self):
        return True

    @transaction.commit_on_success    
    def save(self):
        try:
            if self.role_perms_data:
                delete_old_role_perms()
                #TODO @lyokog improve perfomance. This works but performs poorly though the user won't notice for now
                #Construct the bulk insert sql script in the loop and then run the query outside the loop to improve perfomance
                for item in self.role_perms_data:
                    group_id = item['group_id'] if item['group_id'] else None
                    permission_id = item['permission_id'] if item['permission_id'] else None

                    connection = psycopg2.connect(host=settings.DATABASES['default']['HOST'], database=settings.DATABASES['default']['NAME'],\
                            user=settings.DATABASES['default']['USER'], password=settings.DATABASES['default']['PASSWORD'])
                    cursor = connection.cursor()
                    cursor.execute('insert into auth_group_permissions (group_id, permission_id) values (%s, %s)', (group_id, permission_id))
                    connection.commit()
                    cursor.close()
                    connection.close()
        except Exception as e:
            traceback.print_exc()
            raise e
    
def delete_old_role_perms():  
    try:      
        connection = psycopg2.connect(host=settings.DATABASES['default']['HOST'], database=settings.DATABASES['default']['NAME'],\
                user=settings.DATABASES['default']['USER'], password=settings.DATABASES['default']['PASSWORD'])
        cursor = connection.cursor()
        cursor.execute('delete from auth_group_permissions')
        connection.commit()
        cursor.close()
        connection.close()
    except Exception as e:
        traceback.print_exc()
        raise e