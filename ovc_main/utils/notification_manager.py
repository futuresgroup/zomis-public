from django.core.mail import send_mail
from ovc_auth.user_manager import default_password
from ovc_main.models import RegPersonsContact
from ovc_main.utils import lookup_field_dictionary as lookup
import smtplib
from django.conf import settings

def send_notification(user_model=None, new_user = None, message_type=None, contact_type = None):
    if message_type:
        if message_type == MessageType.NEW_REGISTRATION:
            password = default_password
            email_address = ''
            phone_number = ''
            user_name = ''
            if len(new_user.workforce_id) == 8:
                user_name = new_user.workforce_id
            else:
                user_name = new_user.national_id
            if contact_type == ContactType.EMAIL:
                if len(RegPersonsContact.objects.filter(person=user_model,contact_detail_type_id=lookup.contact_email_address,is_void=False)) > 0:
                    email_address = (RegPersonsContact.objects.get(person=user_model,contact_detail_type_id=lookup.contact_email_address,is_void=False)).contact_detail
                    message = 'You have been successfully registered. You username is '+user_name+' and password is '+password+'. Go to http:// to login. You may also get help and support by joining the ZOMIS users email group at http://groups.google.com/group/zomisusers '
                    try:  
                        send_mail('Registration successful.', message, 'glyoko@gmail.com', [email_address], fail_silently=False)
                    except Exception as e:
                        print e 
                        raise Exception('Failed to send an email to the new user')
                    return True
            
def notify(message_type=None, message=None, contact_info=None, subject=None):
    import socket
    if message_type:
        if not subject:
            subject = 'This is a notification message'

        email_adds = []
        for contact_type, contact_add in contact_info.items():
            if contact_type == ContactType.EMAIL:
                email_adds.append(contact_add)
        try:
            if email_adds:
                send_mail(subject, message, settings.EMAIL_HOST_USER, email_adds, fail_silently=False)
        except smtplib.SMTPException as smtpexception:
            print smtpexception
            print 'something went wrong while sending email. Check that internet is working'
        except socket.gaierror as internetError:
            print internetError
        except Exception as e:
            print type(e), 'exception was thrown'
            raise Exception('Failed to send mail to the user, reason unknown')
            
def send_user_reg_info(person_model=None, appuser=None, message=None):  
    contact_info = get_contact_info(person_model)
    firstname = None
    lastname = None
    reg_message = ''
    if person_model:
        reg_message = 'Dear %s %s,\n\n' % (person_model.first_name, person_model.surname)

    reg_message += 'You have been successfully registered in '+settings.APP_NAME+'. You will be required to change your password on first login.\n  You may subscribe for ongoing help and support by joining the ZOMIS users email group at http://groups.google.com/group/zomisusers '
    if appuser:
        username = get_user_name(appuser)
        reg_message += 'Username: %s\n' % username
        
    subject= '%s Registration successful' % settings.APP_NAME
    for mkey, mdetails in message.items():
        reg_message+='%s: %s\n' % (mkey, mdetails)
    reg_message+='\nFrom the '+settings.APP_NAME+' administrator'
    notify(message_type=MessageType.NEW_REGISTRATION, message=reg_message, contact_info=contact_info, subject=subject)
    
    return True  

def change_passord_notification(person_model, appuser, password):
    contact_info = get_contact_info(person_model)
    username = get_user_name(appuser)
    subject = 'ZOMIS Password reset successful'
    message = ''
    if person_model:
        message = 'Dear %s %s,\n\n' % (person_model.first_name, person_model.surname)
    message += 'Your password was reset.\n\nUsername: %s \nPassword: %s\n\nFrom the ZOMIS Admin' % (username, password)
    
    notify(message_type=MessageType.PASSWORD_RESET, message=message, contact_info=contact_info, subject=subject)
    
    return True

def get_user_name(appuser):
    username = None
    if len(appuser.workforce_id) == 8:#need a good way to determine the beneficiary numbers
        username = appuser.workforce_id
    else:
        username = appuser.national_id
    return  username
    
def get_contact_info(person_model):
    contact_info = {}
    for contact in RegPersonsContact.objects.filter(person=person_model,is_void=False):
        if contact.contact_detail_type_id in [lookup.contact_email_address]:
            contact_info[ContactType.EMAIL] = contact.contact_detail
    return contact_info
#An attempt to simulate enums. we only get enums in python 3.4 :(
class MessageType:
    NEW_REGISTRATION = 1
    PASSWORD_RESET = 2
    SMS_DELIVERY_CONFIRMATION = 3
    
class ContactType:
    SMS = 1
    EMAIL = 2
    ALL = 3