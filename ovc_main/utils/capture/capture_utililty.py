from ovc_main.models import AdminUploadForms, AdminDownload, Forms, AdminCaptureSites, CaptureTaskTracker
from django.db.models import Count
from ovc_main.core.rest import download_upload_settings as dp_settings
from ovc_main.utils import fields_list_provider as field_provider
from django.db.models import Max
import json
from celery.result import AsyncResult
import celery.states
from rest_client import get_recent_running_task_info, get_task_info
from celery.task.control import revoke
import traceback

download_count = 10

def get_upload_queue(task_id='',return_json=False):
    '''
    This method returns data to feed the upload section of the capture site page
    '''
    recent_upload_date = AdminUploadForms.objects.all().aggregate(Max('timestamp_uploaded'))['timestamp_uploaded__max']
    if not recent_upload_date:
        recent_upload_date = 'Never'
    total_forms = len(AdminUploadForms.objects.exclude(timestamp_uploaded=None))
    queue = AdminUploadForms.objects.filter(timestamp_uploaded=None)

    uploads = [] 
    form_types_dic = {}
    for upload in queue:
        form_id = upload.form_id
        form_type_id = (Forms.objects.get(pk__exact=form_id)).form_type_id     
        if not form_types_dic.has_key(form_type_id):
            form_types_dic[form_type_id] = 1
        elif form_types_dic.has_key(form_type_id):
            form_types_dic[form_type_id] = form_types_dic[form_type_id] + 1
    
    for k,v in form_types_dic.items():
        form_type_desc = field_provider.get_description_for_item_id(k)[0]
        item = {'formtype': form_type_desc,
            'count': v, }
        uploads.append(item)
    
    if return_json:
        formatted_date = ''
        if recent_upload_date == 'Never':
            formatted_date = recent_upload_date
        else:
            formatted_date = recent_upload_date.isoformat() if recent_upload_date else recent_upload_date
        to_return = json.dumps({'recent_upload_date':json.dumps(formatted_date), 
                                'uploads':json.dumps(uploads), 
                                'total_forms':json.dumps(total_forms), 
                                'task_id':json.dumps(task_id)})
    else:
        to_return = {'recent_upload_date':recent_upload_date, 
                    'uploads':uploads, 
                    'total_forms':total_forms,
                    'task_id':task_id}
    return to_return

def get_downloads_queue(task_id=None, return_json=False):
    '''
    This method returns data to feed the download section of the capture site page
    '''
    queue = AdminDownload.objects.all().order_by('-timestamp_started')
    to_return = []
    downloads = []
    for downld in queue:
        started = '-'
        ended = '-'
        if downld.timestamp_started:
            started = downld.timestamp_started.isoformat()
        if downld.timestamp_completed:
            ended = downld.timestamp_completed.isoformat()

        item = {'section': field_provider.get_description_for_item_id(downld.section_id)[0],
                'started': started,
                'ended': ended,
                'num_of_recs': downld.number_records,
                'success': downld.success,}
        downloads.append(item)
    
    if return_json:  
        from django.core.paginator import Paginator
        pages = Paginator(downloads, download_count)
        downloads_json = json.dumps(pages.page(1).object_list)
        to_return = json.dumps({
                                'downloads':downloads_json,
                                'task_id':json.dumps(task_id),})
    else:
        to_return = {
                    'downloads':downloads,
                    'task_id':task_id,}
    return to_return    
    
def stop_download(task_id, return_json=False):
    cancelled = False
    task_operation = None
    try:
        m_task_trackers = CaptureTaskTracker.objects.filter(task_id = task_id)
        if len(m_task_trackers) > 0:
            m_task_tracker = m_task_trackers[0]
            task_operation = m_task_tracker.operation
            m_task_tracker.cancelled = True
            m_task_tracker.completed = True
            m_task_tracker.save()
        revoke(task_id, terminate=True)
        cancelled = True
    except Exception as e:
        traceback.print_exc()
        raise e
    
def get_initial_data(task_id, return_json=False):
    '''
    This method is used to return initial data to populate the capture site admin page and also for refreshing the page
    '''
    has_pending_uploads = False
    has_pending_downloads = False
    uploads = []
    downloads = []
    recent_upload_date = 'Never'
    if(AdminUploadForms.objects.filter(timestamp_uploaded=None) > 0):
        has_pending_uploads = True
        uploads_info = get_upload_queue()
        uploads = uploads_info['uploads']
        recent_upload_date = uploads_info['recent_upload_date']
        total_forms = uploads_info['total_forms']
    if(AdminDownload.objects.filter() > 0):
        has_pending_downloads = True
        downloads_data  = get_downloads_queue()
        downloads = downloads_data['downloads']
    downld_sections = tuple([(k, v['title']) for k,v in dp_settings.download_sections.items()])
    downld_sections = sorted(downld_sections, key=lambda t: (t[1][0].isupper(), t[1]))
    
    task_exists = False
    task_completed_success = None
    task_running = None
    task_operation = None
    status = None
    '''
    If a task id is passed from the web page, get its details else get the most recent active active task
    '''
    if task_id:
        print 'Task ID available: ', task_id
        task_id, status, task_operation = get_task_info(task_id=task_id)
    else:
        print 'No task ID available'
        task_id, status, task_operation = get_recent_running_task_info()
    if task_id:
        if status:       
            task_exists = True      
            if status == celery.states.SUCCESS:
                task_completed_success = True
                task_running = False
            elif status == celery.states.FAILURE:
                task_completed_success = False
                task_running = False
            elif status == celery.states.PENDING:
                task_completed_success = False
                task_running = True
                if task_id:
                    if len(CaptureTaskTracker.objects.filter(task_id = task_id)):
                        m_capture = CaptureTaskTracker.objects.filter(task_id = task_id)[0]
                        if m_capture.completed or m_capture.cancelled:
                            task_completed_success = False
                            task_running = False
                            task_operation = m_capture.operation
            elif status == celery.states.REVOKED:
                task_completed_success = False
                task_running = False
                if task_id:
                    if len(CaptureTaskTracker.objects.filter(task_id = task_id)):
                        m_capture = CaptureTaskTracker.objects.filter(task_id = task_id)[0]
                        task_operation = m_capture.operation
            else:
                print status
    else:
        task_id = ''
    """
    print '*********************************'
    print status, 'status'
    print task_id,'task_id'
    print task_completed_success,'task_completed_success'
    print task_running,'task_running'
    print task_operation,'task_operation'
    print '*********************************'
    """
    from django.core.paginator import Paginator
    pages = Paginator(downloads, download_count)
    
    capture_id = field_provider.get_capture_site_id()
    if return_json:
        downloads_json = json.dumps(pages.page(1).object_list)
        pageinfo = {'pageinfo':'pageinfo',
                    'pagescount':pages.num_pages,
                    'total_records':pages.count,
                    'pagenumber': 1}
            
        initial_data = json.dumps({
                                    'has_uploads':json.dumps(has_pending_uploads), 
                                    'uploads':json.dumps(uploads), 
                                    'recent_upload_date':json.dumps(str(recent_upload_date)), 
                                    'total_forms':json.dumps(total_forms), 
                                    'has_downloads':json.dumps(has_pending_downloads), 
                                    'downloads':downloads_json, 
                                    'pageinfo':json.dumps(pageinfo), 
                                    'download_sections':downld_sections,
                                    'task_id':json.dumps(task_id),
                                    'task_running':json.dumps(task_running),
                                    'task_completed_success':json.dumps(task_completed_success),
                                    'task_operation':json.dumps(task_operation),
                                    'capture_id':json.dumps(capture_id),})
    else:
        pageinfo = {'pageinfo':'pageinfo',
        'pagescount':pages.num_pages,
        'total_records':pages.count,
        'pagenumber': 1}
        initial_data = {
                        'has_uploads':has_pending_uploads, 
                        'uploads':uploads, 
                        'recent_upload_date':recent_upload_date, 
                        'total_forms':total_forms, 
                        'has_downloads':has_pending_downloads, 
                        'downloads':pages.page(1), 
                        'pageinfo':pageinfo,
                        'download_sections':downld_sections,
                        'task_id':task_id,
                        'task_running':task_running,
                        'task_completed_success':task_completed_success,
                        'task_operation':task_operation,
                        'capture_id':capture_id,}
    return initial_data

def get_download_precedence():
    '''
    Returns the download details with there precedence
    '''
    downloads_prec = {}
    for section_id in dp_settings.download_sections.keys():
        if section_id == 'all':
            continue
        section_details = dp_settings.download_sections[section_id]
        downloads_prec[section_details['precedence']] = section_id
    return downloads_prec

def get_upload_precedence():
    '''
    Returns the upload details with there precedence
    '''
    upload_prec = {}
    for upload_info in dp_settings.upload_info:
        upload_prec[upload_info['precedence']] = upload_info
    return upload_prec
