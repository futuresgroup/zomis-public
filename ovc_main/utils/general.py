def get_list_from_dic_or_querydict(obj, key):
    from django.http import QueryDict
    if not key in obj:
        return []
    if type(obj) == dict:
        return obj[key]
    if type(obj) == QueryDict:
        return obj.getlist(key)
    
def convert_to_int_array(valuetoconvert):
    if str(valuetoconvert).isdigit():
        return [int(valuetoconvert)]
    elif valuetoconvert:
        tmp = map(int, valuetoconvert)
        return tmp
    else:
        return None
    
def list_has_key(list_to_check, key_value):
    #Check if a given key exists in a tuple
    if key_value in list_to_check:
        return True
    return False

def calculate_age(birth_date, to_date):
    '''Adapted from http://stackoverflow.com/questions/2217488/age-from-birthdate-in-python'''
    tmp = birth_date
    if tmp:
        to_date = to_date
        try: 
            birthday = tmp.replace(year=to_date.year)
        except ValueError: 
            birthday = tmp.replace(year=to_date.year, month=birth_date.month+1, day=1)
        if birthday > to_date:
            return to_date.year - tmp.year - 1
        else:
            return to_date.year - tmp.year
    return -1


