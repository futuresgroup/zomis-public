#Report Types
REPORTING_PERFORMANCE = "1";
OVC_DEMOGRAPHICS = "2";
OVC_BY_GEO_AREA = "3";
OVC_ADVERSE_COND = "4";
OVC_EXP_ADVERSE_COND  = "5";
OVC_MIGRATION = "6";
GUARDIAN_DEATH = "7";
SERVICES_PROVIDED = "8";
BEN_RECV_SERVICE = "9";
ORG_UNIT_PROVIDING_SERVICE = "10";
BEN_RECV_ANY_SERVICE = "28";
WFC_BEN_RATIO_BY_ORG_UNIT = "11";
REFFERALS_MADE = "12";
OVC_ASSESSMENT = "13";
OVC_ASSESSMENT_CSI    = "14";
OVC_ASSESSMENT_ANSWER = "30";
OVC_ASSESSMENT_OVERTIME    = "15";
OVC_ASSESSMENT_CSI_OVERTIME    = "16";
RESIDENTIAL_CHILD_STATUS_SUMMARY    = "17";
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION    = "18";
RESIDENTIAL_DEMOGRAPHICS    = "19";
RESIDENTIAL_ADVERSE_COND    = "20";
RESIDENTIAL_REINTEGRATION_DEMOG    = "21";
RESIDENTIAL_REINTEGRATION_BY_ORG    = "22";
RESIDENTIAL_ASSESSMENT    = "23";
CICWL_ASSESSMENT    = "24";
CICWL_DEMOGRAPHICS    = "25";
WORKFORCE_ACTIVE_BY_GEO    = "26";
WORKFORCE_ASSESSMENT    = "27";
WORKFORCE_BY_ORG    = "29";

#=======REPORTING_PERFORMANCE

#Parameters expected: from_date, to_date, item_id 
#(in this case item_id is an organisational unit type)
REPORTING_PERFORMANCE_ITEM_SQL = "    SELECT tbl_reg_org_units.org_unit_id_vis \"Org unit ID\", tbl_reg_org_units.org_unit_name \"Org unit name\", tbl_list_general.item_description \"Form type\", Count(*) \"Number of forms submitted\"     "
REPORTING_PERFORMANCE_ITEM_SQL +="    FROM      "
REPORTING_PERFORMANCE_ITEM_SQL +="        (    (SELECT tbl_forms.org_unit_id_filled_paper, tbl_forms.form_type_id     "
REPORTING_PERFORMANCE_ITEM_SQL +="                FROM tbl_forms     "
REPORTING_PERFORMANCE_ITEM_SQL +="                WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s     "
REPORTING_PERFORMANCE_ITEM_SQL +="                ) as qry_forms_in_period      "   
REPORTING_PERFORMANCE_ITEM_SQL +="        INNER JOIN tbl_reg_org_units ON qry_forms_in_period.org_unit_id_filled_paper = tbl_reg_org_units.id)     "
REPORTING_PERFORMANCE_ITEM_SQL +="        INNER JOIN tbl_list_general ON qry_forms_in_period.form_type_id = tbl_list_general.item_id     "
REPORTING_PERFORMANCE_ITEM_SQL +="    GROUP BY tbl_reg_org_units.org_unit_id_vis, tbl_reg_org_units.org_unit_name, tbl_list_general.item_description, tbl_reg_org_units.org_unit_type_id     "
REPORTING_PERFORMANCE_ITEM_SQL +="    HAVING tbl_reg_org_units.org_unit_type_id=%s;    "
#qry_forms_in_period gets all forms related to the period
#final query counts how many forms of each form type from eacn org unit there are, and shows only org units of the selected org unit type    

#Parameters expected: from_date, to_date, set_id
REPORTING_PERFORMANCE_SET_SQL= "    SELECT tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit name\", tbl_list_general.item_description AS \"Form type\", Count(*) AS \"Number of forms submitted\"     "
REPORTING_PERFORMANCE_SET_SQL+="    FROM ((                            "
REPORTING_PERFORMANCE_SET_SQL+="            (SELECT tbl_forms.org_unit_id_filled_paper, tbl_forms.form_type_id                    "
REPORTING_PERFORMANCE_SET_SQL+="            FROM tbl_forms                    "
REPORTING_PERFORMANCE_SET_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s                   "
REPORTING_PERFORMANCE_SET_SQL+="            ) as qry_forms_in_period                     "
REPORTING_PERFORMANCE_SET_SQL+="    INNER JOIN tbl_reg_org_units ON qry_forms_in_period.org_unit_id_filled_paper = tbl_reg_org_units.id)                            "
REPORTING_PERFORMANCE_SET_SQL+="    INNER JOIN tbl_list_general ON qry_forms_in_period.form_type_id = tbl_list_general.item_id)                             "
REPORTING_PERFORMANCE_SET_SQL+="    INNER JOIN                             "
REPORTING_PERFORMANCE_SET_SQL+="            (SELECT tbl_reports_sets_org_units.org_unit_id                    "
REPORTING_PERFORMANCE_SET_SQL+="            FROM tbl_reports_sets_org_units                    "
REPORTING_PERFORMANCE_SET_SQL+="            WHERE tbl_reports_sets_org_units.set_id=%s                   "
REPORTING_PERFORMANCE_SET_SQL+="            ) as qry_set_org_units                    "
REPORTING_PERFORMANCE_SET_SQL+="    ON tbl_reg_org_units.id = qry_set_org_units.org_unit_id                            "
REPORTING_PERFORMANCE_SET_SQL+="    GROUP BY tbl_reg_org_units.org_unit_id_vis, tbl_reg_org_units.org_unit_name, tbl_list_general.item_description                            "
#qry_forms_in_period gets all forms related to the period
#qry_set_org_units gets list of selected org_unit_ids
#final query counts how many forms of each form type from eacn org unit there are


#Parameters expected: from_date, to_date, item_id, org_unit_id
REPORTING_PERFORMANCE_ORG_UNIT_SQL = "    SELECT tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit name\", tbl_list_general.item_description AS \"Form type\", Count(*) AS \"Number of forms submitted\"     "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "    FROM     "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "        (    (SELECT tbl_forms.org_unit_id_filled_paper, tbl_forms.form_type_id    "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "                FROM tbl_forms    "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "                WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s    "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "                ) as qry_forms_in_period     "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "    INNER JOIN tbl_reg_org_units ON qry_forms_in_period.org_unit_id_filled_paper = tbl_reg_org_units.id)    "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "    INNER JOIN tbl_list_general ON qry_forms_in_period.form_type_id = tbl_list_general.item_id    "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "    GROUP BY tbl_reg_org_units.id, tbl_reg_org_units.org_unit_id_vis, tbl_reg_org_units.org_unit_name, tbl_list_general.item_description ,tbl_reg_org_units.id   "
REPORTING_PERFORMANCE_ORG_UNIT_SQL += "    HAVING tbl_reg_org_units.id =%s    "
#qry_forms_in_period gets all forms related to the period
#final query counts how many forms of each form type there are, limiting only to the selected org unit

#Parameters expected: from_date, to_date
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL = "    SELECT tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit name\", tbl_list_general.item_description AS \"Form type\", Count(*) AS \"Number of forms submitted\"     "
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL += "    FROM     "
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL += "    ((SELECT tbl_forms.org_unit_id_filled_paper, tbl_forms.form_type_id    "
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL += "    FROM tbl_forms    "
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL += "    WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s    "
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL += "    ) as qry_forms_in_period     "
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL += "    INNER JOIN tbl_reg_org_units ON qry_forms_in_period.org_unit_id_filled_paper = tbl_reg_org_units.id)     "
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL += "    INNER JOIN tbl_list_general ON qry_forms_in_period.form_type_id = tbl_list_general.item_id    "
REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL += "    GROUP BY tbl_reg_org_units.org_unit_id_vis, tbl_reg_org_units.org_unit_name, tbl_list_general.item_description    "
#qry_forms_in_period gets all forms related to the period
#final query counts how many forms of each form type from eacn org unit there are

#END=========#=======REPORTING_PERFORMANCE


#=======OVC DEMOGRAPHICS
#Parameters expected: none
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL = "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC\"    "
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL += "    FROM     "
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL += "        (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth    "
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL += "        FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL += "        WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False    "
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL += "        ) as qry_persons_type_ovc    "
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL += "    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id    "
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL += "    GROUP BY sex, age    "
OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL += "    ORDER BY age, sex    "

# qry_persons_type_ovc gets OVCs only
# final query counts by age and sex

#Parameters expected: area_id
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL = "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC\"    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "    FROM     "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "    (    (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "        FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "        WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "        ) as qry_persons_type_ovc    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)    " 
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "    INNER JOIN     "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "        (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "        FROM tbl_reg_persons_geo    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "        WHERE tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False and area_id = %s    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "        ) as qry_persons_geo_active_sp     "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "    ON qry_persons_type_ovc.person_id = qry_persons_geo_active_sp.person_id    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "    GROUP BY sex, age    "
OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL += "    ORDER BY age, sex    "
# qry_persons_type_ovc gets OVCs only
# qry_persons_geo_active_sp gets OVC currently resident in selected ward
# final query counts by age and sex

#Parameters expected: area_id
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL = "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC\"    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "    FROM     "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "        ((    (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "                FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "                WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "                ) as qry_persons_type_ovc    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)    " 
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "    INNER JOIN     "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "        (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "        FROM tbl_reg_persons_geo    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "        WHERE tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "        ) as qry_persons_geo_active     "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "    ON qry_persons_type_ovc.person_id = qry_persons_geo_active.person_id)    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "    INNER JOIN tbl_list_geo ON qry_persons_geo_active.area_id = tbl_list_geo.area_id    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "    GROUP BY sex, age, parent_area_id    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "    HAVING tbl_list_geo.parent_area_id=%s    "
OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL += "    ORDER BY age, sex "
# qry_persons_type_ovc gets OVCs only
# qry_persons_geo_active gets wards of OVC
# final query looks up constituency and counts by age and sex

#Parameters expected: area_id
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL = "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC\"    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    FROM     "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    (((    (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "            ) as qry_persons_type_ovc    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)    " 
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    INNER JOIN     "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "        (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "        FROM tbl_reg_persons_geo    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "        WHERE tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "        ) as qry_persons_geo_active     "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    ON qry_persons_type_ovc.person_id = qry_persons_geo_active.person_id)    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    INNER JOIN tbl_list_geo as wards ON qry_persons_geo_active.area_id = wards.area_id)    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    INNER JOIN tbl_list_geo as constituencies ON wards.parent_area_id = constituencies.area_id    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    GROUP BY sex, age, constituencies.parent_area_id    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    HAVING constituencies.parent_area_id=%s    "
OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL += "    ORDER BY age, sex    "
# qry_persons_type_ovc gets OVCs only
# qry_persons_geo_active gets wards of OVC
# final query looks up district and counts by age and sex

#Parameters expected: area_id
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL = "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC\"    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "    FROM     "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "    ((((    (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "            ) as qry_persons_type_ovc    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "        INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)    " 
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "        INNER JOIN    " 
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "            (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "            FROM tbl_reg_persons_geo    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "            WHERE tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "            ) as qry_persons_geo_active     "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "        ON qry_persons_type_ovc.person_id = qry_persons_geo_active.person_id)    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "        INNER JOIN tbl_list_geo as wards ON qry_persons_geo_active.area_id = wards.area_id)    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "        INNER JOIN tbl_list_geo as constituencies ON wards.parent_area_id = constituencies.area_id)    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "        INNER JOIN tbl_list_geo as districts ON constituencies.parent_area_id = districts.area_id    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "    GROUP BY sex, age, districts.parent_area_id    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "    HAVING districts.parent_area_id=%s    "
OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL += "    ORDER BY age, sex    "
# qry_persons_type_ovc gets OVCs only
# qry_persons_geo_active gets wards of OVC
# final query looks up province and counts by age and sex

#Parameters expected: org_unit_id
OVC_DEMOGRAPHICS_ORG_UNIT_SQL = "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC\"    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="     FROM     "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="         (    (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="             FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="             WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="             ) as qry_persons_type_ovc    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="     INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)    " 
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="     INNER JOIN     "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="         (SELECT DISTINCT tbl_core_encounters.org_unit_id, tbl_core_encounters.beneficiary_person_id    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="         FROM tbl_core_encounters    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="         WHERE org_unit_id = %s    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="         ) as qry_core_orgs_beneficiaries     "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="     ON qry_persons_type_ovc.person_id = qry_core_orgs_beneficiaries.beneficiary_person_id    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="     GROUP BY sex, age    "
OVC_DEMOGRAPHICS_ORG_UNIT_SQL +="     ORDER BY age, sex    "
# qry_persons_type_ovc gets OVCs only
# qry_core_orgs_beneficiaries gets OVC served by selected org unit
# final query counts by age and sex


#Parameters expected: set_id
OVC_DEMOGRAPHICS_SET_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC\"    "
OVC_DEMOGRAPHICS_SET_SQL+="    FROM (                            "
OVC_DEMOGRAPHICS_SET_SQL+="            (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
OVC_DEMOGRAPHICS_SET_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id                    "
OVC_DEMOGRAPHICS_SET_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False                    "
OVC_DEMOGRAPHICS_SET_SQL+="            ) as qry_persons_type_ovc                    "
OVC_DEMOGRAPHICS_SET_SQL+="    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)                             "
OVC_DEMOGRAPHICS_SET_SQL+="    INNER JOIN                             "
OVC_DEMOGRAPHICS_SET_SQL+="            (SELECT DISTINCT tbl_core_encounters.org_unit_id, tbl_core_encounters.beneficiary_person_id                    "
OVC_DEMOGRAPHICS_SET_SQL+="            FROM tbl_core_encounters                    "
OVC_DEMOGRAPHICS_SET_SQL+="            ) as qry_core_orgs_beneficiaries                     "
OVC_DEMOGRAPHICS_SET_SQL+="    ON qry_persons_type_ovc.person_id = qry_core_orgs_beneficiaries.beneficiary_person_id                            "
OVC_DEMOGRAPHICS_SET_SQL+="    INNER JOIN                             "
OVC_DEMOGRAPHICS_SET_SQL+="            (SELECT tbl_reports_sets_org_units.org_unit_id                    "
OVC_DEMOGRAPHICS_SET_SQL+="            FROM tbl_reports_sets_org_units                    "
OVC_DEMOGRAPHICS_SET_SQL+="            WHERE tbl_reports_sets_org_units.set_id=%s                   "
OVC_DEMOGRAPHICS_SET_SQL+="            ) as qry_set_org_units                    "
OVC_DEMOGRAPHICS_SET_SQL+="    ON qry_core_orgs_beneficiaries.org_unit_id = qry_set_org_units.org_unit_id                            "
OVC_DEMOGRAPHICS_SET_SQL+="    GROUP BY sex, age                            "
OVC_DEMOGRAPHICS_SET_SQL+="    ORDER BY age, sex                            "
# qry_persons_type_ovc gets OVCs only
# qry_core_orgs_beneficiaries gets org units serving OVC
# qry_hier_unit_and_sub_and_sub_sub_units  gets selected org unit and its sub units and its sub sub units
# final query counts by age and sex


#END=========#=======OVC DEMOGRAPHICS


#=======OVC BY GEO

    

#Wards in a constituency
#Parameters expected: param_to_date, param_to_date, param_to_date, param_to_date, param_from_date, param_to_date, param_area_id
OVC_BY_GEO_WARDS_IN_CONST_SQL= "    SELECT qry_rep_ovc_geo_in_const.ward_id as \"Ward code\", qry_rep_ovc_geo_in_const.ward_name as \"Ward\", Count(*) AS \"Number of OVC living in area as of end of period\", Sum(qry_rep_ovc_geo_in_const.received_services) AS \"Number of OVC living in area who have received services or referrals during period\"     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="    FROM     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        (SELECT qry_persons_type_todate_ovc.person_id, qry_persons_geo_todate_s.area_id AS ward_id, wards.area_name AS ward_name, wards.parent_area_id AS const_id,     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        CASE WHEN (beneficiary_person_id is not null) THEN 1 ELSE 0 END AS received_services     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        FROM ((     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            (SELECT tbl_reg_persons.id AS person_id     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            ) as qry_persons_type_todate_ovc     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        INNER JOIN     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            FROM tbl_reg_persons_geo     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            ) as qry_persons_geo_todate_s     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_persons_geo_todate_s.person_id)     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        LEFT JOIN     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            (SELECT DISTINCT qry_core_services_referrals_in_period_benef_a.beneficiary_person_id     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            FROM     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="                (SELECT tbl_core_services.beneficiary_person_id     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="                FROM tbl_core_services     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="                WHERE tbl_core_services.encounter_date>=%s  And tbl_core_services.encounter_date<=%s     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="                ) as qry_core_services_referrals_in_period_benef_a     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="            ) as qry_core_services_referrals_in_period_benef_b     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_core_services_referrals_in_period_benef_b.beneficiary_person_id)     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        INNER JOIN tbl_list_geo AS wards ON qry_persons_geo_todate_s.area_id = wards.area_id     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        WHERE wards.parent_area_id=%s     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="        ) as qry_rep_ovc_geo_in_const     "
OVC_BY_GEO_WARDS_IN_CONST_SQL+="    GROUP BY qry_rep_ovc_geo_in_const.ward_id, qry_rep_ovc_geo_in_const.ward_name     "
# qry_persons_type_todate_ovc gets people who are of OVC type
# qry_persons_geo_todate_s gets the ward lived in at the end of the period
# qry_core_services_referrals_in_period_benef_a and qry_core_services_referrals_in_period_benef_b get list of beneficiaries who received services during the period
# qry_rep_ovc_geo_in_const establishes of the OVC living in the ward, which ones received services.  It also looks up the parent constituency of the wards.
# final query counts and sums by ward

#Wards in a district     "
#Parameters expected: param_to_date, param_to_date, param_to_date, param_to_date, param_from_date, param_to_date, param_area_id
OVC_BY_GEO_WARDS_IN_DIST_SQL= "    SELECT qry_rep_ovc_geo_in_dist.const_id as \"Constituency code\", qry_rep_ovc_geo_in_dist.const_name as \"Constituency\", qry_rep_ovc_geo_in_dist.ward_id as \"Ward code\", qry_rep_ovc_geo_in_dist.ward_name as \"Ward\", Count(*) AS \"Number of OVC living in area as of end of period\", Sum(qry_rep_ovc_geo_in_dist.received_services) AS \"Number of OVC living in area who have received services or referrals during period\"     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="    FROM     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        (SELECT qry_persons_type_todate_ovc.person_id, qry_persons_geo_todate_s.area_id AS ward_id, wards.area_name AS ward_name, constituencies.area_id AS const_id, constituencies.area_name AS const_name,     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        CASE WHEN (beneficiary_person_id is not null) THEN 1 ELSE 0 END AS received_services     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        FROM (((     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            (SELECT tbl_reg_persons.id AS person_id     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            ) as qry_persons_type_todate_ovc     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        INNER JOIN     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            FROM tbl_reg_persons_geo     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            ) as qry_persons_geo_todate_s     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_persons_geo_todate_s.person_id)     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        LEFT JOIN     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            (SELECT DISTINCT qry_core_services_referrals_in_period_benef_a.beneficiary_person_id     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            FROM     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="                (SELECT tbl_core_services.beneficiary_person_id     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="                FROM tbl_core_services     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="                WHERE tbl_core_services.encounter_date>=%s  And tbl_core_services.encounter_date<=%s     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="                ) as qry_core_services_referrals_in_period_benef_a     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="            ) as qry_core_services_referrals_in_period_benef_b     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_core_services_referrals_in_period_benef_b.beneficiary_person_id)     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        INNER JOIN tbl_list_geo AS wards ON qry_persons_geo_todate_s.area_id = wards.area_id)     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        INNER JOIN tbl_list_geo AS constituencies ON wards.parent_area_id = constituencies.area_id     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        WHERE constituencies.parent_area_id=%s     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="        ) as qry_rep_ovc_geo_in_dist     "
OVC_BY_GEO_WARDS_IN_DIST_SQL+="    GROUP BY qry_rep_ovc_geo_in_dist.const_id, qry_rep_ovc_geo_in_dist.const_name, qry_rep_ovc_geo_in_dist.ward_id, qry_rep_ovc_geo_in_dist.ward_name     "
# qry_persons_type_todate_ovc gets people who are of OVC type
# qry_persons_geo_todate_s gets the ward lived in at the end of the period
# qry_core_services_referrals_in_period_benef_a and qry_core_services_referrals_in_period_benef_b get list of beneficiaries who received services during the period
# qry_rep_ovc_geo_in_dist  establishes of the OVC living in the ward, which ones received services.  It also looks up the parent constituency and district of the wards.
# final query counts and sums by ward

#Constituencies in a district     "
#Parameters expected: param_to_date, param_to_date, param_to_date, param_to_date, param_from_date, param_to_date, param_area_id
OVC_BY_GEO_CONST_IN_DIST_SQL= "    SELECT qry_rep_ovc_geo_in_dist.const_id as \"Constituency code\", qry_rep_ovc_geo_in_dist.const_name as \"Constituency\", Count(*) AS \"Number of OVC living in area as of end of period\", Sum(qry_rep_ovc_geo_in_dist.received_services) AS \"Number of OVC living in area who have received services or referrals during period\"     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="    FROM     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        (SELECT qry_persons_type_todate_ovc.person_id, qry_persons_geo_todate_s.area_id AS ward_id, wards.area_name AS ward_name, constituencies.area_id AS const_id, constituencies.area_name AS const_name,     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        CASE WHEN (beneficiary_person_id is not null) THEN 1 ELSE 0 END AS received_services     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        FROM (((     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            (SELECT tbl_reg_persons.id AS person_id     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            ) as qry_persons_type_todate_ovc     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        INNER JOIN     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            FROM tbl_reg_persons_geo     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            ) as qry_persons_geo_todate_s     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_persons_geo_todate_s.person_id)     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        LEFT JOIN     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            (SELECT DISTINCT qry_core_services_referrals_in_period_benef_a.beneficiary_person_id     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            FROM     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="                (SELECT tbl_core_services.beneficiary_person_id     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="                FROM tbl_core_services     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="                WHERE tbl_core_services.encounter_date>=%s  And tbl_core_services.encounter_date<=%s     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="                ) as qry_core_services_referrals_in_period_benef_a     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="            ) as qry_core_services_referrals_in_period_benef_b     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_core_services_referrals_in_period_benef_b.beneficiary_person_id)     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        INNER JOIN tbl_list_geo AS wards ON qry_persons_geo_todate_s.area_id = wards.area_id)     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        INNER JOIN tbl_list_geo AS constituencies ON wards.parent_area_id = constituencies.area_id     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        WHERE constituencies.parent_area_id=%s     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="        ) as qry_rep_ovc_geo_in_dist     "
OVC_BY_GEO_CONST_IN_DIST_SQL+="    GROUP BY qry_rep_ovc_geo_in_dist.const_id, qry_rep_ovc_geo_in_dist.const_name     "
# qry_persons_type_todate_ovc gets people who are of OVC type
# qry_persons_geo_todate_s gets the ward lived in at the end of the period
# qry_core_services_referrals_in_period_benef_a and qry_core_services_referrals_in_period_benef_b get list of beneficiaries who received services during the period
# qry_rep_ovc_geo_in_dist  establishes of the OVC living in the ward, which ones received services.  It also looks up the parent constituency and district of the wards.
# final query counts and sums by constituency
     
     
     
#Constituencies in a province     "
#Parameters expected: param_to_date, param_to_date, param_to_date, param_to_date, param_from_date, param_to_date, param_area_id
OVC_BY_GEO_CONST_IN_PROV_SQL= "    SELECT qry_rep_ovc_geo_in_prov.const_id as \"Constituency code\", qry_rep_ovc_geo_in_prov.const_name as \"Constituency\", Count(*) AS \"Number of OVC living in area as of end of period\", Sum(qry_rep_ovc_geo_in_prov.received_services) AS \"Number of OVC living in area who have received services or referrals during period\"     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="    FROM     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        (SELECT qry_persons_type_todate_ovc.person_id, constituencies.area_id AS const_id, constituencies.area_name AS const_name, districts.area_id AS district_id, districts.area_name AS district_name,     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        CASE WHEN (beneficiary_person_id is not null) THEN 1 ELSE 0 END AS received_services     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        FROM ((((     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            (SELECT tbl_reg_persons.id AS person_id     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            ) as qry_persons_type_todate_ovc     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        INNER JOIN     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            FROM tbl_reg_persons_geo     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            ) as qry_persons_geo_todate_s     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_persons_geo_todate_s.person_id)     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        LEFT JOIN     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            (SELECT DISTINCT qry_core_services_referrals_in_period_benef_a.beneficiary_person_id     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            FROM     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="                (SELECT tbl_core_services.beneficiary_person_id     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="                FROM tbl_core_services     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="                WHERE tbl_core_services.encounter_date>=%s  And tbl_core_services.encounter_date<=%s     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="                ) as qry_core_services_referrals_in_period_benef_a     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="            ) as qry_core_services_referrals_in_period_benef_b     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_core_services_referrals_in_period_benef_b.beneficiary_person_id)     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        INNER JOIN tbl_list_geo AS wards ON qry_persons_geo_todate_s.area_id = wards.area_id)     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        INNER JOIN tbl_list_geo AS constituencies ON wards.parent_area_id = constituencies.area_id)     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        INNER JOIN tbl_list_geo AS districts ON constituencies.parent_area_id = districts.area_id     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        WHERE districts.parent_area_id=%s     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="        ) as qry_rep_ovc_geo_in_prov     "
OVC_BY_GEO_CONST_IN_PROV_SQL+="    GROUP BY qry_rep_ovc_geo_in_prov.const_id, qry_rep_ovc_geo_in_prov.const_name     "
# qry_persons_type_todate_ovc gets people who are of OVC type
# qry_persons_geo_todate_s gets the ward lived in at the end of the period
# qry_core_services_referrals_in_period_benef_a and qry_core_services_referrals_in_period_benef_b get list of beneficiaries who received services during the period
# qry_rep_ovc_geo_in_prov  establishes of the OVC living in the ward, which ones received services.  It also looks up the parent constituency, district and province of the wards.
# final query counts and sums by constituency

     
#Districts in a province     "
#Parameters expected: param_to_date, param_to_date, param_to_date, param_to_date, param_from_date, param_to_date, param_area_id
OVC_BY_GEO_DIST_IN_PROV_SQL= "    SELECT qry_rep_ovc_geo_in_prov.district_id as \"District code\", qry_rep_ovc_geo_in_prov.district_name as \"District\", Count(*) AS \"Number of OVC living in area as of end of period\", Sum(qry_rep_ovc_geo_in_prov.received_services) AS \"Number of OVC living in area who have received services or referrals during period\"     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="    FROM     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        (SELECT qry_persons_type_todate_ovc.person_id, constituencies.area_id AS const_id, constituencies.area_name AS const_name, districts.area_id AS district_id, districts.area_name AS district_name,     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        CASE WHEN (beneficiary_person_id is not null) THEN 1 ELSE 0 END AS received_services     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        FROM ((((     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            (SELECT tbl_reg_persons.id AS person_id     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            ) as qry_persons_type_todate_ovc     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        INNER JOIN     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            FROM tbl_reg_persons_geo     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            ) as qry_persons_geo_todate_s     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_persons_geo_todate_s.person_id)     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        LEFT JOIN     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            (SELECT DISTINCT qry_core_services_referrals_in_period_benef_a.beneficiary_person_id     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="            FROM     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="                (SELECT tbl_core_services.beneficiary_person_id     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="                FROM tbl_core_services     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="                WHERE tbl_core_services.encounter_date>=%s  And tbl_core_services.encounter_date<=%s     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="                ) as qry_core_services_referrals_in_period_benef_a     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        ) as qry_core_services_referrals_in_period_benef_b     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_core_services_referrals_in_period_benef_b.beneficiary_person_id)     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        INNER JOIN tbl_list_geo AS wards ON qry_persons_geo_todate_s.area_id = wards.area_id)     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        INNER JOIN tbl_list_geo AS constituencies ON wards.parent_area_id = constituencies.area_id)     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        INNER JOIN tbl_list_geo AS districts ON constituencies.parent_area_id = districts.area_id     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        WHERE districts.parent_area_id=%s     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="        ) as qry_rep_ovc_geo_in_prov     "
OVC_BY_GEO_DIST_IN_PROV_SQL+="    GROUP BY qry_rep_ovc_geo_in_prov.district_id, qry_rep_ovc_geo_in_prov.district_name     "
# qry_persons_type_todate_ovc gets people who are of OVC type
# qry_persons_geo_todate_s gets the ward lived in at the end of the period
# qry_core_services_referrals_in_period_benef_a and qry_core_services_referrals_in_period_benef_b get list of beneficiaries who received services during the period
# qry_rep_ovc_geo_in_prov  establishes of the OVC living in the ward, which ones received services.  It also looks up the parent constituency, district and province of the wards.
# final query counts and sums by district
     
#Districts in whole country     "
#Parameters expected: param_to_date, param_to_date, param_to_date, param_to_date, param_from_date, param_to_date
OVC_BY_GEO_DIST_ALL_SQL= "    SELECT qry_rep_ovc_geo_in_all.province_id as \"Province code\", qry_rep_ovc_geo_in_all.province_name as \"Province\", qry_rep_ovc_geo_in_all.district_id as \"District code\", qry_rep_ovc_geo_in_all.district_name as \"District\", Count(*) AS \"Number of OVC living in area as of end of period\", Sum(qry_rep_ovc_geo_in_all.received_services) AS \"Number of OVC living in area who have received services or referrals during period\"     "
OVC_BY_GEO_DIST_ALL_SQL+="    FROM     "
OVC_BY_GEO_DIST_ALL_SQL+="        (SELECT qry_persons_type_todate_ovc.person_id, constituencies.area_id AS const_id, constituencies.area_name AS const_name, districts.area_id AS district_id, districts.area_name AS district_name, provinces.area_id AS province_id, provinces.area_name AS province_name,     "
OVC_BY_GEO_DIST_ALL_SQL+="        CASE WHEN (beneficiary_person_id is not null) THEN 1 ELSE 0 END AS received_services     "
OVC_BY_GEO_DIST_ALL_SQL+="        FROM (((((     "
OVC_BY_GEO_DIST_ALL_SQL+="            (SELECT tbl_reg_persons.id AS person_id     "
OVC_BY_GEO_DIST_ALL_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id     "
OVC_BY_GEO_DIST_ALL_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False     "
OVC_BY_GEO_DIST_ALL_SQL+="            ) as qry_persons_type_todate_ovc     "
OVC_BY_GEO_DIST_ALL_SQL+="        INNER JOIN     "
OVC_BY_GEO_DIST_ALL_SQL+="            (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id     "
OVC_BY_GEO_DIST_ALL_SQL+="            FROM tbl_reg_persons_geo     "
OVC_BY_GEO_DIST_ALL_SQL+="            WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)     "
OVC_BY_GEO_DIST_ALL_SQL+="            ) as qry_persons_geo_todate_s     "
OVC_BY_GEO_DIST_ALL_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_persons_geo_todate_s.person_id)     "
OVC_BY_GEO_DIST_ALL_SQL+="        LEFT JOIN     "
OVC_BY_GEO_DIST_ALL_SQL+="            (SELECT DISTINCT qry_core_services_referrals_in_period_benef_a.beneficiary_person_id     "
OVC_BY_GEO_DIST_ALL_SQL+="            FROM     "
OVC_BY_GEO_DIST_ALL_SQL+="                (SELECT tbl_core_services.beneficiary_person_id     "
OVC_BY_GEO_DIST_ALL_SQL+="                FROM tbl_core_services     "
OVC_BY_GEO_DIST_ALL_SQL+="                WHERE tbl_core_services.encounter_date>=%s  And tbl_core_services.encounter_date<=%s     "
OVC_BY_GEO_DIST_ALL_SQL+="                ) as qry_core_services_referrals_in_period_benef_a     "
OVC_BY_GEO_DIST_ALL_SQL+="            ) as qry_core_services_referrals_in_period_benef_b     "
OVC_BY_GEO_DIST_ALL_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_core_services_referrals_in_period_benef_b.beneficiary_person_id)     "
OVC_BY_GEO_DIST_ALL_SQL+="        INNER JOIN tbl_list_geo AS wards ON qry_persons_geo_todate_s.area_id = wards.area_id)     "
OVC_BY_GEO_DIST_ALL_SQL+="        INNER JOIN tbl_list_geo AS constituencies ON wards.parent_area_id = constituencies.area_id)     "
OVC_BY_GEO_DIST_ALL_SQL+="        INNER JOIN tbl_list_geo AS districts ON constituencies.parent_area_id = districts.area_id)     "
OVC_BY_GEO_DIST_ALL_SQL+="        INNER JOIN tbl_list_geo AS provinces ON districts.parent_area_id = provinces.area_id     "
OVC_BY_GEO_DIST_ALL_SQL+="        ) as qry_rep_ovc_geo_in_all     "
OVC_BY_GEO_DIST_ALL_SQL+="    GROUP BY qry_rep_ovc_geo_in_all.province_id, qry_rep_ovc_geo_in_all.province_name, qry_rep_ovc_geo_in_all.district_id, qry_rep_ovc_geo_in_all.district_name     "
# qry_persons_type_todate_ovc gets people who are of OVC type
# qry_persons_geo_todate_s gets the ward lived in at the end of the period
# qry_core_services_referrals_in_period_benef_a and qry_core_services_referrals_in_period_benef_b get list of beneficiaries who received services during the period
# qry_rep_ovc_geo_in_all  establishes of the OVC living in the ward, which ones received services.  It also looks up the parent constituency, district and province of the wards.
# final query counts and sums by district     



     
#Provinces in whole country     
#Parameters expected: param_to_date, param_to_date, param_to_date, param_to_date, param_from_date, param_to_date
OVC_BY_GEO_PROV_ALL_SQL= "    SELECT qry_rep_ovc_geo_in_all.province_id as \"Province code\", qry_rep_ovc_geo_in_all.province_name as \"Province\", Count(*) AS \"Number of OVC living in area as of end of period\", Sum(qry_rep_ovc_geo_in_all.received_services) AS \"Number of OVC living in area who have received services or referrals during period\"     "
OVC_BY_GEO_PROV_ALL_SQL+="    FROM     "
OVC_BY_GEO_PROV_ALL_SQL+="        (SELECT qry_persons_type_todate_ovc.person_id, constituencies.area_id AS const_id, constituencies.area_name AS const_name, districts.area_id AS district_id, districts.area_name AS district_name, provinces.area_id AS province_id, provinces.area_name AS province_name,     "
OVC_BY_GEO_PROV_ALL_SQL+="        CASE WHEN (beneficiary_person_id is not null) THEN 1 ELSE 0 END AS received_services     "
OVC_BY_GEO_PROV_ALL_SQL+="        FROM (((((     "
OVC_BY_GEO_PROV_ALL_SQL+="                (SELECT tbl_reg_persons.id AS person_id     "
OVC_BY_GEO_PROV_ALL_SQL+="                FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id     "
OVC_BY_GEO_PROV_ALL_SQL+="                WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False     "
OVC_BY_GEO_PROV_ALL_SQL+="                ) as qry_persons_type_todate_ovc     "
OVC_BY_GEO_PROV_ALL_SQL+="        INNER JOIN     "
OVC_BY_GEO_PROV_ALL_SQL+="            (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id     "
OVC_BY_GEO_PROV_ALL_SQL+="            FROM tbl_reg_persons_geo     "
OVC_BY_GEO_PROV_ALL_SQL+="            WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)     "
OVC_BY_GEO_PROV_ALL_SQL+="            ) as qry_persons_geo_todate_s     "
OVC_BY_GEO_PROV_ALL_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_persons_geo_todate_s.person_id)     "
OVC_BY_GEO_PROV_ALL_SQL+="        LEFT JOIN     "
OVC_BY_GEO_PROV_ALL_SQL+="            (SELECT DISTINCT qry_core_services_referrals_in_period_benef_a.beneficiary_person_id     "
OVC_BY_GEO_PROV_ALL_SQL+="            FROM     "
OVC_BY_GEO_PROV_ALL_SQL+="                (SELECT tbl_core_services.beneficiary_person_id     "
OVC_BY_GEO_PROV_ALL_SQL+="                FROM tbl_core_services     "
OVC_BY_GEO_PROV_ALL_SQL+="                WHERE tbl_core_services.encounter_date>=%s  And tbl_core_services.encounter_date<=%s     "
OVC_BY_GEO_PROV_ALL_SQL+="                ) as qry_core_services_referrals_in_period_benef_a     "
OVC_BY_GEO_PROV_ALL_SQL+="            ) as qry_core_services_referrals_in_period_benef_b     "
OVC_BY_GEO_PROV_ALL_SQL+="        ON qry_persons_type_todate_ovc.person_id = qry_core_services_referrals_in_period_benef_b.beneficiary_person_id)     "
OVC_BY_GEO_PROV_ALL_SQL+="        INNER JOIN tbl_list_geo AS wards ON qry_persons_geo_todate_s.area_id = wards.area_id)     "
OVC_BY_GEO_PROV_ALL_SQL+="        INNER JOIN tbl_list_geo AS constituencies ON wards.parent_area_id = constituencies.area_id)     "
OVC_BY_GEO_PROV_ALL_SQL+="        INNER JOIN tbl_list_geo AS districts ON constituencies.parent_area_id = districts.area_id)     "
OVC_BY_GEO_PROV_ALL_SQL+="        INNER JOIN tbl_list_geo AS provinces ON districts.parent_area_id = provinces.area_id     "
OVC_BY_GEO_PROV_ALL_SQL+="        ) as qry_rep_ovc_geo_in_all     "
OVC_BY_GEO_PROV_ALL_SQL+="    GROUP BY qry_rep_ovc_geo_in_all.province_id, qry_rep_ovc_geo_in_all.province_name     "
# qry_persons_type_todate_ovc gets people who are of OVC type
# qry_persons_geo_todate_s gets the ward lived in at the end of the period
# qry_core_services_referrals_in_period_benef_a and qry_core_services_referrals_in_period_benef_b get list of beneficiaries who received services during the period
# qry_rep_ovc_geo_in_all  establishes of the OVC living in the ward, which ones received services.  It also looks up the parent constituency, district and province of the wards.
# final query counts and sums by province     

#*******ADVERSE CONDITIONS (ALL)**********

#Parameters expected: none
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL= "    SELECT qry_rep_adverse_cond.adverse_condition_id as \"Adverse condition code\", tbl_list_general.item_description AS \"Adverse condition\", Count(*) AS \"Number of OVC who have ever experienced\"                            "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="    FROM                             "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="            (SELECT qry_adverse_conditions_unique.beneficiary_person_id, qry_adverse_conditions_unique.adverse_condition_id                    "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="            FROM                     "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="                    (SELECT tbl_reg_persons.id AS person_id            "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="                    FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id            "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="                    WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False            "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="                    ) as qry_persons_type_ovc_s             "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="            INNER JOIN                     "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="                    (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id            "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="                    FROM tbl_core_adverse_conditions            "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="                    ) as qry_adverse_conditions_unique             "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="            ON qry_persons_type_ovc_s.person_id = qry_adverse_conditions_unique.beneficiary_person_id                    "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="            ) as qry_rep_adverse_cond                     "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="    INNER JOIN tbl_list_general ON qry_rep_adverse_cond.adverse_condition_id = tbl_list_general.item_id                            "
OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL+="    GROUP BY qry_rep_adverse_cond.adverse_condition_id, tbl_list_general.item_description                            "
#qry_persons_type_ovc_s gets OVCs only
#qry_adverse_conditions_unique ensures each adverse condition counted only once for each beneficiary
#qry_rep_adverse_cond puts everything together
#final query looks up adverse condition description and counts


#param_area_id specified, is a ward
#Parameters expected: param_area_id
OVC_ADVERSE_COND_GEO_WARD_SQL= "    SELECT qry_rep_adverse_cond_geo_0.adverse_condition_id AS \"Adverse condition code\", tbl_list_general.item_description AS \"Adverse condition\", Count(*) AS \"Number of OVC who have ever experienced\"    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="    FROM     "
OVC_ADVERSE_COND_GEO_WARD_SQL+="            (SELECT qry_adverse_conditions_unique.beneficiary_person_id, qry_adverse_conditions_unique.adverse_condition_id    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="            FROM (            "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            (SELECT tbl_reg_persons.id AS person_id    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            ) as qry_persons_type_ovc_s     "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                    INNER JOIN     "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            FROM tbl_core_adverse_conditions    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            ) as qry_adverse_conditions_unique     "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                    ON qry_persons_type_ovc_s.person_id = qry_adverse_conditions_unique.beneficiary_person_id)     "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                    INNER JOIN     "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            FROM tbl_reg_persons_geo    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            WHERE tbl_reg_persons_geo.area_id=%s AND tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                            ) as qry_persons_geo_active_sp     "
OVC_ADVERSE_COND_GEO_WARD_SQL+="                    ON qry_persons_type_ovc_s.person_id = qry_persons_geo_active_sp.person_id    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="            ) as qry_rep_adverse_cond_geo_0    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="    INNER JOIN tbl_list_general ON qry_rep_adverse_cond_geo_0.adverse_condition_id = tbl_list_general.item_id    "
OVC_ADVERSE_COND_GEO_WARD_SQL+="    GROUP BY qry_rep_adverse_cond_geo_0.adverse_condition_id, tbl_list_general.item_description    "
#qry_persons_type_ovc_s gets OVCs only
#qry_adverse_conditions_unique ensures each adverse condition counted only once for each beneficiary
#qry_persons_geo_active_sp gets only beneficiaries currently living in selected ward
#qry_rep_adverse_cond_geo_0 puts everything together
#final query looks up adverse condition description and counts


#param_area_id specified, is a constituency
#Parameters expected: param_area_id
OVC_ADVERSE_COND_GEO_CONST_SQL= "    SELECT qry_rep_adverse_cond_geo_1.adverse_condition_id AS \"Adverse condition code\", tbl_list_general.item_description AS \"Adverse condition\", Count(*) AS \"Number of OVC who have ever experienced\"                               "
OVC_ADVERSE_COND_GEO_CONST_SQL+="    FROM                             "
OVC_ADVERSE_COND_GEO_CONST_SQL+="            (SELECT qry_adverse_conditions_unique.beneficiary_person_id, qry_adverse_conditions_unique.adverse_condition_id                    "
OVC_ADVERSE_COND_GEO_CONST_SQL+="            FROM (                    "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    (SELECT tbl_reg_persons.id AS person_id            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    ) as qry_persons_type_ovc_s             "
OVC_ADVERSE_COND_GEO_CONST_SQL+="            INNER JOIN                     "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    FROM tbl_core_adverse_conditions            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    ) as qry_adverse_conditions_unique             "
OVC_ADVERSE_COND_GEO_CONST_SQL+="            ON qry_persons_type_ovc_s.person_id = qry_adverse_conditions_unique.beneficiary_person_id)                     "
OVC_ADVERSE_COND_GEO_CONST_SQL+="            INNER JOIN (                    "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    FROM tbl_reg_persons_geo            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    WHERE tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="                    ) as qry_persons_geo_active             "
OVC_ADVERSE_COND_GEO_CONST_SQL+="            INNER JOIN tbl_list_geo ON qry_persons_geo_active.area_id = tbl_list_geo.area_id)                     "
OVC_ADVERSE_COND_GEO_CONST_SQL+="            ON qry_persons_type_ovc_s.person_id = qry_persons_geo_active.person_id                    "
OVC_ADVERSE_COND_GEO_CONST_SQL+="    WHERE tbl_list_geo.parent_area_id=%s                           "
OVC_ADVERSE_COND_GEO_CONST_SQL+="    ) as qry_rep_adverse_cond_geo_1                             "
OVC_ADVERSE_COND_GEO_CONST_SQL+="    INNER JOIN tbl_list_general ON qry_rep_adverse_cond_geo_1.adverse_condition_id = tbl_list_general.item_id                            "
OVC_ADVERSE_COND_GEO_CONST_SQL+="    GROUP BY qry_rep_adverse_cond_geo_1.adverse_condition_id, tbl_list_general.item_description                            "
#qry_persons_type_ovc_s gets OVCs only
#qry_adverse_conditions_unique ensures each adverse condition counted only once for each beneficiary
#qry_persons_geo_active gets current wards of beneficiaries
#qry_rep_adverse_cond_geo_1 selects only beneficiaries in wards within selected constituency, and puts everything together
#final query looks up adverse condition description and counts


#param_area_id specified, is a district
#Parameters expected: param_area_id
OVC_ADVERSE_COND_GEO_DIST_SQL= "    SELECT qry_rep_adverse_cond_geo_2.adverse_condition_id  AS \"Adverse condition code\",tbl_list_general.item_description AS \"Adverse condition\",Count(*) AS \"Number of OVC who have ever experienced\"    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="    FROM    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            (SELECT qry_adverse_conditions_unique.beneficiary_person_id,qry_adverse_conditions_unique.adverse_condition_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            FROM    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    (SELECT tbl_reg_persons.id AS person_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    ON tbl_reg_persons.id=tbl_reg_persons_types.person_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended IS NULL AND tbl_reg_persons.is_void=false AND tbl_reg_persons_types.is_void=false    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    ) AS qry_persons_type_ovc_s    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            INNER JOIN     "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id,tbl_core_adverse_conditions.adverse_condition_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    FROM tbl_core_adverse_conditions    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    ) AS qry_adverse_conditions_unique    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            ON qry_persons_type_ovc_s.person_id=qry_adverse_conditions_unique.beneficiary_person_id)    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            INNER JOIN (     "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    (SELECT tbl_reg_persons_geo.person_id,tbl_reg_persons_geo.area_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    FROM tbl_reg_persons_geo    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    WHERE tbl_reg_persons_geo.date_delinked IS NULL AND tbl_reg_persons_geo.is_void=false    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    ) AS qry_persons_geo_active    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            INNER JOIN     "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    (SELECT tbl_list_geo_1.area_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    ON tbl_list_geo.area_id=tbl_list_geo_1.parent_area_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="                    ) AS qry_heir_sub_sub_areas    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            ON qry_persons_geo_active.area_id=qry_heir_sub_sub_areas.area_id)    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            ON qry_persons_type_ovc_s.person_id=qry_persons_geo_active.person_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="            ) AS qry_rep_adverse_cond_geo_2    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="    INNER JOIN tbl_list_general ON qry_rep_adverse_cond_geo_2.adverse_condition_id=tbl_list_general.item_id    "
OVC_ADVERSE_COND_GEO_DIST_SQL+="    GROUP BY qry_rep_adverse_cond_geo_2.adverse_condition_id,tbl_list_general.item_description    "
#qry_persons_type_ovc_s gets OVCs only
#qry_adverse_conditions_unique ensures each adverse condition counted only once for each beneficiary
#qry_persons_geo_active gets current wards of beneficiaries
#qry_heir_sub_sub_areas looks up which wards are in selected district
#qry_rep_adverse_cond_geo_2 puts everything together
#final query looks up adverse condition description and counts


#param_area_id specified, is a province
#Parameters expected: param_area_id
OVC_ADVERSE_COND_GEO_PROV_SQL= "    SELECT qry_rep_adverse_cond_geo_3.adverse_condition_id  AS \"Adverse condition code\", tbl_list_general.item_description AS \"Adverse condition\", Count(*) AS \"Number of OVC who have ever experienced\"                    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="            FROM             "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    (SELECT qry_adverse_conditions_unique.beneficiary_person_id, qry_adverse_conditions_unique.adverse_condition_id    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    FROM (    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            (SELECT tbl_reg_persons.id AS person_id    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended IS NULL AND tbl_reg_persons.is_void=false AND tbl_reg_persons_types.is_void=false    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            ) as qry_persons_type_ovc_s     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    INNER JOIN     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            FROM tbl_core_adverse_conditions    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            ) as qry_adverse_conditions_unique     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    ON qry_persons_type_ovc_s.person_id = qry_adverse_conditions_unique.beneficiary_person_id)     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    INNER JOIN (    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            FROM tbl_reg_persons_geo    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            WHERE tbl_reg_persons_geo.date_delinked IS NULL AND tbl_reg_persons_geo.is_void=false    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            ) as qry_persons_geo_active     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    INNER JOIN     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            (SELECT tbl_list_geo_2.area_id    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            FROM (tbl_list_geo     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                            ) as qry_heir_sub_sub_sub_areas     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    ON qry_persons_geo_active.area_id = qry_heir_sub_sub_sub_areas.area_id)     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    ON qry_persons_type_ovc_s.person_id = qry_persons_geo_active.person_id    "
OVC_ADVERSE_COND_GEO_PROV_SQL+="                    ) as qry_rep_adverse_cond_geo_3     "
OVC_ADVERSE_COND_GEO_PROV_SQL+="            INNER JOIN tbl_list_general ON qry_rep_adverse_cond_geo_3.adverse_condition_id = tbl_list_general.item_id            "
OVC_ADVERSE_COND_GEO_PROV_SQL+="    GROUP BY qry_rep_adverse_cond_geo_3.adverse_condition_id, tbl_list_general.item_description                    "
#qry_persons_type_ovc_s gets OVCs only
#qry_adverse_conditions_unique ensures each adverse condition counted only once for each beneficiary
#qry_persons_geo_active gets current wards of beneficiaries
#qry_heir_sub_sub_sub_areas looks up which wards are in selected province
#qry_rep_adverse_cond_geo_3 puts everything together
#final query looks up adverse condition description and counts



#param_org_unit_id specified
#parameter expected: param_org_unit_id
OVC_ADVERSE_COND_ORG_UNIT_SQL= "    SELECT qry_rep_adverse_cond_org.adverse_condition_id  AS \"Adverse condition code\", tbl_list_general.item_description AS \"Adverse condition\", Count(*) AS \"Number of OVC who have ever experienced\"                           "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="    FROM                             "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="            (SELECT qry_adverse_conditions_unique.beneficiary_person_id, qry_adverse_conditions_unique.adverse_condition_id                    "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="            FROM                     "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    (SELECT tbl_reg_persons.id AS person_id            "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id            "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended IS NULL AND tbl_reg_persons.is_void=false AND tbl_reg_persons_types.is_void=false            "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    ) as qry_persons_type_ovc_s             "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="            INNER JOIN (                    "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id            "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    FROM tbl_core_adverse_conditions            "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    ) as qry_adverse_conditions_unique             "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="            INNER JOIN                     "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    (SELECT DISTINCT tbl_core_encounters.org_unit_id, tbl_core_encounters.beneficiary_person_id            "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="                    FROM tbl_core_encounters) as qry_core_orgs_beneficiaries             "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="            ON qry_adverse_conditions_unique.beneficiary_person_id = qry_core_orgs_beneficiaries.beneficiary_person_id)                     "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="            ON qry_persons_type_ovc_s.person_id = qry_core_orgs_beneficiaries.beneficiary_person_id                    "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="            WHERE qry_core_orgs_beneficiaries.org_unit_id=%s                    "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="            ) as qry_rep_adverse_cond_org                    "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="    INNER JOIN tbl_list_general ON qry_rep_adverse_cond_org.adverse_condition_id = tbl_list_general.item_id                            "
OVC_ADVERSE_COND_ORG_UNIT_SQL+="    GROUP BY qry_rep_adverse_cond_org.adverse_condition_id, tbl_list_general.item_description                            "
#qry_persons_type_ovc_s gets OVCs only
#qry_adverse_conditions_unique ensures each adverse condition counted only once for each beneficiary
#qry_core_orgs_beneficiaries gets org units ever served the beneficiaries
#qry_rep_adverse_cond_org puts everything together and filters for only the selected org unit 
#final query looks up adverse condition description and counts


#set_id specified
#parameter expected: set_id
OVC_ADVERSE_COND_SET_SQL= "    SELECT qry_rep_adverse_cond_set.adverse_condition_id  AS \"Adverse condition code\", tbl_list_general.item_description AS \"Adverse condition\", Count(*) AS \"Number of OVC who have ever experienced\"                           "
OVC_ADVERSE_COND_SET_SQL+="    FROM                             "
OVC_ADVERSE_COND_SET_SQL+="            (SELECT qry_adverse_conditions_unique.beneficiary_person_id, qry_adverse_conditions_unique.adverse_condition_id                    "
OVC_ADVERSE_COND_SET_SQL+="            FROM (                    "
OVC_ADVERSE_COND_SET_SQL+="                    (SELECT tbl_reg_persons.id AS person_id            "
OVC_ADVERSE_COND_SET_SQL+="                    FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id            "
OVC_ADVERSE_COND_SET_SQL+="                    WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended IS NULL AND tbl_reg_persons.is_void=false AND tbl_reg_persons_types.is_void=false            "
OVC_ADVERSE_COND_SET_SQL+="                    ) as qry_persons_type_ovc_s             "
OVC_ADVERSE_COND_SET_SQL+="            INNER JOIN                     "
OVC_ADVERSE_COND_SET_SQL+="                    (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id            "
OVC_ADVERSE_COND_SET_SQL+="                    FROM tbl_core_adverse_conditions            "
OVC_ADVERSE_COND_SET_SQL+="                    ) as qry_adverse_conditions_unique             "
OVC_ADVERSE_COND_SET_SQL+="            ON qry_persons_type_ovc_s.person_id = qry_adverse_conditions_unique.beneficiary_person_id)                     "
OVC_ADVERSE_COND_SET_SQL+="            INNER JOIN                     "
OVC_ADVERSE_COND_SET_SQL+="                    (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id            "
OVC_ADVERSE_COND_SET_SQL+="                    FROM tbl_core_encounters INNER JOIN             "
OVC_ADVERSE_COND_SET_SQL+="                            (SELECT tbl_reports_sets_org_units.org_unit_id    "
OVC_ADVERSE_COND_SET_SQL+="                            FROM tbl_reports_sets_org_units    "
OVC_ADVERSE_COND_SET_SQL+="                            WHERE tbl_reports_sets_org_units.set_id=%s    "
OVC_ADVERSE_COND_SET_SQL+="                            ) as qry_set_org_units     "
OVC_ADVERSE_COND_SET_SQL+="                    ON tbl_core_encounters.org_unit_id = qry_set_org_units.org_unit_id            "
OVC_ADVERSE_COND_SET_SQL+="                    ) as qry_set_org_units_benef             "
OVC_ADVERSE_COND_SET_SQL+="            ON qry_persons_type_ovc_s.person_id = qry_set_org_units_benef.beneficiary_person_id                    "
OVC_ADVERSE_COND_SET_SQL+="            ) as qry_rep_adverse_cond_set                    "
OVC_ADVERSE_COND_SET_SQL+="    INNER JOIN tbl_list_general ON qry_rep_adverse_cond_set.adverse_condition_id = tbl_list_general.item_id                            "
OVC_ADVERSE_COND_SET_SQL+="    GROUP BY qry_rep_adverse_cond_set.adverse_condition_id, tbl_list_general.item_description                            "
#qry_persons_type_ovc_s gets OVCs only
#qry_adverse_conditions_unique ensures each adverse condition counted only once for each beneficiary
#qry_core_orgs_beneficiaries gets org units ever served the beneficiaries
#qry_set_org_units gets the selected set of org units
#qry_set_org_units_benef gets distinct beneficiaries of the selected org units
#qry_rep_adverse_cond_set puts everything together 
#final query looks up adverse condition description and counts

#*******ADVERSE CONDITIONS (SELECTED)**********

#adverse condition only specified
#parameters expected: item_id
OVC_ADVERSE_COND_ONE_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC who have ever experienced this condition\"                            "
OVC_ADVERSE_COND_ONE_SQL+="    FROM (                            "
OVC_ADVERSE_COND_ONE_SQL+="            (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
OVC_ADVERSE_COND_ONE_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id                    "
OVC_ADVERSE_COND_ONE_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False                    "
OVC_ADVERSE_COND_ONE_SQL+="            ) as qry_persons_type_ovc                     "
OVC_ADVERSE_COND_ONE_SQL+="    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)                             "
OVC_ADVERSE_COND_ONE_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                    "
OVC_ADVERSE_COND_ONE_SQL+="            FROM tbl_core_adverse_conditions                    "
OVC_ADVERSE_COND_ONE_SQL+="            WHERE tbl_core_adverse_conditions.adverse_condition_id=%s                  "
OVC_ADVERSE_COND_ONE_SQL+="            ) as qry_adverse_conditions_unique_one                     "
OVC_ADVERSE_COND_ONE_SQL+="    ON qry_persons_type_ovc.person_id = qry_adverse_conditions_unique_one.beneficiary_person_id                            "
OVC_ADVERSE_COND_ONE_SQL+="    GROUP BY sex, age              "
OVC_ADVERSE_COND_ONE_SQL+="    ORDER BY age, sex              "
# qry_persons_type_ovc gets OVCs only, with their date of birth and sex
# qry_adverse_conditions_unique_one gets records with the selected adverse condition
# final query counts by age and sex


#adverse condition and ward specified
#parameters expected: item_id, area_id
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC who have ever experienced this condition\"                            "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="    FROM ((                            "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id                    "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False                    "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            ) as qry_persons_type_ovc                     "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)                             "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                    "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            FROM tbl_core_adverse_conditions                    "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            WHERE tbl_core_adverse_conditions.adverse_condition_id=%s                   "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            ) as qry_adverse_conditions_unique_one                     "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="    ON qry_persons_type_ovc.person_id = qry_adverse_conditions_unique_one.beneficiary_person_id)                             "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id                    "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            FROM tbl_reg_persons_geo                    "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            WHERE tbl_reg_persons_geo.area_id=%s AND tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False                    "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="            ) as qry_persons_geo_active_sp                     "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="    ON qry_persons_type_ovc.person_id = qry_persons_geo_active_sp.person_id                            "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="    GROUP BY sex, age                            "
OVC_ADVERSE_COND_ONE_GEO_WARD_SQL+="    ORDER BY age, sex                            "
# qry_persons_type_ovc gets OVCs only, with their date of birth and sex
# qry_adverse_conditions_unique_one gets records with the selected adverse condition
# qry_persons_geo_active_sp gets people living in the selected ward
# final query counts by age and sex

#adverse condition and constituency specified
#parameters expected: item_id, area_id

OVC_ADVERSE_COND_ONE_GEO_CONST_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC who have ever experienced this condition\"                            "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    FROM (((                            "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            ) as qry_persons_type_ovc                     "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)                             "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            FROM tbl_core_adverse_conditions                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            WHERE tbl_core_adverse_conditions.adverse_condition_id=%s                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            ) as qry_adverse_conditions_unique_one                     "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    ON qry_persons_type_ovc.person_id = qry_adverse_conditions_unique_one.beneficiary_person_id)                             "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            FROM tbl_reg_persons_geo                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            WHERE tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False                    "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="            ) as qry_persons_geo_active                     "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    ON qry_persons_type_ovc.person_id = qry_persons_geo_active.person_id)                             "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    INNER JOIN tbl_list_geo ON qry_persons_geo_active.area_id = tbl_list_geo.area_id                            "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    GROUP BY parent_area_id, sex, age                            "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    HAVING tbl_list_geo.parent_area_id=%s                            "
OVC_ADVERSE_COND_ONE_GEO_CONST_SQL+="    ORDER BY age, sex                            "
# qry_persons_type_ovc gets OVCs only, with their date of birth and sex
# qry_adverse_conditions_unique_one gets records with the selected adverse condition
# qry_persons_geo_active gets wards where people living
# final query selects only those from the selected constituency, counts by age and sex

#adverse condition and constituency specified
#parameters expected: item_id, area_id

OVC_ADVERSE_COND_ONE_GEO_DIST_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC who have ever experienced this condition\"                            "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    FROM (((                            "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            ) as qry_persons_type_ovc                     "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)                             "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            FROM tbl_core_adverse_conditions                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            WHERE tbl_core_adverse_conditions.adverse_condition_id=%s                   "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            ) as qry_adverse_conditions_unique_one                     "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    ON qry_persons_type_ovc.person_id = qry_adverse_conditions_unique_one.beneficiary_person_id)                             "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            FROM tbl_reg_persons_geo                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            WHERE tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            ) as qry_persons_geo_active                     "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    ON qry_persons_type_ovc.person_id = qry_persons_geo_active.person_id)                             "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            (SELECT tbl_list_geo_1.area_id                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            WHERE tbl_list_geo.parent_area_id=%s                    "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="            ) as qry_heir_sub_sub_areas                     "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    ON qry_persons_geo_active.area_id = qry_heir_sub_sub_areas.area_id                            "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    GROUP BY sex, age                            "
OVC_ADVERSE_COND_ONE_GEO_DIST_SQL+="    ORDER BY age, sex                            "
# qry_persons_type_ovc gets OVCs only, with their date of birth and sex
# qry_adverse_conditions_unique_one gets records with the selected adverse condition
# qry_persons_geo_active gets wards where people living
# qry_heir_sub_sub_areas gets a list of wards within the selected district
# final query counts by age and sex

#adverse condition and district specified
#parameters expected: item_id, area_id

OVC_ADVERSE_COND_ONE_GEO_PROV_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC who have ever experienced this condition\"                        "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    FROM (((                            "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            ) as qry_persons_type_ovc                     "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)                             "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            FROM tbl_core_adverse_conditions                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            WHERE tbl_core_adverse_conditions.adverse_condition_id=%s                  "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            ) as qry_adverse_conditions_unique_one                     "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    ON qry_persons_type_ovc.person_id = qry_adverse_conditions_unique_one.beneficiary_person_id)                             "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            (SELECT tbl_reg_persons_geo.person_id, tbl_reg_persons_geo.area_id                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            FROM tbl_reg_persons_geo                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            WHERE tbl_reg_persons_geo.date_delinked Is Null AND tbl_reg_persons_geo.is_void=False                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            ) as qry_persons_geo_active                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    ON qry_persons_type_ovc.person_id = qry_persons_geo_active.person_id)                             "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            (SELECT tbl_list_geo_2.area_id                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)                     "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id            "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            WHERE tbl_list_geo.parent_area_id=%s                    "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="            ) as qry_heir_sub_sub_sub_areas                     "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    ON qry_persons_geo_active.area_id = qry_heir_sub_sub_sub_areas.area_id                            "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    GROUP BY sex, age                            "
OVC_ADVERSE_COND_ONE_GEO_PROV_SQL+="    ORDER BY age, sex                            "
# qry_persons_type_ovc gets OVCs only, with their date of birth and sex
# qry_adverse_conditions_unique_one gets records with the selected adverse condition
# qry_persons_geo_active gets wards where people living
# qry_heir_sub_sub_sub_areas gets a list of wards within the selected province
# final query counts by age and sex

#adverse condition and org unit
#parameters expected: item_id, org_unit_id
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC who have ever experienced this condition\"                            "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    FROM ((                            "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id                    "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False                    "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            ) as qry_persons_type_ovc                     "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)                             "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                    "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            FROM tbl_core_adverse_conditions                    "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            WHERE tbl_core_adverse_conditions.adverse_condition_id=%s                    "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            ) as qry_adverse_conditions_unique_one                     "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    ON qry_persons_type_ovc.person_id = qry_adverse_conditions_unique_one.beneficiary_person_id)                             "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            (SELECT DISTINCT tbl_core_encounters.org_unit_id, tbl_core_encounters.beneficiary_person_id                    "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            FROM tbl_core_encounters                    "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="            ) as qry_core_orgs_beneficiaries                     "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    ON qry_persons_type_ovc.person_id = qry_core_orgs_beneficiaries.beneficiary_person_id                            "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    GROUP BY qry_core_orgs_beneficiaries.org_unit_id, sex, age                            "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    HAVING qry_core_orgs_beneficiaries.org_unit_id=%s                           "
OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL+="    ORDER BY age, sex                            "
# qry_persons_type_ovc gets OVCs only, with their date of birth and sex
# qry_adverse_conditions_unique_one gets records with the selected adverse condition
# qry_core_orgs_beneficiaries gets which organisations have served which beneficiaries
# final query filters only for selected org, and counts by age and sex




#adverse condition and org unit set
#parameters expected: item_id, set_id

OVC_ADVERSE_COND_ONE_SET_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age , Count(*) AS \"Number of OVC who have ever experienced this condition\"                          "
OVC_ADVERSE_COND_ONE_SET_SQL+="    FROM ((                            "
OVC_ADVERSE_COND_ONE_SET_SQL+="            (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
OVC_ADVERSE_COND_ONE_SET_SQL+="            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id                    "
OVC_ADVERSE_COND_ONE_SET_SQL+="            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_ended Is Null AND tbl_reg_persons.is_void=False AND tbl_reg_persons_types.is_void=False                    "
OVC_ADVERSE_COND_ONE_SET_SQL+="            ) as qry_persons_type_ovc                     "
OVC_ADVERSE_COND_ONE_SET_SQL+="    INNER JOIN tbl_list_general ON qry_persons_type_ovc.sex_id = tbl_list_general.item_id)                             "
OVC_ADVERSE_COND_ONE_SET_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_SET_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                    "
OVC_ADVERSE_COND_ONE_SET_SQL+="            FROM tbl_core_adverse_conditions                    "
OVC_ADVERSE_COND_ONE_SET_SQL+="            WHERE tbl_core_adverse_conditions.adverse_condition_id=%s                   "
OVC_ADVERSE_COND_ONE_SET_SQL+="            ) as qry_adverse_conditions_unique_one                     "
OVC_ADVERSE_COND_ONE_SET_SQL+="    ON qry_persons_type_ovc.person_id = qry_adverse_conditions_unique_one.beneficiary_person_id)                             "
OVC_ADVERSE_COND_ONE_SET_SQL+="    INNER JOIN                             "
OVC_ADVERSE_COND_ONE_SET_SQL+="            (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id                    "
OVC_ADVERSE_COND_ONE_SET_SQL+="            FROM tbl_core_encounters INNER JOIN                     "
OVC_ADVERSE_COND_ONE_SET_SQL+="                    (SELECT tbl_reports_sets_org_units.org_unit_id            "
OVC_ADVERSE_COND_ONE_SET_SQL+="                    FROM tbl_reports_sets_org_units            "
OVC_ADVERSE_COND_ONE_SET_SQL+="                    WHERE tbl_reports_sets_org_units.set_id=%s            "
OVC_ADVERSE_COND_ONE_SET_SQL+="                    ) as qry_set_org_units                                 "
OVC_ADVERSE_COND_ONE_SET_SQL+="            ON tbl_core_encounters.org_unit_id = qry_set_org_units.org_unit_id            "
OVC_ADVERSE_COND_ONE_SET_SQL+="            ) as qry_set_org_units_benef                     "
OVC_ADVERSE_COND_ONE_SET_SQL+="    ON qry_persons_type_ovc.person_id = qry_set_org_units_benef.beneficiary_person_id                            "
OVC_ADVERSE_COND_ONE_SET_SQL+="    GROUP BY sex, age                            "
OVC_ADVERSE_COND_ONE_SET_SQL+="    ORDER BY age, sex                            "

# qry_persons_type_ovc gets OVCs only, with their date of birth and sex
# qry_adverse_conditions_unique_one gets records with the selected adverse condition
# qry_set_org_units  gets selected organisational units
# qry_set_org_units_benef gets distinct beneficiaries of these units
# final query filters only for selected org, and counts by age and sex


#list wards within a specified district
#parameters expected: 'param_to_date', 'param_to_date', 'param_from_date', 'param_to_date', param_area_id, param_area_id
OVC_MIGRATION_0_SQL= "    SELECT qry_rep_ovc_migration_0_all.area_id as \"Ward code\", qry_rep_ovc_migration_0_all.area_name as \"Ward\", Sum(qry_rep_ovc_migration_0_all.migration_frm) AS \"Number of OVC who migrated into area during period\", Sum(qry_rep_ovc_migration_0_all.migration_to) AS \"Number of OVC who migrated out of area during period\"                          "
OVC_MIGRATION_0_SQL+="    FROM                             "
OVC_MIGRATION_0_SQL+="            (SELECT tbl_list_geo.area_id, tbl_list_geo.area_name, cast((area_id_frm=area_id And area_id_frm<>area_id_to) as integer) AS migration_frm, cast((area_id_to=area_id And area_id_frm<>area_id_to) as integer) AS migration_to                    "
OVC_MIGRATION_0_SQL+="            FROM tbl_list_geo,                     "
OVC_MIGRATION_0_SQL+="                    (SELECT persons_geo_from.person_id, persons_geo_from.date_delinked, persons_geo_from.area_id AS area_id_frm, persons_geo_to.area_id AS area_id_to            "
OVC_MIGRATION_0_SQL+="                    FROM ((((            "
OVC_MIGRATION_0_SQL+="                            (SELECT tbl_reg_persons.id AS person_id    "
OVC_MIGRATION_0_SQL+="                            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_MIGRATION_0_SQL+="                            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False    "
OVC_MIGRATION_0_SQL+="                            ) as qry_persons_type_todate_ovc     "
OVC_MIGRATION_0_SQL+="                    INNER JOIN             "
OVC_MIGRATION_0_SQL+="                            (tbl_reg_persons_geo AS persons_geo_from     "
OVC_MIGRATION_0_SQL+="                            INNER JOIN tbl_reg_persons_geo AS persons_geo_to ON (persons_geo_from.date_delinked = persons_geo_to.date_linked) AND (persons_geo_from.person_id = persons_geo_to.person_id))     "
OVC_MIGRATION_0_SQL+="                            ON qry_persons_type_todate_ovc.person_id = persons_geo_from.person_id)     "
OVC_MIGRATION_0_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_from ON persons_geo_from.area_id = tbl_list_geo_from.area_id)             "
OVC_MIGRATION_0_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_to ON persons_geo_to.area_id = tbl_list_geo_to.area_id)             "
OVC_MIGRATION_0_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_from_p ON tbl_list_geo_from.parent_area_id = tbl_list_geo_from_p.area_id)             "
OVC_MIGRATION_0_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_to_p ON tbl_list_geo_to.parent_area_id = tbl_list_geo_to_p.area_id            "
OVC_MIGRATION_0_SQL+="                    WHERE persons_geo_from.date_delinked>=%s And persons_geo_from.date_delinked<=%s AND (tbl_list_geo_from_p.parent_area_id=%s OR tbl_list_geo_to_p.parent_area_id=%s)            "
OVC_MIGRATION_0_SQL+="                    ) as qry_rep_ovc_migration_0            "
OVC_MIGRATION_0_SQL+="            WHERE tbl_list_geo.area_type_id='GWRD' AND tbl_list_geo.is_void=False and (area_id_frm = area_id OR area_id_to = area_id)                    "
OVC_MIGRATION_0_SQL+="            ) as qry_rep_ovc_migration_0_all                    "
OVC_MIGRATION_0_SQL+="    GROUP BY qry_rep_ovc_migration_0_all.area_id, qry_rep_ovc_migration_0_all.area_name                            "

#qry_persons_type_todate_ovc gets people who were OVC at the end of the period
#qry_rep_ovc_migration_0 gets OVC who migrated within the period, where either the ward they moved from or the ward they moved to is in the selected district
#qry_rep_ovc_migration_0_all gets a list of all wards within the selected district and gets a list of OVC who either migrated to or from each ward 
#final query counts OVC who migrated into, and OVC who migrated out of, each WARD

#list constituencies
#'param_to_date', 'param_to_date', 'param_from_date', 'param_to_date'

OVC_MIGRATION_1_SQL= "    SELECT qry_rep_ovc_migration_1_all.area_id as \"Constituency code\", qry_rep_ovc_migration_1_all.area_name as \"Constituency\", Sum(qry_rep_ovc_migration_1_all.migration_frm) AS \"Number of OVC who migrated out of area during period\", Sum(qry_rep_ovc_migration_1_all.migration_to) AS \"Number of OVC who migrated into area during period\", Sum(qry_rep_ovc_migration_1_all.migration_within) AS \"Number of OVC who migrated within area during period\"                            "
OVC_MIGRATION_1_SQL+="    FROM                             "
OVC_MIGRATION_1_SQL+="            (SELECT tbl_list_geo.area_id, tbl_list_geo.area_name, cast((area_id_frm=area_id And area_id_frm<>area_id_to) as integer) AS migration_frm, cast((area_id_to=area_id And area_id_frm<>area_id_to) as integer) AS migration_to, cast((area_id_frm=area_id_to) as integer) AS migration_within                    "
OVC_MIGRATION_1_SQL+="            FROM tbl_list_geo,                     "
OVC_MIGRATION_1_SQL+="                    (SELECT persons_geo_from.person_id, persons_geo_from.date_delinked, tbl_list_geo_from.parent_area_id AS area_id_frm, tbl_list_geo_to.parent_area_id AS area_id_to            "
OVC_MIGRATION_1_SQL+="                    FROM ((            "
OVC_MIGRATION_1_SQL+="                            (SELECT tbl_reg_persons.id AS person_id    "
OVC_MIGRATION_1_SQL+="                            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_MIGRATION_1_SQL+="                            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False    "
OVC_MIGRATION_1_SQL+="                            ) as qry_persons_type_todate_ovc     "
OVC_MIGRATION_1_SQL+="                    INNER JOIN (tbl_reg_persons_geo AS persons_geo_from             "
OVC_MIGRATION_1_SQL+="                    INNER JOIN tbl_reg_persons_geo AS persons_geo_to             "
OVC_MIGRATION_1_SQL+="                    ON (persons_geo_from.date_delinked = persons_geo_to.date_linked) AND (persons_geo_from.person_id = persons_geo_to.person_id))             "
OVC_MIGRATION_1_SQL+="                    ON qry_persons_type_todate_ovc.person_id = persons_geo_from.person_id)             "
OVC_MIGRATION_1_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_from ON persons_geo_from.area_id = tbl_list_geo_from.area_id)             "
OVC_MIGRATION_1_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_to ON persons_geo_to.area_id = tbl_list_geo_to.area_id            "
OVC_MIGRATION_1_SQL+="                    WHERE persons_geo_from.date_delinked>=%s And persons_geo_from.date_delinked<=%s            "
OVC_MIGRATION_1_SQL+="                    ) as qry_rep_ovc_migration_1            "
OVC_MIGRATION_1_SQL+="            WHERE (area_id_frm = area_id OR area_id_to = area_id) AND tbl_list_geo.area_type_id='GCON' AND tbl_list_geo.is_void=False                    "
OVC_MIGRATION_1_SQL+="            ) as qry_rep_ovc_migration_1_all                    "
OVC_MIGRATION_1_SQL+="    GROUP BY qry_rep_ovc_migration_1_all.area_id, qry_rep_ovc_migration_1_all.area_name                            "

#qry_persons_type_todate_ovc gets people who were OVC at the end of the period
#qry_rep_ovc_migration_1 gets OVC who migrated within the period, with the constituency they moved from and the constituency they moved to
#qry_rep_ovc_migration_1_all gets a list of all constituencies and gets a list of OVC who either migrated to or from each constituency 
#final query counts OVC who migrated into, and OVC who migrated out of, each constituency

#list districts
#'param_to_date' 'param_to_date' 'param_from_date' 'param_to_date'
OVC_MIGRATION_2_SQL= "    SELECT qry_rep_ovc_migration_2_all.area_id as \"District code\", qry_rep_ovc_migration_2_all.area_name as \"District\", Sum(qry_rep_ovc_migration_2_all.migration_frm) AS \"Number of OVC who migrated out of area during period\", Sum(qry_rep_ovc_migration_2_all.migration_to) AS \"Number of OVC who migrated into area during period\", Sum(qry_rep_ovc_migration_2_all.migration_within) AS \"Number of OVC who migrated within area during period\"                            "
OVC_MIGRATION_2_SQL+="    FROM                             "
OVC_MIGRATION_2_SQL+="            (SELECT tbl_list_geo.area_id, tbl_list_geo.area_name, cast((area_id_frm=area_id And area_id_frm<>area_id_to) as integer) AS migration_frm, cast((area_id_to=area_id And area_id_frm<>area_id_to) as integer) AS migration_to, cast((area_id_frm=area_id_to) as integer) AS migration_within                    "
OVC_MIGRATION_2_SQL+="            FROM tbl_list_geo,                     "
OVC_MIGRATION_2_SQL+="                    (SELECT persons_geo_from.person_id, persons_geo_from.date_delinked, tbl_list_geo_from_p.parent_area_id AS area_id_frm, tbl_list_geo_to_p.parent_area_id AS area_id_to            "
OVC_MIGRATION_2_SQL+="                    FROM ((((            "
OVC_MIGRATION_2_SQL+="                            (SELECT tbl_reg_persons.id AS person_id    "
OVC_MIGRATION_2_SQL+="                            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_MIGRATION_2_SQL+="                            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False    "
OVC_MIGRATION_2_SQL+="                            ) as qry_persons_type_todate_ovc     "
OVC_MIGRATION_2_SQL+="                    INNER JOIN (tbl_reg_persons_geo AS persons_geo_from             "
OVC_MIGRATION_2_SQL+="                    INNER JOIN tbl_reg_persons_geo AS persons_geo_to             "
OVC_MIGRATION_2_SQL+="                    ON (persons_geo_from.date_delinked = persons_geo_to.date_linked) AND (persons_geo_from.person_id = persons_geo_to.person_id))             "
OVC_MIGRATION_2_SQL+="                    ON qry_persons_type_todate_ovc.person_id = persons_geo_from.person_id)             "
OVC_MIGRATION_2_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_from             "
OVC_MIGRATION_2_SQL+="                    ON persons_geo_from.area_id = tbl_list_geo_from.area_id)             "
OVC_MIGRATION_2_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_to ON persons_geo_to.area_id = tbl_list_geo_to.area_id)             "
OVC_MIGRATION_2_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_from_p ON tbl_list_geo_from.parent_area_id = tbl_list_geo_from_p.area_id)             "
OVC_MIGRATION_2_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_to_p ON tbl_list_geo_to.parent_area_id = tbl_list_geo_to_p.area_id            "
OVC_MIGRATION_2_SQL+="                    WHERE persons_geo_from.date_delinked>=%s And persons_geo_from.date_delinked<=%s            "
OVC_MIGRATION_2_SQL+="                    ) as qry_rep_ovc_migration_2            "
OVC_MIGRATION_2_SQL+="            WHERE tbl_list_geo.area_type_id='GDIS' AND tbl_list_geo.is_void=False                    "
OVC_MIGRATION_2_SQL+="            ) as qry_rep_ovc_migration_2_all                    "
OVC_MIGRATION_2_SQL+="    GROUP BY qry_rep_ovc_migration_2_all.area_id, qry_rep_ovc_migration_2_all.area_name                            "

#qry_persons_type_todate_ovc gets people who were OVC at the end of the period
#qry_rep_ovc_migration_2 gets OVC who migrated within the period, with the district they moved from and the district they moved to
#qry_rep_ovc_migration_2_all gets a list of all districts and gets a list of OVC who either migrated to or from or within each district 
#final query counts OVC who migrated into, and OVC who migrated out of, each district

#list provinces
#expected parameters: 'param_to_date' 'param_to_date' 'param_from_date' 'param_to_date' 
OVC_MIGRATION_3_SQL= "    SELECT qry_rep_ovc_migration_3_all.area_id as \"Province code\", qry_rep_ovc_migration_3_all.area_name as \"Province\", Sum(qry_rep_ovc_migration_3_all.migration_frm) AS \"Number of OVC who migrated out of area during period\", Sum(qry_rep_ovc_migration_3_all.migration_to) AS \"Number of OVC who migrated into area during period\", Sum(qry_rep_ovc_migration_3_all.migration_within) AS \"Number of OVC who migrated within area during period\"                            "
OVC_MIGRATION_3_SQL+="    FROM                             "
OVC_MIGRATION_3_SQL+="            (SELECT tbl_list_geo.area_id, tbl_list_geo.area_name, cast((area_id_frm=area_id And area_id_frm<>area_id_to) as integer) AS migration_frm, cast((area_id_to=area_id And area_id_frm<>area_id_to) as integer) AS migration_to, cast((area_id_frm=area_id_to) as integer) AS migration_within                    "
OVC_MIGRATION_3_SQL+="            FROM tbl_list_geo,                     "
OVC_MIGRATION_3_SQL+="                    (SELECT persons_geo_from.person_id, persons_geo_from.date_delinked, tbl_list_geo_from_pp.parent_area_id AS area_id_frm, tbl_list_geo_to_pp.parent_area_id AS area_id_to            "
OVC_MIGRATION_3_SQL+="                    FROM ((((((            "
OVC_MIGRATION_3_SQL+="                            (SELECT tbl_reg_persons.id AS person_id    "
OVC_MIGRATION_3_SQL+="                            FROM tbl_reg_persons INNER JOIN tbl_reg_persons_types ON tbl_reg_persons.id = tbl_reg_persons_types.person_id    "
OVC_MIGRATION_3_SQL+="                            WHERE tbl_reg_persons_types.person_type_id='TBVC' AND tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s) AND tbl_reg_persons.is_void=False    "
OVC_MIGRATION_3_SQL+="                            ) as qry_persons_type_todate_ovc     "
OVC_MIGRATION_3_SQL+="                            INNER JOIN (tbl_reg_persons_geo AS persons_geo_from     "
OVC_MIGRATION_3_SQL+="                            INNER JOIN tbl_reg_persons_geo AS persons_geo_to     "
OVC_MIGRATION_3_SQL+="                            ON (persons_geo_from.date_delinked = persons_geo_to.date_linked) AND (persons_geo_from.person_id = persons_geo_to.person_id))     "
OVC_MIGRATION_3_SQL+="                            ON qry_persons_type_todate_ovc.person_id = persons_geo_from.person_id)     "
OVC_MIGRATION_3_SQL+="                            INNER JOIN tbl_list_geo AS tbl_list_geo_from     "
OVC_MIGRATION_3_SQL+="                            ON persons_geo_from.area_id = tbl_list_geo_from.area_id)     "
OVC_MIGRATION_3_SQL+="                            INNER JOIN tbl_list_geo AS tbl_list_geo_to ON persons_geo_to.area_id = tbl_list_geo_to.area_id)     "
OVC_MIGRATION_3_SQL+="                            INNER JOIN tbl_list_geo AS tbl_list_geo_from_p ON tbl_list_geo_from.parent_area_id = tbl_list_geo_from_p.area_id)     "
OVC_MIGRATION_3_SQL+="                            INNER JOIN tbl_list_geo AS tbl_list_geo_to_p ON tbl_list_geo_to.parent_area_id = tbl_list_geo_to_p.area_id)     "
OVC_MIGRATION_3_SQL+="                            INNER JOIN tbl_list_geo AS tbl_list_geo_from_pp ON tbl_list_geo_from_p.parent_area_id = tbl_list_geo_from_pp.area_id)     "
OVC_MIGRATION_3_SQL+="                            INNER JOIN tbl_list_geo AS tbl_list_geo_to_pp ON tbl_list_geo_to_p.parent_area_id = tbl_list_geo_to_pp.area_id    "
OVC_MIGRATION_3_SQL+="                    WHERE persons_geo_from.date_delinked>=%s And persons_geo_from.date_delinked<=%s           "
OVC_MIGRATION_3_SQL+="                    ) as qry_rep_ovc_migration_3            "
OVC_MIGRATION_3_SQL+="            WHERE tbl_list_geo.area_type_id='GPRV' AND tbl_list_geo.is_void=False                    "
OVC_MIGRATION_3_SQL+="            ) as qry_rep_ovc_migration_3_all                    "
OVC_MIGRATION_3_SQL+="    GROUP BY qry_rep_ovc_migration_3_all.area_id, qry_rep_ovc_migration_3_all.area_name                            "
#qry_persons_type_todate_ovc gets people who were OVC at the end of the period
#qry_rep_ovc_migration_3 gets OVC who migrated within the period, with the province they moved from and the province they moved to
#qry_rep_ovc_migration_2_all gets a list of all provinces and gets a list of OVC who either migrated to or from or within each province 
#final query counts OVC who migrated into, and OVC who migrated out of, each district


#*******GUARDIAN DEATH **********

#expected parameters: 'param_from_date', 'param_to_date'
GUARDIAN_DEATH_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC experiencing death of guardian during period\"                            "
GUARDIAN_DEATH_SQL+="    FROM                             "
GUARDIAN_DEATH_SQL+="            (SELECT DISTINCT OVC.id AS person_id, OVC.sex_id, OVC.date_of_birth                    "
GUARDIAN_DEATH_SQL+="            FROM (tbl_reg_persons AS OVC INNER JOIN tbl_reg_persons_guardians ON OVC.id = tbl_reg_persons_guardians.child_person_id)                     "
GUARDIAN_DEATH_SQL+="            INNER JOIN tbl_reg_persons AS guardians ON tbl_reg_persons_guardians.guardian_person_id = guardians.id                    "
GUARDIAN_DEATH_SQL+="            WHERE OVC.is_void=False AND guardians.date_of_death>=%s And guardians.date_of_death<=%s                    "
GUARDIAN_DEATH_SQL+="            ) as qry_rep_guardian_death                     "
GUARDIAN_DEATH_SQL+="    INNER JOIN tbl_list_general ON qry_rep_guardian_death.sex_id = tbl_list_general.item_id                            "
GUARDIAN_DEATH_SQL+="    GROUP BY age, sex                            "
GUARDIAN_DEATH_SQL+="    ORDER BY age, sex                            "
# qry_rep_guardian_death gets list of OVC whose guardians died within the period 
# final query counts by age and sex

#selected ward
#expected parameters: param_area_id, 'param_to_date','param_to_date','param_from_date', 'param_to_date', 
GUARDIAN_DEATH_GEO_0_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC experiencing death of guardian during period\"                            "
GUARDIAN_DEATH_GEO_0_SQL+="    FROM                             "
GUARDIAN_DEATH_GEO_0_SQL+="            (SELECT DISTINCT OVC.id AS person_id, OVC.sex_id, OVC.date_of_birth                    "
GUARDIAN_DEATH_GEO_0_SQL+="            FROM ((tbl_reg_persons AS OVC INNER JOIN tbl_reg_persons_guardians ON OVC.id = tbl_reg_persons_guardians.child_person_id)                     "
GUARDIAN_DEATH_GEO_0_SQL+="            INNER JOIN tbl_reg_persons AS guardians ON tbl_reg_persons_guardians.guardian_person_id = guardians.id)                     "
GUARDIAN_DEATH_GEO_0_SQL+="            INNER JOIN                     "
GUARDIAN_DEATH_GEO_0_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id            "
GUARDIAN_DEATH_GEO_0_SQL+="                    FROM tbl_reg_persons_geo            "
GUARDIAN_DEATH_GEO_0_SQL+="                    WHERE tbl_reg_persons_geo.area_id=%s AND tbl_reg_persons_geo.date_linked<=%s AND tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>'param_to_date'            "
GUARDIAN_DEATH_GEO_0_SQL+="                    ) as qry_persons_geo_todate_sp             "
GUARDIAN_DEATH_GEO_0_SQL+="            ON OVC.id = qry_persons_geo_todate_sp.person_id                    "
GUARDIAN_DEATH_GEO_0_SQL+="            WHERE OVC.is_void=False AND guardians.date_of_death>=%s And guardians.date_of_death<=%s                    "
GUARDIAN_DEATH_GEO_0_SQL+="            ) as qry_rep_guardian_death_geo_0                     "
GUARDIAN_DEATH_GEO_0_SQL+="    INNER JOIN tbl_list_general ON qry_rep_guardian_death_geo_0.sex_id = tbl_list_general.item_id                            "
GUARDIAN_DEATH_GEO_0_SQL+="    GROUP BY age, sex                            "
GUARDIAN_DEATH_GEO_0_SQL+="    ORDER BY age, sex                         "
# qry_persons_geo_todate_sp gets  OVC living in selected ward as of the end of the period 
# qry_rep_guardian_death_geo_0 OVC who had a guardian who died within the period
# final query counts by age and sex 

#selected constituency
#expected parameters: 'param_to_date','param_to_date','param_from_date', 'param_to_date', param_area_id
GUARDIAN_DEATH_GEO_1_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC experiencing death of guardian during period\"                            "
GUARDIAN_DEATH_GEO_1_SQL+="    FROM                             "
GUARDIAN_DEATH_GEO_1_SQL+="            (SELECT DISTINCT OVC.id AS person_id, OVC.sex_id, OVC.date_of_birth                    "
GUARDIAN_DEATH_GEO_1_SQL+="            FROM ((tbl_reg_persons AS OVC INNER JOIN tbl_reg_persons_guardians ON OVC.id = tbl_reg_persons_guardians.child_person_id)                     "
GUARDIAN_DEATH_GEO_1_SQL+="            INNER JOIN tbl_reg_persons AS guardians ON tbl_reg_persons_guardians.guardian_person_id = guardians.id)                     "
GUARDIAN_DEATH_GEO_1_SQL+="            INNER JOIN (tbl_list_geo INNER JOIN                     "
GUARDIAN_DEATH_GEO_1_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id            "
GUARDIAN_DEATH_GEO_1_SQL+="                    FROM tbl_reg_persons_geo            "
GUARDIAN_DEATH_GEO_1_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s            "
GUARDIAN_DEATH_GEO_1_SQL+="                    ) as qry_persons_geo_todate_s             "
GUARDIAN_DEATH_GEO_1_SQL+="            ON tbl_list_geo.area_id = qry_persons_geo_todate_s.area_id)                     "
GUARDIAN_DEATH_GEO_1_SQL+="            ON OVC.id = qry_persons_geo_todate_s.person_id                    "
GUARDIAN_DEATH_GEO_1_SQL+="            WHERE OVC.is_void=False AND guardians.date_of_death>=%s And guardians.date_of_death<=%s AND tbl_list_geo.parent_area_id=%s                    "
GUARDIAN_DEATH_GEO_1_SQL+="            ) as qry_rep_guardian_death_geo_1                     "
GUARDIAN_DEATH_GEO_1_SQL+="    INNER JOIN tbl_list_general ON qry_rep_guardian_death_geo_1.sex_id = tbl_list_general.item_id                            "
GUARDIAN_DEATH_GEO_1_SQL+="    GROUP BY age, sex                            "
GUARDIAN_DEATH_GEO_1_SQL+="    ORDER BY age, sex                            "
# qry_persons_geo_todate_s gets the area OVC lived in as of the end of the period 
# qry_rep_guardian_death_geo_1 gets OVC in the selected constituency who had a guardian who died within the period
# final query counts by age and sex

#selected district
#expected parameters: 'param_to_date','param_to_date', param_area_id, 'param_from_date', 'param_to_date', 
GUARDIAN_DEATH_GEO_2_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC experiencing death of guardian during period\"                           "
GUARDIAN_DEATH_GEO_2_SQL+="    FROM                             "
GUARDIAN_DEATH_GEO_2_SQL+="            (SELECT DISTINCT OVC.id AS person_id, OVC.sex_id, OVC.date_of_birth                    "
GUARDIAN_DEATH_GEO_2_SQL+="            FROM (((tbl_reg_persons AS OVC INNER JOIN tbl_reg_persons_guardians ON OVC.id = tbl_reg_persons_guardians.child_person_id)                     "
GUARDIAN_DEATH_GEO_2_SQL+="            INNER JOIN tbl_reg_persons AS guardians ON tbl_reg_persons_guardians.guardian_person_id = guardians.id)                     "
GUARDIAN_DEATH_GEO_2_SQL+="            INNER JOIN                     "
GUARDIAN_DEATH_GEO_2_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id            "
GUARDIAN_DEATH_GEO_2_SQL+="                    FROM tbl_reg_persons_geo            "
GUARDIAN_DEATH_GEO_2_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s            "
GUARDIAN_DEATH_GEO_2_SQL+="                    ) as qry_persons_geo_todate_s             "
GUARDIAN_DEATH_GEO_2_SQL+="            ON OVC.id = qry_persons_geo_todate_s.person_id)                     "
GUARDIAN_DEATH_GEO_2_SQL+="            INNER JOIN                     "
GUARDIAN_DEATH_GEO_2_SQL+="                    (SELECT tbl_list_geo_1.area_id            "
GUARDIAN_DEATH_GEO_2_SQL+="                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id            "
GUARDIAN_DEATH_GEO_2_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s            "
GUARDIAN_DEATH_GEO_2_SQL+="                    ) as qry_heir_sub_sub_areas             "
GUARDIAN_DEATH_GEO_2_SQL+="            ON qry_persons_geo_todate_s.area_id = qry_heir_sub_sub_areas.area_id                    "
GUARDIAN_DEATH_GEO_2_SQL+="            WHERE OVC.is_void=False AND guardians.date_of_death>=%s And guardians.date_of_death<=%s                    "
GUARDIAN_DEATH_GEO_2_SQL+="            ) as qry_rep_guardian_death_geo_2                     "
GUARDIAN_DEATH_GEO_2_SQL+="    INNER JOIN tbl_list_general ON qry_rep_guardian_death_geo_2.sex_id = tbl_list_general.item_id                            "
GUARDIAN_DEATH_GEO_2_SQL+="    GROUP BY age, sex                            "
GUARDIAN_DEATH_GEO_2_SQL+="    ORDER BY age, sex                            "
# qry_persons_geo_todate_s gets the ward OVC lived in as of the end of the period
# qry_heir_sub_sub_areas gets wards within the selected district
# qry_rep_guardian_death_geo_2 gets OVC in those wards who had a guardian who died within the period
# final query counts by age and sex
 
#selected province
#expected parameters: 'param_to_date','param_to_date', param_area_id, 'param_from_date', 'param_to_date',  
GUARDIAN_DEATH_GEO_3_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC experiencing death of guardian during period\"                            "
GUARDIAN_DEATH_GEO_3_SQL+="    FROM                             "
GUARDIAN_DEATH_GEO_3_SQL+="            (SELECT DISTINCT OVC.id AS person_id, OVC.sex_id, OVC.date_of_birth                    "
GUARDIAN_DEATH_GEO_3_SQL+="            FROM (((tbl_reg_persons AS OVC INNER JOIN tbl_reg_persons_guardians ON OVC.id = tbl_reg_persons_guardians.child_person_id)                     "
GUARDIAN_DEATH_GEO_3_SQL+="            INNER JOIN tbl_reg_persons AS guardians ON tbl_reg_persons_guardians.guardian_person_id = guardians.id) INNER JOIN                     "
GUARDIAN_DEATH_GEO_3_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id            "
GUARDIAN_DEATH_GEO_3_SQL+="                    FROM tbl_reg_persons_geo            "
GUARDIAN_DEATH_GEO_3_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s            "
GUARDIAN_DEATH_GEO_3_SQL+="                    ) as qry_persons_geo_todate_s             "
GUARDIAN_DEATH_GEO_3_SQL+="            ON OVC.id = qry_persons_geo_todate_s.person_id) INNER JOIN                     "
GUARDIAN_DEATH_GEO_3_SQL+="                    (SELECT tbl_list_geo_2.area_id            "
GUARDIAN_DEATH_GEO_3_SQL+="                    FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)             "
GUARDIAN_DEATH_GEO_3_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id            "
GUARDIAN_DEATH_GEO_3_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s            "
GUARDIAN_DEATH_GEO_3_SQL+="                    ) as qry_heir_sub_sub_sub_areas             "
GUARDIAN_DEATH_GEO_3_SQL+="            ON qry_persons_geo_todate_s.area_id = qry_heir_sub_sub_sub_areas.area_id                    "
GUARDIAN_DEATH_GEO_3_SQL+="            WHERE OVC.is_void=False AND guardians.date_of_death>=%s And guardians.date_of_death<=%s                    "
GUARDIAN_DEATH_GEO_3_SQL+="            ) as qry_rep_guardian_death_geo_3                     "
GUARDIAN_DEATH_GEO_3_SQL+="    INNER JOIN tbl_list_general ON qry_rep_guardian_death_geo_3.sex_id = tbl_list_general.item_id                            "
GUARDIAN_DEATH_GEO_3_SQL+="    GROUP BY age, sex                            "
GUARDIAN_DEATH_GEO_3_SQL+="    ORDER BY age, sex                            "
# qry_persons_geo_todate_s gets the ward OVC lived in as of the end of the period
# qry_heir_sub_sub_sub_areas  gets wards within the selected province
# qry_rep_guardian_death_geo_3 gets OVC in those wards who had a guardian who died within the period
# final query counts by age and sex

#selected org unit
#expected parameters: 'param_from_date', 'param_to_date',param_org_unit_id
GUARDIAN_DEATH_ORG_UNIT_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC experiencing death of guardian during period\"                            "
GUARDIAN_DEATH_ORG_UNIT_SQL+="    FROM                             "
GUARDIAN_DEATH_ORG_UNIT_SQL+="            (SELECT DISTINCT OVC.id AS person_id, OVC.sex_id, OVC.date_of_birth                    "
GUARDIAN_DEATH_ORG_UNIT_SQL+="            FROM ((tbl_reg_persons AS OVC INNER JOIN tbl_reg_persons_guardians ON OVC.id = tbl_reg_persons_guardians.child_person_id)                     "
GUARDIAN_DEATH_ORG_UNIT_SQL+="            INNER JOIN tbl_reg_persons AS guardians ON tbl_reg_persons_guardians.guardian_person_id = guardians.id)                     "
GUARDIAN_DEATH_ORG_UNIT_SQL+="            INNER JOIN                     "
GUARDIAN_DEATH_ORG_UNIT_SQL+="                    (SELECT DISTINCT tbl_core_encounters.org_unit_id, tbl_core_encounters.beneficiary_person_id            "
GUARDIAN_DEATH_ORG_UNIT_SQL+="                    FROM tbl_core_encounters            "
GUARDIAN_DEATH_ORG_UNIT_SQL+="                    ) as qry_core_orgs_beneficiaries             "
GUARDIAN_DEATH_ORG_UNIT_SQL+="            ON OVC.id = qry_core_orgs_beneficiaries.beneficiary_person_id                    "
GUARDIAN_DEATH_ORG_UNIT_SQL+="            WHERE OVC.is_void=False AND guardians.date_of_death>=%s And guardians.date_of_death<=%s AND qry_core_orgs_beneficiaries.org_unit_id=%s                    "
GUARDIAN_DEATH_ORG_UNIT_SQL+="            ) as qry_rep_guardian_death_org                   "
GUARDIAN_DEATH_ORG_UNIT_SQL+="    INNER JOIN tbl_list_general ON qry_rep_guardian_death_org.sex_id = tbl_list_general.item_id                            "
GUARDIAN_DEATH_ORG_UNIT_SQL+="    GROUP BY age, sex                            "
GUARDIAN_DEATH_ORG_UNIT_SQL+="    ORDER BY age, sex                            "
#qry_core_orgs_beneficiaries gets list of people ever served by selected org unit 
#qry_rep_guardian_death_org gets OVCs who had a guardian who died during the period
#final query counts by age and sex

#selected set of org units
#expected parameters: set_id, 'param_from_date', 'param_to_date'
GUARDIAN_DEATH_SET_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC experiencing death of guardian during period\"                           "
GUARDIAN_DEATH_SET_SQL+="    FROM                             "
GUARDIAN_DEATH_SET_SQL+="            (SELECT DISTINCT OVC.id AS person_id, OVC.sex_id, OVC.date_of_birth                    "
GUARDIAN_DEATH_SET_SQL+="            FROM ((tbl_reg_persons AS OVC INNER JOIN tbl_reg_persons_guardians ON OVC.id = tbl_reg_persons_guardians.child_person_id)                     "
GUARDIAN_DEATH_SET_SQL+="            INNER JOIN tbl_reg_persons AS guardians ON tbl_reg_persons_guardians.guardian_person_id = guardians.id)                     "
GUARDIAN_DEATH_SET_SQL+="            INNER JOIN                     "
GUARDIAN_DEATH_SET_SQL+="                    (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id            "
GUARDIAN_DEATH_SET_SQL+="                    FROM tbl_core_encounters INNER JOIN             "
GUARDIAN_DEATH_SET_SQL+="                            (SELECT tbl_reports_sets_org_units.org_unit_id    "
GUARDIAN_DEATH_SET_SQL+="                            FROM tbl_reports_sets_org_units    "
GUARDIAN_DEATH_SET_SQL+="                            WHERE tbl_reports_sets_org_units.set_id=%s   "
GUARDIAN_DEATH_SET_SQL+="                            ) as qry_set_org_units     "
GUARDIAN_DEATH_SET_SQL+="                    ON tbl_core_encounters.org_unit_id = qry_set_org_units.org_unit_id            "
GUARDIAN_DEATH_SET_SQL+="                    ) as qry_set_org_units_benef             "
GUARDIAN_DEATH_SET_SQL+="            ON OVC.id = qry_set_org_units_benef.beneficiary_person_id                    "
GUARDIAN_DEATH_SET_SQL+="            WHERE OVC.is_void=False AND guardians.date_of_death>=%s And guardians.date_of_death<=%s                    "
GUARDIAN_DEATH_SET_SQL+="            ) as qry_rep_guardian_death_set                     "
GUARDIAN_DEATH_SET_SQL+="    INNER JOIN tbl_list_general ON qry_rep_guardian_death_set.sex_id=tbl_list_general.item_id                            "
GUARDIAN_DEATH_SET_SQL+="    GROUP BY age, sex                            "
GUARDIAN_DEATH_SET_SQL+="    ORDER BY age, sex                            "
#qry_set_org_units get selected set of org units 
#qry_set_org_units_benef gets distinct list of people ever served by selected set of org units 
#qry_rep_guardian_death_set gets OVCs who had a guardian who died during the period
#final query counts by age and sex

#*******SERVICES PROVIDED (ALL) **********

    

# only period specified
#expected parameters: 'param_from_date', 'param_to_date', 'param_to_date', 'param_to_date'  
SERVICES_PROVIDED_SQL= "    SELECT qry_rep_services_all.core_item_id as \"Service code\", tbl_list_general.item_description as \"Service\", Sum(qry_rep_services_all.number_times_provided) AS \"Number of times provided\", Sum(qry_rep_services_all.OVC) AS \"Number of distinct OVC receiving\", Sum(qry_rep_services_all.Guardian) AS \"Number of distinct guardians receiving\"                          "
SERVICES_PROVIDED_SQL+="    FROM                             "
SERVICES_PROVIDED_SQL+="            (SELECT qry_core_services_referrals_in_period_s.core_item_id, qry_core_services_referrals_in_period_s.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICES_PROVIDED_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICES_PROVIDED_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICES_PROVIDED_SQL+="                    Count(*) AS number_times_provided            "
SERVICES_PROVIDED_SQL+="            FROM                     "
SERVICES_PROVIDED_SQL+="                    (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICES_PROVIDED_SQL+="                    FROM tbl_core_services            "
SERVICES_PROVIDED_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s           "
SERVICES_PROVIDED_SQL+="                    ) as qry_core_services_referrals_in_period_s             "
SERVICES_PROVIDED_SQL+="            INNER JOIN                     "
SERVICES_PROVIDED_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICES_PROVIDED_SQL+="                    FROM tbl_reg_persons_types            "
SERVICES_PROVIDED_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICES_PROVIDED_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICES_PROVIDED_SQL+="            ON qry_core_services_referrals_in_period_s.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICES_PROVIDED_SQL+="            GROUP BY qry_core_services_referrals_in_period_s.core_item_id, qry_core_services_referrals_in_period_s.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, ovc, guardian                    "
SERVICES_PROVIDED_SQL+="            ) as qry_rep_services_all                     "
SERVICES_PROVIDED_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_all.core_item_id = tbl_list_general.item_id                            "
SERVICES_PROVIDED_SQL+="    GROUP BY core_item_id, item_description, item_category, the_order                            "
SERVICES_PROVIDED_SQL+="    ORDER BY item_category DESC, the_order ASC                            "
# qry_core_services_referrals_in_period_s gets services provided during the period
# qry_persons_type_todate_s determines whether recipients are OVC or guardians
# qry_rep_services_all gets distinct service-recipient combination, and makes a column containing 1 for OVC, a column containing 1 for guardians, and a column counting how many times the service was received by the recipient during the period
# final query lists the services and for each counts number of OVC, number of guardians, and total number of times service provided


# period and ward specified
#expected parameters: 'param_from_date', 'param_to_date', param_area_id, 'param_to_date', 'param_to_date'   
SERVICES_PROVIDED_GEO_0_SQL= "    SELECT qry_rep_services_all_geo_0.core_item_id as \"Service code\", tbl_list_general.item_description as \"Service\", Sum(qry_rep_services_all_geo_0.number_times_provided) AS \"Number of times provided\", Sum(qry_rep_services_all_geo_0.OVC) AS \"Number of distinct OVC receiving\", Sum(qry_rep_services_all_geo_0.Guardian) AS \"Number of distinct guardians receiving\"                            "
SERVICES_PROVIDED_GEO_0_SQL+="    FROM                             "
SERVICES_PROVIDED_GEO_0_SQL+="            (SELECT qry_core_services_referrals_in_period_with_enc_geo_0.core_item_id, qry_core_services_referrals_in_period_with_enc_geo_0.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICES_PROVIDED_GEO_0_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICES_PROVIDED_GEO_0_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICES_PROVIDED_GEO_0_SQL+="                    Count(*) AS number_times_provided            "
SERVICES_PROVIDED_GEO_0_SQL+="            FROM                     "
SERVICES_PROVIDED_GEO_0_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.core_item_id            "
SERVICES_PROVIDED_GEO_0_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICES_PROVIDED_GEO_0_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_encounters.area_id=%s            "
SERVICES_PROVIDED_GEO_0_SQL+="                    ) as qry_core_services_referrals_in_period_with_enc_geo_0             "
SERVICES_PROVIDED_GEO_0_SQL+="            INNER JOIN                     "
SERVICES_PROVIDED_GEO_0_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICES_PROVIDED_GEO_0_SQL+="                    FROM tbl_reg_persons_types            "
SERVICES_PROVIDED_GEO_0_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICES_PROVIDED_GEO_0_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICES_PROVIDED_GEO_0_SQL+="            ON qry_core_services_referrals_in_period_with_enc_geo_0.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICES_PROVIDED_GEO_0_SQL+="            GROUP BY qry_core_services_referrals_in_period_with_enc_geo_0.core_item_id, qry_core_services_referrals_in_period_with_enc_geo_0.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICES_PROVIDED_GEO_0_SQL+="            ) as qry_rep_services_all_geo_0                     "
SERVICES_PROVIDED_GEO_0_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_all_geo_0.core_item_id = tbl_list_general.item_id                            "
SERVICES_PROVIDED_GEO_0_SQL+="    GROUP BY core_item_id, item_description, item_category, the_order                            "
SERVICES_PROVIDED_GEO_0_SQL+="    ORDER BY item_category DESC, the_order ASC                            "
# qry_core_services_referrals_in_period_with_enc_geo_0 gets services provided during the period in the selected ward
# qry_persons_type_todate_s determines whether recipients are OVC or guardians
# qry_rep_services_all_geo_0  gets distinct service-recipient combination, and makes a column containing 1 for OVC, a column containing 1 for guardians, and a column counting how many times the service was received by the recipient during the period
# final query lists the services and for each counts number of OVC, number of guardians, and total number of times service provided


# period and constituency specified
#expected parameters: 'param_from_date', 'param_to_date', param_area_id, 'param_to_date', 'param_to_date'   
SERVICES_PROVIDED_GEO_1_SQL= "    SELECT qry_rep_services_all_geo_1.core_item_id as \"Service code\", tbl_list_general.item_description as \"Service\", Sum(qry_rep_services_all_geo_1.OVC) AS \"Number of distinct OVC receiving\", Sum(qry_rep_services_all_geo_1.number_times_provided) AS \"Number of times provided\", Sum(qry_rep_services_all_geo_1.Guardian) AS \"Number of distinct guardians receiving\"                          "
SERVICES_PROVIDED_GEO_1_SQL+="    FROM                             "
SERVICES_PROVIDED_GEO_1_SQL+="            (SELECT qry_core_services_referrals_in_period_with_enc_geo_1.core_item_id, qry_core_services_referrals_in_period_with_enc_geo_1.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICES_PROVIDED_GEO_1_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICES_PROVIDED_GEO_1_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICES_PROVIDED_GEO_1_SQL+="                    Count(*) AS number_times_provided            "
SERVICES_PROVIDED_GEO_1_SQL+="            FROM                     "
SERVICES_PROVIDED_GEO_1_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.core_item_id            "
SERVICES_PROVIDED_GEO_1_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICES_PROVIDED_GEO_1_SQL+="                    INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id            "
SERVICES_PROVIDED_GEO_1_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_list_geo.parent_area_id=%s            "
SERVICES_PROVIDED_GEO_1_SQL+="                    ) as qry_core_services_referrals_in_period_with_enc_geo_1             "
SERVICES_PROVIDED_GEO_1_SQL+="            INNER JOIN                     "
SERVICES_PROVIDED_GEO_1_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICES_PROVIDED_GEO_1_SQL+="                    FROM tbl_reg_persons_types            "
SERVICES_PROVIDED_GEO_1_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICES_PROVIDED_GEO_1_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICES_PROVIDED_GEO_1_SQL+="            ON qry_core_services_referrals_in_period_with_enc_geo_1.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICES_PROVIDED_GEO_1_SQL+="            GROUP BY qry_core_services_referrals_in_period_with_enc_geo_1.core_item_id, qry_core_services_referrals_in_period_with_enc_geo_1.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICES_PROVIDED_GEO_1_SQL+="            ) as qry_rep_services_all_geo_1                     "
SERVICES_PROVIDED_GEO_1_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_all_geo_1.core_item_id = tbl_list_general.item_id                            "
SERVICES_PROVIDED_GEO_1_SQL+="    GROUP BY core_item_id, item_description, item_category, the_order                            "
SERVICES_PROVIDED_GEO_1_SQL+="    ORDER BY item_category DESC, the_order ASC                            "
# qry_core_services_referrals_in_period_with_enc_geo_1  gets services provided during the period in the selected constituency
# qry_persons_type_todate_s determines whether recipients are OVC or guardians
# qry_rep_services_all_geo_1  gets distinct service-recipient combination, and makes a column containing 1 for OVC, a column containing 1 for guardians, and a column counting how many times the service was received by the recipient during the period
# final query lists the services and for each counts number of OVC, number of guardians, and total number of times service provided



# period and district specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', 'param_to_date', 'param_to_date'   
SERVICES_PROVIDED_GEO_2_SQL= "    SELECT qry_rep_services_all_geo_2.core_item_id as \"Service code\", tbl_list_general.item_description as \"Service\", Sum(qry_rep_services_all_geo_2.number_times_provided) AS \"Number of times provided\", Sum(qry_rep_services_all_geo_2.OVC) AS \"Number of distinct OVC receiving\", Sum(qry_rep_services_all_geo_2.Guardian) AS \"Number of distinct guardians receiving\"                            "
SERVICES_PROVIDED_GEO_2_SQL+="    FROM                             "
SERVICES_PROVIDED_GEO_2_SQL+="            (SELECT qry_core_services_referrals_in_period_with_enc_geo_2.core_item_id, qry_core_services_referrals_in_period_with_enc_geo_2.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICES_PROVIDED_GEO_2_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICES_PROVIDED_GEO_2_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICES_PROVIDED_GEO_2_SQL+="                    Count(*) AS number_times_provided            "
SERVICES_PROVIDED_GEO_2_SQL+="            FROM                     "
SERVICES_PROVIDED_GEO_2_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.core_item_id            "
SERVICES_PROVIDED_GEO_2_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICES_PROVIDED_GEO_2_SQL+="                    INNER JOIN             "
SERVICES_PROVIDED_GEO_2_SQL+="                            (SELECT tbl_list_geo_1.area_id    "
SERVICES_PROVIDED_GEO_2_SQL+="                            FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id    "
SERVICES_PROVIDED_GEO_2_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s    "
SERVICES_PROVIDED_GEO_2_SQL+="                            ) as qry_heir_sub_sub_areas     "
SERVICES_PROVIDED_GEO_2_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id            "
SERVICES_PROVIDED_GEO_2_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s            "
SERVICES_PROVIDED_GEO_2_SQL+="                    ) as qry_core_services_referrals_in_period_with_enc_geo_2             "
SERVICES_PROVIDED_GEO_2_SQL+="            INNER JOIN                     "
SERVICES_PROVIDED_GEO_2_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICES_PROVIDED_GEO_2_SQL+="                    FROM tbl_reg_persons_types            "
SERVICES_PROVIDED_GEO_2_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICES_PROVIDED_GEO_2_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICES_PROVIDED_GEO_2_SQL+="            ON qry_core_services_referrals_in_period_with_enc_geo_2.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICES_PROVIDED_GEO_2_SQL+="            GROUP BY qry_core_services_referrals_in_period_with_enc_geo_2.core_item_id, qry_core_services_referrals_in_period_with_enc_geo_2.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICES_PROVIDED_GEO_2_SQL+="            ) as qry_rep_services_all_geo_2                     "
SERVICES_PROVIDED_GEO_2_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_all_geo_2.core_item_id = tbl_list_general.item_id                            "
SERVICES_PROVIDED_GEO_2_SQL+="    GROUP BY core_item_id, item_description, item_category, the_order                            "
SERVICES_PROVIDED_GEO_2_SQL+="    ORDER BY item_category DESC, the_order ASC                            "
# qry_heir_sub_sub_areas gets wards in the selected district
# qry_core_services_referrals_in_period_with_enc_geo_2  gets services provided during the period in the selected district
# qry_persons_type_todate_s determines whether recipients are OVC or guardians
# qry_rep_services_all_geo_2  gets distinct service-recipient combination, and makes a column containing 1 for OVC, a column containing 1 for guardians, and a column counting how many times the service was received by the recipient during the period
# final query lists the services and for each counts number of OVC, number of guardians, and total number of times service provided


# period and province specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', 'param_to_date', 'param_to_date'   
SERVICES_PROVIDED_GEO_3_SQL= "    SELECT qry_rep_services_all_geo_3.core_item_id as \"Service code\", tbl_list_general.item_description as \"Service\", Sum(qry_rep_services_all_geo_3.number_times_provided) AS \"Number of times provided\", Sum(qry_rep_services_all_geo_3.OVC) AS \"Number of distinct OVC receiving\", Sum(qry_rep_services_all_geo_3.Guardian) AS \"Number of distinct guardians receiving\"                            "
SERVICES_PROVIDED_GEO_3_SQL+="    FROM                             "
SERVICES_PROVIDED_GEO_3_SQL+="            (SELECT qry_core_services_referrals_in_period_with_enc_geo_3.core_item_id, qry_core_services_referrals_in_period_with_enc_geo_3.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICES_PROVIDED_GEO_3_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICES_PROVIDED_GEO_3_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICES_PROVIDED_GEO_3_SQL+="                    Count(*) AS number_times_provided            "
SERVICES_PROVIDED_GEO_3_SQL+="            FROM                     "
SERVICES_PROVIDED_GEO_3_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.core_item_id            "
SERVICES_PROVIDED_GEO_3_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICES_PROVIDED_GEO_3_SQL+="                    INNER JOIN             "
SERVICES_PROVIDED_GEO_3_SQL+="                            (SELECT tbl_list_geo_2.area_id    "
SERVICES_PROVIDED_GEO_3_SQL+="                            FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id) INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id    "
SERVICES_PROVIDED_GEO_3_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s    "
SERVICES_PROVIDED_GEO_3_SQL+="                            ) as qry_heir_sub_sub_sub_areas     "
SERVICES_PROVIDED_GEO_3_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_sub_areas.area_id            "
SERVICES_PROVIDED_GEO_3_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s           "
SERVICES_PROVIDED_GEO_3_SQL+="                    ) as qry_core_services_referrals_in_period_with_enc_geo_3             "
SERVICES_PROVIDED_GEO_3_SQL+="            INNER JOIN                     "
SERVICES_PROVIDED_GEO_3_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICES_PROVIDED_GEO_3_SQL+="                    FROM tbl_reg_persons_types            "
SERVICES_PROVIDED_GEO_3_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICES_PROVIDED_GEO_3_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICES_PROVIDED_GEO_3_SQL+="            ON qry_core_services_referrals_in_period_with_enc_geo_3.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICES_PROVIDED_GEO_3_SQL+="            GROUP BY qry_core_services_referrals_in_period_with_enc_geo_3.core_item_id, qry_core_services_referrals_in_period_with_enc_geo_3.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICES_PROVIDED_GEO_3_SQL+="            ) as qry_rep_services_all_geo_3                     "
SERVICES_PROVIDED_GEO_3_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_all_geo_3.core_item_id = tbl_list_general.item_id                            "
SERVICES_PROVIDED_GEO_3_SQL+="    GROUP BY core_item_id, item_description, item_category, the_order                            "
SERVICES_PROVIDED_GEO_3_SQL+="    ORDER BY item_category DESC, the_order ASC                            "
# qry_heir_sub_sub_sub_areas gets wards in the selected province
# qry_core_services_referrals_in_period_with_enc_geo_3  gets services provided during the period in the selected province
# qry_persons_type_todate_s determines whether recipients are OVC or guardians
# qry_rep_services_all_geo_3  gets distinct service-recipient combination, and makes a column containing 1 for OVC, a column containing 1 for guardians, and a column counting how many times the service was received by the recipient during the period
# final query lists the services and for each counts number of OVC, number of guardians, and total number of times service provided

# period and org unit specified
#expected parameters: 'param_to_date', 'param_to_date', param_org_unit_id, 'param_from_date', 'param_to_date'
SERVICES_PROVIDED_ORG_UNIT_SQL= "    SELECT qry_rep_services_all_org.core_item_id as \"Service code\", tbl_list_general.item_description as \"Service\", Sum(qry_rep_services_all_org.number_times_provided) AS \"Number of times provided\", Sum(qry_rep_services_all_org.OVC) AS \"Number of distinct OVC receiving\", Sum(qry_rep_services_all_org.Guardian) AS \"Number of distinct guardians receiving\"                           "
SERVICES_PROVIDED_ORG_UNIT_SQL+="    FROM                             "
SERVICES_PROVIDED_ORG_UNIT_SQL+="            (SELECT qry_core_services_referrals_in_period_with_enc_org.core_item_id, qry_core_services_referrals_in_period_with_enc_org.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    Count(*) AS number_times_provided            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="            FROM                     "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    FROM tbl_reg_persons_types            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICES_PROVIDED_ORG_UNIT_SQL+="            INNER JOIN                     "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.core_item_id            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    WHERE tbl_core_encounters.org_unit_id=%s AND tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s          "
SERVICES_PROVIDED_ORG_UNIT_SQL+="                    ) as qry_core_services_referrals_in_period_with_enc_org             "
SERVICES_PROVIDED_ORG_UNIT_SQL+="            ON qry_persons_type_todate_s.person_id = qry_core_services_referrals_in_period_with_enc_org.beneficiary_person_id                    "
SERVICES_PROVIDED_ORG_UNIT_SQL+="            GROUP BY qry_core_services_referrals_in_period_with_enc_org.core_item_id, qry_core_services_referrals_in_period_with_enc_org.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICES_PROVIDED_ORG_UNIT_SQL+="            ) as qry_rep_services_all_org                     "
SERVICES_PROVIDED_ORG_UNIT_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_all_org.core_item_id = tbl_list_general.item_id                            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="    GROUP BY core_item_id, item_description, item_category, the_order                            "
SERVICES_PROVIDED_ORG_UNIT_SQL+="    ORDER BY item_category DESC, the_order ASC                            "
# qry_persons_type_todate_s determines whether recipients are OVC or guardians
# qry_core_services_referrals_in_period_with_enc_org  gets services provided during the period by the selected org unit
# qry_rep_services_all_org  gets distinct service-recipient combination, and makes a column containing 1 for OVC, a column containing 1 for guardians, and a column counting how many times the service was received by the recipient during the period
# final query lists the services and for each counts number of OVC, number of guardians, and total number of times service provided


# period and set specified
#expected parameters: 'param_from_date', 'param_to_date', param_set_id,'param_to_date', 'param_to_date'
SERVICES_PROVIDED_SET_SQL= "    SELECT qry_rep_services_all_set.core_item_id as \"Service code\", tbl_list_general.item_description as \"Service\", Sum(qry_rep_services_all_set.number_times_provided) AS \"Number of times provided\", Sum(qry_rep_services_all_set.OVC) AS \"Number of distinct OVC receiving\", Sum(qry_rep_services_all_set.Guardian) AS \"Number of distinct guardians receiving\"                            "
SERVICES_PROVIDED_SET_SQL+="    FROM                             "
SERVICES_PROVIDED_SET_SQL+="            (SELECT qry_core_services_referrals_in_period_with_enc_set.core_item_id, qry_core_services_referrals_in_period_with_enc_set.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICES_PROVIDED_SET_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICES_PROVIDED_SET_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICES_PROVIDED_SET_SQL+="                    Count(*) AS number_times_provided            "
SERVICES_PROVIDED_SET_SQL+="            FROM                     "
SERVICES_PROVIDED_SET_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.core_item_id            "
SERVICES_PROVIDED_SET_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICES_PROVIDED_SET_SQL+="                    INNER JOIN tbl_reports_sets_org_units ON tbl_core_encounters.org_unit_id = tbl_reports_sets_org_units.org_unit_id            "
SERVICES_PROVIDED_SET_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_reports_sets_org_units.set_id=%s            "
SERVICES_PROVIDED_SET_SQL+="                    ) as qry_core_services_referrals_in_period_with_enc_set             "
SERVICES_PROVIDED_SET_SQL+="            INNER JOIN                     "
SERVICES_PROVIDED_SET_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICES_PROVIDED_SET_SQL+="                    FROM tbl_reg_persons_types            "
SERVICES_PROVIDED_SET_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICES_PROVIDED_SET_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICES_PROVIDED_SET_SQL+="            ON qry_core_services_referrals_in_period_with_enc_set.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICES_PROVIDED_SET_SQL+="            GROUP BY qry_core_services_referrals_in_period_with_enc_set.core_item_id, qry_core_services_referrals_in_period_with_enc_set.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICES_PROVIDED_SET_SQL+="            ) as qry_rep_services_all_set                     "
SERVICES_PROVIDED_SET_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_all_set.core_item_id = tbl_list_general.item_id                            "
SERVICES_PROVIDED_SET_SQL+="    GROUP BY core_item_id, item_description, item_category, the_order                            "
SERVICES_PROVIDED_SET_SQL+="    ORDER BY item_category DESC, the_order ASC                            "
# qry_core_services_referrals_in_period_with_enc_set  gets services provided during the period by the org units in selected set
# qry_persons_type_todate_s determines whether recipients are OVC or guardians
# qry_rep_services_all_set  gets distinct service-recipient combination, and makes a column containing 1 for OVC, a column containing 1 for guardians, and a column counting how many times the service was received by the recipient during the period
# final query lists the services and for each counts number of OVC, number of guardians, and total number of times service provided

#*******SERVICES PROVIDED (ONE, BY BENEFICIARY DEMOG) **********


# only period specified
#expected parameters: 'param_from_date', 'param_to_date', 'param_item_id'  

SERVICE_PROVIDED_ONE_DEMOG_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received this service\", Sum(qry_rep_services_one_demog.num_times_received) AS \"Number of times beneficiaries have received service\"                            "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="            (SELECT qry_core_services_referrals_in_period_sp.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth, Count(*) AS num_times_received                    "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="                    FROM tbl_core_services            "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s            "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="                    ) as qry_core_services_referrals_in_period_sp             "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="            ) as qry_rep_services_one_demog                     "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ONE_DEMOG_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp gets records for the specified service during the period 
# qry_rep_services_one_demog groups by beneficiary and counts how many times in the period the service was provided to each
# final query counts the number of beneficiaries, and sums the number of times the service was provided, by age and sex 

# period, ward specified
#expected parameters: 'param_from_date', 'param_to_date', 'param_item_id', param_area_id

SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL= "    SELECT tbl_list_general.item_description AS sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received this service\", Sum(qry_rep_services_one_demog_geo_0.num_times_received) AS \"Number of times beneficiaries have received service\"                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_geo_0.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth, Count(*) AS num_times_received                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s AND tbl_core_encounters.area_id=%s            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_0             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_geo_0.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_geo_0.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="            ) as qry_rep_services_one_demog_geo_0                     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_geo_0.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp_with_enc_geo_0  gets records for the specified service during the period in the specified ward
# qry_rep_services_one_demog_geo_0 groups by beneficiary and counts how many times in the period the service was provided to each
# final query counts the number of beneficiaries, and sums the number of times the service was provided, by age and sex 

# period, constituency specified
#expected parameters: 'param_from_date', 'param_to_date', 'param_item_id', param_area_id

SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received this service\", Sum(qry_rep_services_one_demog_geo_1.num_times_received) AS \"Number of times beneficiaries have received service\"                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_geo_1.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth, Count(*) AS num_times_received                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="                    INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s AND tbl_list_geo.parent_area_id=%s            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_1             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_geo_1.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_geo_1.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth) as qry_rep_services_one_demog_geo_1 INNER JOIN tbl_list_general ON qry_rep_services_one_demog_geo_1.sex_id = tbl_list_general.item_id                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp_with_enc_geo_1  gets records for the specified service during the period in the specified constituency
# qry_rep_services_one_demog_geo_1 groups by beneficiary and counts how many times in the period the service was provided to each
# final query counts the number of beneficiaries, and sums the number of times the service was provided, by age and sex 

# period, district specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', 'param_item_id' 

SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received this service\", Sum(qry_rep_services_one_demog_geo_2.num_times_received) AS \"Number of times beneficiaries have received service\"                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_geo_2.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth, Count(*) AS num_times_received                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                    INNER JOIN             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                            (SELECT tbl_list_geo_1.area_id    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                            FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                            ) as qry_heir_sub_sub_areas     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_2             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_geo_2.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_geo_2.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="            ) as qry_rep_services_one_demog_geo_2                     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_geo_2.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL+="    ORDER BY age, sex                            "
# qry_heir_sub_sub_areas gets wards in the selected district
# qry_core_services_referrals_in_period_sp_with_enc_geo_2  gets records for the specified service during the period in the specified district
# qry_rep_services_one_demog_geo_2 groups by beneficiary and counts how many times in the period the service was provided to each
# final query counts the number of beneficiaries, and sums the number of times the service was provided, by age and sex 

# period, province specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', 'param_item_id' 

SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received this service\", Sum(qry_rep_services_one_demog_geo_3.num_times_received) AS \"Number of times beneficiaries have received service\"                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_geo_3.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth, Count(*) AS num_times_received                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                    INNER JOIN             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                            (SELECT tbl_list_geo_2.area_id    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                            FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id) INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                            ) as qry_heir_sub_sub_sub_areas     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_sub_areas.area_id            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_3             "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_geo_3.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_geo_3.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="            ) as qry_rep_services_one_demog_geo_3                     "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_geo_3.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL+="    ORDER BY age, sex                            "
# qry_heir_sub_sub_sub_areas gets wards in the selected province
# qry_core_services_referrals_in_period_sp_with_enc_geo_3  gets records for the specified service during the period in the specified province
# qry_rep_services_one_demog_geo_3 groups by beneficiary and counts how many times in the period the service was provided to each
# final query counts the number of beneficiaries, and sums the number of times the service was provided, by age and sex 

# period, org unit specified
#expected parameters:'param_from_date', 'param_to_date', 'param_item_id', param_org_unit_id
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received this service\", Sum(qry_rep_services_one_demog_org.num_times_received) AS \"Number of times beneficiaries have received service\"                            "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_org.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth, Count(*) AS num_times_received                    "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s AND tbl_core_encounters.org_unit_id=%s            "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_org             "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_org.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_org.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="            ) as qry_rep_services_one_demog_org                     "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_org.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp_with_enc_org  gets records for the specified service during the period by the specified org unit
# qry_rep_services_one_demog_org groups by beneficiary and counts how many times in the period the service was provided to each
# final query counts the number of beneficiaries, and sums the number of times the service was provided, by age and sex 

# period, org unit set specified
#expected parameters:'param_from_date', 'param_to_date', 'param_item_id', param_set_id
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received this service\", Sum(qry_rep_services_one_demog_set.num_times_received) AS \"Number of times beneficiaries have received service\"                            "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_set.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth, Count(*) AS num_times_received                    "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="                    INNER JOIN tbl_reports_sets_org_units ON tbl_core_encounters.org_unit_id = tbl_reports_sets_org_units.org_unit_id            "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s AND tbl_reports_sets_org_units.set_id=%s            "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_set             "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_set.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="    GROUP BY qry_core_services_referrals_in_period_sp_with_enc_set.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth) as qry_rep_services_one_demog_set INNER JOIN tbl_list_general ON qry_rep_services_one_demog_set.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ONE_DEMOG_SET_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp_with_enc_set  gets records for the specified service during the period by the specified set of org units
# qry_rep_services_one_demog_set groups by beneficiary and counts how many times in the period the service was provided to each
# final query counts the number of beneficiaries, and sums the number of times the service was provided, by age and sex 


#*******SERVICES PROVIDED (ONE, BY ORG) **********

# only period specified
#expected parameters: 'param_from_date', 'param_to_date', 'param_item_id', 'param_to_date', 'param_to_date'
SERVICE_PROVIDED_ONE_BY_ORG_SQL= "    SELECT qry_rep_services_one_by_org.org_unit_id as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", Sum(qry_rep_services_one_by_org.OVC) AS \"Number of distinct OVC receiving service\", Sum(qry_rep_services_one_by_org.Guardian) AS \"Number of distinct guardians receiving service\", Sum(qry_rep_services_one_by_org.number_times_provided) AS \"Number of times service provided\"                          "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    Count(*) AS number_times_provided            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, tbl_core_encounters.org_unit_id            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc             "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="            INNER JOIN                     "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    FROM tbl_reg_persons_types            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="            ON qry_core_services_referrals_in_period_sp_with_enc.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="            ) as qry_rep_services_one_by_org                     "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_services_one_by_org.org_unit_id = tbl_reg_org_units.id                            "
SERVICE_PROVIDED_ONE_BY_ORG_SQL+="    GROUP BY qry_rep_services_one_by_org.org_unit_id, tbl_reg_org_units.org_unit_name                            "
# qry_core_services_referrals_in_period_sp_with_enc gets records for the selected service during the selected time period 
# qry_persons_type_todate_s determines whether service recipients are OVC or guardians as of the end of the period
# qry_rep_services_one_by_org lists each beneficiary, makes a column to count OVCs with 1s, a column to count guardians with 1s, and a column with number of times that beneficiary received the service
# final query list org units and counts OVCs, guardians and sums up number of times service provided

# period, ward specified
#expected parameters: 'param_from_date', 'param_to_date', 'param_item_id', param_area_id, 'param_to_date', 'param_to_date'

SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL= "    SELECT qry_rep_services_one_by_org_geo_0.org_unit_id as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", Sum(qry_rep_services_one_by_org_geo_0.OVC) AS \"Number of distinct OVC receiving service\", Sum(qry_rep_services_one_by_org_geo_0.Guardian) AS \"Number of distinct guardians receiving service\", Sum(qry_rep_services_one_by_org_geo_0.number_times_provided) AS \"Number of times service provided\"                          "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_geo_0.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc_geo_0.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    Count(*) AS number_times_provided            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, tbl_core_encounters.org_unit_id            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s AND tbl_core_encounters.area_id=%s            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_0             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="            INNER JOIN                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    FROM tbl_reg_persons_types            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="            ON qry_core_services_referrals_in_period_sp_with_enc_geo_0.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_geo_0.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc_geo_0.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="            ) as qry_rep_services_one_by_org_geo_0                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_services_one_by_org_geo_0.org_unit_id = tbl_reg_org_units.id                            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL+="    GROUP BY qry_rep_services_one_by_org_geo_0.org_unit_id, tbl_reg_org_units.org_unit_name                            "
# qry_core_services_referrals_in_period_sp_with_enc_geo_0  gets records for the selected service during the selected time period in the selected ward
# qry_persons_type_todate_s determines whether service recipients are OVC or guardians as of the end of the period
# qry_rep_services_one_by_org_geo_0 lists each beneficiary, makes a column to count OVCs with 1s, a column to count guardians with 1s, and a column with number of times that beneficiary received the service
# final query list org units and counts OVCs, guardians and sums up number of times service provided

# period, constituency specified
#expected parameters: 'param_from_date', 'param_to_date', 'param_item_id', param_area_id, 'param_to_date', 'param_to_date'

SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL= "    SELECT qry_rep_services_one_by_org_geo_1.org_unit_id as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", Sum(qry_rep_services_one_by_org_geo_1.OVC) AS \"Number of distinct OVC receiving service\", Sum(qry_rep_services_one_by_org_geo_1.Guardian) AS \"Number of distinct guardians receiving service\", Sum(qry_rep_services_one_by_org_geo_1.number_times_provided) AS \"Number of times service provided\"                          "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_geo_1.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc_geo_1.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    Count(*) AS number_times_provided            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, tbl_core_encounters.org_unit_id            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id) INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s AND tbl_list_geo.parent_area_id=%s            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_1             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="            INNER JOIN                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    FROM tbl_reg_persons_types            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="            ON qry_core_services_referrals_in_period_sp_with_enc_geo_1.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_geo_1.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc_geo_1.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="            ) as qry_rep_services_one_by_org_geo_1                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_services_one_by_org_geo_1.org_unit_id = tbl_reg_org_units.id                            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL+="    GROUP BY qry_rep_services_one_by_org_geo_1.org_unit_id, tbl_reg_org_units.org_unit_name;                            "
# qry_core_services_referrals_in_period_sp_with_enc_geo_1  gets records for the selected service during the selected time period in the selected constituency
# qry_persons_type_todate_s determines whether service recipients are OVC or guardians as of the end of the period
# qry_rep_services_one_by_org_geo_1 lists each beneficiary, makes a column to count OVCs with 1s, a column to count guardians with 1s, and a column with number of times that beneficiary received the service
# final query list org units and counts OVCs, guardians and sums up number of times service provided

# period, district specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', 'param_item_id', 'param_to_date', 'param_to_date'

SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL= "    SELECT qry_rep_services_one_by_org_geo_2.org_unit_id as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", Sum(qry_rep_services_one_by_org_geo_2.OVC) AS \"Number of distinct OVC receiving service\", Sum(qry_rep_services_one_by_org_geo_2.Guardian) AS \"Number of distinct guardians receiving service\", Sum(qry_rep_services_one_by_org_geo_2.number_times_provided) AS \"Number of times service provided\"                          "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_geo_2.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc_geo_2.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    Count(*) AS number_times_provided            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, tbl_core_encounters.org_unit_id            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    FROM (tbl_core_encounters INNER JOIN             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                            (SELECT tbl_list_geo_1.area_id    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                            FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s   "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                            ) as qry_heir_sub_sub_areas     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id)             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_2             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="            INNER JOIN                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    FROM tbl_reg_persons_types            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="            ON qry_core_services_referrals_in_period_sp_with_enc_geo_2.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_geo_2.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc_geo_2.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="            ) as qry_rep_services_one_by_org_geo_2                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_services_one_by_org_geo_2.org_unit_id = tbl_reg_org_units.id                            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL+="    GROUP BY qry_rep_services_one_by_org_geo_2.org_unit_id, tbl_reg_org_units.org_unit_name                            "
# qry_heir_sub_sub_areas gets wards within the selected district
# qry_core_services_referrals_in_period_sp_with_enc_geo_2  gets records for the selected service during the selected time period in the selected district
# qry_persons_type_todate_s determines whether service recipients are OVC or guardians as of the end of the period
# qry_rep_services_one_by_org_geo_2 lists each beneficiary, makes a column to count OVCs with 1s, a column to count guardians with 1s, and a column with number of times that beneficiary received the service
# final query list org units and counts OVCs, guardians and sums up number of times service provided

# period, province specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', 'param_item_id', 'param_to_date', 'param_to_date'

SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL= "    SELECT qry_rep_services_one_by_org_geo_3.org_unit_id as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", Sum(qry_rep_services_one_by_org_geo_3.OVC) AS \"Number of distinct OVC receiving service\", Sum(qry_rep_services_one_by_org_geo_3.Guardian) AS \"Number of distinct guardians receiving service\", Sum(qry_rep_services_one_by_org_geo_3.number_times_provided) AS \"Number of times service provided\"                          "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="    FROM                             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="            (SELECT qry_core_services_referrals_in_period_sp_with_enc_geo_3.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc_geo_3.beneficiary_person_id, qry_persons_type_todate_s.person_type_id,                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    CASE WHEN (person_type_id='TBVC') THEN 1 ELSE 0 END AS OVC,            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    CASE WHEN (person_type_id='TBGR') THEN 1 ELSE 0 END AS Guardian,             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    Count(*) AS number_times_provided            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="            FROM                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, tbl_core_encounters.org_unit_id            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    FROM (tbl_core_encounters INNER JOIN             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                            (SELECT tbl_list_geo_2.area_id    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                            FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id) INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                            ) as qry_heir_sub_sub_sub_areas    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_sub_areas.area_id)             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_services.core_item_id=%s            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_3             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="            INNER JOIN                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    FROM tbl_reg_persons_types            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="                    ) as qry_persons_type_todate_s             "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="            ON qry_core_services_referrals_in_period_sp_with_enc_geo_3.beneficiary_person_id = qry_persons_type_todate_s.person_id                    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="            GROUP BY qry_core_services_referrals_in_period_sp_with_enc_geo_3.org_unit_id, qry_core_services_referrals_in_period_sp_with_enc_geo_3.beneficiary_person_id, qry_persons_type_todate_s.person_type_id, OVC, Guardian                    "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="            ) as qry_rep_services_one_by_org_geo_3                     "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_services_one_by_org_geo_3.org_unit_id = tbl_reg_org_units.id                            "
SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL+="    GROUP BY qry_rep_services_one_by_org_geo_3.org_unit_id, tbl_reg_org_units.org_unit_name                            "
# qry_heir_sub_sub_sub_areas gets wards within the selected province
# qry_core_services_referrals_in_period_sp_with_enc_geo_3  gets records for the selected service during the selected time period in the selected province
# qry_persons_type_todate_s determines whether service recipients are OVC or guardians as of the end of the period
# qry_rep_services_one_by_org_geo_3 lists each beneficiary, makes a column to count OVCs with 1s, a column to count guardians with 1s, and a column with number of times that beneficiary received the service
# final query list org units and counts OVCs, guardians and sums up number of times service provided

#*******SERVICES PROVIDED (ANY, BY BENEFICIARY DEMOG) **********


# only period specified
#expected parameters: 'param_from_date', 'param_to_date'  

SERVICE_PROVIDED_ANY_DEMOG_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received any service or referral\"                       "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="    FROM                             "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="            (SELECT distinct qry_core_services_referrals_in_period_sp.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                   "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="            FROM                     "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="                    FROM tbl_core_services            "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s            "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="                    ) as qry_core_services_referrals_in_period_sp             "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="            ) as qry_rep_services_one_demog                     "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ANY_DEMOG_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp gets records for any service during the period 
# qry_rep_services_one_demog groups by distinct beneficiary 
# final query counts the number of beneficiaries  by age and sex 

# period, ward specified
#expected parameters: 'param_from_date', 'param_to_date',  param_area_id

SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL= "    SELECT tbl_list_general.item_description AS sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received any service or referral\"                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="    FROM                             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="            (SELECT distinct qry_core_services_referrals_in_period_sp_with_enc_geo_0.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="            FROM                     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_encounters.area_id=%s            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_0             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_geo_0.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="            ) as qry_rep_services_one_demog_geo_0                     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_geo_0.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_0_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp_with_enc_geo_0  gets records for any service during the period in the specified ward
# qry_rep_services_one_demog_geo_0 gets distinct beneficiaries 
# final query counts the number of beneficiaries  by age and sex 

# period, constituency specified
#expected parameters: 'param_from_date', 'param_to_date',  param_area_id

SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received any service or referral\"                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="    FROM                             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="            (SELECT distinct qry_core_services_referrals_in_period_sp_with_enc_geo_1.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                  "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="            FROM                     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="                    INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_list_geo.parent_area_id=%s            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_1             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_geo_1.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="            ) as qry_rep_services_one_demog_geo_1     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_geo_1.sex_id = tbl_list_general.item_id                    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_1_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp_with_enc_geo_1  gets records for any service during the period in the specified constituency
# qry_rep_services_one_demog_geo_1 groups by distinct beneficiary 
# final query counts the number of beneficiaries  by age and sex 

# period, district specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date'

SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received any service or referral\"                           "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="    FROM                             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="            (SELECT distinct qry_core_services_referrals_in_period_sp_with_enc_geo_2.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                  "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="            FROM                     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                    INNER JOIN             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                            (SELECT tbl_list_geo_1.area_id    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                            FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                            ) as qry_heir_sub_sub_areas     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_2             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_geo_2.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="            ) as qry_rep_services_one_demog_geo_2                     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_geo_2.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_2_SQL+="    ORDER BY age, sex                            "
# qry_heir_sub_sub_areas gets wards in the selected district
# qry_core_services_referrals_in_period_sp_with_enc_geo_2  gets records for any service during the period in the specified district
# qry_rep_services_one_demog_geo_2 groups by distinct beneficiary 
# final query counts the number of beneficiaries by age and sex 

# period, province specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date'

SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received any service or referral\"                           "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="    FROM                             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="            (SELECT distinct qry_core_services_referrals_in_period_sp_with_enc_geo_3.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                   "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="            FROM                     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                    (SELECT  tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                    INNER JOIN             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                            (SELECT tbl_list_geo_2.area_id    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                            FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id) INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                            ) as qry_heir_sub_sub_sub_areas     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_sub_areas.area_id            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_geo_3             "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_geo_3.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="            ) as qry_rep_services_one_demog_geo_3                     "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_geo_3.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ANY_DEMOG_GEO_3_SQL+="    ORDER BY age, sex                            "
# qry_heir_sub_sub_sub_areas gets wards in the selected province
# qry_core_services_referrals_in_period_sp_with_enc_geo_3  gets records for any service during the period in the specified province
# qry_rep_services_one_demog_geo_3 groups by distinct beneficiary
# final query counts the number of beneficiaries by age and sex 

# period, org unit specified
#expected parameters:'param_from_date', 'param_to_date',  param_org_unit_id
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received any service or referral\"                            "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="    FROM                             "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="            (SELECT distinct qry_core_services_referrals_in_period_sp_with_enc_org.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                    "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="            FROM                     "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_core_encounters.org_unit_id=%s            "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_org             "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_org.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="            ) as qry_rep_services_one_demog_org                     "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_org.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ANY_DEMOG_ORG_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp_with_enc_org  gets records for any service during the period by the specified org unit
# qry_rep_services_one_demog_org groups by distinct beneficiary 
# final query counts the number of beneficiaries  by age and sex 

# period, org unit set specified
#expected parameters:'param_from_date', 'param_to_date',  param_set_id
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(current_date-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of distinct beneficiaries who have received any service or referral\"                            "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="    FROM                             "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="            (SELECT distinct qry_core_services_referrals_in_period_sp_with_enc_set.beneficiary_person_id, tbl_reg_persons.sex_id, tbl_reg_persons.date_of_birth                   "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="            FROM                     "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="                    (SELECT tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date            "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="                    FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id))             "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="                    INNER JOIN tbl_reports_sets_org_units ON tbl_core_encounters.org_unit_id = tbl_reports_sets_org_units.org_unit_id            "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="                    WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND tbl_reports_sets_org_units.set_id=%s            "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="                    ) as qry_core_services_referrals_in_period_sp_with_enc_set             "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="            INNER JOIN tbl_reg_persons ON qry_core_services_referrals_in_period_sp_with_enc_set.beneficiary_person_id = tbl_reg_persons.id                    "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="            ) as qry_rep_services_one_demog_set "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="    INNER JOIN tbl_list_general ON qry_rep_services_one_demog_set.sex_id = tbl_list_general.item_id                            "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="    GROUP BY sex, age                            "
SERVICE_PROVIDED_ANY_DEMOG_SET_SQL+="    ORDER BY age, sex                            "
# qry_core_services_referrals_in_period_sp_with_enc_set  gets records for any service during the period by the specified set of org units
# qry_rep_services_one_demog_set groups by distinct beneficiary 
# final query counts the number of beneficiaries by age and sex 




#*******WORKFORCE BENEFICIARY RATIO BY ORG UNIT **********

# only period specified
#expected parameters: 'param_from_date', 'param_to_date',  'param_from_date', 'param_to_date'

WF_BEN_RATIO_BY_ORG_SQL= "    SELECT tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", qry_rep_wf_ben_ratio_all_wf_ct.number_workforce as \"Number of distinct workforce members providing services\", qry_rep_wf_ben_ratio_all_ben_ct.number_beneficiaries as \"Number of distinct beneficiaries receiving services\"                            "
WF_BEN_RATIO_BY_ORG_SQL+="    FROM (                            "
WF_BEN_RATIO_BY_ORG_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_wf.org_unit_id, Count(*) AS number_workforce                    "
WF_BEN_RATIO_BY_ORG_SQL+="            FROM                     "
WF_BEN_RATIO_BY_ORG_SQL+="                    (SELECT DISTINCT qry_core_encounters_in_period.org_unit_id, qry_core_encounters_in_period.workforce_person_id            "
WF_BEN_RATIO_BY_ORG_SQL+="                    FROM             "
WF_BEN_RATIO_BY_ORG_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id    "
WF_BEN_RATIO_BY_ORG_SQL+="                            FROM tbl_core_encounters    "
WF_BEN_RATIO_BY_ORG_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s    "
WF_BEN_RATIO_BY_ORG_SQL+="                            ) as qry_core_encounters_in_period    "
WF_BEN_RATIO_BY_ORG_SQL+="                    ) as qry_rep_wf_ben_ratio_all_wf            "
WF_BEN_RATIO_BY_ORG_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_wf.org_unit_id                    "
WF_BEN_RATIO_BY_ORG_SQL+="            ) as qry_rep_wf_ben_ratio_all_wf_ct                     "
WF_BEN_RATIO_BY_ORG_SQL+="    INNER JOIN                             "
WF_BEN_RATIO_BY_ORG_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_ben.org_unit_id, Count(*) AS number_beneficiaries                    "
WF_BEN_RATIO_BY_ORG_SQL+="            FROM                    "
WF_BEN_RATIO_BY_ORG_SQL+="                    (SELECT DISTINCT qry_core_encounters_in_period.org_unit_id, qry_core_encounters_in_period.beneficiary_person_id            "
WF_BEN_RATIO_BY_ORG_SQL+="                    FROM             "
WF_BEN_RATIO_BY_ORG_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id    "
WF_BEN_RATIO_BY_ORG_SQL+="                            FROM tbl_core_encounters    "
WF_BEN_RATIO_BY_ORG_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s    "
WF_BEN_RATIO_BY_ORG_SQL+="                            ) as qry_core_encounters_in_period    "
WF_BEN_RATIO_BY_ORG_SQL+="                    ) as qry_rep_wf_ben_ratio_all_ben            "
WF_BEN_RATIO_BY_ORG_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_ben.org_unit_id                    "
WF_BEN_RATIO_BY_ORG_SQL+="            ) as qry_rep_wf_ben_ratio_all_ben_ct                     "
WF_BEN_RATIO_BY_ORG_SQL+="    ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = qry_rep_wf_ben_ratio_all_ben_ct.org_unit_id)                             "
WF_BEN_RATIO_BY_ORG_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = tbl_reg_org_units.id                            "
#qry_core_encounters_in_period gets records in the selected period
#qry_rep_wf_ben_ratio_all_wf gets distinct org unit workforce combinations
#qry_rep_wf_ben_ratio_all_wf_ct counts number of distinct workforce members by org unit
#qry_rep_wf_ben_ratio_all_ben gets distinct org unit beneficiary combinations
#qry_rep_wf_ben_ratio_all_ben_ct counts number of distinct beneficiary members by org unit
#final query puts everything together, and gets the org unit name

# period, constituency specified
#expected parameters: 'param_from_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date', param_area_id

WF_BEN_RATIO_BY_ORG_GEO_0_SQL= "    SELECT tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", qry_rep_wf_ben_ratio_all_wf_ct.number_workforce as \"Number of distinct workforce members providing services\", qry_rep_wf_ben_ratio_all_ben_ct.number_beneficiaries as \"Number of distinct beneficiaries receiving services\"                            "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="    FROM (                            "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_wf.org_unit_id, Count(*) AS number_workforce                    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="            FROM                     "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                    (SELECT DISTINCT org_unit_id, workforce_person_id            "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                    FROM             "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                            FROM tbl_core_encounters    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s And tbl_core_encounters.area_id=%s    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                            ) as qry_core_encounters_in_period_geo_0    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                    ) as qry_rep_wf_ben_ratio_all_wf            "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_wf.org_unit_id                    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="            ) as qry_rep_wf_ben_ratio_all_wf_ct                     "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="    INNER JOIN                             "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_ben.org_unit_id, Count(*) AS number_beneficiaries                    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="            FROM                    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                    (SELECT DISTINCT org_unit_id, beneficiary_person_id            "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                    FROM             "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                            FROM tbl_core_encounters    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s And tbl_core_encounters.area_id=%s    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                            ) as qry_core_encounters_in_period_geo_0    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="                    ) as qry_rep_wf_ben_ratio_all_ben            "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_ben.org_unit_id                    "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="            ) as qry_rep_wf_ben_ratio_all_ben_ct                     "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="    ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = qry_rep_wf_ben_ratio_all_ben_ct.org_unit_id)                             "
WF_BEN_RATIO_BY_ORG_GEO_0_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = tbl_reg_org_units.id                            "
#qry_core_encounters_in_period_geo_0 gets records in the selected period in the selected ward
#qry_rep_wf_ben_ratio_all_wf gets distinct org unit workforce combinations
#qry_rep_wf_ben_ratio_all_wf_ct counts number of distinct workforce members by org unit
#qry_rep_wf_ben_ratio_all_ben gets distinct org unit beneficiary combinations
#qry_rep_wf_ben_ratio_all_ben_ct counts number of distinct beneficiary members by org unit
#final query puts everything together, and gets the org unit name

# period, district specified
#expected parameters: 'param_from_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date', param_area_id
WF_BEN_RATIO_BY_ORG_GEO_1_SQL= "    SELECT tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", qry_rep_wf_ben_ratio_all_wf_ct.number_workforce as \"Number of distinct workforce members providing services\", qry_rep_wf_ben_ratio_all_ben_ct.number_beneficiaries as \"Number of distinct beneficiaries receiving services\"                            "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="    FROM (                            "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_wf.org_unit_id, Count(*) AS number_workforce                    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="            FROM                     "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                    (SELECT DISTINCT org_unit_id, workforce_person_id            "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                    FROM             "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                            FROM tbl_core_encounters INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s And tbl_list_geo.parent_area_id=%s    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                            ) as qry_core_encounters_in_period_geo_1    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                    ) as qry_rep_wf_ben_ratio_all_wf            "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_wf.org_unit_id                    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="            ) as qry_rep_wf_ben_ratio_all_wf_ct                     "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="    INNER JOIN                             "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_ben.org_unit_id, Count(*) AS number_beneficiaries                    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="            FROM                    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                    (SELECT DISTINCT org_unit_id, beneficiary_person_id            "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                    FROM             "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                            FROM tbl_core_encounters INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s And tbl_list_geo.parent_area_id=%s    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                            ) as qry_core_encounters_in_period_geo_1    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="                    ) as qry_rep_wf_ben_ratio_all_ben            "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_ben.org_unit_id                    "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="            ) as qry_rep_wf_ben_ratio_all_ben_ct                     "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="    ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = qry_rep_wf_ben_ratio_all_ben_ct.org_unit_id)                             "
WF_BEN_RATIO_BY_ORG_GEO_1_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = tbl_reg_org_units.id                            "
#qry_core_encounters_in_period_geo_1 gets records in the selected period in the selected constituency
#qry_rep_wf_ben_ratio_all_wf gets distinct org unit workforce combinations
#qry_rep_wf_ben_ratio_all_wf_ct counts number of distinct workforce members by org unit
#qry_rep_wf_ben_ratio_all_ben gets distinct org unit beneficiary combinations
#qry_rep_wf_ben_ratio_all_ben_ct counts number of distinct beneficiary members by org unit
#final query puts everything together, and gets the org unit name

# period, district specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date'

WF_BEN_RATIO_BY_ORG_GEO_2_SQL= "    SELECT tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", qry_rep_wf_ben_ratio_all_wf_ct.number_workforce as \"Number of distinct workforce members providing services\", qry_rep_wf_ben_ratio_all_ben_ct.number_beneficiaries as \"Number of distinct beneficiaries receiving services\"                                    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="    FROM (                                    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_wf.org_unit_id, Count(*) AS number_workforce                            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="            FROM                             "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                    (SELECT DISTINCT org_unit_id, workforce_person_id                    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                    FROM                     "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            FROM tbl_core_encounters INNER JOIN             "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                                    (SELECT tbl_list_geo_1.area_id    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                                    WHERE tbl_list_geo.parent_area_id=%s    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                                    ) as qry_heir_sub_sub_areas     "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            ) as qry_core_encounters_in_period_geo_2            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                    ) as qry_rep_wf_ben_ratio_all_wf                    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_wf.org_unit_id                            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="            ) as qry_rep_wf_ben_ratio_all_wf_ct                             "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="    INNER JOIN                                     "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_ben.org_unit_id, Count(*) AS number_beneficiaries                            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="            FROM                            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                    (SELECT DISTINCT org_unit_id, beneficiary_person_id                    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                    FROM                     "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            FROM tbl_core_encounters INNER JOIN             "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                                    (SELECT tbl_list_geo_1.area_id    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                                    WHERE tbl_list_geo.parent_area_id=%s    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                                    ) as qry_heir_sub_sub_areas     "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                            ) as qry_core_encounters_in_period_geo_2            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="                    ) as qry_rep_wf_ben_ratio_all_ben                    "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_ben.org_unit_id                            "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="            ) as qry_rep_wf_ben_ratio_all_ben_ct                             "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="    ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = qry_rep_wf_ben_ratio_all_ben_ct.org_unit_id)                                     "
WF_BEN_RATIO_BY_ORG_GEO_2_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = tbl_reg_org_units.id                                    "
#qry_heir_sub_sub_areas gets wards within the selected district
#qry_core_encounters_in_period_geo_2 gets records in the selected period in the selected district
#qry_rep_wf_ben_ratio_all_wf gets distinct org unit workforce combinations
#qry_rep_wf_ben_ratio_all_wf_ct counts number of distinct workforce members by org unit
#qry_rep_wf_ben_ratio_all_ben gets distinct org unit beneficiary combinations
#qry_rep_wf_ben_ratio_all_ben_ct counts number of distinct beneficiary members by org unit
#final query puts everything together, and gets the org unit name

# period, province specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date'

WF_BEN_RATIO_BY_ORG_GEO_3_SQL= "    SELECT tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", qry_rep_wf_ben_ratio_all_wf_ct.number_workforce as \"Number of distinct workforce members providing services\", qry_rep_wf_ben_ratio_all_ben_ct.number_beneficiaries as \"Number of distinct beneficiaries receiving services\"                                    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="    FROM (                                    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_wf.org_unit_id, Count(*) AS number_workforce                            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="            FROM                             "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                    (SELECT DISTINCT org_unit_id, workforce_person_id                    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                    FROM                     "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            FROM tbl_core_encounters INNER JOIN             "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    (SELECT tbl_list_geo_2.area_id    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)     "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    WHERE tbl_list_geo.parent_area_id=%s    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    ) as qry_heir_sub_sub_sub_areas     "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            ON tbl_core_encounters.area_id = qry_heir_sub_sub_sub_areas.area_id            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            ) as qry_core_encounters_in_period_geo_3            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                    ) as qry_rep_wf_ben_ratio_all_wf                    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_wf.org_unit_id                            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="            ) as qry_rep_wf_ben_ratio_all_wf_ct                             "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="    INNER JOIN                                     "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="            (SELECT qry_rep_wf_ben_ratio_all_ben.org_unit_id, Count(*) AS number_beneficiaries                            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="            FROM                            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                    (SELECT DISTINCT org_unit_id, beneficiary_person_id                    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                    FROM                     "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            (SELECT tbl_core_encounters.beneficiary_person_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.org_unit_id            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            FROM tbl_core_encounters INNER JOIN             "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    (SELECT tbl_list_geo_2.area_id    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)     "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    WHERE tbl_list_geo.parent_area_id=%s    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                                    ) as qry_heir_sub_sub_sub_areas     "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            ON tbl_core_encounters.area_id = qry_heir_sub_sub_sub_areas.area_id            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                            ) as qry_core_encounters_in_period_geo_3            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="                    ) as qry_rep_wf_ben_ratio_all_ben                    "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="            GROUP BY qry_rep_wf_ben_ratio_all_ben.org_unit_id                            "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="            ) as qry_rep_wf_ben_ratio_all_ben_ct                             "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="    ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = qry_rep_wf_ben_ratio_all_ben_ct.org_unit_id)                                     "
WF_BEN_RATIO_BY_ORG_GEO_3_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_wf_ben_ratio_all_wf_ct.org_unit_id = tbl_reg_org_units.id                                    "
#qry_heir_sub_sub_sub_areas gets wards within the selected province
#qry_core_encounters_in_period_geo_3 gets records in the selected period in the selected province
#qry_rep_wf_ben_ratio_all_wf gets distinct org unit workforce combinations
#qry_rep_wf_ben_ratio_all_wf_ct counts number of distinct workforce members by org unit
#qry_rep_wf_ben_ratio_all_ben gets distinct org unit beneficiary combinations
#qry_rep_wf_ben_ratio_all_ben_ct counts number of distinct beneficiary members by org unit
#final query puts everything together, and gets the org unit name


#*******REFERRALS MADE AND COMPLETED **********

# only period specified
#expected parameters: 'param_from_date', 'param_to_date',  'param_from_date'

REFERRALS_SQL= "    SELECT qry_rep_referrals_all_distinct.core_item_id as \"Referral code\", item_description AS \"Referral type\", Count(*) AS \"Number of beneficiaries receiving referral during period\", Sum(qry_rep_referrals_all_distinct.referral_completed) AS \"Of whom number of beneficiaries completing referral\"                            "
REFERRALS_SQL+="    FROM                             "
REFERRALS_SQL+="            (SELECT DISTINCT qry_rep_referrals_all.beneficiary_person_id, qry_rep_referrals_all.core_item_id, qry_rep_referrals_all.referral_completed                    "
REFERRALS_SQL+="            FROM                     "
REFERRALS_SQL+="                    (SELECT qry_core_referrals_made_in_period_s.beneficiary_person_id, qry_core_referrals_made_in_period_s.core_item_id, qry_core_referrals_comp_in_period_s.encounter_date,             "
REFERRALS_SQL+="                            CASE WHEN (qry_core_referrals_comp_in_period_s.encounter_date is not null) THEN 1 ELSE 0 END AS referral_completed    "
REFERRALS_SQL+="                    FROM             "
REFERRALS_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right    "
REFERRALS_SQL+="                            FROM tbl_core_services    "
REFERRALS_SQL+="                            WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND Left(core_item_id,2)='RM'    "
REFERRALS_SQL+="                            ) as qry_core_referrals_made_in_period_s     "
REFERRALS_SQL+="                    LEFT JOIN             "
REFERRALS_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right    "
REFERRALS_SQL+="                            FROM tbl_core_services    "
REFERRALS_SQL+="                            WHERE tbl_core_services.encounter_date>=%s AND Left(core_item_id,2)='RC'    "
REFERRALS_SQL+="                            ) as qry_core_referrals_comp_in_period_s     "
REFERRALS_SQL+="                    ON (qry_core_referrals_made_in_period_s.beneficiary_person_id = qry_core_referrals_comp_in_period_s.beneficiary_person_id) AND (qry_core_referrals_made_in_period_s.core_item_id_right = qry_core_referrals_comp_in_period_s.core_item_id_right)            "
REFERRALS_SQL+="                    WHERE qry_core_referrals_comp_in_period_s.encounter_date>qry_core_referrals_made_in_period_s.encounter_date Or qry_core_referrals_comp_in_period_s.encounter_date Is Null            "
REFERRALS_SQL+="                    ) as qry_rep_referrals_all            "
REFERRALS_SQL+="            ) as qry_rep_referrals_all_distinct                     "
REFERRALS_SQL+="    INNER JOIN tbl_list_general ON qry_rep_referrals_all_distinct.core_item_id = tbl_list_general.item_id                            "
REFERRALS_SQL+="    GROUP BY qry_rep_referrals_all_distinct.core_item_id, item_description                            "
#qry_core_referrals_made_in_period_s gets referrals made during period
#qry_core_referrals_comp_in_period_s  gets referrals completed any time after start of period 
#qry_rep_referrals_all gets referrals made, and if same beneficiary completed same referral after it was made, marks referral_completed with 1
#qry_rep_referrals_all_distinct ensures each beneficiary and referral type combination counted only once
#final query counts referrals made, and of those, how many completed


#  period, ward specified
#expected parameters: 'param_from_date', 'param_to_date', param_area_id, 'param_from_date'
REFERRALS_GEO_0_SQL= "    SELECT qry_rep_referrals_all_distinct.core_item_id as \"Referral code\", item_description AS \"Referral type\", Count(*) AS \"Number of beneficiaries receiving referral during period\", Sum(qry_rep_referrals_all_distinct.referral_completed) AS \"Of whom number of beneficiaries completing referral\"                            "
REFERRALS_GEO_0_SQL+="    FROM                             "
REFERRALS_GEO_0_SQL+="            (SELECT DISTINCT qry_rep_referrals_all.beneficiary_person_id, qry_rep_referrals_all.core_item_id, qry_rep_referrals_all.referral_completed                    "
REFERRALS_GEO_0_SQL+="            FROM                     "
REFERRALS_GEO_0_SQL+="                    (SELECT qry_core_referrals_in_period_with_enc_geo_0.beneficiary_person_id, qry_core_referrals_in_period_with_enc_geo_0.core_item_id, qry_core_referrals_comp_in_period_s.encounter_date,             "
REFERRALS_GEO_0_SQL+="                            CASE WHEN (qry_core_referrals_comp_in_period_s.encounter_date is not null) THEN 1 ELSE 0 END AS referral_completed    "
REFERRALS_GEO_0_SQL+="                    FROM             "
REFERRALS_GEO_0_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right    "
REFERRALS_GEO_0_SQL+="                            FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)    "
REFERRALS_GEO_0_SQL+="                            WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND Left(core_item_id,2)='RM' AND tbl_core_encounters.area_id=%s    "
REFERRALS_GEO_0_SQL+="                            ) as qry_core_referrals_in_period_with_enc_geo_0    "
REFERRALS_GEO_0_SQL+="                    LEFT JOIN             "
REFERRALS_GEO_0_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right    "
REFERRALS_GEO_0_SQL+="                            FROM tbl_core_services    "
REFERRALS_GEO_0_SQL+="                            WHERE tbl_core_services.encounter_date>=%s AND Left(core_item_id,2)='RC'    "
REFERRALS_GEO_0_SQL+="                            ) as qry_core_referrals_comp_in_period_s     "
REFERRALS_GEO_0_SQL+="                    ON (qry_core_referrals_in_period_with_enc_geo_0.beneficiary_person_id = qry_core_referrals_comp_in_period_s.beneficiary_person_id) AND (qry_core_referrals_in_period_with_enc_geo_0.core_item_id_right = qry_core_referrals_comp_in_period_s.core_item_id_right)            "
REFERRALS_GEO_0_SQL+="                    WHERE qry_core_referrals_comp_in_period_s.encounter_date>qry_core_referrals_in_period_with_enc_geo_0.encounter_date Or qry_core_referrals_comp_in_period_s.encounter_date Is Null            "
REFERRALS_GEO_0_SQL+="                    ) as qry_rep_referrals_all            "
REFERRALS_GEO_0_SQL+="            ) as qry_rep_referrals_all_distinct                     "
REFERRALS_GEO_0_SQL+="    INNER JOIN tbl_list_general ON qry_rep_referrals_all_distinct.core_item_id = tbl_list_general.item_id                            "
REFERRALS_GEO_0_SQL+="    GROUP BY qry_rep_referrals_all_distinct.core_item_id, item_description                            "
#qry_core_referrals_in_period_with_enc_geo_0 gets referrals made during period in the selected ward
#qry_core_referrals_comp_in_period_s  gets referrals completed any time after start of period (anywhere)
#qry_rep_referrals_all gets referrals made, and if same beneficiary completed same referral after it was made, marks referral_completed with 1
#qry_rep_referrals_all_distinct ensures each beneficiary and referral type combination counted only once
#final query counts referrals made, and of those, how many completed

#  period, constituency specified
#expected parameters: 'param_from_date', 'param_to_date', param_area_id, 'param_from_date'
REFERRALS_GEO_1_SQL= "    SELECT qry_rep_referrals_all_distinct.core_item_id as \"Referral code\", item_description AS \"Referral type\", Count(*) AS \"Number of beneficiaries receiving referral during period\", Sum(qry_rep_referrals_all_distinct.referral_completed) AS \"Of whom number of beneficiaries completing referral\"                            "
REFERRALS_GEO_1_SQL+="    FROM                             "
REFERRALS_GEO_1_SQL+="            (SELECT DISTINCT qry_rep_referrals_all.beneficiary_person_id, qry_rep_referrals_all.core_item_id, qry_rep_referrals_all.referral_completed                    "
REFERRALS_GEO_1_SQL+="            FROM                     "
REFERRALS_GEO_1_SQL+="                    (SELECT qry_core_referrals_in_period_with_enc_geo_1.beneficiary_person_id, qry_core_referrals_in_period_with_enc_geo_1.core_item_id, qry_core_referrals_comp_in_period_s.encounter_date,             "
REFERRALS_GEO_1_SQL+="                            CASE WHEN (qry_core_referrals_comp_in_period_s.encounter_date is not null) THEN 1 ELSE 0 END AS referral_completed    "
REFERRALS_GEO_1_SQL+="                    FROM             "
REFERRALS_GEO_1_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right    "
REFERRALS_GEO_1_SQL+="                            FROM (tbl_core_encounters INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id)     "
REFERRALS_GEO_1_SQL+="                            INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)    "
REFERRALS_GEO_1_SQL+="                            WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND Left(core_item_id,2)='RM' AND tbl_list_geo.parent_area_id=%s    "
REFERRALS_GEO_1_SQL+="                            ) as qry_core_referrals_in_period_with_enc_geo_1    "
REFERRALS_GEO_1_SQL+="                    LEFT JOIN             "
REFERRALS_GEO_1_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right    "
REFERRALS_GEO_1_SQL+="                            FROM tbl_core_services    "
REFERRALS_GEO_1_SQL+="                            WHERE tbl_core_services.encounter_date>=%s AND Left(core_item_id,2)='RC'    "
REFERRALS_GEO_1_SQL+="                            ) as qry_core_referrals_comp_in_period_s     "
REFERRALS_GEO_1_SQL+="                    ON (qry_core_referrals_in_period_with_enc_geo_1.beneficiary_person_id = qry_core_referrals_comp_in_period_s.beneficiary_person_id) AND (qry_core_referrals_in_period_with_enc_geo_1.core_item_id_right = qry_core_referrals_comp_in_period_s.core_item_id_right)            "
REFERRALS_GEO_1_SQL+="                    WHERE qry_core_referrals_comp_in_period_s.encounter_date>qry_core_referrals_in_period_with_enc_geo_1.encounter_date Or qry_core_referrals_comp_in_period_s.encounter_date Is Null            "
REFERRALS_GEO_1_SQL+="                    ) as qry_rep_referrals_all            "
REFERRALS_GEO_1_SQL+="            ) as qry_rep_referrals_all_distinct                     "
REFERRALS_GEO_1_SQL+="    INNER JOIN tbl_list_general ON qry_rep_referrals_all_distinct.core_item_id = tbl_list_general.item_id                            "
REFERRALS_GEO_1_SQL+="    GROUP BY qry_rep_referrals_all_distinct.core_item_id, item_description                            "
#qry_core_referrals_in_period_with_enc_geo_1 gets referrals made during period in the selected constituency
#qry_core_referrals_comp_in_period_s  gets referrals completed any time after start of period (anywhere)
#qry_rep_referrals_all gets referrals made, and if same beneficiary completed same referral after it was made, marks referral_completed with 1
#qry_rep_referrals_all_distinct ensures each beneficiary and referral type combination counted only once
#final query counts referrals made, and of those, how many completed

#  period, district specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', 'param_from_date'
REFERRALS_GEO_2_SQL= "    SELECT qry_rep_referrals_all_distinct.core_item_id as \"Referral code\", item_description AS \"Referral type\", Count(*) AS \"Number of beneficiaries receiving referral during period\", Sum(qry_rep_referrals_all_distinct.referral_completed) AS \"Of whom number of beneficiaries completing referral\"                                    "
REFERRALS_GEO_2_SQL+="    FROM                                     "
REFERRALS_GEO_2_SQL+="            (SELECT DISTINCT qry_rep_referrals_all.beneficiary_person_id, qry_rep_referrals_all.core_item_id, qry_rep_referrals_all.referral_completed                            "
REFERRALS_GEO_2_SQL+="            FROM                             "
REFERRALS_GEO_2_SQL+="                    (SELECT qry_core_referrals_in_period_with_enc_geo_2.beneficiary_person_id, qry_core_referrals_in_period_with_enc_geo_2.core_item_id, qry_core_referrals_comp_in_period_s.encounter_date,                     "
REFERRALS_GEO_2_SQL+="                            CASE WHEN (qry_core_referrals_comp_in_period_s.encounter_date is not null) THEN 1 ELSE 0 END AS referral_completed            "
REFERRALS_GEO_2_SQL+="                    FROM                     "
REFERRALS_GEO_2_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right            "
REFERRALS_GEO_2_SQL+="                            FROM (tbl_core_encounters INNER JOIN             "
REFERRALS_GEO_2_SQL+="                                    (SELECT tbl_list_geo_1.area_id    "
REFERRALS_GEO_2_SQL+="                                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id    "
REFERRALS_GEO_2_SQL+="                                    WHERE tbl_list_geo.parent_area_id=%s    "
REFERRALS_GEO_2_SQL+="                                    ) as qry_heir_sub_sub_areas     "
REFERRALS_GEO_2_SQL+="                            ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id)             "
REFERRALS_GEO_2_SQL+="                            INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
REFERRALS_GEO_2_SQL+="                            WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND Left(core_item_id,2)='RM'            "
REFERRALS_GEO_2_SQL+="                            ) as qry_core_referrals_in_period_with_enc_geo_2            "
REFERRALS_GEO_2_SQL+="                    LEFT JOIN                     "
REFERRALS_GEO_2_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right            "
REFERRALS_GEO_2_SQL+="                            FROM tbl_core_services            "
REFERRALS_GEO_2_SQL+="                            WHERE tbl_core_services.encounter_date>=%s AND Left(core_item_id,2)='RC'            "
REFERRALS_GEO_2_SQL+="                            ) as qry_core_referrals_comp_in_period_s             "
REFERRALS_GEO_2_SQL+="                    ON (qry_core_referrals_in_period_with_enc_geo_2.beneficiary_person_id = qry_core_referrals_comp_in_period_s.beneficiary_person_id) AND (qry_core_referrals_in_period_with_enc_geo_2.core_item_id_right = qry_core_referrals_comp_in_period_s.core_item_id_right)                    "
REFERRALS_GEO_2_SQL+="                    WHERE qry_core_referrals_comp_in_period_s.encounter_date>qry_core_referrals_in_period_with_enc_geo_2.encounter_date Or qry_core_referrals_comp_in_period_s.encounter_date Is Null                    "
REFERRALS_GEO_2_SQL+="                    ) as qry_rep_referrals_all                    "
REFERRALS_GEO_2_SQL+="            ) as qry_rep_referrals_all_distinct                             "
REFERRALS_GEO_2_SQL+="    INNER JOIN tbl_list_general ON qry_rep_referrals_all_distinct.core_item_id = tbl_list_general.item_id                                    "
REFERRALS_GEO_2_SQL+="    GROUP BY qry_rep_referrals_all_distinct.core_item_id, item_description                                    "
#qry_heir_sub_sub_areas gets wards in the selected district
#qry_core_referrals_in_period_with_enc_geo_2 gets referrals made during period in the selected district
#qry_core_referrals_comp_in_period_s  gets referrals completed any time after start of period (anywhere)
#qry_rep_referrals_all gets referrals made, and if same beneficiary completed same referral after it was made, marks referral_completed with 1
#qry_rep_referrals_all_distinct ensures each beneficiary and referral type combination counted only once
#final query counts referrals made, and of those, how many completed

#  period, province specified
#expected parameters: param_area_id, 'param_from_date', 'param_to_date', 'param_from_date'

REFERRALS_GEO_3_SQL= "    SELECT qry_rep_referrals_all_distinct.core_item_id as \"Referral code\", item_description AS \"Referral type\", Count(*) AS \"Number of beneficiaries receiving referral during period\", Sum(qry_rep_referrals_all_distinct.referral_completed) AS \"Of whom number of beneficiaries completing referral\"                                    "
REFERRALS_GEO_3_SQL+="    FROM                                     "
REFERRALS_GEO_3_SQL+="            (SELECT DISTINCT qry_rep_referrals_all.beneficiary_person_id, qry_rep_referrals_all.core_item_id, qry_rep_referrals_all.referral_completed                            "
REFERRALS_GEO_3_SQL+="            FROM                             "
REFERRALS_GEO_3_SQL+="                    (SELECT qry_core_referrals_in_period_with_enc_geo_3.beneficiary_person_id, qry_core_referrals_in_period_with_enc_geo_3.core_item_id, qry_core_referrals_comp_in_period_s.encounter_date,                     "
REFERRALS_GEO_3_SQL+="                            CASE WHEN (qry_core_referrals_comp_in_period_s.encounter_date is not null) THEN 1 ELSE 0 END AS referral_completed            "
REFERRALS_GEO_3_SQL+="                    FROM                     "
REFERRALS_GEO_3_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right            "
REFERRALS_GEO_3_SQL+="                            FROM (tbl_core_encounters INNER JOIN             "
REFERRALS_GEO_3_SQL+="                                    (SELECT tbl_list_geo_2.area_id    "
REFERRALS_GEO_3_SQL+="                                    FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)     "
REFERRALS_GEO_3_SQL+="                                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id    "
REFERRALS_GEO_3_SQL+="                                    WHERE tbl_list_geo.parent_area_id=%s   "
REFERRALS_GEO_3_SQL+="                                    ) as qry_heir_sub_sub_sub_areas     "
REFERRALS_GEO_3_SQL+="                            ON tbl_core_encounters.area_id = qry_heir_sub_sub_sub_areas.area_id)             "
REFERRALS_GEO_3_SQL+="                            INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
REFERRALS_GEO_3_SQL+="                            WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND Left(core_item_id,2)='RM'            "
REFERRALS_GEO_3_SQL+="                            ) as qry_core_referrals_in_period_with_enc_geo_3            "
REFERRALS_GEO_3_SQL+="                    LEFT JOIN                     "
REFERRALS_GEO_3_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right            "
REFERRALS_GEO_3_SQL+="                            FROM tbl_core_services            "
REFERRALS_GEO_3_SQL+="                            WHERE tbl_core_services.encounter_date>=%s AND Left(core_item_id,2)='RC'            "
REFERRALS_GEO_3_SQL+="                            ) as qry_core_referrals_comp_in_period_s             "
REFERRALS_GEO_3_SQL+="                    ON (qry_core_referrals_in_period_with_enc_geo_3.beneficiary_person_id = qry_core_referrals_comp_in_period_s.beneficiary_person_id) AND (qry_core_referrals_in_period_with_enc_geo_3.core_item_id_right = qry_core_referrals_comp_in_period_s.core_item_id_right)                    "
REFERRALS_GEO_3_SQL+="                    WHERE qry_core_referrals_comp_in_period_s.encounter_date>qry_core_referrals_in_period_with_enc_geo_3.encounter_date Or qry_core_referrals_comp_in_period_s.encounter_date Is Null                    "
REFERRALS_GEO_3_SQL+="                    ) as qry_rep_referrals_all                    "
REFERRALS_GEO_3_SQL+="            ) as qry_rep_referrals_all_distinct                             "
REFERRALS_GEO_3_SQL+="    INNER JOIN tbl_list_general ON qry_rep_referrals_all_distinct.core_item_id = tbl_list_general.item_id                                    "
REFERRALS_GEO_3_SQL+="    GROUP BY qry_rep_referrals_all_distinct.core_item_id, item_description                                    "
#qry_heir_sub_sub_sub_areas gets wards in the selected province
#qry_core_referrals_in_period_with_enc_geo_3 gets referrals made during period in the selected province
#qry_core_referrals_comp_in_period_s  gets referrals completed any time after start of period (anywhere)
#qry_rep_referrals_all gets referrals made, and if same beneficiary completed same referral after it was made, marks referral_completed with 1
#qry_rep_referrals_all_distinct ensures each beneficiary and referral type combination counted only once
#final query counts referrals made, and of those, how many completed

#  period, org unit specified
#expected parameters: 'param_from_date', 'param_to_date', param_org_unit_id, 'param_from_date'

REFERRALS_ORG_SQL= "    SELECT qry_rep_referrals_all_distinct.core_item_id as \"Referral code\", item_description AS \"Referral type\", Count(*) AS \"Number of beneficiaries receiving referral during period\", Sum(qry_rep_referrals_all_distinct.referral_completed) AS \"Of whom number of beneficiaries completing referral\"                                    "
REFERRALS_ORG_SQL+="    FROM                                     "
REFERRALS_ORG_SQL+="            (SELECT DISTINCT qry_rep_referrals_all.beneficiary_person_id, qry_rep_referrals_all.core_item_id, qry_rep_referrals_all.referral_completed                            "
REFERRALS_ORG_SQL+="            FROM                             "
REFERRALS_ORG_SQL+="                    (SELECT qry_core_referrals_in_period_with_enc_org.beneficiary_person_id, qry_core_referrals_in_period_with_enc_org.core_item_id, qry_core_referrals_comp_in_period_s.encounter_date,                     "
REFERRALS_ORG_SQL+="                            CASE WHEN (qry_core_referrals_comp_in_period_s.encounter_date is not null) THEN 1 ELSE 0 END AS referral_completed            "
REFERRALS_ORG_SQL+="                    FROM                     "
REFERRALS_ORG_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right            "
REFERRALS_ORG_SQL+="                            FROM tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)            "
REFERRALS_ORG_SQL+="                            WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND Left(core_item_id,2)='RM' AND tbl_core_encounters.org_unit_id=%s            "
REFERRALS_ORG_SQL+="                            ) as qry_core_referrals_in_period_with_enc_org            "
REFERRALS_ORG_SQL+="                    LEFT JOIN                     "
REFERRALS_ORG_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right            "
REFERRALS_ORG_SQL+="                            FROM tbl_core_services            "
REFERRALS_ORG_SQL+="                            WHERE tbl_core_services.encounter_date>=%s AND Left(core_item_id,2)='RC'            "
REFERRALS_ORG_SQL+="                            ) as qry_core_referrals_comp_in_period_s             "
REFERRALS_ORG_SQL+="                    ON (qry_core_referrals_in_period_with_enc_org.beneficiary_person_id = qry_core_referrals_comp_in_period_s.beneficiary_person_id) AND (qry_core_referrals_in_period_with_enc_org.core_item_id_right = qry_core_referrals_comp_in_period_s.core_item_id_right)                    "
REFERRALS_ORG_SQL+="                    WHERE qry_core_referrals_comp_in_period_s.encounter_date>qry_core_referrals_in_period_with_enc_org.encounter_date Or qry_core_referrals_comp_in_period_s.encounter_date Is Null                    "
REFERRALS_ORG_SQL+="                    ) as qry_rep_referrals_all                    "
REFERRALS_ORG_SQL+="            ) as qry_rep_referrals_all_distinct                             "
REFERRALS_ORG_SQL+="    INNER JOIN tbl_list_general ON qry_rep_referrals_all_distinct.core_item_id = tbl_list_general.item_id                                    "
REFERRALS_ORG_SQL+="    GROUP BY qry_rep_referrals_all_distinct.core_item_id, item_description                                    "
#qry_core_referrals_in_period_with_enc_org gets referrals made during period by the selected org unit
#qry_core_referrals_comp_in_period_s  gets referrals completed any time after start of period (by anyone)
#qry_rep_referrals_all gets referrals made, and if same beneficiary completed same referral after it was made, marks referral_completed with 1
#qry_rep_referrals_all_distinct ensures each beneficiary and referral type combination counted only once
#final query counts referrals made, and of those, how many completed

#  period, org unit set specified
#expected parameters: 'param_from_date', 'param_to_date', param_set_id, 'param_from_date'

REFERRALS_SET_SQL= "    SELECT qry_rep_referrals_all_distinct.core_item_id as \"Referral code\", item_description AS \"Referral type\", Count(*) AS \"Number of beneficiaries receiving referral during period\", Sum(qry_rep_referrals_all_distinct.referral_completed) AS \"Of whom number of beneficiaries completing referral\"                                    "
REFERRALS_SET_SQL+="    FROM                                     "
REFERRALS_SET_SQL+="            (SELECT DISTINCT qry_rep_referrals_all.beneficiary_person_id, qry_rep_referrals_all.core_item_id, qry_rep_referrals_all.referral_completed                            "
REFERRALS_SET_SQL+="            FROM                             "
REFERRALS_SET_SQL+="                    (SELECT qry_core_referrals_in_period_with_enc_set.beneficiary_person_id, qry_core_referrals_in_period_with_enc_set.core_item_id, qry_core_referrals_comp_in_period_s.encounter_date,                     "
REFERRALS_SET_SQL+="                            CASE WHEN (qry_core_referrals_comp_in_period_s.encounter_date is not null) THEN 1 ELSE 0 END AS referral_completed            "
REFERRALS_SET_SQL+="                    FROM                     "
REFERRALS_SET_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right            "
REFERRALS_SET_SQL+="                            FROM (tbl_core_encounters INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)) INNER JOIN tbl_reports_sets_org_units ON tbl_core_encounters.org_unit_id = tbl_reports_sets_org_units.org_unit_id            "
REFERRALS_SET_SQL+="                            WHERE tbl_core_services.encounter_date>=%s And tbl_core_services.encounter_date<=%s AND Left(core_item_id,2)='RM' AND tbl_reports_sets_org_units.set_id=%s            "
REFERRALS_SET_SQL+="                            ) as qry_core_referrals_in_period_with_enc_set            "
REFERRALS_SET_SQL+="                    LEFT JOIN                     "
REFERRALS_SET_SQL+="                            (SELECT tbl_core_services.core_item_id, tbl_core_services.beneficiary_person_id, tbl_core_services.encounter_date, Right(core_item_id,2) AS core_item_id_right            "
REFERRALS_SET_SQL+="                            FROM tbl_core_services            "
REFERRALS_SET_SQL+="                            WHERE tbl_core_services.encounter_date>=%s AND Left(core_item_id,2)='RC'            "
REFERRALS_SET_SQL+="                            ) as qry_core_referrals_comp_in_period_s             "
REFERRALS_SET_SQL+="                    ON (qry_core_referrals_in_period_with_enc_set.beneficiary_person_id = qry_core_referrals_comp_in_period_s.beneficiary_person_id) AND (qry_core_referrals_in_period_with_enc_set.core_item_id_right = qry_core_referrals_comp_in_period_s.core_item_id_right)                    "
REFERRALS_SET_SQL+="                    WHERE qry_core_referrals_comp_in_period_s.encounter_date>qry_core_referrals_in_period_with_enc_set.encounter_date Or qry_core_referrals_comp_in_period_s.encounter_date Is Null                    "
REFERRALS_SET_SQL+="                    ) as qry_rep_referrals_all                    "
REFERRALS_SET_SQL+="            ) as qry_rep_referrals_all_distinct                             "
REFERRALS_SET_SQL+="    INNER JOIN tbl_list_general ON qry_rep_referrals_all_distinct.core_item_id = tbl_list_general.item_id                                    "
REFERRALS_SET_SQL+="    GROUP BY qry_rep_referrals_all_distinct.core_item_id, item_description                                    "
#qry_core_referrals_in_period_with_enc_set gets referrals made during period by the org units in the selected set
#qry_core_referrals_comp_in_period_s  gets referrals completed any time after start of period (by anyone)
#qry_rep_referrals_all gets referrals made, and if same beneficiary completed same referral after it was made, marks referral_completed with 1
#qry_rep_referrals_all_distinct ensures each beneficiary and referral type combination counted only once
#final query counts referrals made, and of those, how many completed


#*******OVC ASSESSMENT **********

# only period specified
#expected parameters: 'param_from_date', 'param_to_date',  param_question_id

OVC_ASSESSMENT_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_SQL+="    FROM                                     "
OVC_ASSESSMENT_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_SQL+="            FROM tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id                            "
OVC_ASSESSMENT_SQL+="            WHERE tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_forms.date_began>=%s AND tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s                            "
OVC_ASSESSMENT_SQL+="            ) as qry_rep_assessment_OVC_all                             "
OVC_ASSESSMENT_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_OVC_all.answer_id = tbl_list_answers.id                                    "
OVC_ASSESSMENT_SQL+="    GROUP BY tbl_list_answers.answer, qry_rep_assessment_OVC_all.latest_is_1, tbl_list_answers.the_order                                    "
OVC_ASSESSMENT_SQL+="    HAVING qry_rep_assessment_OVC_all.latest_is_1=1                                    "
OVC_ASSESSMENT_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_rep_assessment_OVC_all gets all OVC assessment answers from the selected question and the selected period 
#final query takes only the most recent answer for each beneficiary, and counts them by answer 

# period, ward specified
#expected parameters: param_area_id, 'param_to_date', 'param_to_date', 'param_from_date', 'param_to_date',  param_question_id

OVC_ASSESSMENT_GEO_0_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_GEO_0_SQL+="    FROM                                     "
OVC_ASSESSMENT_GEO_0_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_GEO_0_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_GEO_0_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_GEO_0_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_GEO_0_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_GEO_0_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_GEO_0_SQL+="                    WHERE tbl_reg_persons_geo.area_id=%s AND tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_GEO_0_SQL+="                    ) as qry_persons_geo_todate_sp                    "
OVC_ASSESSMENT_GEO_0_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_sp.person_id                            "
OVC_ASSESSMENT_GEO_0_SQL+="            WHERE tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_forms.date_began>=%s AND tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s                            "
OVC_ASSESSMENT_GEO_0_SQL+="            ) as qry_rep_assessment_OVC_geo_0                             "
OVC_ASSESSMENT_GEO_0_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_OVC_geo_0.answer_id = tbl_list_answers.id                                    "
OVC_ASSESSMENT_GEO_0_SQL+="    GROUP BY tbl_list_answers.answer, qry_rep_assessment_OVC_geo_0.latest_is_1, tbl_list_answers.the_order                                    "
OVC_ASSESSMENT_GEO_0_SQL+="    HAVING qry_rep_assessment_OVC_geo_0.latest_is_1=1                                    "
OVC_ASSESSMENT_GEO_0_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_persons_geo_todate_sp gets people resident in the selected ward
#qry_rep_assessment_OVC_geo_0 gets all OVC assessment answers from the selected question and the selected period 
#final query takes only the most recent answer for each beneficiary, and counts them by answer 

# period, constituency specified
#expected parameters: 'param_to_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date',  param_question_id

OVC_ASSESSMENT_GEO_1_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_GEO_1_SQL+="    FROM                                     "
OVC_ASSESSMENT_GEO_1_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_GEO_1_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_GEO_1_SQL+="            FROM ((tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_GEO_1_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_GEO_1_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_GEO_1_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_GEO_1_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_GEO_1_SQL+="                    ) as qry_persons_geo_todate_s                    "
OVC_ASSESSMENT_GEO_1_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id)                             "
OVC_ASSESSMENT_GEO_1_SQL+="            INNER JOIN tbl_list_geo ON qry_persons_geo_todate_s.area_id = tbl_list_geo.area_id                            "
OVC_ASSESSMENT_GEO_1_SQL+="            WHERE tbl_list_geo.parent_area_id=%s and tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_forms.date_began>=%s AND tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s                            "
OVC_ASSESSMENT_GEO_1_SQL+="            ) as qry_rep_assessment_OVC_geo_1                             "
OVC_ASSESSMENT_GEO_1_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_OVC_geo_1.answer_id = tbl_list_answers.id                                    "
OVC_ASSESSMENT_GEO_1_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_OVC_geo_1.latest_is_1                                    "
OVC_ASSESSMENT_GEO_1_SQL+="    HAVING qry_rep_assessment_OVC_geo_1.latest_is_1=1                                    "
OVC_ASSESSMENT_GEO_1_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_persons_geo_todate_s gets which ward people are resident in
#qry_rep_assessment_OVC_geo_1 gets all OVC assessment answers from the selected constituency, selected form, the selected answer and the selected period 
#final query takes only the most recent answer for each beneficiary, and counts them by answer 

# period, district specified
#expected parameters: 'param_to_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date',  param_question_id

OVC_ASSESSMENT_GEO_2_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_GEO_2_SQL+="    FROM                                     "
OVC_ASSESSMENT_GEO_2_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_GEO_2_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_GEO_2_SQL+="            FROM ((tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_GEO_2_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_GEO_2_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_GEO_2_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_GEO_2_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_GEO_2_SQL+="                    ) as qry_persons_geo_todate_s                    "
OVC_ASSESSMENT_GEO_2_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id)                             "
OVC_ASSESSMENT_GEO_2_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_GEO_2_SQL+="                    (SELECT tbl_list_geo_1.area_id                    "
OVC_ASSESSMENT_GEO_2_SQL+="                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id                    "
OVC_ASSESSMENT_GEO_2_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s                    "
OVC_ASSESSMENT_GEO_2_SQL+="                    ) as qry_heir_sub_sub_areas                     "
OVC_ASSESSMENT_GEO_2_SQL+="            ON qry_persons_geo_todate_s.area_id = qry_heir_sub_sub_areas.area_id                            "
OVC_ASSESSMENT_GEO_2_SQL+="            WHERE  tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_forms.date_began>=%s AND tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s                            "
OVC_ASSESSMENT_GEO_2_SQL+="            ) as qry_rep_assessment_OVC_geo_2                             "
OVC_ASSESSMENT_GEO_2_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_OVC_geo_2.answer_id = tbl_list_answers.id                                    "
OVC_ASSESSMENT_GEO_2_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_OVC_geo_2.latest_is_1                                    "
OVC_ASSESSMENT_GEO_2_SQL+="    HAVING qry_rep_assessment_OVC_geo_2.latest_is_1=1                                    "
OVC_ASSESSMENT_GEO_2_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_persons_geo_todate_s gets which ward people are resident in
#qry_heir_sub_sub_areas gets wards in the selected district
#qry_rep_assessment_OVC_geo_2 gets all OVC assessment answers from the selected question and the selected period 
#final query takes only the most recent answer for each beneficiary, and counts them by answer 

# period, province specified
#expected parameters: 'param_to_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date',  param_question_id

OVC_ASSESSMENT_GEO_3_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_GEO_3_SQL+="    FROM                                     "
OVC_ASSESSMENT_GEO_3_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_GEO_3_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_GEO_3_SQL+="            FROM ((tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_GEO_3_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_GEO_3_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_GEO_3_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_GEO_3_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_GEO_3_SQL+="                    ) as qry_persons_geo_todate_s                    "
OVC_ASSESSMENT_GEO_3_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id)                             "
OVC_ASSESSMENT_GEO_3_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_GEO_3_SQL+="                    (SELECT tbl_list_geo_2.area_id                    "
OVC_ASSESSMENT_GEO_3_SQL+="                    FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)                     "
OVC_ASSESSMENT_GEO_3_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id                    "
OVC_ASSESSMENT_GEO_3_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s                    "
OVC_ASSESSMENT_GEO_3_SQL+="                    ) as qry_heir_sub_sub_sub_areas                     "
OVC_ASSESSMENT_GEO_3_SQL+="            ON qry_persons_geo_todate_s.area_id = qry_heir_sub_sub_sub_areas.area_id                            "
OVC_ASSESSMENT_GEO_3_SQL+="            WHERE  tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_forms.date_began>=%s AND tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s                            "
OVC_ASSESSMENT_GEO_3_SQL+="            ) as qry_rep_assessment_OVC_geo_3                             "
OVC_ASSESSMENT_GEO_3_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_OVC_geo_3.answer_id = tbl_list_answers.id                                    "
OVC_ASSESSMENT_GEO_3_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_OVC_geo_3.latest_is_1                                    "
OVC_ASSESSMENT_GEO_3_SQL+="    HAVING qry_rep_assessment_OVC_geo_3.latest_is_1=1                                    "
OVC_ASSESSMENT_GEO_3_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_persons_geo_todate_s gets which ward people are resident in
#qry_heir_sub_sub_sub_areas gets wards in the selected province
#qry_rep_assessment_OVC_geo_3 gets all OVC assessment answers from the selected question and the selected period 
#final query takes only the most recent answer for each beneficiary, and counts them by answer 

# period, org unit specified
#expected parameters: param_org_unit_id, 'param_from_date', 'param_to_date',  param_question_id

OVC_ASSESSMENT_ORG_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_ORG_SQL+="    FROM                                     "
OVC_ASSESSMENT_ORG_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_ORG_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_ORG_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_ORG_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_ORG_SQL+="                    (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id                    "
OVC_ASSESSMENT_ORG_SQL+="                    FROM tbl_core_encounters                    "
OVC_ASSESSMENT_ORG_SQL+="                    WHERE tbl_core_encounters.org_unit_id=%s                    "
OVC_ASSESSMENT_ORG_SQL+="                    ) as qry_core_orgs_beneficiaries_sp                     "
OVC_ASSESSMENT_ORG_SQL+="            ON tbl_forms.form_subject_id = qry_core_orgs_beneficiaries_sp.beneficiary_person_id                            "
OVC_ASSESSMENT_ORG_SQL+="            WHERE  tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_forms.date_began>=%s AND tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s                            "
OVC_ASSESSMENT_ORG_SQL+="            ) as qry_rep_assessment_OVC_org                             "
OVC_ASSESSMENT_ORG_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_OVC_org.answer_id = tbl_list_answers.id                                    "
OVC_ASSESSMENT_ORG_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_OVC_org.latest_is_1                                    "
OVC_ASSESSMENT_ORG_SQL+="    HAVING qry_rep_assessment_OVC_org.latest_is_1=1                                    "
OVC_ASSESSMENT_ORG_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_core_orgs_beneficiaries_sp gets people who have ever received services from the selected organisational unit
#qry_rep_assessment_OVC_org gets all OVC assessment answers from the selected question and the selected period 
#final query takes only the most recent answer for each beneficiary, and counts them by answer 

# period, org unit set specified
#expected parameters: param_set_id, 'param_from_date', 'param_to_date',  param_question_id

OVC_ASSESSMENT_SET_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_SET_SQL+="    FROM                                     "
OVC_ASSESSMENT_SET_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_SET_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_SET_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_SET_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_SET_SQL+="                    (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id                    "
OVC_ASSESSMENT_SET_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_reports_sets_org_units ON tbl_core_encounters.org_unit_id = tbl_reports_sets_org_units.org_unit_id                    "
OVC_ASSESSMENT_SET_SQL+="                    WHERE tbl_reports_sets_org_units.set_id=%s                   "
OVC_ASSESSMENT_SET_SQL+="                    ) as qry_core_orgs_beneficiaries_set                     "
OVC_ASSESSMENT_SET_SQL+="            ON tbl_forms.form_subject_id = qry_core_orgs_beneficiaries_set.beneficiary_person_id                            "
OVC_ASSESSMENT_SET_SQL+="            WHERE  tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_forms.date_began>=%s AND tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s                            "
OVC_ASSESSMENT_SET_SQL+="            ) as qry_rep_assessment_OVC_set                             "
OVC_ASSESSMENT_SET_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_OVC_set.answer_id = tbl_list_answers.id                                    "
OVC_ASSESSMENT_SET_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_OVC_set.latest_is_1                                    "
OVC_ASSESSMENT_SET_SQL+="    HAVING qry_rep_assessment_OVC_set.latest_is_1=1                                    "
OVC_ASSESSMENT_SET_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_core_orgs_beneficiaries_set gets people who have ever received services from any org unit in the selected set of 
#qry_rep_assessment_OVC_set gets all OVC assessment answers from the selected question and the selected period 
#final query takes only the most recent answer for each beneficiary, and counts them by answer 


#*******OVC ASSESSMENT CSI **********

# only period specified
#expected parameters: 'param_from_date', 'param_to_date'

OVC_ASSESSMENT_CSI_SQL= "    SELECT tbl_list_general.item_description AS \"CSI domain\", sum(score_1) AS \"Number of OVC scoring 1\", sum(score_2) AS \"Number of OVC scoring 2\", sum(score_3) AS \"Number of OVC scoring 3\", sum(score_4) AS \"Number of OVC scoring 4\"                                    "
OVC_ASSESSMENT_CSI_SQL+="    FROM                                     "
OVC_ASSESSMENT_CSI_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.domain_id,                             "
OVC_ASSESSMENT_CSI_SQL+="                    rank() OVER (PARTITION BY form_subject_id, domain_id ORDER BY date_began DESC) AS latest_is_1,                    "
OVC_ASSESSMENT_CSI_SQL+="                    CASE WHEN score=1 THEN 1 ELSE 0 END As score_1,                    "
OVC_ASSESSMENT_CSI_SQL+="                    CASE WHEN score=2 THEN 1 ELSE 0 END As score_2,                    "
OVC_ASSESSMENT_CSI_SQL+="                    CASE WHEN score=3 THEN 1 ELSE 0 END As score_3,                    "
OVC_ASSESSMENT_CSI_SQL+="                    CASE WHEN score=4 THEN 1 ELSE 0 END As score_4                     "
OVC_ASSESSMENT_CSI_SQL+="            FROM tbl_forms INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id                            "
OVC_ASSESSMENT_CSI_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_CSI_SQL+="            ) as qry_rep_assessment_CSI_all                             "
OVC_ASSESSMENT_CSI_SQL+="    INNER JOIN tbl_list_general ON qry_rep_assessment_CSI_all.domain_id = tbl_list_general.item_id                                    "
OVC_ASSESSMENT_CSI_SQL+="    GROUP BY tbl_list_general.item_description, qry_rep_assessment_CSI_all.latest_is_1, tbl_list_general.the_order                                    "
OVC_ASSESSMENT_CSI_SQL+="    HAVING qry_rep_assessment_CSI_all.latest_is_1=1                                    "
OVC_ASSESSMENT_CSI_SQL+="    ORDER BY tbl_list_general.the_order                                    "
# qry_rep_assessment_CSI_all gets csi scores from the selected period
# final query takes only the latest score from each OVC-domain combination, and counts how many OVC received each score

# period, ward specified
#expected parameters: param_area_id, 'param_to_date', 'param_to_date', 'param_from_date', 'param_to_date'

OVC_ASSESSMENT_CSI_GEO_0_SQL= "    SELECT tbl_list_general.item_description AS \"CSI domain\", Sum(qry_rep_assessment_CSI_geo_0.score_1) AS \"Number of OVC scoring 1\", Sum(qry_rep_assessment_CSI_geo_0.score_2) AS \"Number of OVC scoring 2\", Sum(qry_rep_assessment_CSI_geo_0.score_3) AS \"Number of OVC scoring 3\", Sum(qry_rep_assessment_CSI_geo_0.score_4) AS \"Number of OVC scoring 4\"                                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="    FROM                                     "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.domain_id,                             "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    rank() OVER (PARTITION BY form_subject_id, domain_id ORDER BY date_began DESC) AS latest_is_1,                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    CASE WHEN score=1 THEN 1 ELSE 0 END As score_1,                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    CASE WHEN score=2 THEN 1 ELSE 0 END As score_2,                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    CASE WHEN score=3 THEN 1 ELSE 0 END As score_3,                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    CASE WHEN score=4 THEN 1 ELSE 0 END As score_4                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id)                             "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    WHERE tbl_reg_persons_geo.area_id=%s AND tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="                    ) as qry_persons_geo_todate_sp                     "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_sp.person_id                            "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="            ) as qry_rep_assessment_CSI_geo_0                             "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="    INNER JOIN tbl_list_general ON qry_rep_assessment_CSI_geo_0.domain_id = tbl_list_general.item_id                                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="    GROUP BY tbl_list_general.item_description, qry_rep_assessment_CSI_geo_0.latest_is_1, tbl_list_general.the_order                                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="    HAVING qry_rep_assessment_CSI_geo_0.latest_is_1=1                                    "
OVC_ASSESSMENT_CSI_GEO_0_SQL+="    ORDER BY tbl_list_general.the_order                                    "
# qry_persons_geo_todate_sp gets people resident in the selected ward
# qry_rep_assessment_CSI_geo_0 gets csi scores from the selected period
# final query takes only the latest score from each OVC-domain combination, and counts how many OVC received each score

# period, constituency specified
#expected parameters: 'param_to_date', 'param_to_date', 'param_from_date', 'param_to_date', param_area_id

OVC_ASSESSMENT_CSI_GEO_1_SQL= "    SELECT tbl_list_general.item_description AS \"CSI domain\", Sum(qry_rep_assessment_CSI_geo_1.score_1) AS \"Number of OVC scoring 1\", Sum(qry_rep_assessment_CSI_geo_1.score_2) AS \"Number of OVC scoring 2\", Sum(qry_rep_assessment_CSI_geo_1.score_3) AS \"Number of OVC scoring 3\", Sum(qry_rep_assessment_CSI_geo_1.score_4) AS \"Number of OVC scoring 4\"                                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="    FROM                                     "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.domain_id,                             "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    rank() OVER (PARTITION BY form_subject_id, domain_id ORDER BY date_began DESC) AS latest_is_1,                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    CASE WHEN score=1 THEN 1 ELSE 0 END As score_1,                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    CASE WHEN score=2 THEN 1 ELSE 0 END As score_2,                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    CASE WHEN score=3 THEN 1 ELSE 0 END As score_3,                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    CASE WHEN score=4 THEN 1 ELSE 0 END As score_4                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="            FROM ((tbl_forms INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id)                             "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="                    ) as qry_persons_geo_todate_s                     "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id)                             "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="            INNER JOIN tbl_list_geo ON qry_persons_geo_todate_s.area_id = tbl_list_geo.area_id                            "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_list_geo.parent_area_id=%s                            "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="            ) as qry_rep_assessment_CSI_geo_1                             "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="    INNER JOIN tbl_list_general ON qry_rep_assessment_CSI_geo_1.domain_id = tbl_list_general.item_id                                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="    GROUP BY tbl_list_general.item_description, qry_rep_assessment_CSI_geo_1.latest_is_1, tbl_list_general.the_order                                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="    HAVING qry_rep_assessment_CSI_geo_1.latest_is_1=1                                    "
OVC_ASSESSMENT_CSI_GEO_1_SQL+="    ORDER BY tbl_list_general.the_order                                    "
# qry_persons_geo_todate_s gets the wards where people live
# qry_rep_assessment_CSI_geo_1 gets csi scores from the selected period and selected constituency
# final query takes only the latest score from each OVC-domain combination, and counts how many OVC received each score

# period, district specified
#expected parameters: 'param_to_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date' 

OVC_ASSESSMENT_CSI_GEO_2_SQL= "    SELECT tbl_list_general.item_description AS \"CSI domain\", Sum(qry_rep_assessment_CSI_geo_2.score_1) AS \"Number of OVC scoring 1\", Sum(qry_rep_assessment_CSI_geo_2.score_2) AS \"Number of OVC scoring 2\", Sum(qry_rep_assessment_CSI_geo_2.score_3) AS \"Number of OVC scoring 3\", Sum(qry_rep_assessment_CSI_geo_2.score_4) AS \"Number of OVC scoring 4\"                                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="    FROM                                     "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.domain_id,                             "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    rank() OVER (PARTITION BY form_subject_id, domain_id ORDER BY date_began DESC) AS latest_is_1,                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    CASE WHEN score=1 THEN 1 ELSE 0 END As score_1,                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    CASE WHEN score=2 THEN 1 ELSE 0 END As score_2,                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    CASE WHEN score=3 THEN 1 ELSE 0 END As score_3,                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    CASE WHEN score=4 THEN 1 ELSE 0 END As score_4                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="            FROM ((tbl_forms INNER JOIN                             "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    ) as qry_persons_geo_todate_s                     "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id)                             "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="            INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id)                             "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    (SELECT tbl_list_geo_1.area_id                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s                   "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="                    ) as qry_heir_sub_sub_areas                     "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="            ON qry_persons_geo_todate_s.area_id = qry_heir_sub_sub_areas.area_id                            "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="            ) as qry_rep_assessment_CSI_geo_2                             "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="    INNER JOIN tbl_list_general ON qry_rep_assessment_CSI_geo_2.domain_id = tbl_list_general.item_id                                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="    GROUP BY tbl_list_general.item_description, qry_rep_assessment_CSI_geo_2.latest_is_1, tbl_list_general.the_order                                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="    HAVING qry_rep_assessment_CSI_geo_2.latest_is_1=1                                    "
OVC_ASSESSMENT_CSI_GEO_2_SQL+="    ORDER BY tbl_list_general.the_order                                    "
# qry_persons_geo_todate_s gets the wards where people live
# qry_heir_sub_sub_areas gets wards in the selected district
# qry_rep_assessment_CSI_geo_2 gets csi scores from the selected period and selected district
# final query takes only the latest score from each OVC-domain combination, and counts how many OVC received each score

# period, province specified
#expected parameters: 'param_to_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date' 
OVC_ASSESSMENT_CSI_GEO_3_SQL= "    SELECT tbl_list_general.item_description AS \"CSI domain\", Sum(qry_rep_assessment_CSI_geo_3.score_1) AS \"Number of OVC scoring 1\", Sum(qry_rep_assessment_CSI_geo_3.score_2) AS \"Number of OVC scoring 2\", Sum(qry_rep_assessment_CSI_geo_3.score_3) AS \"Number of OVC scoring 3\", Sum(qry_rep_assessment_CSI_geo_3.score_4) AS \"Number of OVC scoring 4\"                                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="    FROM                                     "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.domain_id,                             "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    rank() OVER (PARTITION BY form_subject_id, domain_id ORDER BY date_began DESC) AS latest_is_1,                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    CASE WHEN score=1 THEN 1 ELSE 0 END As score_1,                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    CASE WHEN score=2 THEN 1 ELSE 0 END As score_2,                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    CASE WHEN score=3 THEN 1 ELSE 0 END As score_3,                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    CASE WHEN score=4 THEN 1 ELSE 0 END As score_4                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="            FROM ((tbl_forms INNER JOIN                             "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    ) as qry_persons_geo_todate_s                     "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id)                             "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="            INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id)                             "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    (SELECT tbl_list_geo_2.area_id                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)                     "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="                    ) as qry_heir_sub_sub_sub_areas                     "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="            ON qry_persons_geo_todate_s.area_id = qry_heir_sub_sub_sub_areas.area_id                            "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="            ) as qry_rep_assessment_CSI_geo_3                             "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="    INNER JOIN tbl_list_general ON qry_rep_assessment_CSI_geo_3.domain_id = tbl_list_general.item_id                                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="    GROUP BY tbl_list_general.item_description, qry_rep_assessment_CSI_geo_3.latest_is_1, tbl_list_general.the_order                                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="    HAVING qry_rep_assessment_CSI_geo_3.latest_is_1=1                                    "
OVC_ASSESSMENT_CSI_GEO_3_SQL+="    ORDER BY tbl_list_general.the_order                                    "
# qry_persons_geo_todate_s gets the wards where people live
# qry_heir_sub_sub_sub_areas gets wards in the selected province
# qry_rep_assessment_CSI_geo_3 gets csi scores from the selected period and selected province
# final query takes only the latest score from each OVC-domain combination, and counts how many OVC received each score

# period, org unit specified
#expected parameters: param_org_unit_id, 'param_from_date', 'param_to_date' 
OVC_ASSESSMENT_CSI_ORG_SQL= "    SELECT tbl_list_general.item_description AS \"CSI domain\", Sum(qry_rep_assessment_CSI_org.score_1) AS \"Number of OVC scoring 1\", Sum(qry_rep_assessment_CSI_org.score_2) AS \"Number of OVC scoring 2\", Sum(qry_rep_assessment_CSI_org.score_3) AS \"Number of OVC scoring 3\", Sum(qry_rep_assessment_CSI_org.score_4) AS \"Number of OVC scoring 4\"                                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="    FROM                                     "
OVC_ASSESSMENT_CSI_ORG_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.domain_id,                             "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    rank() OVER (PARTITION BY form_subject_id, domain_id ORDER BY date_began DESC) AS latest_is_1,                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    CASE WHEN score=1 THEN 1 ELSE 0 END As score_1,                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    CASE WHEN score=2 THEN 1 ELSE 0 END As score_2,                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    CASE WHEN score=3 THEN 1 ELSE 0 END As score_3,                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    CASE WHEN score=4 THEN 1 ELSE 0 END As score_4                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id)                             "
OVC_ASSESSMENT_CSI_ORG_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    FROM tbl_core_encounters                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    WHERE tbl_core_encounters.org_unit_id=%s                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="                    ) as qry_core_orgs_beneficiaries_sp                     "
OVC_ASSESSMENT_CSI_ORG_SQL+="            ON tbl_forms.form_subject_id = qry_core_orgs_beneficiaries_sp.beneficiary_person_id                            "
OVC_ASSESSMENT_CSI_ORG_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_CSI_ORG_SQL+="            ) as qry_rep_assessment_CSI_org                             "
OVC_ASSESSMENT_CSI_ORG_SQL+="    INNER JOIN tbl_list_general ON qry_rep_assessment_CSI_org.domain_id = tbl_list_general.item_id                                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="    GROUP BY tbl_list_general.item_description, qry_rep_assessment_CSI_org.latest_is_1, tbl_list_general.the_order                                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="    HAVING qry_rep_assessment_CSI_org.latest_is_1=1                                    "
OVC_ASSESSMENT_CSI_ORG_SQL+="    ORDER BY tbl_list_general.the_order                                    "
# qry_core_orgs_beneficiaries_sp gets beneficiaries ever served by selected org unit
# qry_rep_assessment_CSI_org gets csi scores from the selected period 
# final query takes only the latest score from each OVC-domain combination, and counts how many OVC received each score

# period, org unit set specified
#expected parameters: set_id, 'param_from_date', 'param_to_date' 
OVC_ASSESSMENT_CSI_SET_SQL= "    SELECT tbl_list_general.item_description AS \"CSI domain\", Sum(qry_rep_assessment_CSI_set.score_1) AS \"Number of OVC scoring 1\", Sum(qry_rep_assessment_CSI_set.score_2) AS \"Number of OVC scoring 2\", Sum(qry_rep_assessment_CSI_set.score_3) AS \"Number of OVC scoring 3\", Sum(qry_rep_assessment_CSI_set.score_4) AS \"Number of OVC scoring 4\"                                    "
OVC_ASSESSMENT_CSI_SET_SQL+="    FROM                                     "
OVC_ASSESSMENT_CSI_SET_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.domain_id,                             "
OVC_ASSESSMENT_CSI_SET_SQL+="                    rank() OVER (PARTITION BY form_subject_id, domain_id ORDER BY date_began DESC) AS latest_is_1,                    "
OVC_ASSESSMENT_CSI_SET_SQL+="                    CASE WHEN score=1 THEN 1 ELSE 0 END As score_1,                    "
OVC_ASSESSMENT_CSI_SET_SQL+="                    CASE WHEN score=2 THEN 1 ELSE 0 END As score_2,                    "
OVC_ASSESSMENT_CSI_SET_SQL+="                    CASE WHEN score=3 THEN 1 ELSE 0 END As score_3,                    "
OVC_ASSESSMENT_CSI_SET_SQL+="                    CASE WHEN score=4 THEN 1 ELSE 0 END As score_4                    "
OVC_ASSESSMENT_CSI_SET_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id)                             "
OVC_ASSESSMENT_CSI_SET_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_CSI_SET_SQL+="                    (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id                    "
OVC_ASSESSMENT_CSI_SET_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_reports_sets_org_units ON tbl_core_encounters.org_unit_id = tbl_reports_sets_org_units.org_unit_id                    "
OVC_ASSESSMENT_CSI_SET_SQL+="                    WHERE tbl_reports_sets_org_units.set_id=%s                    "
OVC_ASSESSMENT_CSI_SET_SQL+="                    ) as qry_core_orgs_beneficiaries_set                     "
OVC_ASSESSMENT_CSI_SET_SQL+="            ON tbl_forms.form_subject_id = qry_core_orgs_beneficiaries_set.beneficiary_person_id                            "
OVC_ASSESSMENT_CSI_SET_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_CSI_SET_SQL+="            ) as qry_rep_assessment_CSI_set                             "
OVC_ASSESSMENT_CSI_SET_SQL+="    INNER JOIN tbl_list_general ON qry_rep_assessment_CSI_set.domain_id = tbl_list_general.item_id                                    "
OVC_ASSESSMENT_CSI_SET_SQL+="    GROUP BY tbl_list_general.item_description, qry_rep_assessment_CSI_set.latest_is_1, tbl_list_general.the_order                                    "
OVC_ASSESSMENT_CSI_SET_SQL+="    HAVING qry_rep_assessment_CSI_set.latest_is_1=1                                    "
OVC_ASSESSMENT_CSI_SET_SQL+="    ORDER BY tbl_list_general.the_order                                    "
# qry_core_orgs_beneficiaries_set gets beneficiaries ever served by any org unit in the selected set
# qry_rep_assessment_CSI_org gets csi scores from the selected period 
# final query takes only the latest score from each OVC-domain combination, and counts how many OVC received each score

#*******OVC ASSESSMENT SPECIFIC ANSWER **********
#expected parameters: 'param_from_date', 'param_to_date', param_quesiton_id, param_answer_id
OVC_ASSESSMENT_ANSWER_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(date_began-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_ANSWER_SQL+="    FROM (                                    "
OVC_ASSESSMENT_ANSWER_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_forms.date_began,                            "
OVC_ASSESSMENT_ANSWER_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_ANSWER_SQL+="            FROM tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id                            "
OVC_ASSESSMENT_ANSWER_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_form_gen_answers.question_id=%s AND tbl_form_gen_answers.answer_id=%s                           "
OVC_ASSESSMENT_ANSWER_SQL+="            ) as qry_rep_assessment_OVC_answer_all                             "
OVC_ASSESSMENT_ANSWER_SQL+="            INNER JOIN tbl_reg_persons ON qry_rep_assessment_OVC_answer_all.form_subject_id = tbl_reg_persons.id)                             "
OVC_ASSESSMENT_ANSWER_SQL+="            INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id                            "
OVC_ASSESSMENT_ANSWER_SQL+="    GROUP BY sex, age, qry_rep_assessment_OVC_answer_all.latest_is_1                                    "
OVC_ASSESSMENT_ANSWER_SQL+="    HAVING qry_rep_assessment_OVC_answer_all.latest_is_1=1                                    "
OVC_ASSESSMENT_ANSWER_SQL+="    ORDER BY age, sex                                    "
#qry_rep_assessment_OVC_answer_all gets all forms with the specified answer during the period
#final query ensures each OVC only counted once, and counts and groups by age and sex

# ward specified
#expected parameters: param_area_id, 'param_to_date', 'param_to_date', 'param_from_date', 'param_to_date', param_quesiton_id, param_answer_id
OVC_ASSESSMENT_ANSWER_GEO_0_SQL= "    SELECT tbl_list_general.item_description AS sex, div(date_began-date_of_birth, 365.25) AS age, Count(*) AS \"Number of OVC\"                                   "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="    FROM                                     "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="                    WHERE tbl_reg_persons_geo.area_id=%s AND tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="                    ) as qry_persons_geo_todate_sp                     "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_sp.person_id                            "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_form_gen_answers.question_id=%s AND tbl_form_gen_answers.answer_id=%s                           "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="            ) as qry_rep_assessment_OVC_answer_geo_0                             "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="    INNER JOIN (tbl_reg_persons INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id) ON qry_rep_assessment_OVC_answer_geo_0.form_subject_id = tbl_reg_persons.id                                    "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="    GROUP BY sex, age, qry_rep_assessment_OVC_answer_geo_0.latest_is_1                                    "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="    HAVING qry_rep_assessment_OVC_answer_geo_0.latest_is_1=1                                    "
OVC_ASSESSMENT_ANSWER_GEO_0_SQL+="    ORDER BY age, sex                                    "
#qry_persons_geo_todate_sp gets OVC who live in the specified ward
#qry_rep_assessment_OVC_answer_geo_0 gets forms for those OVCs which are filled in the period and contain the specified answer
#final query ensures each OVC only counted once, and counts and groups by age and sex

# consituency specified
#expected parameters: 'param_to_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date', param_quesiton_id, param_answer_id
OVC_ASSESSMENT_ANSWER_GEO_1_SQL= "    SELECT tbl_list_general.item_description AS sex, div(date_began-date_of_birth, 365.25) AS age, Count(*) AS \"Number of OVC\"                                   "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="    FROM (tbl_reg_persons INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id)                                     "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="    INNER JOIN                                     "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="            INNER JOIN (                            "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="                    ) as qry_persons_geo_todate_s                     "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="            INNER JOIN tbl_list_geo ON qry_persons_geo_todate_s.area_id = tbl_list_geo.area_id)                             "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id                            "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="            WHERE tbl_list_geo.parent_area_id=%s AND tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_form_gen_answers.question_id=%s AND tbl_form_gen_answers.answer_id=%s                          "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="            ) as qry_rep_assessment_OVC_answer_geo_1                             "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="    ON tbl_reg_persons.id = qry_rep_assessment_OVC_answer_geo_1.form_subject_id                                    "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="    GROUP BY sex, age, qry_rep_assessment_OVC_answer_geo_1.latest_is_1                                    "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="    HAVING qry_rep_assessment_OVC_answer_geo_1.latest_is_1=1                                    "
OVC_ASSESSMENT_ANSWER_GEO_1_SQL+="    ORDER BY age, sex                                    "
#qry_persons_geo_todate_s gets the ward that OVC live in
#qry_rep_assessment_OVC_answer_geo_1 gets forms for those OVCs which are filled in the period and contain the specified answer, where the ward is within the specified constituency
#final query ensures each OVC only counted once, and counts and groups by age and sex
 
# district specified
#expected parameters: 'param_to_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date', param_quesiton_id, param_answer_id
OVC_ASSESSMENT_ANSWER_GEO_2_SQL= "    SELECT tbl_list_general.item_description AS sex, div(date_began-date_of_birth, 365.25) AS age, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="    FROM                                     "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="            FROM ((tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    ) as qry_persons_geo_todate_s                     "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id)                             "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    (SELECT tbl_list_geo_1.area_id                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="                    ) as qry_heir_sub_sub_areas                     "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="            ON qry_persons_geo_todate_s.area_id = qry_heir_sub_sub_areas.area_id                            "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_form_gen_answers.question_id=%s AND tbl_form_gen_answers.answer_id=%s                            "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="            ) as qry_rep_assessment_OVC_answer_geo_2                             "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="    INNER JOIN (tbl_reg_persons INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id) ON qry_rep_assessment_OVC_answer_geo_2.form_subject_id = tbl_reg_persons.id                                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="    GROUP BY sex, age, qry_rep_assessment_OVC_answer_geo_2.latest_is_1                                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="    HAVING qry_rep_assessment_OVC_answer_geo_2.latest_is_1=1                                    "
OVC_ASSESSMENT_ANSWER_GEO_2_SQL+="    ORDER BY age, sex                                    "
#qry_persons_geo_todate_s gets the ward that OVC live in
#qry_heir_sub_sub_areas  gets wards within the selected district
#qry_rep_assessment_OVC_answer_geo_2 gets forms for those OVCs which are filled in the period and contain the specified answer
#final query ensures each OVC only counted once, and counts and groups by age and sex

# province specified
#expected parameters: 'param_to_date', 'param_to_date', param_area_id, 'param_from_date', 'param_to_date', param_quesiton_id, param_answer_id
OVC_ASSESSMENT_ANSWER_GEO_3_SQL= "    SELECT tbl_list_general.item_description AS sex, div(date_began-date_of_birth, 365.25) AS age, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="    FROM                                     "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="            FROM ((tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    (SELECT tbl_reg_persons_geo.id AS person_id, tbl_reg_persons_geo.area_id                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    FROM tbl_reg_persons_geo                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    WHERE tbl_reg_persons_geo.date_linked<=%s AND (tbl_reg_persons_geo.date_delinked Is Null Or tbl_reg_persons_geo.date_delinked>%s)                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    ) as qry_persons_geo_todate_s                     "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="            ON tbl_forms.form_subject_id = qry_persons_geo_todate_s.person_id)                             "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    (SELECT tbl_list_geo_2.area_id                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)                     "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s                   "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="                    ) as qry_heir_sub_sub_sub_areas                     "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="            ON qry_persons_geo_todate_s.area_id = qry_heir_sub_sub_sub_areas.area_id                            "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_form_gen_answers.question_id=%s AND tbl_form_gen_answers.answer_id=%s                            "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="            ) as qry_rep_assessment_OVC_answer_geo_3                             "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="    INNER JOIN (tbl_reg_persons INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id) ON qry_rep_assessment_OVC_answer_geo_3.form_subject_id = tbl_reg_persons.id                                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="    GROUP BY sex, age, qry_rep_assessment_OVC_answer_geo_3.latest_is_1                                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="    HAVING qry_rep_assessment_OVC_answer_geo_3.latest_is_1=1                                    "
OVC_ASSESSMENT_ANSWER_GEO_3_SQL+="    ORDER BY age, sex                                    "
#qry_persons_geo_todate_s gets the ward that OVC live in
#qry_heir_sub_sub_sub_areas  gets wards within the selected province
#qry_rep_assessment_OVC_answer_geo_3 gets forms for those OVCs which are filled in the period and contain the specified answer
#final query ensures each OVC only counted once, and counts and groups by age and sex

# org unit specified
#expected parameters: org_unit_id, 'param_from_date', 'param_to_date', param_quesiton_id, param_answer_id
OVC_ASSESSMENT_ANSWER_ORG_SQL= "    SELECT tbl_list_general.item_description AS sex, div(date_began-date_of_birth, 365.25) AS age, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="    FROM                                     "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="                    (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id                    "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="                    FROM tbl_core_encounters                    "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="                    WHERE tbl_core_encounters.org_unit_id=%s                   "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="                    ) as qry_core_orgs_beneficiaries_sp                     "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="            ON tbl_forms.form_subject_id = qry_core_orgs_beneficiaries_sp.beneficiary_person_id                            "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_form_gen_answers.question_id=%s AND tbl_form_gen_answers.answer_id=%s                            "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="            ) as qry_rep_assessment_OVC_org                             "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="    INNER JOIN (tbl_reg_persons INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id) ON qry_rep_assessment_OVC_org.form_subject_id = tbl_reg_persons.id                                    "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="    GROUP BY sex, age, qry_rep_assessment_OVC_org.latest_is_1                                    "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="    HAVING qry_rep_assessment_OVC_org.latest_is_1=1                                    "
OVC_ASSESSMENT_ANSWER_ORG_SQL+="    ORDER BY age, sex                                    "
#qry_core_orgs_beneficiaries_sp   gets beneficiaries who have ever been served by the specified org unit
#qry_rep_assessment_OVC_org  gets forms for those OVCs which are filled in the period and contain the specified answer
#final query ensures each OVC only counted once, and counts and groups by age and sex

# org unit set specified
#expected parameters: param_set_id, 'param_from_date', 'param_to_date', param_quesiton_id, param_answer_id

OVC_ASSESSMENT_ANSWER_SET_SQL= "    SELECT tbl_list_general.item_description AS sex, div(date_began-date_of_birth, 365.25) AS age, Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_ANSWER_SET_SQL+="    FROM (tbl_reg_persons INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id)                                     "
OVC_ASSESSMENT_ANSWER_SET_SQL+="    INNER JOIN                                     "
OVC_ASSESSMENT_ANSWER_SET_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_forms.date_began,                             "
OVC_ASSESSMENT_ANSWER_SET_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_ANSWER_SET_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
OVC_ASSESSMENT_ANSWER_SET_SQL+="            INNER JOIN                             "
OVC_ASSESSMENT_ANSWER_SET_SQL+="                    (SELECT DISTINCT tbl_core_encounters.beneficiary_person_id                    "
OVC_ASSESSMENT_ANSWER_SET_SQL+="                    FROM tbl_core_encounters INNER JOIN tbl_reports_sets_org_units ON tbl_core_encounters.org_unit_id = tbl_reports_sets_org_units.org_unit_id                    "
OVC_ASSESSMENT_ANSWER_SET_SQL+="                    WHERE tbl_reports_sets_org_units.set_id=param_set_id                    "
OVC_ASSESSMENT_ANSWER_SET_SQL+="                    ) as qry_core_orgs_beneficiaries_set                     "
OVC_ASSESSMENT_ANSWER_SET_SQL+="            ON tbl_forms.form_subject_id = qry_core_orgs_beneficiaries_set.beneficiary_person_id                            "
OVC_ASSESSMENT_ANSWER_SET_SQL+="            WHERE tbl_forms.date_began>='param_from_date' And tbl_forms.date_began<='param_to_date' AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D' AND tbl_form_gen_answers.question_id=param_question_id AND tbl_form_gen_answers.answer_id=param_answer_id                            "
OVC_ASSESSMENT_ANSWER_SET_SQL+="            ) as qry_rep_assessment_OVC_answer_set                             "
OVC_ASSESSMENT_ANSWER_SET_SQL+="    ON tbl_reg_persons.id = qry_rep_assessment_OVC_answer_set.form_subject_id                                    "
OVC_ASSESSMENT_ANSWER_SET_SQL+="    GROUP BY sex, age, qry_rep_assessment_OVC_answer_set.latest_is_1                                    "
OVC_ASSESSMENT_ANSWER_SET_SQL+="    HAVING qry_rep_assessment_OVC_answer_set.latest_is_1=1                                    "
OVC_ASSESSMENT_ANSWER_SET_SQL+="    ORDER BY age, sex                                    "
#qry_core_orgs_beneficiaries_set   gets beneficiaries who have ever been served by any organisation in the specified org unit
#qry_rep_assessment_OVC_org  gets forms for those OVCs which are filled in the period and contain the specified answer
#final query ensures each OVC only counted once, and counts and groups by age and sex



#*******OVC ASSESSMENT OVER TIME **********

#expected parameters: 'param_from_date', 'param_to_date', 'param_quesiton_id', 'param_from_date_comp', 'param_to_date_comp', 'param_quesiton_id'

OVC_ASSESSMENT_OVERTIME_SQL= "    SELECT tbl_list_answers_init.answer AS \"Answer in first period\", tbl_list_answers_comp.answer AS \"Answer in second period\", Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_OVERTIME_SQL+="    FROM ((                                    "
OVC_ASSESSMENT_OVERTIME_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id,                             "
OVC_ASSESSMENT_OVERTIME_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began) AS first_is_1                    "
OVC_ASSESSMENT_OVERTIME_SQL+="            FROM tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id                            "
OVC_ASSESSMENT_OVERTIME_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_OVERTIME_SQL+="            ) as qry_rep_assessment_overtime_init_OVC_all                             "
OVC_ASSESSMENT_OVERTIME_SQL+="    INNER JOIN                                     "
OVC_ASSESSMENT_OVERTIME_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id,                             "
OVC_ASSESSMENT_OVERTIME_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_OVERTIME_SQL+="            FROM tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id                            "
OVC_ASSESSMENT_OVERTIME_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_form_gen_answers.question_id=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_OVERTIME_SQL+="            ) as qry_rep_assessment_overtime_comp_OVC_all                             "
OVC_ASSESSMENT_OVERTIME_SQL+="    ON qry_rep_assessment_overtime_init_OVC_all.form_subject_id = qry_rep_assessment_overtime_comp_OVC_all.form_subject_id)                                     "
OVC_ASSESSMENT_OVERTIME_SQL+="    INNER JOIN tbl_list_answers AS tbl_list_answers_init ON qry_rep_assessment_overtime_init_OVC_all.answer_id = tbl_list_answers_init.id)                                     "
OVC_ASSESSMENT_OVERTIME_SQL+="    INNER JOIN tbl_list_answers AS tbl_list_answers_comp ON qry_rep_assessment_overtime_comp_OVC_all.answer_id = tbl_list_answers_comp.id                                    "
OVC_ASSESSMENT_OVERTIME_SQL+="    GROUP BY tbl_list_answers_init.answer, tbl_list_answers_comp.answer, tbl_list_answers_init.the_order, tbl_list_answers_comp.the_order, qry_rep_assessment_overtime_init_OVC_all.first_is_1, qry_rep_assessment_overtime_comp_OVC_all.latest_is_1                                    "
OVC_ASSESSMENT_OVERTIME_SQL+="    HAVING qry_rep_assessment_overtime_init_OVC_all.first_is_1=1 AND qry_rep_assessment_overtime_comp_OVC_all.latest_is_1=1                                    "
OVC_ASSESSMENT_OVERTIME_SQL+="    ORDER BY tbl_list_answers_init.the_order, tbl_list_answers_comp.the_order                                    "
#qry_rep_assessment_overtime_init_OVC_all gets answers to the selected question in the first time period 
#qry_rep_assessment_overtime_comp_OVC_all gets answers to the selected question in the second time period
#final query takes only OVC who have an answer in both periods, takes the earliest answer from the first period and the latest answer from the second period, and counts the number of OVC who had each first-answer-second-answer combination 

#*******OVC ASSESSMENT CSI OVER TIME **********

#expected parameters: 'param_from_date', 'param_to_date', 'item_id', 'param_from_date_comp', 'param_to_date_comp', 'item_id'

OVC_ASSESSMENT_CSI_OVERTIME_SQL= "    SELECT qry_rep_assessment_overtime_init_CSI_all.score AS \"Score in first period\", qry_rep_assessment_overtime_comp_CSI_all.score AS \"Score in second period\", Count(*) AS \"Number of OVC\"                                    "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="    FROM                                     "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.score,                             "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began) AS first_is_1                    "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="            FROM tbl_forms INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id                            "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_form_csi.domain_id=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="            ) as qry_rep_assessment_overtime_init_CSI_all                             "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="    INNER JOIN                                     "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_csi.score,                             "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="            FROM tbl_forms INNER JOIN tbl_form_csi ON tbl_forms.id = tbl_form_csi.form_id                            "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_form_csi.domain_id=%s AND  tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3D'                            "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="            ) as qry_rep_assessment_overtime_comp_CSI_all                             "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="    ON qry_rep_assessment_overtime_init_CSI_all.form_subject_id = qry_rep_assessment_overtime_comp_CSI_all.form_subject_id                                    "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="    GROUP BY qry_rep_assessment_overtime_init_CSI_all.score, qry_rep_assessment_overtime_comp_CSI_all.score, qry_rep_assessment_overtime_init_CSI_all.first_is_1, qry_rep_assessment_overtime_comp_CSI_all.latest_is_1                                    "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="    HAVING qry_rep_assessment_overtime_init_CSI_all.first_is_1=1 AND qry_rep_assessment_overtime_comp_CSI_all.latest_is_1=1                                    "
OVC_ASSESSMENT_CSI_OVERTIME_SQL+="    ORDER BY qry_rep_assessment_overtime_init_CSI_all.score, qry_rep_assessment_overtime_comp_CSI_all.score                                    "
#qry_rep_assessment_overtime_init_CSI_all gets scores in the selected domain in the first time period 
#qry_rep_assessment_overtime_comp_CSI_all gets scores in the selected domain in the second time period
#final query takes only OVC who have an score in both periods, takes the earliest score from the first period and the latest score from the second period, and counts the number of OVC who had each first-score-second-score combination

#****RESIDENTIAL INSTITUTIONS********** 

#expected paramenters - 'param_to_date'
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL= "    SELECT tbl_list_general_res_status.item_description AS \"Residential status\", tbl_list_general_fam_status.item_description AS \"Family status\", Count(*) AS \"Number of OVC\"                                    "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="    FROM (                                    "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="            (SELECT tbl_form_res_children.child_person_id, tbl_form_res_children.residential_status_id, tbl_form_res_children.family_status_id, tbl_forms.date_began,                             "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="                    rank() OVER (PARTITION BY child_person_id ORDER BY date_began DESC) AS latest_is_1                    "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="            FROM tbl_forms INNER JOIN tbl_form_res_children ON tbl_forms.id = tbl_form_res_children.form_id                            "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="            WHERE tbl_forms.date_began<=%s                            "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="            ) as qry_rep_res_status_tot_a                             "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="    INNER JOIN tbl_list_general AS tbl_list_general_res_status ON qry_rep_res_status_tot_a.residential_status_id = tbl_list_general_res_status.item_id)                                     "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="    LEFT JOIN tbl_list_general AS tbl_list_general_fam_status ON qry_rep_res_status_tot_a.family_status_id = tbl_list_general_fam_status.item_id                                    "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="    GROUP BY tbl_list_general_res_status.item_description, tbl_list_general_fam_status.item_description, qry_rep_res_status_tot_a.residential_status_id, qry_rep_res_status_tot_a.family_status_id, qry_rep_res_status_tot_a.latest_is_1                                    "
RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL+="    HAVING latest_is_1=1                                    "
#qry_rep_res_status_tot_a gets the residential and family status of children as of the date
#final query takes only the most recent record of each child, and counts how many children have each status

#expected paramenters - 'param_to_date'
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL= "    SELECT tbl_list_geo_2.area_name AS District, tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", tbl_list_general_res_status.item_description AS \"Residential status\", tbl_list_general_fam_status.item_description AS \"Family status\", number_of_OVC as \"Number of OVC\"                                    "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    FROM ((((((                                    "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="            (SELECT qry_rep_res_status_a.institution_id, qry_rep_res_status_a.residential_status_id, qry_rep_res_status_a.family_status_id, Count(*) AS number_of_OVC                            "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="            FROM                             "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="                    (SELECT tbl_form_res_children.institution_id, tbl_form_res_children.child_person_id, tbl_form_res_children.residential_status_id, tbl_form_res_children.family_status_id, tbl_forms.date_began,                     "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="                            rank() OVER (PARTITION BY child_person_id ORDER BY date_began DESC) AS latest_is_1            "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="                    FROM tbl_forms INNER JOIN tbl_form_res_children ON tbl_forms.id = tbl_form_res_children.form_id                    "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="                    WHERE tbl_forms.date_began<=%s                           "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="                    ) as qry_rep_res_status_a                    "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="            GROUP BY qry_rep_res_status_a.institution_id, qry_rep_res_status_a.residential_status_id, qry_rep_res_status_a.family_status_id, qry_rep_res_status_a.latest_is_1                            "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="            HAVING latest_is_1 = 1                            "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="            ) as qry_rep_res_status_b                             "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    INNER JOIN tbl_list_general AS tbl_list_general_res_status ON qry_rep_res_status_b.residential_status_id = tbl_list_general_res_status.item_id)                                     "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    LEFT JOIN tbl_list_general AS tbl_list_general_fam_status ON qry_rep_res_status_b.family_status_id = tbl_list_general_fam_status.item_id)                                     "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    INNER JOIN tbl_reg_org_units ON qry_rep_res_status_b.institution_id = tbl_reg_org_units.id)                                     "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    INNER JOIN tbl_reg_org_units_geo ON tbl_reg_org_units.id = tbl_reg_org_units_geo.org_unit_id)                                     "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    INNER JOIN tbl_list_geo ON tbl_reg_org_units_geo.area_id = tbl_list_geo.area_id)                                     "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.parent_area_id = tbl_list_geo_1.area_id)                                     "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.parent_area_id = tbl_list_geo_2.area_id                                    "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    WHERE tbl_list_geo.area_type_id='GWRD' AND tbl_reg_org_units_geo.is_void=False AND tbl_reg_org_units_geo.date_delinked Is Null                                    "
RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL+="    ORDER BY tbl_list_geo_2.area_name, tbl_reg_org_units.org_unit_name, tbl_list_general_res_status.the_order, tbl_list_general_fam_status.the_order                                    "
#qry_rep_res_status_a gets the residential and family status of children as of the date
#qry_rep_res_status_b gets only the most recent record of each child, and counts how many children have each status
#final query gets the descriptive fields for the statuses, the institution and the district where it is located

#expected paramenters - 'param_to_date', 'param_to_date'
RESIDENTIAL_DEMOG_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(%s-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC in residential institutions\"                                    "
RESIDENTIAL_DEMOG_SQL+="    FROM (                                    "
RESIDENTIAL_DEMOG_SQL+="            (SELECT tbl_form_res_children.child_person_id, tbl_form_res_children.residential_status_id, tbl_forms.date_began,                             "
RESIDENTIAL_DEMOG_SQL+="                    rank() OVER (PARTITION BY child_person_id ORDER BY date_began DESC) AS latest_is_1                    "
RESIDENTIAL_DEMOG_SQL+="            FROM tbl_forms INNER JOIN tbl_form_res_children ON tbl_forms.id = tbl_form_res_children.form_id                            "
RESIDENTIAL_DEMOG_SQL+="            WHERE tbl_forms.date_began<=%s  and is_void=false                          "
RESIDENTIAL_DEMOG_SQL+="            ) as qry_res_all_todate                             "
RESIDENTIAL_DEMOG_SQL+="    INNER JOIN tbl_reg_persons ON qry_res_all_todate.child_person_id = tbl_reg_persons.id)                                     "
RESIDENTIAL_DEMOG_SQL+="    INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id                                    "
RESIDENTIAL_DEMOG_SQL+="    GROUP BY qry_res_all_todate.residential_status_id, qry_res_all_todate.latest_is_1, age, sex                                    "
RESIDENTIAL_DEMOG_SQL+="    HAVING qry_res_all_todate.residential_status_id='RSRS' AND qry_res_all_todate.latest_is_1=1                                    "
RESIDENTIAL_DEMOG_SQL+="    ORDER BY age, sex                                    "
#qry_res_all_todate gets children associated with residential institutions as of the date 
#final query gets only children marked as resident in their most recent record as of the date, and counts them by age and sex

#expected paramenters - 'param_to_date', 'param_to_date', param_org_unit_id
RESIDENTIAL_DEMOG_ORG_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(%s-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of OVC in residential institution\"                                    "
RESIDENTIAL_DEMOG_ORG_SQL+="    FROM (                                    "
RESIDENTIAL_DEMOG_ORG_SQL+="            (SELECT tbl_form_res_children.child_person_id, tbl_form_res_children.residential_status_id, tbl_forms.date_began,                             "
RESIDENTIAL_DEMOG_ORG_SQL+="                    rank() OVER (PARTITION BY child_person_id ORDER BY date_began DESC) AS latest_is_1                    "
RESIDENTIAL_DEMOG_ORG_SQL+="            FROM tbl_forms INNER JOIN tbl_form_res_children ON tbl_forms.id = tbl_form_res_children.form_id                            "
RESIDENTIAL_DEMOG_ORG_SQL+="            WHERE tbl_forms.date_began<=%s AND tbl_form_res_children.institution_id=%s and is_void=false                          "
RESIDENTIAL_DEMOG_ORG_SQL+="            ) as qry_res_org_todate                             "
RESIDENTIAL_DEMOG_ORG_SQL+="    INNER JOIN tbl_reg_persons ON qry_res_org_todate.child_person_id = tbl_reg_persons.id)                                     "
RESIDENTIAL_DEMOG_ORG_SQL+="    INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id                                    "
RESIDENTIAL_DEMOG_ORG_SQL+="    GROUP BY qry_res_org_todate.residential_status_id, qry_res_org_todate.latest_is_1, age, sex                                    "
RESIDENTIAL_DEMOG_ORG_SQL+="    HAVING qry_res_org_todate.residential_status_id='RSRS' AND qry_res_org_todate.latest_is_1=1                                    "
RESIDENTIAL_DEMOG_ORG_SQL+="    ORDER BY age, sex                                    "
#qry_res_org_todate gets children associated with the selected residential institutions as of the date 
#final query gets only children marked as resident in their most recent record as of the date, and counts them by age and sex

#parameters expected: none
RESIDENTIAL_ADVERSE_COND_SQL= "    SELECT tbl_list_general.item_description AS \"Adverse condition\", Count(*) AS \"Number of OVC  who have ever been in residential institutions who have ever experienced adverse condition\"                                    "
RESIDENTIAL_ADVERSE_COND_SQL+="    FROM (                                    "
RESIDENTIAL_ADVERSE_COND_SQL+="            (SELECT DISTINCT tbl_form_res_children.child_person_id                            "
RESIDENTIAL_ADVERSE_COND_SQL+="            FROM tbl_form_res_children                            "
RESIDENTIAL_ADVERSE_COND_SQL+="            WHERE tbl_form_res_children.residential_status_id<>'RSNV'                            "
RESIDENTIAL_ADVERSE_COND_SQL+="            ) as qry_res_all                             "
RESIDENTIAL_ADVERSE_COND_SQL+="    INNER JOIN                                     "
RESIDENTIAL_ADVERSE_COND_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                            "
RESIDENTIAL_ADVERSE_COND_SQL+="            FROM tbl_core_adverse_conditions                            "
RESIDENTIAL_ADVERSE_COND_SQL+="            ) as qry_adverse_conditions_unique                             "
RESIDENTIAL_ADVERSE_COND_SQL+="    ON qry_res_all.child_person_id = qry_adverse_conditions_unique.beneficiary_person_id)                                     "
RESIDENTIAL_ADVERSE_COND_SQL+="    INNER JOIN tbl_list_general ON qry_adverse_conditions_unique.adverse_condition_id = tbl_list_general.item_id                                    "
RESIDENTIAL_ADVERSE_COND_SQL+="    GROUP BY tbl_list_general.item_description, tbl_list_general.the_order                                    "
RESIDENTIAL_ADVERSE_COND_SQL+="    ORDER BY tbl_list_general.the_order                                    "
#qry_res_all gets a list of OVC who have ever been resident in a residential institution 
#qry_adverse_conditions_unique lists adverse conditions experienced by OVC 
#final query counts by adverse condition 

#parameters expected: param_org_unit_id
RESIDENTIAL_ADVERSE_COND_ORG_SQL= "    SELECT tbl_list_general.item_description AS \"Adverse condition\", Count(*) AS \"Number of OVC  who have ever been in residential institutions who have ever experienced adverse condition\"                                    "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="    FROM                                     "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="            (SELECT DISTINCT tbl_form_res_children.child_person_id                            "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="            FROM tbl_form_res_children                            "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="            WHERE tbl_form_res_children.residential_status_id<>'RSNV' AND tbl_form_res_children.institution_id=%s                            "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="            ) as qry_res_org                             "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="    INNER JOIN (                                    "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="            (SELECT DISTINCT tbl_core_adverse_conditions.beneficiary_person_id, tbl_core_adverse_conditions.adverse_condition_id                            "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="            FROM tbl_core_adverse_conditions                            "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="            ) as qry_adverse_conditions_unique                             "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="    INNER JOIN tbl_list_general ON qry_adverse_conditions_unique.adverse_condition_id = tbl_list_general.item_id                                    "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="    ) ON qry_res_org.child_person_id = qry_adverse_conditions_unique.beneficiary_person_id                                    "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="    GROUP BY tbl_list_general.item_description, tbl_list_general.the_order                                    "
RESIDENTIAL_ADVERSE_COND_ORG_SQL+="    ORDER BY tbl_list_general.the_order                                    "
#qry_res_org gets a list of OVC who have ever been resident in the selected residential institution 
#qry_adverse_conditions_unique lists adverse conditions experienced by OVC 
#final query counts by adverse condition 



#parameters expected: 'param_from_date', 'param_to_date'
RESIDENTIAL_REINTEGRATION_DEMOG_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(date_left-date_of_birth, 365.25) AS \"Age when re-integrated\", Count(*) AS \"Number of OVC re-integrated during period\"                                    "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="    FROM (                                    "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="            (SELECT tbl_form_res_children.child_person_id, tbl_form_res_children.date_left,                             "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="                    rank() OVER (PARTITION BY child_person_id ORDER BY date_left DESC) AS latest_is_1                    "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="            FROM tbl_form_res_children                            "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="            WHERE tbl_form_res_children.date_left>=%s And tbl_form_res_children.date_left<=%s AND tbl_form_res_children.residential_status_id='RSRI'                            "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="            ) as qry_rep_res_reint                             "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="    INNER JOIN tbl_reg_persons ON qry_rep_res_reint.child_person_id = tbl_reg_persons.id)                                     "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="    INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id                                    "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="    GROUP BY sex, \"Age when re-integrated\", qry_rep_res_reint.latest_is_1                                    "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="    HAVING qry_rep_res_reint.latest_is_1=1                                    "
RESIDENTIAL_REINTEGRATION_DEMOG_SQL+="    ORDER BY \"Age when re-integrated\",sex                                    "
#qry_rep_res_reint gets records of children who have re integrated
#final query gets the age (on the date of re-integration), ensures each child counted only once, and counts by age and sex

#parameters expected: 'param_from_date', 'param_to_date'
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL= "    SELECT tbl_list_geo_2.area_name AS District, tbl_reg_org_units.org_unit_id_vis as \"Org unit ID\", tbl_reg_org_units.org_unit_name as \"Org unit\", Count(*) AS \"Number of OVC re-integrated during period\"                                    "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="    FROM                                     "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="            (SELECT DISTINCT tbl_form_res_children.institution_id, tbl_form_res_children.child_person_id                            "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="            FROM tbl_form_res_children                            "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="            WHERE tbl_form_res_children.residential_status_id='RSRI' AND tbl_form_res_children.date_left>=%s And tbl_form_res_children.date_left<=%s                            "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="            ) as qry_rep_res_reint_by_org                             "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="    INNER JOIN (tbl_reg_org_units INNER JOIN                                     "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="            (((tbl_reg_org_units_geo INNER JOIN tbl_list_geo ON tbl_reg_org_units_geo.area_id = tbl_list_geo.area_id) INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.parent_area_id = tbl_list_geo_1.area_id) INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.parent_area_id = tbl_list_geo_2.area_id)                             "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="            ON tbl_reg_org_units.id = tbl_reg_org_units_geo.org_unit_id)                             "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="            ON qry_rep_res_reint_by_org.institution_id = tbl_reg_org_units.id                            "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="    GROUP BY tbl_list_geo_2.area_name, tbl_reg_org_units.org_unit_id_vis, tbl_reg_org_units.org_unit_name                                    "
RESIDENTIAL_REINTEGRATION_BY_ORG_SQL+="    ORDER BY tbl_list_geo_2.area_name, tbl_reg_org_units.org_unit_name                                    "
#qry_rep_res_reint_by_org gets distinct children who have re integrated by each institution
#final query gets the district the institution is in, its details, and counts reintegrated children for each institution

#parameters expected: 'param_from_date', 'param_to_date', param_question_id
RESIDENTIAL_ASSESSMENT_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of institutions\"                                    "
RESIDENTIAL_ASSESSMENT_SQL+="    FROM                                     "
RESIDENTIAL_ASSESSMENT_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began, 1 AS latest_is_1                            "
RESIDENTIAL_ASSESSMENT_SQL+="            FROM tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id                            "
RESIDENTIAL_ASSESSMENT_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT1f' AND tbl_form_gen_answers.question_id=%s                           "
RESIDENTIAL_ASSESSMENT_SQL+="            ) as qry_rep_assessment_res_all                             "
RESIDENTIAL_ASSESSMENT_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_res_all.answer_id = tbl_list_answers.id                                    "
RESIDENTIAL_ASSESSMENT_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_res_all.latest_is_1                                    "
RESIDENTIAL_ASSESSMENT_SQL+="    HAVING qry_rep_assessment_res_all.latest_is_1=1                                    "
RESIDENTIAL_ASSESSMENT_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_rep_assessment_res_all gets all residential institution assessment answers from the selected question and the selected period
#final query ensures the latest assessment for each residential institution is used, and counts each answer 

#****CHILD IN CONFLICT WITH LAW**********

# only period specified
#parameters expected: 'param_from_date', 'param_to_date', param_question_id
CICWL_ASSESSMENT_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of children\"                                    "
CICWL_ASSESSMENT_SQL+="    FROM                                     "
CICWL_ASSESSMENT_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
CICWL_ASSESSMENT_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
CICWL_ASSESSMENT_SQL+="            FROM tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id                            "
CICWL_ASSESSMENT_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3f' AND tbl_form_gen_answers.question_id=%s                            "
CICWL_ASSESSMENT_SQL+="            ) as qry_rep_assessment_cicwl_all                             "
CICWL_ASSESSMENT_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_cicwl_all.answer_id = tbl_list_answers.id                                    "
CICWL_ASSESSMENT_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_cicwl_all.latest_is_1                                    "
CICWL_ASSESSMENT_SQL+="    HAVING qry_rep_assessment_cicwl_all.latest_is_1=1                                    "
CICWL_ASSESSMENT_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_rep_assessment_res_all gets all child in conflict with law answers from the selected question and the selected period (using date of arrest which is date_began in tbl_forms)
#final query ensures the latest form for each child is used, and counts each answer

# period, district specified
#parameters expected: param_area_id, 'param_from_date', 'param_to_date', param_question_id
CICWL_ASSESSMENT_GEO_2_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of children\"                                    "
CICWL_ASSESSMENT_GEO_2_SQL+="    FROM                                     "
CICWL_ASSESSMENT_GEO_2_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
CICWL_ASSESSMENT_GEO_2_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
CICWL_ASSESSMENT_GEO_2_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
CICWL_ASSESSMENT_GEO_2_SQL+="            INNER JOIN                             "
CICWL_ASSESSMENT_GEO_2_SQL+="                    (SELECT tbl_list_geo_1.area_id                    "
CICWL_ASSESSMENT_GEO_2_SQL+="                    FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id                    "
CICWL_ASSESSMENT_GEO_2_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s                   "
CICWL_ASSESSMENT_GEO_2_SQL+="                    ) as qry_heir_sub_sub_areas                     "
CICWL_ASSESSMENT_GEO_2_SQL+="            ON tbl_forms.form_area_id = qry_heir_sub_sub_areas.area_id                            "
CICWL_ASSESSMENT_GEO_2_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3f' AND tbl_form_gen_answers.question_id=%s                            "
CICWL_ASSESSMENT_GEO_2_SQL+="            ) as qry_rep_assessment_cicwl_geo_2                             "
CICWL_ASSESSMENT_GEO_2_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_cicwl_geo_2.answer_id = tbl_list_answers.id                                    "
CICWL_ASSESSMENT_GEO_2_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_cicwl_geo_2.latest_is_1                                    "
CICWL_ASSESSMENT_GEO_2_SQL+="    HAVING qry_rep_assessment_cicwl_geo_2.latest_is_1=1                                    "
CICWL_ASSESSMENT_GEO_2_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_heir_sub_sub_areas gets wards in the selected district
#qry_rep_assessment_res_geo_2 gets all child in conflict with law answers from the selected question and the selected period (using date of arrest which is date_began in tbl_forms)
#final query ensures the latest form for each child is used, and counts each answer

# period, province specified
#parameters expected: param_area_id, 'param_from_date', 'param_to_date', param_question_id
CICWL_ASSESSMENT_GEO_3_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of children\"                                    "
CICWL_ASSESSMENT_GEO_3_SQL+="    FROM                                     "
CICWL_ASSESSMENT_GEO_3_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
CICWL_ASSESSMENT_GEO_3_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
CICWL_ASSESSMENT_GEO_3_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
CICWL_ASSESSMENT_GEO_3_SQL+="            INNER JOIN                             "
CICWL_ASSESSMENT_GEO_3_SQL+="                    (SELECT tbl_list_geo_2.area_id                    "
CICWL_ASSESSMENT_GEO_3_SQL+="                    FROM (tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id)                     "
CICWL_ASSESSMENT_GEO_3_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON tbl_list_geo_1.area_id = tbl_list_geo_2.parent_area_id                    "
CICWL_ASSESSMENT_GEO_3_SQL+="                    WHERE tbl_list_geo.parent_area_id=%s                    "
CICWL_ASSESSMENT_GEO_3_SQL+="                    ) as qry_heir_sub_sub_sub_areas                     "
CICWL_ASSESSMENT_GEO_3_SQL+="            ON tbl_forms.form_area_id = qry_heir_sub_sub_sub_areas.area_id                            "
CICWL_ASSESSMENT_GEO_3_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3f' AND tbl_form_gen_answers.question_id=%s                            "
CICWL_ASSESSMENT_GEO_3_SQL+="            ) as qry_rep_assessment_cicwl_geo_3                             "
CICWL_ASSESSMENT_GEO_3_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_cicwl_geo_3.answer_id = tbl_list_answers.id                                    "
CICWL_ASSESSMENT_GEO_3_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_cicwl_geo_3.latest_is_1                                    "
CICWL_ASSESSMENT_GEO_3_SQL+="    HAVING qry_rep_assessment_cicwl_geo_3.latest_is_1=1                                    "
CICWL_ASSESSMENT_GEO_3_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_heir_sub_sub_areas gets wards in the selected province
#qry_rep_assessment_res_geo_3 gets all child in conflict with law answers from the selected question and the selected period (using date of arrest which is date_began in tbl_forms)
#final query ensures the latest form for each child is used, and counts each answer


#parameters expected: 'param_from_date', 'param_to_date' 
CICWL_DEMOG_SQL= "    SELECT tbl_list_general.item_description AS Sex, div(date_began-date_of_birth, 365.25) AS Age, Count(*) AS \"Number of children\"                                    "
CICWL_DEMOG_SQL+="    FROM (                                    "
CICWL_DEMOG_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_forms.date_began,                             "
CICWL_DEMOG_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
CICWL_DEMOG_SQL+="            FROM tbl_forms                            "
CICWL_DEMOG_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT3f'                            "
CICWL_DEMOG_SQL+="            ) as qry_rep_cicwl_demog                             "
CICWL_DEMOG_SQL+="    INNER JOIN tbl_reg_persons ON qry_rep_cicwl_demog.form_subject_id = tbl_reg_persons.id)                                     "
CICWL_DEMOG_SQL+="    INNER JOIN tbl_list_general ON tbl_reg_persons.sex_id = tbl_list_general.item_id                                    "
CICWL_DEMOG_SQL+="    GROUP BY sex, age, latest_is_1                                    "
CICWL_DEMOG_SQL+="    HAVING latest_is_1=1                                    "
CICWL_DEMOG_SQL+="    ORDER BY age, sex                                    "
#qry_rep_cicwl_demog gets child in conflict with law forms (cases) in the period 
#final query ensures each child counted only once, calculates age on the date of arrest and counts by age and sex 

#*******WORKFORCE**************

#list districts
#expected paramenters 'param_to_date' 'param_to_date' 'param_from_date' 'param_to_date'
WORKFORCE_ACTIVE_BY_GEO2_SQL= "    SELECT tbl_list_geo_3.area_name AS Province, tbl_list_geo_2.area_name AS District, Sum(qry_rep_wf_by_geo2.government_employee) AS \"Number of active Government employees\", Sum(qry_rep_wf_by_geo2.ngo_employee) AS \"Number of active NGO employees\", Sum(qry_rep_wf_by_geo2.volunteer) AS \"Number of active volunteers\"                                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="    FROM (                                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="            (SELECT qry_core_encounters_in_period_wf_g2.parent_area_id,                             "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    CASE WHEN person_type_id = 'TWGE' THEN 1 ELSE 0 END AS government_employee,                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    CASE WHEN person_type_id = 'TWNE' THEN 1 ELSE 0 END AS ngo_employee,                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    CASE WHEN person_type_id = 'TWVL' THEN 1 ELSE 0 END AS volunteer                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="            FROM                             "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    (SELECT DISTINCT qry_persons_type_todate_s.person_type_id, tbl_core_encounters.workforce_person_id, tbl_list_geo_1.parent_area_id                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    FROM ((tbl_core_encounters INNER JOIN                     "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                            (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                            FROM tbl_reg_persons_types            "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                            WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                            ) as qry_persons_type_todate_s             "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    ON tbl_core_encounters.workforce_person_id = qry_persons_type_todate_s.person_id)                     "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id)                     "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.parent_area_id = tbl_list_geo_1.area_id                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="                    ) as qry_core_encounters_in_period_wf_g2                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="            ) as qry_rep_wf_by_geo2                             "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="    INNER JOIN tbl_list_geo AS tbl_list_geo_2 ON qry_rep_wf_by_geo2.parent_area_id = tbl_list_geo_2.area_id)                                     "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="    INNER JOIN tbl_list_geo AS tbl_list_geo_3 ON tbl_list_geo_2.parent_area_id = tbl_list_geo_3.area_id                                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="    GROUP BY tbl_list_geo_3.area_name, tbl_list_geo_2.area_name                                    "
WORKFORCE_ACTIVE_BY_GEO2_SQL+="    ORDER BY tbl_list_geo_3.area_name, tbl_list_geo_2.area_name                                    "
#qry_persons_type_todate_s  gets the  type of each person (gov employee, ngo employee or volunteer) as of the end of the period
#qry_core_encounters_in_period_wf_g2 gets distinct workforce members who were active in the each district during the period 
#qry_rep_wf_by_geo2 puts the workforce types into separate columns
#final query counts by district 

#list wards in a selected district
#expected paramenters 'param_to_date' 'param_to_date' param_area_id 'param_from_date' 'param_to_date'
WORKFORCE_ACTIVE_BY_GEO0_SQL= "    SELECT tbl_list_geo.area_name as Ward, Sum(qry_rep_wf_by_geo0.government_employee) AS \"Number of active Government employees\", Sum(qry_rep_wf_by_geo0.ngo_employee) AS \"Number of active NGO employees\", Sum(qry_rep_wf_by_geo0.volunteer) AS \"Number of active volunteers\"                                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="    FROM                                     "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="            (SELECT qry_core_encounters_in_period_wf_g0.area_id,                            "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    CASE WHEN person_type_id = 'TWGE' THEN 1 ELSE 0 END AS government_employee,                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    CASE WHEN person_type_id = 'TWNE' THEN 1 ELSE 0 END AS ngo_employee,                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    CASE WHEN person_type_id = 'TWVL' THEN 1 ELSE 0 END AS volunteer                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="            FROM                             "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    (SELECT DISTINCT qry_persons_type_todate_s.person_type_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.area_id                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    FROM (tbl_core_encounters INNER JOIN                     "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                            (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                            FROM tbl_reg_persons_types            "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                            WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                            ) as qry_persons_type_todate_s             "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    ON tbl_core_encounters.workforce_person_id = qry_persons_type_todate_s.person_id)                     "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    INNER JOIN                     "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                            (SELECT tbl_list_geo_1.area_id            "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                            FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id            "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s            "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                            ) as qry_heir_sub_sub_areas             "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s                 "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="                    ) as qry_core_encounters_in_period_wf_g0                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="            ) as qry_rep_wf_by_geo0                             "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="    INNER JOIN tbl_list_geo ON qry_rep_wf_by_geo0.area_id = tbl_list_geo.area_id                                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="    GROUP BY tbl_list_geo.area_name                                    "
WORKFORCE_ACTIVE_BY_GEO0_SQL+="    ORDER BY tbl_list_geo.area_name                                    "
#qry_persons_type_todate_s  gets the type of each person (gov employee, ngo employee or volunteer) as of the end of the period 
#qry_heir_sub_sub_areas  gets wards in the selected district
#qry_core_encounters_in_period_wf_g0  gets distinct workforce members who were active in each ward during the period 
#qry_rep_wf_by_geo0 puts the workforce types into different columns
#final query counts by ward 

#list districts, one service
#expected paramenters 'param_to_date' 'param_to_date' 'param_from_date' 'param_to_date', param_item_id
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL= "    SELECT tbl_list_geo_3.area_name AS Province, tbl_list_geo_2.area_name AS District, Sum(qry_rep_wf_by_geo2_sp.government_employee) AS \"Number of active Government employees\", Sum(qry_rep_wf_by_geo2_sp.ngo_employee) AS \"Number of active NGO employees\", Sum(qry_rep_wf_by_geo2_sp.volunteer) AS \"Number of active volunteers\"                                    "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="    FROM                                     "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="            (SELECT qry_core_encounters_in_period_wf_g2_sp.parent_area_id,                             "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                    CASE WHEN person_type_id = 'TWGE' THEN 1 ELSE 0 END AS government_employee,                    "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                    CASE WHEN person_type_id = 'TWNE' THEN 1 ELSE 0 END AS ngo_employee,                    "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                    CASE WHEN person_type_id = 'TWVL' THEN 1 ELSE 0 END AS volunteer                    "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="            FROM                             "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                    (SELECT DISTINCT qry_persons_type_todate_s.person_type_id, tbl_core_encounters.workforce_person_id, tbl_list_geo_1.parent_area_id                    "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                    FROM (((tbl_core_encounters INNER JOIN                     "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                            (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                            FROM tbl_reg_persons_types            "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                            WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                            ) as qry_persons_type_todate_s             "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="                    ON tbl_core_encounters.workforce_person_id = qry_persons_type_todate_s.person_id)                     "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="            INNER JOIN tbl_list_geo ON tbl_core_encounters.area_id = tbl_list_geo.area_id)                             "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="            INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.parent_area_id = tbl_list_geo_1.area_id)                             "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="            INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)                            "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="            WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s AND tbl_core_services.core_item_id=%s                            "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="            ) as qry_core_encounters_in_period_wf_g2_sp                            "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="    ) as qry_rep_wf_by_geo2_sp                                     "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="    INNER JOIN (tbl_list_geo AS tbl_list_geo_2 INNER JOIN tbl_list_geo AS tbl_list_geo_3 ON tbl_list_geo_2.parent_area_id = tbl_list_geo_3.area_id)                                     "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="    ON qry_rep_wf_by_geo2_sp.parent_area_id = tbl_list_geo_2.area_id                                    "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="    GROUP BY tbl_list_geo_3.area_name, tbl_list_geo_2.area_name                                    "
WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL+="    ORDER BY tbl_list_geo_3.area_name, tbl_list_geo_2.area_name                                    "
#qry_persons_type_todate_s  gets the  type of each person (gov employee, ngo employee or volunteer) as of the end of the period
#qry_core_encounters_in_period_wf_g2_sp gets distinct workforce members who were active providing selected service in the each district during the period 
#qry_rep_wf_by_geo2_sp puts the workforce types into separate columns
#final query counts by district 

#list wards in a selected district, one service
#expected paramenters 'param_to_date' 'param_to_date' param_area_id 'param_from_date' 'param_to_date', 'param_item_id'
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL= "    SELECT tbl_list_geo.area_name as Ward, Sum(qry_rep_wf_by_geo0_sp.government_employee) AS \"Number of active Government employees\", Sum(qry_rep_wf_by_geo0_sp.ngo_employee) AS \"Number of active NGO employees\", Sum(qry_rep_wf_by_geo0_sp.volunteer) AS \"Number of active volunteers\"                                    "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="    FROM tbl_list_geo INNER JOIN                                     "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="            (SELECT qry_core_encounters_in_period_wf_g0_sp.area_id,                             "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    CASE WHEN person_type_id = 'TWGE' THEN 1 ELSE 0 END AS government_employee,                    "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    CASE WHEN person_type_id = 'TWNE' THEN 1 ELSE 0 END AS ngo_employee,                    "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    CASE WHEN person_type_id = 'TWVL' THEN 1 ELSE 0 END AS volunteer                    "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="            FROM                             "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    (SELECT DISTINCT qry_persons_type_todate_s.person_type_id, tbl_core_encounters.workforce_person_id, tbl_core_encounters.area_id                    "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    FROM ((tbl_core_encounters INNER JOIN                     "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                            (SELECT tbl_reg_persons_types.person_id, tbl_reg_persons_types.person_type_id            "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                            FROM tbl_reg_persons_types            "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                            WHERE tbl_reg_persons_types.date_began<=%s AND (tbl_reg_persons_types.date_ended Is Null Or tbl_reg_persons_types.date_ended>%s)            "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                            ) as qry_persons_type_todate_s             "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    ON tbl_core_encounters.workforce_person_id = qry_persons_type_todate_s.person_id)                     "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    INNER JOIN                     "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                            (SELECT tbl_list_geo_1.area_id            "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                            FROM tbl_list_geo INNER JOIN tbl_list_geo AS tbl_list_geo_1 ON tbl_list_geo.area_id = tbl_list_geo_1.parent_area_id            "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                            WHERE tbl_list_geo.parent_area_id=%s           "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                            ) as qry_heir_sub_sub_areas             "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    ON tbl_core_encounters.area_id = qry_heir_sub_sub_areas.area_id)                     "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    INNER JOIN tbl_core_services ON (tbl_core_encounters.encounter_date = tbl_core_services.encounter_date) AND (tbl_core_encounters.beneficiary_person_id = tbl_core_services.beneficiary_person_id) AND (tbl_core_encounters.workforce_person_id = tbl_core_services.workforce_person_id)                    "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    WHERE tbl_core_encounters.encounter_date>=%s And tbl_core_encounters.encounter_date<=%s AND tbl_core_services.core_item_id=%s                   "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="                    ) as qry_core_encounters_in_period_wf_g0_sp                    "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="            ) as qry_rep_wf_by_geo0_sp                             "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="            ON tbl_list_geo.area_id = qry_rep_wf_by_geo0_sp.area_id                            "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="    GROUP BY tbl_list_geo.area_name                                    "
WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL+="    ORDER BY tbl_list_geo.area_name                                    "
#qry_persons_type_todate_s  gets the type of each person (gov employee, ngo employee or volunteer) as of the end of the period 
#qry_heir_sub_sub_areas  gets wards in the selected district
#qry_core_encounters_in_period_wf_g0_sp  gets distinct workforce members who were active providing selected service in each ward during the period 
#qry_rep_wf_by_geo0 puts the workforce types into different columns
#final query counts by ward 

#period, question specified
#parameters expected: 'param_from_date', 'param_to_date', param_question_id 
WORKFORCE_ASSESSMENT_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of workforce members\"                                    "
WORKFORCE_ASSESSMENT_SQL+="    FROM                                     "
WORKFORCE_ASSESSMENT_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
WORKFORCE_ASSESSMENT_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
WORKFORCE_ASSESSMENT_SQL+="            FROM tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id                            "
WORKFORCE_ASSESSMENT_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT2c' AND tbl_form_gen_answers.question_id=%s                           "
WORKFORCE_ASSESSMENT_SQL+="            ) as qry_rep_assessment_wf_all                             "
WORKFORCE_ASSESSMENT_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_wf_all.answer_id = tbl_list_answers.id                                    "
WORKFORCE_ASSESSMENT_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_wf_all.latest_is_1                                    "
WORKFORCE_ASSESSMENT_SQL+="    HAVING qry_rep_assessment_wf_all.latest_is_1=1                                    "
WORKFORCE_ASSESSMENT_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_rep_assessment_wf_all gets all workforce assessment answers from the selected question and the selected period 
#final query takes only the most recent answer for each workforce member, and counts them by answer


#period, org unit, question specified
#parameters expected: 'param_to_date', 'param_to_date', 'param_from_date', 'param_to_date', param_question_id, param_org_unit_id 
WORKFORCE_ASSESSMENT_ORG_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of workforce members\"                                    "
WORKFORCE_ASSESSMENT_ORG_SQL+="    FROM                                     "
WORKFORCE_ASSESSMENT_ORG_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
WORKFORCE_ASSESSMENT_ORG_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
WORKFORCE_ASSESSMENT_ORG_SQL+="            FROM (tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
WORKFORCE_ASSESSMENT_ORG_SQL+="            INNER JOIN                             "
WORKFORCE_ASSESSMENT_ORG_SQL+="                    (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons_org_units.parent_org_unit_id                    "
WORKFORCE_ASSESSMENT_ORG_SQL+="                    FROM tbl_reg_persons INNER JOIN tbl_reg_persons_org_units ON tbl_reg_persons.id = tbl_reg_persons_org_units.person_id                    "
WORKFORCE_ASSESSMENT_ORG_SQL+="                    WHERE tbl_reg_persons_org_units.date_linked<=%s AND (tbl_reg_persons_org_units.date_delinked Is Null Or tbl_reg_persons_org_units.date_delinked>%s) AND tbl_reg_persons.is_void=False                    "
WORKFORCE_ASSESSMENT_ORG_SQL+="                    ) as qry_persons_parent_org_todate                     "
WORKFORCE_ASSESSMENT_ORG_SQL+="            ON tbl_forms.form_subject_id = qry_persons_parent_org_todate.person_id                            "
WORKFORCE_ASSESSMENT_ORG_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT2c' AND tbl_form_gen_answers.question_id=%s AND qry_persons_parent_org_todate.parent_org_unit_id=%s                            "
WORKFORCE_ASSESSMENT_ORG_SQL+="            ) as qry_rep_assessment_wf_org                             "
WORKFORCE_ASSESSMENT_ORG_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_wf_org.answer_id = tbl_list_answers.id                                    "
WORKFORCE_ASSESSMENT_ORG_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_wf_org.latest_is_1                                    "
WORKFORCE_ASSESSMENT_ORG_SQL+="    HAVING qry_rep_assessment_wf_org.latest_is_1=1                                    "
WORKFORCE_ASSESSMENT_ORG_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_persons_parent_org_todate gets parent orgs of workforce members as of the end of the period 
#qry_rep_assessment_wf_org gets workforce assessment answers from workforce members in the selected org unit, from the selected question and the selected period 
#final query takes only the most recent answer for each workforce member, and counts them by answer

#period, org unit set, question specified
#parameters expected: 'param_to_date', 'param_to_date', 'param_from_date', 'param_to_date', param_question_id, param_set_id 
WORKFORCE_ASSESSMENT_SET_SQL= "    SELECT tbl_list_answers.Answer, Count(*) AS \"Number of workforce members\"                                    "
WORKFORCE_ASSESSMENT_SET_SQL+="    FROM                                     "
WORKFORCE_ASSESSMENT_SET_SQL+="            (SELECT tbl_forms.form_subject_id, tbl_form_gen_answers.answer_id, tbl_forms.date_began,                             "
WORKFORCE_ASSESSMENT_SET_SQL+="                    rank() OVER (PARTITION BY form_subject_id ORDER BY date_began DESC) AS latest_is_1                    "
WORKFORCE_ASSESSMENT_SET_SQL+="            FROM ((tbl_forms INNER JOIN tbl_form_gen_answers ON tbl_forms.id = tbl_form_gen_answers.form_id)                             "
WORKFORCE_ASSESSMENT_SET_SQL+="            INNER JOIN                             "
WORKFORCE_ASSESSMENT_SET_SQL+="                    (SELECT tbl_reg_persons.id AS person_id, tbl_reg_persons_org_units.parent_org_unit_id                    "
WORKFORCE_ASSESSMENT_SET_SQL+="                    FROM tbl_reg_persons INNER JOIN tbl_reg_persons_org_units ON tbl_reg_persons.id = tbl_reg_persons_org_units.person_id                    "
WORKFORCE_ASSESSMENT_SET_SQL+="                    WHERE tbl_reg_persons_org_units.date_linked<=%s AND (tbl_reg_persons_org_units.date_delinked Is Null Or tbl_reg_persons_org_units.date_delinked>%s) AND tbl_reg_persons.is_void=False                    "
WORKFORCE_ASSESSMENT_SET_SQL+="                    ) as qry_persons_parent_org_todate                     "
WORKFORCE_ASSESSMENT_SET_SQL+="            ON tbl_forms.form_subject_id = qry_persons_parent_org_todate.person_id)                             "
WORKFORCE_ASSESSMENT_SET_SQL+="            INNER JOIN tbl_reports_sets_org_units ON qry_persons_parent_org_todate.parent_org_unit_id = tbl_reports_sets_org_units.org_unit_id                            "
WORKFORCE_ASSESSMENT_SET_SQL+="            WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT2c' AND tbl_form_gen_answers.question_id=%s AND tbl_reports_sets_org_units.set_id=%s                   "
WORKFORCE_ASSESSMENT_SET_SQL+="            ) as qry_rep_assessment_wf_set                             "
WORKFORCE_ASSESSMENT_SET_SQL+="    INNER JOIN tbl_list_answers ON qry_rep_assessment_wf_set.answer_id = tbl_list_answers.id                                    "
WORKFORCE_ASSESSMENT_SET_SQL+="    GROUP BY tbl_list_answers.answer, tbl_list_answers.the_order, qry_rep_assessment_wf_set.latest_is_1                                    "
WORKFORCE_ASSESSMENT_SET_SQL+="    HAVING qry_rep_assessment_wf_set.latest_is_1=1                                    "
WORKFORCE_ASSESSMENT_SET_SQL+="    ORDER BY tbl_list_answers.the_order                                    "
#qry_persons_parent_org_todate gets parent orgs of workforce members as of the end of the period 
#qry_rep_assessment_wf_org gets workforce assessment answers from workforce members in the selected set of org units, from the selected question and the selected period 
#final query takes only the most recent answer for each workforce member, and counts them by answer

 
#period specified
#parameters expected: 'param_from_date', 'param_to_date', 'param_to_date' 
WF_BY_ORG_SQL= "    SELECT tbl_reg_org_units.org_unit_id_vis AS \"Org unit ID\" , tbl_reg_org_units.org_unit_name AS \"Org unit name\", tbl_list_general.item_description AS \"Org unit type\", Count(*) AS \"Number of users\", Sum(qry_rep_wf_by_org_a.is_workforce) AS \"Number of workforce members\", Sum(qry_rep_wf_by_org_a.assessed_during_period) AS \"Number of workforce members assessed during period\"                                    "
WF_BY_ORG_SQL+="    FROM                                     "
WF_BY_ORG_SQL+="            (SELECT tbl_reg_persons_org_units.parent_org_unit_id, tbl_reg_persons_org_units.person_id,                             "
WF_BY_ORG_SQL+="                    CASE WHEN (tbl_reg_persons.workforce_id is not null and tbl_reg_persons.workforce_id <> '') THEN 1 ELSE 0 END AS is_workforce,                     "
WF_BY_ORG_SQL+="                    CASE WHEN (form_subject_id is not null) THEN 1 ELSE 0 END AS assessed_during_period                     "
WF_BY_ORG_SQL+="            FROM tbl_reg_persons                             "
WF_BY_ORG_SQL+="            INNER JOIN (tbl_reg_persons_org_units                             "
WF_BY_ORG_SQL+="            LEFT JOIN                             "
WF_BY_ORG_SQL+="                    (SELECT DISTINCT tbl_forms.form_subject_id                    "
WF_BY_ORG_SQL+="                    FROM tbl_forms                    "
WF_BY_ORG_SQL+="                    WHERE tbl_forms.date_began>=%s And tbl_forms.date_began<=%s AND tbl_forms.is_void=False AND tbl_forms.form_type_id='FT2c'                    "
WF_BY_ORG_SQL+="                    ) as qry_rep_wf_by_org_assess                     "
WF_BY_ORG_SQL+="            ON tbl_reg_persons_org_units.person_id = qry_rep_wf_by_org_assess.form_subject_id)                             "
WF_BY_ORG_SQL+="            ON tbl_reg_persons.id = tbl_reg_persons_org_units.person_id                            "
WF_BY_ORG_SQL+="             INNER JOIN auth_user ON tbl_reg_persons.id = auth_user.reg_person_id                            "
WF_BY_ORG_SQL+="            WHERE (tbl_reg_persons_org_units.date_delinked Is Null Or tbl_reg_persons_org_units.date_delinked>%s) AND auth_user.is_active=True                            "
WF_BY_ORG_SQL+="            ) as qry_rep_wf_by_org_a                             "
WF_BY_ORG_SQL+="    INNER JOIN (tbl_reg_org_units INNER JOIN tbl_list_general ON tbl_reg_org_units.org_unit_type_id = tbl_list_general.item_id) ON qry_rep_wf_by_org_a.parent_org_unit_id = tbl_reg_org_units.id                                    "
WF_BY_ORG_SQL+="    GROUP BY tbl_reg_org_units.org_unit_id_vis, tbl_reg_org_units.org_unit_name, tbl_list_general.item_description                                    "
WF_BY_ORG_SQL+="    ORDER BY item_description, org_unit_name                                    "
#qry_rep_wf_by_org_assess gets distinct users/wf who have been asssessed during the period 
#qry_rep_wf_by_org_a lists all active users/wf with their parent organisational units and marks whether they are workforce members or not and whether they have been assessed during period or not
 #qry_rep_wf_by_org_b lists org units and their type and counts how many users, how many are workforce members and how many have been assessed during period 