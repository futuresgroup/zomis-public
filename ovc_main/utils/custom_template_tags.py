from crispy_forms.layout import Div,LayoutObject
from django.template.loader import render_to_string

class CustomDiv(Div):
    
    def __init__(self, *fields, **kwargs):
        super(LayoutObject).__init__(*fields,**kwargs)
        
    def render(self, form, form_style, contextt, template_pack):
        fields = ''
        for field in self.fields:
            tmpfield = render_field(field, form, form_style, context, template_pack=template_pack)
            if self.template != 'uni_form/layout/div.html':
                #print self.custom_fields
                print 'out type of field', field
            if field in form.fields:
                
                self.custom_fields.append(form.fields[field])
            fields += tmpfield
        if self.template != 'uni_form/layout/div.html':
            print self.custom_fields
            print 'fornm fields', form.fields
        return render_to_string(self.template, Context({'div': self, 'fields': fields, 'custom_fields':self.custom_fields}))