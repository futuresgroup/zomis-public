from ovc_main import workforce_registration

def get_workforce_orgs(workforce_id):

    workforce = workforce_registration.load_wfc_from_id(workforce_id,include_dead=True)
    
    return workforce.org_units if workforce else None
    
def workforce_orgs_for_drop_list(workforce_id):
    orgs = get_workforce_orgs(workforce_id)
    
    droplist = [('','-----')]
    
    if orgs:
        droplist =  tuple(droplist + [(org.org_id_int, '%s - %s' % (org.org_id, org.org_name)) for org in orgs])

    return droplist

def workforce_orgs_dict_for_display(workforce_id):
    tmporgs = {}
    orgs = get_workforce_orgs(workforce_id)
    for org in orgs:
        if org.primary_org == 'Yes':
            tmporgs[-1] = org.org_id_int
        #Add the rest of the orgs
    for org in orgs:
        if org.org_id_int not in tmporgs:
            tmporgs[org.org_id_int] = '%s - %s' % (org.org_id, org.org_name)
    return tmporgs