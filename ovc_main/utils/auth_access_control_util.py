from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.models import SetupList, SetupGeorgraphy, RegOrgUnit
from ovc_main.utils.fields_list_provider import get_org_list, get_list_of_organisations_by_ids
from functools import partial
from ovc_main.utils.geo_location import get_communities_in_ward, get_all_org_subunits_and_self
from ovc_auth import user_manager
DateInput = partial(forms.DateInput, {'class': 'datepicker'})
import datetime
from ovc_auth.models import OVCUserRoleGeoOrg
import traceback


def get_related_organisation(organisation_id):
    
    rel_orgs = RegOrgUnit.objects.filter(parent_org_unit_id=organisation_id).values_list('pk', flat=True)
    if not rel_orgs:
        return [organisation_id]
    tmp_orgs = list(rel_orgs)
    tmp_orgs.append(organisation_id)
    return tmp_orgs

def get_user_perm_org_area(user):
    geo_org_perms = OVCUserRoleGeoOrg.objects.filter(user=user)
    
    perm_org_area = []
    for geo_org_perm in geo_org_perms:
        perm_org_area = perm_org_area + [(perm, geo_org_perm.org_unit_id, geo_org_perm.area_id) for perm in geo_org_perm.group.permissions.all()]
    
    return perm_org_area

def get_user_perm_rel_org(user):
    geo_org_perms = OVCUserRoleGeoOrg.objects.filter(user=user)
    perm_rel_orgs = []
    for geo_org_perm in geo_org_perms:
        for perm in geo_org_perm.group.permissions.all():
            rel_orgs = []
            rel_orgs.append(geo_org_perm.org_unit_id)
            for org_unit in RegOrgUnit.objects.filter(parent_org_unit_id=geo_org_perm.org_unit_id):
                rel_orgs.append(org_unit.pk)
                
            perm_rel_orgs.appen((perm, rel_orgs))

    return perm_rel_orgs

def update_object(object_to_update, objects_with_updates, fields_to_update):
    withupdates = objects_with_updates.__dict__
    #print withupdates, 'objects with updates to make'
    import copy
    tmp = copy.copy(withupdates)
    
    for field in tmp:
        #print field, 'fields in fields'
        if field not in fields_to_update:
            del withupdates[field]
    
    object_to_update.__dict__.update(withupdates)
    
    return object_to_update
    
def fields_to_show(user, permission_field_dic, organisation_id=None, area_id=None, owner_id = None):
    userpermissions = user.get_all_permissions()
    permissions_to_return = []
    for permission in userpermissions:
        if permission in permission_field_dic:
            permissions_to_return = permissions_to_return + permission_field_dic[permission]
    return list(set(permissions_to_return))

def filter_fields_for_display(user, permission_field_dic, organisation_id=None, area_id=None, owner_id=None):
    permissions_to_return = []
    try:
        userpermissions = user.get_all_permissions()
        perm_org_area = get_user_perm_org_area(user)
        
        if user.is_superuser:
            for perm_type in permission_field_dic:
                for tmp_perms in permission_field_dic[perm_type]:
                    permissions_to_return = permissions_to_return + permission_field_dic[perm_type][tmp_perms]
            return list(set(permissions_to_return))

        if not str(area_id).isdigit() or area_id is None:
            area_id = None
        else:
            area_id = int(area_id)
        for perm, org_id, geo_id in perm_org_area:
            tmp_permission = '%s.%s' % (perm.content_type.app_label, perm.name)

            if 'geo' in permission_field_dic and tmp_permission in permission_field_dic['geo'] and geo_id == area_id:
                #print 'geo permissions'
                permissions_to_return = permissions_to_return + permission_field_dic['geo'][tmp_permission]
                
            if 'org' in permission_field_dic and tmp_permission in permission_field_dic['org'] and org_id == organisation_id:
                #print 'org permissions'
                permissions_to_return = permissions_to_return + permission_field_dic['org'][tmp_permission]
            
            if 'rel_org' in permission_field_dic and \
                tmp_permission in permission_field_dic['rel_org'] and \
                organisation_id and org_id and int(organisation_id) in get_related_organisation(org_id):
                permissions_to_return = permissions_to_return + permission_field_dic['rel_org'][tmp_permission]
                
            
        
        for permission in userpermissions:
            if 'other' in permission_field_dic and permission in permission_field_dic['other']:
                #print 'we are not suppose to be here'
                permissions_to_return = permissions_to_return + permission_field_dic['other'][permission]
            elif 'own' in permission_field_dic and permission in permission_field_dic['own'] and str(owner_id).isdigit() and user.reg_person.pk == int(owner_id):
                #print 'own permissions'
                permissions_to_return = permissions_to_return + permission_field_dic['own'][permission]
    except Exception as e:
        traceback.print_exc()
        raise Exception("filtering fields for access control failed")
    return list(set(permissions_to_return))

def del_form_field(formlayout, searchitems):
    tmp_formlayout = formlayout[:] #make copy by value
    
    for obj in tmp_formlayout:
        
               #can't on the same list and make changes to it, causes silly gotchas
        myobjindex = formlayout.index(obj)
        obj = formlayout[myobjindex]
        
        if isinstance(obj, MultiField):
            del_form_field(obj, searchitems)
            continue
         
        if isinstance(obj, Field):
            #print obj.fields
            #print obj.__dict__
            #print dir(obj), 'functions of object'
            #print type(obj), 'this is the type'
            for attr in obj.attrs:
                #print attr
                if attr and 'mandatory' in obj.attrs[attr]:
                    continue
            for field in obj.fields:
                if isinstance(field, str):
                    if field not in searchitems:
                        i = formlayout.index(obj)
                        popped =  formlayout.pop(i)
                else:
                    del_form_field(field,searchitems)
            continue
            

        
        elif isinstance(obj, Div):
            if (obj.css_class and 'panel-heading' in obj.css_class) or (obj.css_class and 'mandatory' in obj.css_class):
                if 'deepsearch' in obj.css_class:
                    del_form_field(obj, searchitems)
                #print obj, 'panel heading'
                continue
            del_form_field(obj, searchitems)
            
        elif isinstance(obj, AppendedText):
            field = obj.field
            if field not in searchitems:
                i  = formlayout.index(obj)
                popped = formlayout.pop(i)
                #print popped, 'this was popped--==========='
            continue
        
        elif isinstance(obj, HTML):
            found=False
            if obj.html.strip() == '<p/>' or 'mandatory' in obj.html.strip():
                continue
            for item in searchitems:
                if item in obj.html:
                    found = True
                    break
            if not found:
                i = formlayout.index(obj)
                popped = formlayout.pop(i)
                #print popped.html, 'this was popped--==========='
            continue
                
        elif isinstance(obj, str):
            if obj not in searchitems:
                i = formlayout.index(obj)
                popped =  formlayout.pop(i)
                #print popped, 'this was popped--==========='
            continue
        
def custom_form_is_valid(form_errors_items, forms_fields_to_consider):
    #print forms_fields_to_consider, 'thes we are considering'
    for field, value in form_errors_items:
        if field in forms_fields_to_consider:
            return False
    return True

def get_user_org_ids_by_role(user, rolename):
    user_role_geo_org = user_manager.get_user_role_geo_org(user)
    org_ids = [role_geo_org.org_unit.pk for role_geo_org in user_role_geo_org \
            if role_geo_org.group.group_name == rolename ]

    return org_ids
def reg_assistant_orgs(user, subunit_limit=1):
    org_ids = get_user_org_ids_by_role(user, 'Registration assistant')
    org_and_sub_units = get_all_org_subunits_and_self(org_ids, subunit_limit)

    return get_list_of_organisations_by_ids(org_and_sub_units, user)

