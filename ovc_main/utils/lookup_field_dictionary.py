#workforce
workforce_type_cat = 'Person type (workforce member)'
workforce_type = 'Person type (user/workforce member)'
workforce_type_ngo_id = 'TWNE'
workforce_type_gov_id = 'TWGE'
workforce_type_vol_id = 'TWVL'

steps_ovc_caregiver = 'ISOV'
govt_man_number  = 'IMAN'
police_sign_number = 'IPCS'
teacher_service_id = 'IDTS'
empty_workforce_id = 'N/A'

#general
sex = 'Sex'
gen_YesNo = 'General YesNo'
dist = 'district'
ward = 'ward'

#organisation
org_unit_type = 'Organisational unit type'
org_unit_type_res_Institution = 'TNRS'
gdclsu_text = 'TNCW'#CWAC

contact_phone_number = 'CPHL'
contact_mobile_phone = 'CPHM'
contact_designated_mobile_phone = 'CPHD'
contact_email_address = 'CEMA'
contact_post_address = 'CPOA'
contact_physical_address = 'CPHA'
contact_landline_phone = 'CPHL'

geo_type_district = 'district'
geo_type_ward = 'ward'
geo_type_district_code = 'GDIS'
geo_type_province_code = 'GPRV'
geo_type_constituency_code = 'GCON'
geo_type_ward_code = 'GWRD'


geo_type_community = 'community'
geo_type_residential_institution = 'residential'

#beneficiary
adverse_condition = 'Adverse condition'
child_beneficiary_ovc = 'TBVC'
child_guardian_no_guardian = 'noguardian'
child_guardian_guardian = 'TBGR'
child_guardian_notes_relationship = 'notesrelationship'
beneficiary_category = 'Person type (beneficiary)'

paper_trail_wfc = ''

#general
web_interface_id = 'INTW'
sms_interface_id = 'INTS'
capture_interface_id = 'INTC'

register_org_unit = 'REGU'
register_user = 'REGS'
register_workforce_member = 'REGW'

update_beneficiary = 'UPDB'
update_org_unit = 'UPDU'
update_user = 'UPDS'
update_workforce_member = 'UPDW'

register_beneficiary = 'REGB'
update_beneficiary = 'UPDB'

org_type_community_based_org = 'TNCB'
org_type_community_level_committee = 'TNCM'
org_type_cwac = 'TNCW'
org_type_district_level_committee = 'TNCD'
org_type_district_office_gov = 'TNGD'
org_type_national_level_gov = 'TNGN'
org_type_ngo_private_national_hq = 'TNNH'
org_type_ngo_private_single_district ='TNND'
org_type_ngo_private_multiple_district = 'TNNM'
org_type_provincial_level_committee = 'TNCP'
org_type_provincial_office_gov ='TNGP'
org_type_residential_children = 'TNRS'
org_type_sub_district_gov ='TNGS'

legal_org_registration_type_ngo_number = 'INGO'

ANSWER_TYPE_MULTI_SELECT = 'ATMS'
ANSWER_TYPE_NUMERIC = 'ATNM'
ANSWER_TYPE_TEXT = 'ATTX'
ANSWER_TYPE_SINGLE_SELECT = 'ATSS'
ANSWER_TYPE_DECIMAL = 'ATDC'
ANSWER_TYPE_DATE = 'ATDT'

#CSI Domains
CSI_DOMAIN_FOOD_NUTRITION = ''

#Residential institution
category_family_status = 'Family status'
category_residential_status = 'Residential status'
category_role_in_residential = 'Position in residential institution'
category_employment_status = 'Employment status'
frm_res_inst_report_by_type_institution_option_id = 41
frm_res_inst_report_by_type_ext_inspector_option_id = 42

#Form types
form_type_residential_institution = 'FT1f'
form_type_workforce_training = 'FT2d'
form_type_public_sensitization = 'FT1e'

REFERRAL_MADE_CATEGORY_DB_TEXT = 'Referral made'
REFERRAL_COMPLETED_CATEGORY_DB_TEXT = 'Referral completed'
SERVICE_TYPE_CATEGORY_DB_TEXT = 'Service type'
ENCOUNTER_TYPE_CATEGORY_DB_TEXT = 'Encounter type'

CUSTOM_MODEL_TEXT = 'custom'

ENCOUNTER_TYPE_ID_SOCIAL = 'EINT'
ENCOUNTER_TYPE_WHO_WAS_INTERVIEWED = 'NTWI'
ENCOUNTER_NOTES = 'NTNT'
ENCOUNTER_RECOMMENDATIONS = 'NTRC'

ENCOUNTER_TYPE_JM = 'NTJM'
ENCOUNTER_TYPE_IO = 'NTIO'

ENCOUNTER_TYPE_COURT_HIGH = 'ECTH'
ENCOUNTER_TYPE_COURT_SUB = 'ECTS'
