from django.db import models
from ovc_main.models import RegOrgUnit, RegPerson

class RegOrgUnitsAuditTrail(models.Model):
    from ovc_auth.models import AppUser
    transaction_id = models.AutoField(primary_key=True)
    org_unit = models.ForeignKey(RegOrgUnit)
    transaction_type_id = models.CharField(max_length=4, null=True, db_index=True)
    interface_id = models.CharField(max_length=4, null=True, db_index=True)
    timestamp_modified = models.DateTimeField(auto_now=True)
    app_user = models.ForeignKey(AppUser, db_column = 'user_id_modified')

    class Meta:
        db_table = 'tbl_reg_org_units_audit_trail'
        app_label = 'ovc_main'


class RegPersonsAuditTrail(models.Model):
    from ovc_auth.models import AppUser
    transaction_id = models.AutoField(primary_key=True)
    person = models.ForeignKey(RegPerson)
    transaction_type_id = models.CharField(max_length=4, null=True, db_index=True)
    interface_id = models.CharField(max_length=4, null=True, db_index=True)
    date_recorded_paper = models.DateField(null=True)
    person_id_recorded_paper = models.IntegerField(max_length=4, null=True)
    timestamp_modified  = models.DateTimeField(auto_now=True)
    app_user = models.ForeignKey(AppUser, db_column = 'user_id_modified')

    class Meta:
        db_table = 'tbl_reg_persons_audit_trail'
        app_label = 'ovc_main'