   
'''
Created on Sep 18, 2014

@author: GLyoko
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText, InlineRadios
from ovc_main.utils import fields_list_provider as workforceFields, validators as validate_helper
from ovc_main.utils import lookup_field_dictionary as fdict
from functools import partial
from models import SetupGeorgraphy, RegPerson, RegPersonsTypes, RegPersonsOrgUnits, RegPersonsGeo, RegPersonsGdclsu, RegOrgUnit, RegPersonsContact
import workforceforms_labels as form_label
import json
#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})
from ovc_main.utils.auth_access_control_util import del_form_field, fields_to_show,filter_fields_for_display
#from workforce_registration import get_workforce_permission_fields
from django.conf import settings
import traceback


roles_and_fields_to_show = {}
fields_by_perms = {'rel_org':
                        {        
                        'auth.update wkf general rel':['workforce_type', 'man_number', 'ts_number', 'sign_number','steps_ovc_number','direct_services','org_unit','primary_parent_org_unit',
                                                    'workforce_type_change_date','parent_org_change_date','work_locations_change_date','nrc','date_of_death','edit_mode_hidden','workforce_id', 'districts', 'wards', 'communities', 'wfc_system_id', 'org_data_hidden'],
                        'auth.update wkf contact rel':['designated_phone_number', 'other_mobile_number', 'email_address', 'physical_address','edit_mode_hidden','workforce_id', 'wfc_system_id'],
                        },
                
                    'own':
                        {
                        'auth.update wkf contact own':['designated_phone_number', 'other_mobile_number', 'email_address', 'physical_address','edit_mode_hidden','workforce_id', 'wfc_system_id']
                        },
                    'other':
                        {
                        'auth.update wkf special':['first_name', 'last_name', 'other_names', 'sex', 'date_of_birth','nrc','edit_mode_hidden','workforce_id', 'wfc_system_id'],
                        'auth.update wkf contact':['designated_phone_number', 'other_mobile_number', 'email_address', 'physical_address','edit_mode_hidden','workforce_id', 'wfc_system_id'],
                        'auth.update wkf general':['workforce_type', 'man_number', 'ts_number', 'sign_number','steps_ovc_number','direct_services','org_unit','primary_parent_org_unit','workforce_type_change_date',
                                                    'parent_org_change_date','work_locations_change_date','nrc','date_of_death','edit_mode_hidden','workforce_id', 'districts', 'wards', 'communities', 'wfc_system_id', 'org_data_hidden']
                        }
                    }


class WorkforceRegistrationForm(forms.Form):
    is_update_page = False
    wfc_id = ''
    workforce_id = forms.CharField(label=form_label.label_workforce_id, required=False, initial = wfc_id, widget=forms.HiddenInput())
    nrc = forms.RegexField(regex=r'^\b\d{6}\/\b\d{2}\/\b[1-3]{1}$',error_message = (form_label.error_message_nrc),label=form_label.label_nrc, required=False)
    wfc_system_id =  forms.CharField(label=form_label.label_person_id, required=False, widget=forms.HiddenInput())
    #table table-hover table-condensed
    
    first_name = forms.CharField(label=form_label.label_first_name)
    last_name = forms.CharField(label=form_label.label_last_name)
    other_names = forms.CharField(label=form_label.label_other_names, required=False)
    sex = forms.ChoiceField(label=form_label.label_sex,choices=workforceFields.get_sex_list())
    date_of_birth = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl,validators=[validate_helper.validate_date_of_birth])#,input_formats=validate_helper.date_input_formats)
        
    workforce_type = forms.ChoiceField(label=form_label.label_workforce_member_type,choices=workforceFields.get_Workforce_Type())
    man_number = forms.CharField(label=form_label.label_man_number, required=False)
    ts_number = forms.CharField(label=form_label.label_ts_number, required=False)
    sign_number = forms.CharField(label=form_label.label_police_sign_number, required=False)
    direct_services = forms.ChoiceField(widget=forms.RadioSelect, choices=workforceFields.get_YesNo_Enums(),label=form_label.label_services_to_children)#(label='Do you provide services directly to children?: ',wid)

    org_unit = forms.CharField(required=False, label=form_label.label_org_unit)
    primary_parent_org_unit = forms.ChoiceField(required=False, widget=forms.RadioSelect, choices=workforceFields.get_YesNo_Enums(),label=form_label.label_parent_unit)
    workforce_reg_assistant = forms.ChoiceField(required=False, widget=forms.RadioSelect, choices=workforceFields.get_YesNo_Enums(),label=form_label.label_registration_assistant)
    
    designated_phone_number = forms.RegexField(required=False,label=form_label.label_designated_mobile_number, regex=validate_helper.mobile_phone_number_regex, error_message = (form_label.error_message_phone_number))
    other_mobile_number = forms.RegexField(required=False,label=form_label.label_other_mobile_number,regex=validate_helper.mobile_phone_number_regex, error_message = (form_label.error_message_phone_number))
    email_address = forms.EmailField(required=False,label=form_label.label_email_address)
    physical_address = forms.CharField(required=False,widget=forms.Textarea(attrs={'rows': 4}), label=form_label.label_physical_address)

    org_data_hidden = forms.CharField(label='',required=True)
    steps_ovc_number = forms.CharField(label = form_label.label_steps_number, required=False)

    workforce_type_change_date = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, required=False,label='')#,input_formats=validate_helper.date_input_formats)
    parent_org_change_date = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, required=False,label = '')#,input_formats=validate_helper.date_input_formats)
    work_locations_change_date = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, required=False, label = '')#,input_formats=validate_helper.date_input_formats)
    date_of_death = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, required=False,validators=[validate_helper.deny_future_dates])#,input_formats=validate_helper.date_input_formats)
    
    edit_mode_hidden = forms.CharField(required=False)
            
    def __init__(self, user, *args, **kwargs):
        self.user = user
        is_update_page = False
        do_display_status = 'display:None'
        hide_status = 'text'
        unhide_status = 'visibility:hidden'
        dont_display_status = ''
        show_direct_services = ''
        actionurl = '/workforce/new_workforce/'
        header_text = """<div class="note note-info"><h4><strong>Register New Workforce Member ("""+settings.APP_NAME+""" form 2a) / Register user</strong>
                        </h4><h5><p>This form is used to register workforce members who provide services directly to vulnerable children, as wells as other users of this system</h5></div>"""
        submitButtonText = form_label.label_button_save    
        
        
        _districts_options_list = ''
        self.districts = []
        self.wards = []
        self.communities = []

        if args and 'districts' in args[0]:
            self.districts = get_list_from_dic_or_querydict(args[0], 'districts')
            self.districts = convert_to_int_array(self.districts)
            _districts_options_list = workforceFields.geo_control_select_list(fdict.geo_type_district_code, self.districts, setdisabled=False)
        else:
            _districts_options_list = workforceFields.geo_control_select_list(fdict.geo_type_district_code)   
        
        _ward_option_list = ''
        if args and 'wards' in args[0]:
            self.wards = get_list_from_dic_or_querydict(args[0], 'wards')
            self.wards = convert_to_int_array(self.wards)
            if self.districts:
                _ward_option_list = workforceFields.geo_control_select_list(fdict.geo_type_ward_code, self.wards, grandparentfilter=self.districts, setdisabled=False) 
        elif self.districts:
            _ward_option_list = workforceFields.geo_control_select_list(fdict.geo_type_ward_code, grandparentfilter=self.districts)  
        
        #print wards, '1in communities wards to print'
        _community_option_list = ''
        if args and 'communities' in args[0]:
            self.communities = get_list_from_dic_or_querydict(args[0], 'communities')
            self.communities = convert_to_int_array(self.communities)
            if self.communities:
                _community_option_list = workforceFields.community_control_populate(self.communities, self.wards)
            else:
                _community_option_list = workforceFields.community_control_populate(parent_filter=self.wards)
        elif self.wards:
            _community_option_list = workforceFields.community_control_populate(parent_filter=self.wards)
        
        self.failed_save = False
        self.national_id = None
        self.workforce_related_to_user = False
        self.workforce_is_logged_in_user = False
        if len(args) > 0:    
            if 'nrc' in args[0]:            
                self.national_id = args[0]['nrc']
            if 'is_related_to_user' in args[0]:            
                self.workforce_related_to_user = args[0]['is_related_to_user']
            if 'is_logged_in_user' in args[0]:            
                self.workforce_is_logged_in_user = args[0]['is_logged_in_user']
            
        wfc_system_id=None
        if len(args) > 0 and 'wfc_system_id' in args[0]:
            wfc_system_id = args[0]['wfc_system_id']
        primary_org_id=None
        if len(args) > 0 and 'primary_org_id' in args[0]:
            primary_org_id = args[0]['primary_org_id']
        
        is_from_submit=None
        if len(args) > 0 and 'submit' in args[0]:
            is_from_submit = args[0]['submit']
            
        if 'workforce_id' in args:
            wfc_id = args.pop('workforce_id')
        self.geo_control_mandatory = ''
        if 'is_update_page' in kwargs:
            self.is_update_page = kwargs.pop('is_update_page')
            if self.is_update_page:
                do_display_status = ''
                dont_display_status = 'display:None'
                hide_status = 'visibility:hidden'
                unhide_status = ''
                header_text = """<div class="note note-info"><h4><strong>File update workforce member Update("""+settings.APP_NAME+""" form 2a) / Update user</strong></h4><h5><p>This form is used to update a workforce member</h5></div>"""
                actionurl = '/workforce/update_wfc/'
                submitButtonText = form_label.label_button_update
                self.edit_mode_hidden = forms.CharField()
                direct_services = '0'
                if 'direct_services' in args[0]:
                    direct_services = args[0]['direct_services']
                    if direct_services == '1':
                        show_direct_services = 'display:None'

        super(WorkforceRegistrationForm, self).__init__(*args, **kwargs)
                        
        #self.fields['date_of_birth'].widget.attrs['readonly'] = True
        #self.fields['date_of_death'].widget.attrs['readonly'] = True
        
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = actionurl
        self.helper.layout = Layout(
            MultiField(
                        '',
                        Div(
                            Div(HTML("""
                                        <div class="panel-heading">"""+form_label.label_workforce_summary+"""</div>
                                        <div class="panel-body pan mandatory" ><p/> 
                                                <div class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-5'>
                                                      <dt>"""+form_label.label_workforce_id+"""</dt>
                                                      <dd>{{workforce.workforce_id}}</dd>
                                                     </dl>
                                                     <dl class='dl-horizontal col-md-7'>
                                                      <dt>"""+form_label.label_workforce_name+"""</dt>
                                                      <dd>{{workforce.name}}</dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-5'>
                                                      <dt>"""+form_label.label_workforce_type+"""</dt>
                                                      <dd>{{workforce.person_type}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-7'>
                                                      <dt>"""+form_label.label_parent_unit+"""</dt>
                                                      <dd>{{workforce.primary_org_unit_name}}</dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-5'>
                                                      <dt>"""+form_label.label_sex+"""</dt>
                                                      <dd>{{workforce.sex}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-7'>
                                                      <dt>"""+form_label.label_date_of_birth+"""</dt>
                                                      <dd>{{workforce.date_of_birth}}</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                        """
                                         ),
                                    css_class='panel panel-primary mandatory',
                                    style='%s' % do_display_status
                                ),
                            
                                Div(
                                    HTML('<p/>'),
                                    'workforce_id',
                                    Field(Div('wfc_system_id', style='display:inline'), css_class='form-inline mandatory'),
                                    'nrc',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                                Div(HTML('<input id="id_edit_mode" class="mandatory" type="radio" name="edit_mode" value="1" {{select_fields_radio}}>'+form_label.label_option_update_workforce_user_details 
                                    ),
                                    css_class = 'panel-heading clearfix'
                                    ),
                                css_class='panel panel-primary',
                                style = do_display_status,
                            ),
                        Div(
                                Div(HTML(form_label.label_section_personal_details),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'first_name',
                                    'last_name',
                                    AppendedText('other_names','<span data-toggle="tooltip" title="'+form_label.label_tooltip_other_names+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'sex',
                                    AppendedText('date_of_birth','<span data-toggle="tooltip" title="'+form_label.label_tooltip_date_of_birth+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                                Div(
                                    Div(
                                        HTML('<span class="mandatory col-md-5">'+form_label.label_section_workforce_type+'</span>'),
                                    ),
                                    Div(
                                        Field('workforce_type_change_date', template='ovc_main/custom_workforce_type_date.html', style = do_display_status)
                                    ),                                            
                                    css_class = 'panel-heading deepsearch col-md-12'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    'workforce_type',
                                    'man_number',
                                    'ts_number',
                                    'sign_number',
                                    'steps_ovc_number',
                                    Field(Div(InlineRadios('direct_services'), style=show_direct_services), css_class='form-inline'),
                                    css_class = 'panel-body pan col-md-12'
                                    ),
                                css_class='panel panel-primary',
                        
                            ),
                        Div(
                                Div(
                                    Div(
                                        HTML('<span class="mandatory col-md-5">'+form_label.label_section_parent_org_units+'</span>'),
                                    ),
                                    Div(
                                        Field('parent_org_change_date', template='ovc_main/custom_workforce_org_date.html', style = do_display_status)
                                    ),                                            
                                    css_class = 'panel-heading deepsearch col-md-12'
                                    ),
                                    
                                 
                                Div(
                                    HTML('<p/>'),
                                    Field('org_unit', style="display:inline", css_class="autocompleteovc form-inline", id="id_org_unit"),
                                    InlineRadios('primary_parent_org_unit'),
                                    Field(Div(InlineRadios('workforce_reg_assistant'), style=dont_display_status), css_class='form-inline'),
                                    Field(Div('org_data_hidden', style='display:None'), css_class='form-inline mandatory'),                                    
                                    HTML("<label id=\"valid_org_unit\" style=\"color:red;margin-left:50px;display:None\"><strong>  </strong></label>"),
                                    css_class = 'panel-body pan  col-md-12'
                                    ),
                                Div(
                                    HTML("<br><a type=\"button\" onclick=\"addOrgUnitData()\" class=\"btn btn-info org_data_hidden\"><i class=\"fa fa-plus\"></i>"+form_label.label_button_add+"</a><br><br>"),
                                    style='margin-left:41%',
                                    ),
                                Div(
                                    HTML("""
                                    
                                    <table id="orgTable"  class="table table-hover table-condensed">
                                        <thead>
                                        <tr>
                                            <th style="width:1%"></th>
                                            <th>"""+form_label.label_table_heading_org_unit+"""</th>
                                            <th>"""+form_label.label_table_heading_org_unit_id+"""</th>
                                            <th>"""+form_label.label_table_heading_primary_parent_org_unit+"""</th>
                                            <th  id = "id_reg_assistant_col" style=\""""+hide_status+"""\">"""+form_label.label_table_heading_registration_assistant+"""</th>
                                            <th>"""+form_label.label_button_remove+"""</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    
                                    <input id="org_unit_id_hidden" class="org_data_hidden" type="hidden">
                                    <input id="org_unit_name_hidden" class="org_data_hidden" type="hidden">  
                                    <input id="eligible_primary_parent_hidden" class="org_data_hidden" type="hidden"> 
                                                                 
                                    <!-- Button trigger modal -->
                                    <button id="confirm_dialog_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                      Launch demo modal
                                    </button>
                                    
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Duplicate Person</h4>
                                          </div>
                                          <div class="modal-body">
                                            """+form_label.warning_message_duplicate_dialog+"""
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button class="btn btn-success" id="submit">Save anyway</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>                                    
                                    
                                    <!-- Button trigger modal -->
                                    <button id="duplicate_dialog_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#dupModal">
                                      Launch demo modal
                                    </button>
                                    
                                    <!-- Modal -->
                                    <div class="modal fade" id="dupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Duplicate Person</h4>
                                          </div>
                                          <div class="modal-body">
                                            """+form_label.warning_message_deactivation_dialog+""" 
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            <button class="btn btn-success" id="submit">Yes</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                                    <!-- Button trigger modal -->
                                    <button id="enter_date_of_death_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#dateOfDeathModal">
                                      Launch demo modal
                                    </button>
                                    
                                    <!-- Modal -->
                                    <div class="modal fade" id="dateOfDeathModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Enter date of death</h4>
                                          </div>
                                          <div class="modal-body">
                                            """+form_label.warning_message_blank_date_of_death_dialog+""" 
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                                    
                                    <script>
                                        //$( "#id_date_of_birth" ).datepicker( "option", "dateFormat", 'd MM, yy' );
                                    
                                        function addOrgUnitData() {
                                            //http://stackoverflow.com/questions/171027/add-table-row-in-jquery                        
                                            var primary_index = $('input:radio[name=primary_parent_org_unit]:checked').val();
                                            var primary_org_unit_index = primary_index;

                                            if(!primaryOrgCountOk(primary_index)){
                                                return;
                                            }

                                            if(!validateOrgUnitInput()){
                                                return;
                                            }
                                            
                                            if(!validPrimaryParentSelected(primary_index)){
                                                return;
                                            }
                                            
                                            if($("#org_unit_id_hidden").val()==''){
                                                $("#valid_org_unit").text("");
                                                $("#valid_org_unit").css('display','None');
                                                $("#valid_org_unit").text('"""+form_label.error_message_invalid_org_unit+"""');
                                                $("#valid_org_unit").show();
                                                return false;
                                            }

                                            var primary_org_unit = '';
                                            $("#valid_org_unit").text("");
                                            $("#valid_org_unit").css('display','None');
                                            switch(primary_org_unit_index){
                                                case '1':
                                                    primary_org_unit = 'Yes';
                                                    break;
                                                case '2':
                                                    primary_org_unit = 'No';
                                                    break;
                                                default:
                                                    $("#valid_org_unit").text('"""+form_label.error_message_primary_parent_option+"""');
                                                    $("#valid_org_unit").show();
                                                    return;
                                            }

                                            var is_workforce_reg_assistant_index = $('input:radio[name=workforce_reg_assistant]:checked').val();
                                            var is_workforce_reg_assistant = '';
                                            switch(is_workforce_reg_assistant_index){
                                                case '1':
                                                    is_workforce_reg_assistant = 'Yes';
                                                    break;
                                                case '2':
                                                    is_workforce_reg_assistant = 'No';
                                                    break;
                                            }

                                            $("#orgTable").find('tbody')
                                                .append($('<tr>')
                                                    .append($('<td id="0">')
                                                        .append($('<label>')
                                                            .text('')
                                                            .css('visibility','hidden')
                                                            .css('width','1%')
                                                        )
                                                    )
                                                    .append($('<td id="org_unit_name">')
                                                        .append($('<label>')
                                                            .text($("#org_unit_name_hidden").val())
                                                        )
                                                    )
                                                    .append($('<td id="org_unit_id">')
                                                        .append($('<label>')
                                                            .text($("#org_unit_id_hidden").val())
                                                        )
                                                    )
                                                    .append($('<td id="primary_org">')
                                                        .append($('<label>')
                                                            .text(primary_org_unit)
                                                        )
                                                    )
                                                    .append($('<td id="reg_assistant" style=\""""+hide_status+"""\">')
                                                        .append($('<label>')
                                                            .text(is_workforce_reg_assistant)
                                                        )
                                                    )
                                                    .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                                );

                                                updateTableData();
                                                clearForm();
                                                $( "#primary_parent_org_unit" ).prop( "checked", false );
                                        }
                                        
                                        function validPrimaryParentSelected(primary_index){
                                            var toReturn = true;
                                            $("#valid_org_unit").text("");
                                            $("#valid_org_unit").css('display','None');
                                            if($("#eligible_primary_parent_hidden").val() == 'false' && primary_index == 1){                                                
                                                $("#valid_org_unit").text('"""+form_label.error_message_no_reg_perm_for_primary_parent+"""');
                                                $("#valid_org_unit").show();
                                                toReturn = false;
                                            }
                                            return toReturn;
                                        }
                                        
					                function primaryOrgCountOk(primary_org_selected){
                                            var count = 0;
                                            var toReturn = true;
                                            $("#valid_org_unit").text("");
                                            $("#valid_org_unit").css('display','None');
                                            $("#orgTable tr").each(function(i, v){
                                                $(this).children('td').each(function(ii, vv){
                                                    if($(this).attr("id") == 'primary_org'){
                                                        if($(this).text() == 'Yes'){
                                                            count++;
                                                        }
                                                        if(count>=1 && primary_org_selected == 1){//Yes previously selected
                                                            $("#valid_org_unit").text('"""+form_label.error_message_only_one_primary_parent+"""');
                                                            $("#valid_org_unit").show();
                                                            toReturn = false;
                                                        }
                                                    }
                                                    if($(this).attr("id") == 'org_unit_id'){
                                                        if($(this).text() == $("#org_unit_id_hidden").val()){
                                                            $("#valid_org_unit").text('"""+form_label.error_message_duplicate_org_unit+"""');
                                                            $("#valid_org_unit").show();
                                                            toReturn = false;
                                                        } 
                                                    }													
                                                });
                                            });
                                            return toReturn;
                                        }
                                        
                                        function validateOrgUnitFormat(orgunit) { 
                                          // http://stackoverflow.com/a/46181/11236
                                            var re = /^U\d{6}\t.*$/;
                                            return re.test(orgunit);
                                        }

                                        function validateOrgUnitInput(){
                                            $("#valid_org_unit").text("");
                                            $("#valid_org_unit").css('display','None');
                                            var org_unit_text = $("#id_org_unit").val();
                                            if (!validateOrgUnitFormat(org_unit_text)){
                                                $("#valid_org_unit").text('"""+form_label.error_message_invalid_org_unit+"""');
                                                $("#valid_org_unit").show();
                                                return false;
                                            } 
                                            return true;
                                        }
                                        
                                        
	                                function updateTableData() {
	                                    if($( "#id_org_data_hidden" ).text() != '')
	                                    {
		                                    return;
	                                    }
	                                    var data = {};
	                                    var sindex = 1;
	                                    $("#orgTable tr").each(function (i, v) {
		                                    if (i < sindex) {
			                                    return;
		                                    }
		                                    data[i] = {};
		                                    $(this).children('td').each(function (ii, vv) {
			                                    data[i][this.id] = $(this).text();
		                                    });
	                                    });
	                                    var org_data = JSON.stringify(data);
	                                    $( "#id_org_data_hidden" ).val(org_data);
                                    }
                                        
                                        function clearForm(){
                                            $('input[name="org_unit"]').val('');
                                            $('input[id="org_unit_id_hidden"]').val('');
                                            $('input[id="org_unit_name_hidden"]').val('');
                                            $('input[id="eligible_primary_parent_hidden"]').val('');
                                            //http://fronteed.com/iCheck/ so cool :)
                                            $( 'input[id="id_primary_parent_org_unit_1"]').iCheck('uncheck');
                                            $( 'input[id="id_workforce_reg_assistant_1"]').iCheck('uncheck');
                                            $( 'input[id="id_primary_parent_org_unit_2"]').iCheck('uncheck');
                                            $( 'input[id="id_workforce_reg_assistant_2"]').iCheck('uncheck');                                                       
                                        }   
                                        
                                        function myDeleteFunction(){
                                            $(document).on('click', 'button.removebutton', function () { 
                                               $(this).closest('tr').remove();
                                               updateTableData();
                                               return false;
                                           });
                                        }
                                        
                                        function updateFinale(e){
                                            updateTableData();
                                            checkUpdateMode(e);
                                        }
                                        
                                        function checkUpdateMode(e){
                                            var edit_mode = $('input:radio[name=edit_mode]:checked').val();
                                            $( "#id_edit_mode_hidden" ).val(edit_mode);  
                                            
                                            // Check if the user is being voided
                                            if (edit_mode == 3)  
                                            {
                                                e.preventDefault();
                                                $("#duplicate_dialog_box").click();
                                            }
                                            if (edit_mode == 2)  
                                            {
                                                if($('#id_date_of_death').val() == ''){
                                                    e.preventDefault();
                                                    $("#enter_date_of_death_box").click();
                                                }
                                            }
                                            else
                                            {
                                                var person = checkNames();
                                                if (person != undefined)
                                                {
                                                    e.preventDefault();
                                                    $("#confirm_dialog_box").click();
                                                }
                                            }
                                            
                                        }
                                        
                                        function checkNames(){
                                            var tmpperson;
                                            $.ajax({
                                                    dataType: "json",
                                                    url: "/workforce/check_names?first_name=" + $( "#id_first_name" ).val() + "&last_name=" + $( "#id_last_name" ).val() + "&pk_id=" + $( "#id_wfc_system_id" ).val(),
                                                    contentType: "application/json; charset=utf-8",
                                                    async: false,
                                                    success: function (person) {
                                                    	tmpperson = person;
                                                    }
                                                });
                                            return tmpperson;
                                        }
                                        
                                    </script>
                                                                        
                                    """),
                                    css_class = 'panel-body '
                                    ),
                                css_class='panel panel-primary',
                        
                            ),
                        Div(
                        
                                Div(
                                    Div(
                                        HTML('<span class="mandatory col-md-5">'+form_label.label_section_where_work+'</span>'),
                                    ),
                                    Div(
                                        Field('work_locations_change_date', template='ovc_main/custom_workforce_locations_date.html', style = do_display_status)
                                    ),                                            
                                    css_class = 'panel-heading deepsearch col-md-12'
                                ),
                                Div(
                                    HTML('<p/>'),
                                    Div(
                                        #HTML('<p/>'),
                                        HTML("""
                                             
                                             <label class="control-label col-md-3 """+self.geo_control_mandatory+"""">"""+form_label.label_districts+"""</label>
                                             <div class="col-md-6 """+self.geo_control_mandatory+"""" id="multipledistricts">
                                                 <select id="districts" name="districts" multiple="multiple" ismultiselect="True">"""+
                                                    _districts_options_list+"""
                                                </select>
                                            </div>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered',
                                        css_id = 'workforce_district_location'
                                        ),
                                     Div(
                                        HTML("""
                                             
                                             <label class="control-label col-md-3 """+self.geo_control_mandatory+"""">"""+form_label.label_wards+"""</label>
                                             <div class="col-md-6 """+self.geo_control_mandatory+"""">
                                                 <select id="wards" multiple="multiple" name="wards" ismultiselect="True">
                                                    """+_ward_option_list+"""
                                                </select>
                                            </div>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered',
                                        css_id = 'workforce_ward_location'
                                        ),
                                    Div(
                                        HTML("""
                                             
                                             <label class="control-label col-md-3 """+self.geo_control_mandatory+"""">"""+form_label.label_communities+"""</label>
                                             <div class="col-md-6 """+self.geo_control_mandatory+"""">
                                                 <select id="communities" multiple="multiple" name="communities">
                                                    """+_community_option_list+"""
                                                </select>
                                            </div>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered',
                                        css_id = 'workforce_community_location'
                                        ),
                                    css_class = 'panel-body col-md-27'
                                    ),
                                    css_class='panel panel-primary',
                        
                            ),
                            Div(
                                Div(HTML(form_label.label_table_heading_contact_details),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    AppendedText('designated_phone_number','<span data-toggle="tooltip" title="'+form_label.label_tooltip_designated_phone_number+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'other_mobile_number',
                                    AppendedText('email_address','<span data-toggle="tooltip" title="'+form_label.label_tooltip_email_address+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'physical_address',
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary',
                        
                            ),
                            Div(
                                Div(HTML('<input id="id_edit_mode" class="mandatory" type="radio" name="edit_mode" value="2" {{select_death_radio}}> '+form_label.label_option_died),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Field(Div('date_of_death', style='visibility:text'), css_class='form-inline mandatory'),
                                    Field(Div('edit_mode_hidden', style='display:None'), css_class='form-inline mandatory'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary',
                                style = do_display_status,
                            ),
                            Div(
                                Div(HTML('<input id="id_edit_mode" class="mandatory" type="radio" name="edit_mode"  value="3"/> '+form_label.label_option_never_existed),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                    ),
                                css_class='panel panel-primary',
                            ),
                            Div(
                                ButtonHolder(
                                    HTML("<Button onclick=\"updateFinale(event)\" type=\"submit\" name=\"submit\" style=\"margin-right:4px\" class=\"btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+submitButtonText+"</Button>"),
                                    HTML("""<a type=\"button\" href=\"/workforce/wfcsearch/\"class=\"btn btn-success\" ><i class="fa fa-times-circle-o"></i> """+form_label.label_button_cancel+"""</a>"""),
                                ),
                                style='margin-left:41%'
                            ),
            )
        )
        
        #if self.is_update_page:
        #    if self.user:
        #        if user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf general rel'):
        #            self.geo_control_mandatory = 'mandatory'
        #    #print len(roles_and_fields_to_show) == 0,'len(roles_and_fields_to_show) == 0'
        #    if len(roles_and_fields_to_show) == 0:
        #        compile_permissions(self.user,self.national_id,self.workforce_related_to_user,self.workforce_is_logged_in_user,roles_and_fields_to_show)
        #    #print roles_and_fields_to_show,'roles_and_fields_to_show in form 1'
        #    layout = self.helper.layout
        #    searchitems = fields_to_show(self.user, roles_and_fields_to_show['Workforce Update'])
        #    #print searchitems,'searchitems'
        #    if searchitems:
        #        del_form_field(layout, searchitems)
        #        self.helper.layout = layout
        
        if self.is_update_page:
            if self.user:
                if user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf general rel'):
                    self.geo_control_mandatory = 'mandatory'
            #print len(roles_and_fields_to_show) == 0,'len(roles_and_fields_to_show) == 0'
            if len(roles_and_fields_to_show) == 0:
                compile_permissions(self.user,self.national_id,self.workforce_related_to_user,self.workforce_is_logged_in_user,roles_and_fields_to_show)
            #print roles_and_fields_to_show,'roles_and_fields_to_show in form 1'
            layout = self.helper.layout
            searchitems = filter_fields_for_display(self.user, fields_by_perms, owner_id=wfc_system_id, organisation_id=primary_org_id)
            
            if args and 'nrc' in args[0] and 'submit' in args[0]:
                pass
                
            elif not((user.has_perm('auth.update wkf special') and self.national_id) or 
                       ((user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf general rel')) 
                            and not self.national_id)):
                if 'nrc' in searchitems:
                    searchitems.pop(searchitems.index('nrc'))
                
            if searchitems:
                del_form_field(layout, searchitems)
                self.helper.layout = layout
            
    def clean(self):
        #print self.cleaned_data.get('org_data_hidden'), 'worforce forms hidden'        
        cleaned_data = super(WorkforceRegistrationForm, self).clean()
        try:
            if (cleaned_data.get('org_data_hidden') != None):
                value = cleaned_data.get('org_data_hidden')
                if value:
                    no_of_primary_org_units = 0
                    orgs = json.loads(value)
                    #Check how many primary org units are registered
                    if orgs:
                        for key, org_data in orgs.items():
                            if org_data['primary_org'] == 'Yes':
                                no_of_primary_org_units+=1
                    #if 0 then V eror
                    if no_of_primary_org_units == 0:
                        self._errors["org_unit"] = self.error_class([form_label.error_message_no_primary_unit])
            else:         
                self._errors["org_unit"] = self.error_class([form_label.error_message_no_primary_unit])  


            if (cleaned_data.get('nrc') != None):
                value = cleaned_data.get('nrc')
                if value:
                    if not self.is_update_page:
                        if(RegPerson.objects.filter(national_id=value,is_void=False).count() > 0):
                            m_person = RegPerson.objects.filter(national_id=value,is_void=False)[0]          
                            self._errors["nrc"] = self.error_class([form_label.error_message_duplicate_nrc_first_part+m_person.first_name+' '+m_person.other_names+' '+m_person.surname+form_label.error_message_duplicate_nrc_last_part])
                    if self.is_update_page:
                        person_id = ''
                        if (cleaned_data.get('wfc_system_id') != None):
                            person_id = cleaned_data.get('wfc_system_id')
                        if(RegPerson.objects.filter(national_id=value,is_void=False).count() > 0):
                            m_person = RegPerson.objects.filter(national_id=value,is_void=False)[0]
                            if str(m_person.pk) == str(person_id):
                                #we do nothing really. just nothing
                                print 'they are equal so its fine'
                            else:    
                                self._errors["nrc"] = self.error_class([form_label.error_message_duplicate_nrc_first_part+m_person.first_name+' '+ m_person.surname+ form_label.error_message_duplicate_nrc_last_part])

            if (cleaned_data.get('designated_phone_number') != None):
                value = cleaned_data.get('designated_phone_number')
                if value:
                    if not self.is_update_page:
                        if(RegPersonsContact.objects.filter(contact_detail=value,contact_detail_type_id=fdict.contact_designated_mobile_phone,is_void=False).count() > 0):
                            m_person_contact = RegPersonsContact.objects.filter(contact_detail=value,contact_detail_type_id=fdict.contact_designated_mobile_phone,is_void=False)[0]      
                            self._errors["designated_phone_number"] = self.error_class([form_label.error_message_duplicate_designated_phone_first_part+m_person_contact.person.first_name+' '+m_person_contact.person.other_names+' '+m_person_contact.person.surname+form_label.error_message_duplicate_designated_phone_last_part])
                    if self.is_update_page:
                        person_id = ''
                        if (cleaned_data.get('wfc_system_id') != None):
                            person_id = cleaned_data.get('wfc_system_id')
                        if(RegPersonsContact.objects.filter(contact_detail=value,contact_detail_type_id=fdict.contact_designated_mobile_phone,is_void=False).count() > 0):
                            m_person_contact = RegPersonsContact.objects.filter(contact_detail=value,contact_detail_type_id=fdict.contact_designated_mobile_phone,is_void=False)[0]
                            if str(m_person_contact.person.pk) == str(person_id):
                                #we do nothing really. just nothing
                                print 'they are equal so its fine'
                            else:    
                                self._errors["designated_phone_number"] = self.error_class([form_label.error_message_duplicate_designated_phone_first_part+m_person_contact.person.first_name+' '+ m_person_contact.person.surname+ form_label.error_message_duplicate_designated_phone_last_part])

            if self.is_update_page:  
                '''
                Ensure that we prevent removal of the NRC if the user does not have a workforce id 
                '''
                workforce_id = ''
                if cleaned_data.get('workforce_id') != None:
                    workforce_id = cleaned_data.get('workforce_id')
                if workforce_id == fdict.empty_workforce_id: 
                    if not cleaned_data.get('nrc'):
                        self._errors["nrc"] = self.error_class([form_label.error_message_enter_nrc])

                #ensure that the workforce type change date is greater than the previous one
                if (self.cleaned_data.get('workforce_type_change_date') != None):
                    entered_value = self.cleaned_data.get('workforce_type_change_date')
                    person_id = 0
                    if (cleaned_data.get('wfc_system_id') != None):
                        person_id = cleaned_data.get('wfc_system_id')
                    if len(RegPersonsTypes.objects.filter(person_id=int(person_id), date_ended=None, is_void=False)) > 0:
                        m_type = RegPersonsTypes.objects.get(person_id=int(person_id), date_ended=None, is_void=False)
                        prev_type_change_date = m_type.date_began
                        if entered_value < prev_type_change_date:
                            self._errors["workforce_type_change_date"] = self.error_class([u""+form_label.error_message_change_date])

                #ensure that the parent organisation change date is greater than the previous one
                if (self.cleaned_data.get('parent_org_change_date') != None):
                    entered_value = self.cleaned_data.get('parent_org_change_date')
                    person_id = 0
                    if (cleaned_data.get('wfc_system_id') != None):
                        person_id = cleaned_data.get('wfc_system_id')
                    org_data_value = None
                    if (cleaned_data.get('org_data_hidden') != None):
                        org_data_value = cleaned_data.get('org_data_hidden')
                    success = False
                    if org_data_value:
                        no_of_primary_org_units = 0
                        orgs = json.loads(org_data_value)
                        for key, org in orgs.items():
                            org_id = org['org_unit_id']
                            primary=org['primary_org']
                            if primary == 'Yes':
                                primary = True 
                            else:
                                primary = False                                   
                            if len(RegOrgUnit.objects.filter(org_unit_id_vis=org_id, is_void = False, date_closed = None)) > 0:
                                m_org = RegOrgUnit.objects.get(org_unit_id_vis=org_id, is_void = False, date_closed = None)                
                                if len(RegPersonsOrgUnits.objects.filter(person_id=int(person_id), parent_org_unit=m_org, date_delinked=None, is_void=False)) == 1:
                                    m_person_org = RegPersonsOrgUnits.objects.get(person_id=int(person_id), parent_org_unit=m_org, date_delinked=None, is_void=False)
                                    prev_primary_change_date = m_person_org.date_linked
                                    if entered_value < prev_primary_change_date:
                                        self._errors["parent_org_change_date"] = self.error_class([u""+form_label.error_message_change_date])

                #ensure that the work locations change date is greater than the previous one
                if (self.cleaned_data.get('work_locations_change_date') != None):
                    entered_value = self.cleaned_data.get('work_locations_change_date')
                    person_id = 0
                    if (cleaned_data.get('wfc_system_id') != None):
                        person_id = cleaned_data.get('wfc_system_id')

                    geos = self.districts + self.wards
                    for geo in geos:
                        if len(RegPersonsGeo.objects.filter(person_id=int(person_id), area_id=geo, date_delinked=None, is_void=False)) > 0:
                            m_person_geo = RegPersonsGeo.objects.get(person_id=int(person_id), area_id=geo, date_delinked=None, is_void=False)
                            prev_location_change_date = m_person_geo.date_linked
                            if entered_value < prev_location_change_date:
                                self._errors["work_locations_change_date"] = self.error_class([u""+form_label.error_message_change_date])
                    for comm in self.communities:
                        if len(RegPersonsGdclsu.objects.filter(person_id=int(person_id), gdclsu_id=comm, date_delinked=None, is_void=False)) > 0:
                            m_person_gdcslu = RegPersonsGdclsu.objects.get(person_id=int(person_id), gdclsu_id=comm, date_delinked=None, is_void=False)
                            prev_location_change_date = m_person_gdcslu.date_linked
                            if entered_value < prev_location_change_date:
                                self._errors["work_locations_change_date"] = self.error_class([u""+form_label.error_message_change_date])

            elif not self.is_update_page:      
                direct_services = ''
                nrc = ''       

                if cleaned_data.get('direct_services') != None:
                    direct_services = cleaned_data.get('direct_services')
                #print cleaned_data.get('nrc'),'self.cleaned_data.get(\'nrc\')'
                if cleaned_data.get('nrc') != None:
                    nrc = cleaned_data.get('nrc')
                if direct_services == '2' and nrc == '': 
                    if cleaned_data.get('nrc') is None:
                        pass
                    else:
                        self._errors["nrc"] = self.error_class([form_label.error_message_enter_nrc])

            if self._errors:
                self._errors["__all__"] = self.error_class([u""+form_label.error_message_save_not_successful])
        except Exception as e:
            traceback.print_exc()
            self._errors["__all__"] = self.error_class([u"Something went bad, very bad go back and recreate this workforce"])
          
        return self.cleaned_data

def compile_permissions(user=None,nrc=None,workforce_related_to_user=None,workforce_is_logged_in_user=None,roles_and_fields_to_show=None): 
    #nrc field
    nrc_special = ''
    nrc_general = ''
    #contact fields
    designated_phone_number = ''
    other_mobile_number = ''
    email_address = ''
    physical_address = ''
    #general fields
    workforce_type = ''
    man_number = ''
    ts_number = ''
    sign_number = ''
    steps_ovc_number = ''
    direct_services = ''
    org_unit = ''
    parent_org_change_date = ''
    primary_parent_org_unit = ''
    workforce_type_change_date = ''
    work_locations_change_date = ''
    date_of_death = ''
    
    if user:
        #Handle NRC special case
        if nrc:
            nrc_special = 'nrc'
            nrc_general = ''
            
        elif not nrc:
            nrc_special = ''
            nrc_general = 'nrc'
        #contact fields permissions
        if user.has_perm('auth.update wkf contact'):
            designated_phone_number = 'designated_phone_number'
            other_mobile_number = 'other_mobile_number'
            email_address = 'email_address'
            physical_address = 'physical_address'
            
        elif user.has_perm('auth.update wkf contact rel'):
            if workforce_related_to_user:
                designated_phone_number = 'designated_phone_number'
                other_mobile_number = 'other_mobile_number'
                email_address = 'email_address'
                physical_address = 'physical_address'
                
        elif user.has_perm('auth.update wkf contact own'):
            if workforce_is_logged_in_user:
                designated_phone_number = 'designated_phone_number'
                other_mobile_number = 'other_mobile_number'
                email_address = 'email_address'
                physical_address = 'physical_address'
        
        #general fields permissions
        if user.has_perm('auth.update wkf general'):
            workforce_type = 'workforce_type'
            man_number = 'man_number'
            ts_number = 'ts_number'
            sign_number = 'sign_number'
            steps_ovc_number = 'steps_ovc_number'
            direct_services = 'direct_services'
            org_unit = 'org_unit'
            primary_parent_org_unit = 'primary_parent_org_unit'
            parent_org_change_date = 'parent_org_change_date'
            workforce_type_change_date = 'workforce_type_change_date'
            work_locations_change_date = 'work_locations_change_date'
            date_of_death = 'date_of_death'
            
        elif user.has_perm('auth.update wkf general rel'):
            if workforce_related_to_user:
                workforce_type = 'workforce_type'
                man_number = 'man_number'
                ts_number = 'ts_number'
                sign_number = 'sign_number'
                steps_ovc_number = 'steps_ovc_number'
                direct_services = 'direct_services'
                org_unit = 'org_unit'
                primary_parent_org_unit = 'primary_parent_org_unit'
                parent_org_change_date = 'parent_org_change_date'
                workforce_type_change_date = 'workforce_type_change_date'
                work_locations_change_date = 'work_locations_change_date'
                date_of_death = 'date_of_death'
            
    roles_and_fields_to_show['Workforce Update'] ={ 'auth.update wkf special':['first_name', 'last_name', 'other_names', 'sex', 'date_of_birth',nrc_special,'edit_mode_hidden','workforce_id',direct_services],
	'auth.update wkf contact':[designated_phone_number, other_mobile_number, email_address, physical_address,'edit_mode_hidden','workforce_id',direct_services],
        'auth.update wkf contact own':[designated_phone_number, other_mobile_number, email_address, physical_address,'edit_mode_hidden','workforce_id',direct_services],
        'auth.update wkf contact rel':[designated_phone_number, other_mobile_number, email_address, physical_address,'edit_mode_hidden','workforce_id',direct_services],
        'auth.update wkf general':[workforce_type, man_number, ts_number, sign_number,steps_ovc_number,direct_services,org_unit,primary_parent_org_unit,workforce_type_change_date,parent_org_change_date,work_locations_change_date,nrc_general,date_of_death,'edit_mode_hidden','workforce_id'],
        'auth.update wkf general rel':[workforce_type, man_number, ts_number, sign_number,steps_ovc_number,direct_services,org_unit,primary_parent_org_unit,workforce_type_change_date,parent_org_change_date,work_locations_change_date,nrc_general,date_of_death,'edit_mode_hidden','workforce_id']
        }
    return roles_and_fields_to_show

def get_list_from_dic_or_querydict(obj, key):
    from django.http import QueryDict
    if not key in obj:
        return []
    if type(obj) == dict:
        return obj[key]
    if type(obj) == QueryDict:
        #print dir(obj)
        return obj.getlist(key)
def convert_to_int_array(valuetoconvert):
    if str(valuetoconvert).isdigit():
        return [int(valuetoconvert)]
    elif valuetoconvert:
        tmp = map(int, valuetoconvert)
        return tmp
    else:
        return None
