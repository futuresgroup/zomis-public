   
'''
Created on Sep 29, 2014

@author: PKaumba
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import fields_list_provider as db_fieldchoices, validators as validate_helper
from ovc_main.utils.general import convert_to_int_array, get_list_from_dic_or_querydict
from functools import partial
from django.conf import settings
from ovc_main.utils.auth_access_control_util import del_form_field, filter_fields_for_display
import datetime
import ovc_assess_labels as form_label
from ovc_main.beneficiary_registration import load_ben_from_id as reg
from django.core.validators import MinValueValidator, MaxValueValidator
from ovc_main.utils.custom_template_tags import CustomDiv
#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

answer_set_ids_add_empty_list = {1:False, 2:True, 3:False, 4:True, 5:True, 6:True, 7:True, }
form_db_lookups = db_fieldchoices.get_answer_set(answer_set_ids = answer_set_ids_add_empty_list)

class OvcAssessmentForm(forms.Form):
    ovc_subject = None
    form_type = forms.CharField(required = False,widget=forms.HiddenInput)
    subject = forms.CharField(required = False,widget=forms.HiddenInput)
    org_options = None
    form_id = forms.CharField(required = False,widget=forms.HiddenInput)
    wfc_id_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    wfc_assessing_child_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    hidden_wfc_id_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    
    #Section A
    wfc_assessing_child = forms.CharField(label=form_label.label_wfc_member,widget=forms.TextInput(attrs={'class':'autocompletefrmwfc'}))
    org_assessing_child = forms.ChoiceField(label=form_label.label_org_assess,choices=db_fieldchoices.get_list_of_cwacs(all_orgs=True))
    date_assessment_comp = forms.DateField(widget=DateInput('%d %B %Y'),label=form_label.label_date_assess, input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])#forms.DateField(widget=DateInput(),label=form_label.label_date_assess ,input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    Q001 = forms.MultipleChoiceField(label=form_label.label_src_info, choices = form_db_lookups[1], widget = forms.CheckboxSelectMultiple())
    Q002 = forms.CharField(max_length=255,required = False,label=form_label.label_src_info_other)
    
    #Section C
    Q003 = forms.ChoiceField(required = False,label=form_label.label_primary_liv, choices = form_db_lookups[2])
    Q004 = forms.CharField(max_length=4, required = False, label=form_label.label_num_hse_18)#forms.IntegerField(min_value=1, max_value=30, required = False, label=form_label.label_num_hse_18)
    Q005 = forms.CharField(max_length=4, required = False, label=form_label.label_num_hse_59)#forms.IntegerField(min_value=0, max_value=30, required = False, label=form_label.label_num_hse_59)
    Q006 = forms.CharField(max_length=4, required = False, label=form_label.label_num_hse_60)#forms.IntegerField(min_value=0, max_value=30, required = False, label=form_label.label_num_hse_60)
    Q007 = forms.MultipleChoiceField(required = False, label=form_label.label_main_income, choices = form_db_lookups[3], widget = forms.CheckboxSelectMultiple())
    Q008 = forms.ChoiceField(required = False, label=form_label.label_incur, choices = form_db_lookups[4])
    Q009 = forms.ChoiceField(required = False, label=form_label.label_pay, choices = form_db_lookups[4])
    Q010 = forms.CharField(max_length=255,required = False, label=form_label.label_details_dealt)
    Q011 = forms.CharField(max_length=4, required = False, label=form_label.label_highest_school)#forms.IntegerField(min_value=0, max_value=12, required = False, label=form_label.label_highest_school)
    Q012 = forms.CharField(max_length=4,required = False, label=form_label.label_nutrition_upper_arm)
    Q013 = forms.ChoiceField(required = False, label=form_label.label_birth_reg_status, choices = form_db_lookups[5])
    Q014 = forms.RegexField(required = False, label=form_label.label_birth_reg_num, regex=r'^\b\w{3}\/\b\d{6}\/\d{4}$',error_message = (form_label.error_message_birth_cert_num))
    Q015 = forms.ChoiceField(required = False, label=form_label.label_health_sick, choices = form_db_lookups[4])
    Q016 = forms.ChoiceField(required = False, label=form_label.label_health_slept, choices = form_db_lookups[4])
    Q017 = forms.ChoiceField(required = False, label=form_label.label_health_immune, choices = form_db_lookups[4])
    Q018 = forms.ChoiceField(required = False, label=form_label.label_health_dont_result, choices = form_db_lookups[4])
    Q019 = forms.ChoiceField(required = False, label=form_label.label_health_all_know_result, choices = form_db_lookups[4])
    Q020 = forms.ChoiceField(required = False, label=form_label.label_health_followup_hiv, choices = form_db_lookups[6])
    Q021 = forms.ChoiceField(required = False, label=form_label.label_health_on_art, choices = form_db_lookups[6])
    Q022 = forms.ChoiceField(required = False, label=form_label.label_sanitation_has_soap, choices = form_db_lookups[4])
    Q023 = forms.ChoiceField(required = False, label=form_label.label_education_enrolled, choices = form_db_lookups[6])
    Q024 = forms.ChoiceField(required = False, label=form_label.label_education_type_school, choices = form_db_lookups[7])
    Q025 = forms.CharField(max_length=4, required = False, label=form_label.label_education_grade) #forms.IntegerField(min_value=0, max_value=12, required = False, label=form_label.label_education_grade)
    Q026 = forms.ChoiceField(required = False, label=form_label.label_education_miss, choices = form_db_lookups[6])
    Q027 = forms.ChoiceField(required = False, label=form_label.label_other_linked, choices = form_db_lookups[4])
    Q028 = forms.ChoiceField(required = False, label=form_label.label_other_activities, choices = form_db_lookups[6])
    Q029 = forms.CharField(max_length=255,required = False, label=form_label.label_other_activities_description)
    
    adverse_cond =  forms.MultipleChoiceField(required = False,choices=db_fieldchoices.get_list_adverse_cond(), widget=forms.SelectMultiple(attrs={'class':'multiselect-searchable'}), label='.')
    
    Q030 = forms.CharField(max_length=255,required = False, label=form_label.label_comments_on_child)
    Q031 = forms.CharField(max_length=255,required = False, label=form_label.label_assessors_recommend)
    
    #CSI
    csi_options = [('','-----'),(4,'4-Good'),(3,'3-Fair'),(2,'2-Bad'),(1,'1-Very bad')]
    #Food and Nutrition
    CS1A = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS1A_det = forms.CharField(max_length=255,required = False, label='')
    CS1B = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS1B_det = forms.CharField(max_length=255,required = False, label='')
    
    #Shelter and Care
    CS2A = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS2A_det = forms.CharField(max_length=255,required = False, label='')
    CS2B = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS2B_det = forms.CharField(max_length=255,required = False, label='')
    
    #Child Protection
    CS3A = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS3A_det = forms.CharField(max_length=255,required = False, label='')
    CS3B = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS3B_det = forms.CharField(max_length=255,required = False, label='')
    
    #Health
    CS4A = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS4A_det = forms.CharField(max_length=255,required = False, label='')
    CS4B = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS4B_det = forms.CharField(max_length=255,required = False, label='')
    
    #PSYCHOSOCIAL
    CS5A = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS5A_det = forms.CharField(max_length=255,required = False, label='')
    CS5B = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS5B_det = forms.CharField(max_length=255,required = False, label='')
    
    #EDUCATION AND SKILLS TRAINING
    CS6A = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS6A_det = forms.CharField(max_length=255,required = False, label='')
    CS6B = forms.ChoiceField(required = False, label='', choices = csi_options)
    CS6B_det = forms.CharField(max_length=255,required = False, label='')

    
    def __init__(self, user,*args, **kwargs):
        self.user = user
        submitButtonText = form_label.label_button_save
        super(OvcAssessmentForm, self).__init__(*args, **kwargs)
        
        org_options = None
        print 'args',args
        print 'kwargs',kwargs
        
        if user.is_superuser or user.has_perm('auth.enter frm high sensitive'):
            self.fields['wfc_assessing_child'] = forms.CharField(label=form_label.label_wfc_member,widget=forms.TextInput(attrs={'class':'autocompletefrmwfc'}))
        else:
            selected_wfc = ''
            selected_wfc_id = ''
            if user.reg_person:
                selected_wfc = "%s \t %s \t %s"% (user.reg_person.workforce_id,user.reg_person.national_id,user.reg_person.full_name)
                selected_wfc_id = '%s'%(user.reg_person.pk)
            if selected_wfc:
                self.fields['wfc_assessing_child'] = forms.CharField(required = False,label=form_label.label_wfc_member,widget=forms.TextInput(attrs={'class':'autocompletefrmwfc','disabled':'disabled','value':selected_wfc}))
            self.fields['wfc_id_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc_id}))
            self.fields['wfc_assessing_child_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc}))
            self.fields['hidden_wfc_id_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc_id}))
        
        if args[0] and 'org_options' in args[0]:
            org_opt = get_list_from_dic_or_querydict(args[0], 'org_options')
            org_options = org_opt
            submitButtonText = form_label.label_button_update
            
        if not org_options:
            org_options = db_fieldchoices.get_list_of_cwacs(all_orgs=True)
            
        self.fields['org_assessing_child'] = forms.ChoiceField(label=form_label.label_org_assess,choices=org_options)
        
        self.helper = FormHelper()
                            
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = '/capture/add_form/'
        self.helper.layout = Layout(
            Fieldset(
                        '',
                        Div(
                            Div(HTML("""
                                        <div class="panel-heading">"""+form_label.label_head_child_details+"""</div>
                                        <div class="panel-body pan mandatory" ><p/> 
                                                <div class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_id+"""</dt>
                                                      <dd>{{subject.beneficiary_id}}</dd>
                                                     </dl>
                                                     <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_first_name+"""</dt>
                                                      <dd>{{subject.first_name}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_sex+"""</dt>
                                                      <dd>{{subject.sex}}</dd>
                                                    </dl>
                                                    
                                                     
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_birth_cert+"""</dt>
                                                      <dd>{{subject.birth_cert_num}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_surname+"""</dt>
                                                      <dd>{{subject.last_name}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_dob+"""</dt>
                                                      <dd>{{subject.date_of_birth}}</dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_community+"""</dt>
                                                      <dd>{{subject.residence.community}}</dd>
                                                      <dt>"""+form_label.label_ward+"""</dt>
                                                      <dd>{{subject.residence.ward}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                        """
                                         ),
                                    css_class='panel panel-primary',
                                ),
                            ),
                        Div(
                                Div(HTML(form_label.label_head_sec_A),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'wfc_assessing_child',
                                    'org_assessing_child',
                                    'date_assessment_comp',
                                    #Field('Q001', template='ovc_main/Forms/sources_info.html'),
                                    'Q001',
                                    'Q002',
                                    'form_type',
                                    #'subject',
                                    css_class = 'panel-body pan',
                                    #template = 'ovc_main/Forms/test_temp.html'
                                    ),
                                css_class='panel panel-primary',
                            ),
                       Div(
                                Div(HTML(form_label.label_head_sec_B),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    Div(
                                        HTML('<label for="id_score" class="col-md-2"></label>'),
                                        HTML('<label for="id_score" class="col-md-3">Score</label>'),
                                        HTML('<label for="id_score" class="col-md-6">Observation</label>'),
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(HTML('<label for="id_food_and_nutrition" style="background-color: #d3d3d3" class="col-md-12">1 -- FOOD AND NUTRITION</label>'),  css_class = 'col-md-12'),
                                    Div(
                                        HTML('<span class="col-md-2"">1A. Food Security</span>'),
                                        Div('CS1A', css_class='col-md-3'),
                                        'CS1A_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(
                                        HTML('<span class="col-md-2"">1B. Nutrition and Growth</span>'),
                                        Div('CS1B',  css_class = 'col-md-3'),
                                        'CS1B_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    
                                    Div(HTML('<label for="id_shelter_and_care" style="background-color: #d3d3d3" class="col-md-12">2 -- SHELTER AND CARE</label>'),  css_class = 'col-md-12'),
                                    Div(
                                        HTML('<span class="col-md-2"">2A. Shelter</span>'),
                                        Div('CS2A',  css_class = 'col-md-3'),
                                        'CS2A_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(
                                        HTML('<span class="col-md-2"">2B. Care</span>'),
                                        Div('CS2B',  css_class = 'col-md-3'),
                                        'CS2B_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(HTML('<label for="id_shelter_and_care" style="background-color: #d3d3d3" class="col-md-12">3 -- CHILD PROTECTION</label>'),  css_class = 'col-md-12'),
                                    Div(
                                        HTML('<span class="col-md-2"">3A. Abuse and Exploitation</span>'),
                                        Div('CS3A',  css_class = 'col-md-3'),
                                        'CS3A_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(
                                        HTML('<span class="col-md-2"">3B. Legal Protection</span>'),
                                        Div('CS3B',  css_class = 'col-md-3'),
                                        'CS3B_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(HTML('<label for="id_shelter_and_care" style="background-color: #d3d3d3" class="col-md-12">4 -- HEALTH</label>'),  css_class = 'col-md-12'),
                                    Div(
                                        HTML('<span class="col-md-2"">4A. Wellness</span>'),
                                        Div('CS4A',  css_class = 'col-md-3'),
                                        'CS4A_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(
                                        HTML('<span class="col-md-2"">4B. Health Care Service</span>'),
                                        Div('CS4B',  css_class = 'col-md-3'),
                                        'CS4B_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(HTML('<label for="id_shelter_and_care" style="background-color: #d3d3d3" class="col-md-12">5 -- PSYCHOSOCIAL</label>'),  css_class = 'col-md-12'),
                                    Div(
                                        HTML('<span class="col-md-2"">5A. Emotional Health</span>'),
                                        Div('CS5A',  css_class = 'col-md-3'),
                                        'CS5A_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(
                                        HTML('<span class="col-md-2"">5B. Social Behaviour</span>'),
                                        Div('CS5B',  css_class = 'col-md-3'),
                                        'CS5B_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(HTML('<label for="id_shelter_and_care" style="background-color: #d3d3d3" class="col-md-12">6 -- EDUCATION AND SKILLS TRAINING</label>'),  css_class = 'col-md-12'),
                                    Div(
                                        HTML('<span class="col-md-2"">6A. Performance</span>'),
                                        Div('CS6A',  css_class = 'col-md-3'),
                                        'CS6A_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    Div(
                                        HTML('<span class="col-md-2"">6B. Education and Work</span>'),
                                        Div('CS6B',  css_class = 'col-md-3'),
                                        'CS6B_det',
                                        form_class = 'form-horizontal'
                                        ),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                            Div(HTML(form_label.label_head_sec_C),
                                css_class = 'panel-heading'
                                ),
                            Div(
                                    HTML('<p/>'),
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Household</b></u></font></span>'),
                                    'Q003',
                                    'Q004',
                                    'Q005',
                                    'Q006',
                                    'Q007',
                                    'Q008',
                                    'Q009',
                                    'Q010',
                                    'Q011',
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Nutrition</b></u></font></span>'),
                                    'Q012',
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Birth registration</b></u></font></span>'),
                                    'Q013',
                                    'Q014',
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Health</b></u></font></span>'),
                                    'Q015',
                                    'Q016',
                                    'Q017',
                                    'Q018',
                                    'Q019',
                                    'Q020',
                                    'Q021',
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Water and sanitation</b></u></font></span>'),
                                    'Q022',
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Education</b></u></font></span>'),
                                    'Q023',
                                    'Q024',
                                    Div('Q025', css_class='grades'),
                                    'Q026',
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Other</b></u></font></span>'),
                                    'Q027',
                                    'Q028',
                                    'Q029',
                                    'form_id',
                                    css_class = 'panel-body pan',
                                ),
                                css_class='panel panel-primary',
                            ), 
                       Div(
                                Div(HTML(form_label.label_head_sec_D),
                                    css_class = 'panel-heading',
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Div('hidden_wfc_id_hidden',css_class='hide'),
                                    'adverse_cond',
                                    #Field('adverse_cond', template='ovc_main/adverse_conditions.html'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary'
                            ),
                        Div(
                                Div(HTML(form_label.label_head_sec_E),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Div('wfc_assessing_child_hidden',css_class='hide'),
                                    'Q030',
                                    'Q031',
                                    Div('wfc_id_hidden',css_class='hide'),
                                    HTML(""" 
                                    <input id="id_subject" name="subject" type="hidden" value="{{subject.id_int}}">
                                    """),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                       Div(
                        HTML("<Button id='submit_ovc_assessment_form' onclick=\"customValidate(event)\" type=\"submit\" style=\"margin-right:4px\" class=\"btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+ submitButtonText +"</Button>"),
                        HTML("<a type=\"button\" href=\"/capture/forms_home/\"class=\"btn btn-success\"><i class=\"fa fa-undo\"></i> "+form_label.label_button_cancel+"</a><br><br>"),
                        style='margin-left:38%'
                        ),
            )
        )
        
    def clean(self):
        if self._errors:
            self._errors["__all__"] = self.error_class([u"OVC Assessment not saved - please correct the errors in red below"])
            
        return self.cleaned_data
