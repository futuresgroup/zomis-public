from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset,\
    ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
    
from functools import partial
DateInput = partial(forms.DateInput, {'class': 'datepicker'})
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import validators as validate_helper
from ovc_main.utils import fields_list_provider as db_fieldchoices, lookup_field_dictionary as db_constants
import constants_workforce_training as form_label
from ovc_main.models import Forms
'''
Call get_answer_set() with the dictionary of answer_set_ids and add empty option boolean that you will need for all the questions on the page.
In the control itself access the choice list by index as shown below

choices = form_db_lookups[9]
'''
answer_set_ids_add_empty_list = {18: False}
form_db_lookups = db_fieldchoices.get_answer_set(answer_set_ids = answer_set_ids_add_empty_list)

class WorkforceTrainingForm(forms.Form):
    ''' General fields '''
    form_type = forms.CharField(required = False,widget=forms.HiddenInput)
    form_id = forms.CharField(required = False,widget=forms.HiddenInput)
    ''' Section A '''
    date_training_started = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = form_label.DATE_TRAINING_STARTED,validators=[validate_helper.validate_general_date])
    date_training_ended = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = form_label.DATE_TRAINING_ENDED,validators=[validate_helper.validate_general_date])
    duration_of_training = forms.DecimalField(decimal_places=1, min_value=0, widget=forms.NumberInput(attrs={'decimal_places':'1'}), label='')#form_label.DURATION_OF_TRAINING)
    Q081 = forms.CharField(required = False, widget=forms.HiddenInput)
    Q082 = forms.CharField(required = False, widget=forms.HiddenInput)
    duration_units = forms.ChoiceField(label='', choices=(('', '-----'), (1, 'Hours'), (2, 'Days')), required=True)
    training_title = forms.CharField(label = form_label.TRAINING_TITLE)
    #event_title_duplicate = forms.CharField(required = False)#,widget=forms.HiddenInput)
    #id_event_title_duplicate
    Q084 = forms.CharField(label = form_label.TRAINING_DESCRIPTION, required=False)
    where_training_took_place_district = forms.ChoiceField(label = form_label.WHERE_TRAINING_TOOK_PLACE, required = True, choices = db_fieldchoices.get_list_of_districts(first_entry = 'Select district'))
    where_training_took_place_ward = forms.ChoiceField(label = '.', required = True, choices = db_fieldchoices.get_list_of_wards(first_entry = 'Select ward'))
    ''' Section B '''
    Q085 = forms.MultipleChoiceField( choices=form_db_lookups[18], \
                                    widget=forms.SelectMultiple(attrs={'class':'multiselect-searchable'}), label=form_label.TOPICS_COVERED)
    ''' Section C '''
    organisational_unit = forms.CharField(label=form_label.ORGANISATIONAL_UNIT, required=False)
    organisation_contribution = forms.MultipleChoiceField(required=False, widget = forms.CheckboxSelectMultiple(), choices = db_fieldchoices.get_item_id_and_description_for_cat(item_category = 'Contribution'))
    org_contributions_data = forms.CharField(label='',required=False)
    
    ''' Section D '''
    workforce_member = forms.CharField(label=form_label.WORKFORCE_MEMBER, required=False)
    workforce_member_id = forms.CharField(label='', required = False)
    participation_level = forms.ChoiceField(required=False, label = form_label.PARTICIPATION_LEVEL, choices = db_fieldchoices.get_item_id_and_description_for_cat(item_category = 'Participation level', add_initial=True)) 
    workforce_member_data = forms.CharField(label='',required=False)

    def __init__(self, user, *args, **kwargs):
        self.is_update_page = False
        self.form_id_int = ''
        if len(args) > 0 and args[0]: 
            if 'form_id' in args[0]:  
                self.form_id_int = args[0]['form_id']
                if self.form_id_int:
                    self.is_update_page = True

        super(WorkforceTrainingForm, self).__init__(*args, **kwargs)
        
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = '/capture/add_form/'
        
        self.helper.layout = \
            Layout(
                Div(
                    Div(
                        HTML(form_label.SECTION_A),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        'form_id',
                        'date_training_started',
                        'date_training_ended', 
                        Div(
                            Field('duration_of_training', template='ovc_main/custom_duration_control.html', style = ''),
                            Field('duration_units', template='ovc_main/custom_duration_units_control.html', style = ''),
                            css_class="form-group",
                            ),               
                        #'duration_of_training',
                        #'duration_units',
                        'Q081',
                        'Q082',
                        'training_title',
                        #'event_title_duplicate',
                        'Q084',
                        'where_training_took_place_district',
                         'where_training_took_place_ward',#Field('where_training_took_place_ward', style='display:None', css_class='form-inline mandatory'),
                        HTML(""" 
                        <input id="id_form_type" name="form_type" type="hidden" value=\"""" + \
                            db_constants.form_type_workforce_training + """\">

                            <!-- Button trigger modal -->
                            <button id="confirm_dialog_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                Launch demo modal
                            </button>
                                    
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Duplicate Person</h4>
                                    </div>
                                    <div class="modal-body">
                                    """+form_label.ERROR_MESSAGE_DUPLICATE_EVENT_TITLE+"""
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button class="btn btn-success" id="submit">Save anyway</button>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <script>
                            	function updateFinale(e){
		                            var title_exists = checkTitle();
                                    if (title_exists === true)
                                    {
                                        e.preventDefault();
                                        $("#confirm_dialog_box").click();
                                    }
	                            }

	                            function checkTitle(){
                                    var tmpresult;
                                    $.ajax({
                                            dataType: "json",
                                            url: "/forms/check_form_titles?title=" + $( "#id_training_title" ).val() + "&form_id=" + $( "#id_form_id" ).val(),
                                            contentType: "application/json; charset=utf-8",
                                            async: false,
                                            success: function (result) {
                                                tmpresult = result;
                                            }
                                        });
                                    return tmpresult;
                                }
                            </script>

                        """),
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    Div(
                        HTML(form_label.SECTION_B),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        'Q085',
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    Div(
                        HTML(form_label.SECTION_C),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        Field('organisational_unit', style="display:inline", css_class="organisation_autocomplete form-inline", id="id_organisational_unit"),
                        HTML("""    <input id="id_org_unit_id" class="organisation_hidden" type="hidden">
                                    <input id="id_org_unit_name" class="organisation_hidden" type="hidden">  
                                    """),
                        'organisation_contribution',
                        Field(Div('org_contributions_data', style='display:None'), css_class='form-inline mandatory'),
                        HTML("<label id=\"valid_orgs_data\" style=\"color:red;margin-left:50px;display:text\"><strong>  </strong></label>"),
                        Div(
                            HTML("<br><a type=\"button\" onclick=\"addOrgData()\" class=\"btn btn-info\"><i class=\"fa fa-plus\"></i>"+form_label.LABEL_BUTTON_ADD+"</a><br><br>"),
                            style='margin-left:41%',
                            ),
                        Div(
                            HTML("""
                            <table id="organisationContributionTable"  class="table table-hover table-condensed table-bordered">
                                <thead>
                                <tr>
                                    <th>""" + form_label.TBL_HEADER_ORG_UNIT + """</th>
                                    <th>""" + form_label.TBL_HEADER_ORG_UNIT_ID + """</th>
                                    <th>""" + form_label.TBL_HEADER_CONTRIBUTIONS + """</th>
                                    <th style="display: None">""" + form_label.TBL_HEADER_CONTRIBUTIONS_ID + """</th>
                                    <th>""" + form_label.LABEL_BUTTON_REMOVE + """</th>
                                </tr>
                                </thead>
                                <tbody>
                    
                                </tbody>
                            </table>"""), 
                            ),
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    Div(
                        HTML(form_label.SECTION_D),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        Field('workforce_member', style="display:inline", css_class="workforce_training_autocompletewfc form-inline", id="id_workforce_member"),
                        HTML("""    <input id="id_workforce_member_id" class="workforce_hidden" type="hidden">
                                    <input id="id_workforce_member_nrc" class="workforce_hidden" type="hidden">  
                                    <input id="id_workforce_member_name" class="workforce_hidden" type="hidden"> 
                                    """),
                        'participation_level',
                        Field(Div('workforce_member_data', style='display:None'), css_class='form-inline mandatory'),
                        HTML("<label id=\"valid_workforce_data\" style=\"color:red;margin-left:50px;display:text\"><strong>  </strong></label>"),
                        Div(
                            HTML("<br><a type=\"button\" onclick=\"addWorkforceData()\" class=\"btn btn-info\"><i class=\"fa fa-plus\"></i>"+form_label.LABEL_BUTTON_ADD+"</a><br><br>"),
                            style='margin-left:41%',
                            ),
                        Div(
                            HTML("""
                            <table id="workforceContributionTable"  class="table table-hover table-condensed table-bordered">
                                <thead>
                                <tr>
                                    <th>""" + form_label.TBL_HEADER_WORKFORCE_MEMBER_ID + """</th>
                                    <th>""" + form_label.TBL_HEADER_WORKFORCE_MEMBER_NRC + """</th>
                                    <th>""" + form_label.TBL_HEADER_WORKFORCE_MEMBER_NAME + """</th>
                                    <th>""" + form_label.TBL_HEADER_PARTICIPATION_LEVEL + """</th>
                                    <th style="display: None">""" + form_label.TBL_HEADER_PARTICIPATION_LEVEL_ID + """</th>
                                    <th>""" + form_label.LABEL_BUTTON_REMOVE + """</th>
                                </tr>
                                </thead>
                                <tbody>
                    
                                </tbody>
                            </table>"""), 
                            ),
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    ButtonHolder(
                        HTML("<Button onclick=\"updateFinale(event)\" type=\"submit\" name=\"submit\" style=\"margin-right:4px\" class=\"btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+form_label.LABEL_BUTTON_SUBMIT+"</Button>"),
                        HTML("""<a type=\"button\" href=\"/capture/forms_home/\"class=\"btn btn-success\" ><i class="fa fa-times-circle-o"></i> """+form_label.LABEL_BUTTON_CANCEL+"""</a>"""),
                    ),
                    style='margin-left:41%'
                    ),
            )
    
    '''
    Server side validations for this page are handled here
    '''
    def clean(self):      
        cleaned_data = super(WorkforceTrainingForm, self).clean()    
        
        date_training_started = None
        date_training_ended = None
        training_title = None

        if cleaned_data.get('date_training_started'):
            date_training_started = cleaned_data.get('date_training_started')
        if cleaned_data.get('date_training_ended'):
            date_training_ended = cleaned_data.get('date_training_ended')

        if date_training_started and date_training_ended:
            if date_training_started > date_training_ended: 
                self._errors["date_training_ended"] = self.error_class([form_label.ERROR_MESSAGE_DATE_ENDED_GT_DATE_STARTED])  
        '''
        if cleaned_data.get('training_title'):
            training_title = cleaned_data.get('training_title')
            m_form = Forms.objects.filter(form_title=training_title, is_void=False)
            dups_detected = False
            if self.form_id_int:
                for form in m_form:
                    if form.pk == int(self.form_id_int):
                        dups_detected = False
                    else:
                        dups_detected = True
                        break
            else:
                if len(m_form) > 0:
                    dups_detected = True
            if dups_detected:
                event_title_duplicate
                self._errors["training_title"] = self.error_class([form_label.ERROR_MESSAGE_DUPLICATE_EVENT_TITLE])  '''
            
        if (cleaned_data.get('org_contributions_data') != None):
            value = cleaned_data.get('org_contributions_data')
            if not value or value == "{}":
                self._errors["organisational_unit"] = self.error_class([form_label.ERROR_MESSAGE_ENTER_ATLEAST_ONE_ORG_CONTRIBUTION])

        if (cleaned_data.get('workforce_member_data') != None):
            value = cleaned_data.get('workforce_member_data')
            if not value:
                self._errors["workforce_member"] = self.error_class([form_label.ERROR_MESSAGE_ENTER_ATLEAST_ONE_PERSON_PARTICIPATION])

        if self._errors:
            self._errors["__all__"] = self.error_class([u""+form_label.ERROR_MESSAGE_SAVE_NOT_SUCCESSFUL])
          
        return self.cleaned_data
    
'''
duration_controls= Layout(
    Div(
        HTML("""
            <label for="id_estimate_of_people_reached" class="control-label col-md-3">
				Duration of training: 
			</label>
            <div class = "row">
                <table ">
                                       
                    <tbody>
                        <th> 
                            <td class="col-md-6">"""), 
                                Field('duration_of_training', 
                                        style="display:text", 
                                        css_class="form-inline counts form-control col-md-12", 
                                        id="id_duration_of_training"),
                            HTML("""</td>
                            <td class="col-md-6">"""), 
                                Field('duration_units', 
                                        style="display:text", 
                                        css_class="form-inline counts form-control col-md-12", 
                                        id="id_duration_units"),
                            HTML("""</td>
                            <!--td><input class="textinput textInput form-control col-md-3" id="id_our_place_holder" name="our_place_holder" type="hidden"> </td-->
                        </th>                                            
                    </tbody>
                </table>
                </div>"""), 
            css_class = "controls col-md-12",
        ),
)
'''