''' Headings '''
SECTION_A = u'Section A'
SECTION_B = u'Section B - Topics covered'
SECTION_C = u'Section C - Contributions'
SECTION_D = u'Section D - Trainees'

LABEL_BUTTON_SUBMIT = u"Save"
LABEL_BUTTON_CANCEL = u"Cancel"
LABEL_BUTTON_ADD = u"Add"
LABEL_BUTTON_REMOVE = "Remove"

''' Section A '''
DATE_TRAINING_STARTED = u'Date training started:'
DATE_TRAINING_ENDED = u'Date training ended:'
DURATION_OF_TRAINING = u'Duration of training:'
TRAINING_TITLE = u'Training title:'
TRAINING_DESCRIPTION = u'Training description:'
WHERE_TRAINING_TOOK_PLACE = u'Where training took place:'
TYPE_OF_EVENT = u'Type of event:'

''' Section B '''
TOPICS_COVERED = u'Topics covered:'

''' Section C '''
ORGANISATIONAL_UNIT = u'Organisational unit::'
TBL_HEADER_ORG_UNIT = u'Org Unit'
TBL_HEADER_ORG_UNIT_ID = u'Org unit ID'
TBL_HEADER_CONTRIBUTIONS = u'Contributions'
TBL_HEADER_CONTRIBUTIONS_ID = u'Contribution id'

''' Section D '''
WORKFORCE_MEMBER = u'Workforce member:'
PARTICIPATION_LEVEL = u'Participation level:'
TBL_HEADER_WORKFORCE_MEMBER_ID = u'Workforce member ID'
TBL_HEADER_WORKFORCE_MEMBER_NRC = u'Workforce member NRC'
TBL_HEADER_WORKFORCE_MEMBER_NAME = u'Workforce member name'
TBL_HEADER_PARTICIPATION_LEVEL = u'Participation level'
TBL_HEADER_PARTICIPATION_LEVEL_ID  = u'Participation level ID'

''' Errors '''
ERROR_MESSAGE_SAVE_NOT_SUCCESSFUL = u'Save not successful - please make corrections below'
ERROR_MESSAGE_DATE_ENDED_GT_DATE_STARTED = u'The date training ended cannot be before the date training started'
ERROR_MESSAGE_DUPLICATE_EVENT_TITLE = u'This title already exists. Please make the title more specific'
ERROR_MESSAGE_ENTER_ATLEAST_ONE_ORG_CONTRIBUTION  = u'Please enter atleast one organisation and it''s contribution before saving'
ERROR_MESSAGE_ENTER_ATLEAST_ONE_PERSON_PARTICIPATION  = u'Please enter at least one trainee'