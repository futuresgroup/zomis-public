   
'''
Created on Sep 29, 2014

@author: PKaumba
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import fields_list_provider as db_fieldchoices, validators as validate_helper
from ovc_main.utils.general import convert_to_int_array, get_list_from_dic_or_querydict
from functools import partial
from django.conf import settings
from ovc_main.utils.auth_access_control_util import del_form_field, filter_fields_for_display
import datetime
import workforce_assessment_form_labels as form_label
from ovc_main.beneficiary_registration import load_ben_from_id as reg
from django.core.validators import MinValueValidator, MaxValueValidator
from ovc_main.utils.custom_template_tags import CustomDiv
#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

answer_set_ids_add_empty_list = { 15:True, 16:True, 17:True, 18:False }
form_db_lookups = db_fieldchoices.get_answer_set(answer_set_ids = answer_set_ids_add_empty_list)

class WorkforceAssessmentForm(forms.Form):
    wfc_subject = None
    form_type = forms.CharField(required = False,widget=forms.HiddenInput)
    subject = forms.CharField(required = False,widget=forms.HiddenInput)
    org_options = None
    form_id = forms.CharField(required = False,widget=forms.HiddenInput)
    wfc_id_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    wfc_assessing_wfc_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    hidden_wfc_id_hidden = forms.CharField(required = False,widget=forms.HiddenInput)   
        
    #Section A
    wfc_assessing_wfc = forms.CharField(label=form_label.label_assessor_perf_assessment,widget=forms.TextInput(attrs={'class':'autocompletefrmwfc'}))
    org_assessing_wfc = forms.ChoiceField(label=form_label.label_org_unit,choices=db_fieldchoices.get_list_of_cwacs(all_orgs=True))
    date_assessment_comp = forms.DateField(widget=DateInput('%d %B %Y'),label=form_label.label_date_assess_comp, input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    Q067 = forms.CharField(max_length=255,required = False,label=form_label.label_wfc_job_title)
    
    #Section B
    Q068 = forms.ChoiceField(required = False,label=form_label.label_read_signed_org_policy_child_protection, choices = form_db_lookups[15])
    Q069 = forms.ChoiceField(required = False,label=form_label.label_read_signed_confience_policy, choices = form_db_lookups[15])
    Q070 = forms.ChoiceField(required = False, label=form_label.label_crime_clearance, choices = form_db_lookups[16])
    Q071 = forms.CharField(max_length=255,required = False, label=form_label.label_crime_details)
    
    #Section C
    Q072 = forms.ChoiceField(required = False, label=form_label.label_literacy_level, choices = form_db_lookups[17])
    Q073_control = forms.CharField(required = False, label=form_label.label_low_literacy_worker,widget=forms.TextInput(attrs={'class':'autocompletepaired'}))
    Q073 = forms.CharField(max_length=255,required = False,widget=forms.HiddenInput)
    Q074 = forms.CharField(max_length=2, required = False, label=form_label.label_high_school_level_attained)
    Q075 = forms.CharField(max_length=255,required = False, label=form_label.label_further_qualification)
    Q076 = forms.CharField(max_length=255,required = False, label=form_label.label_childcare_training_received)
    
    #Section D
    Q077 = forms.CharField(max_length=4, required = False, label=form_label.label_num_of_years_experience)
    Q078 = forms.CharField(max_length=255,required = False, label=form_label.label_details_experience)
    
    #Section E
    Q079 = forms.CharField(max_length=255,required = False, label=form_label.label_training_needs_assessment)
    Q080 = forms.MultipleChoiceField(required = False,choices = form_db_lookups[18], widget=forms.SelectMultiple(attrs={'class':'multiselect-searchable'}), label=form_label.label_priority_areas)
    
    
    def __init__(self, user,*args, **kwargs):
        self.user = user
        submitButtonText = form_label.label_button_save
        super(WorkforceAssessmentForm, self).__init__(*args, **kwargs)
        
        if user.is_superuser or user.has_perm('auth.enter frm high sensitive'):
            self.fields['wfc_assessing_wfc'] = forms.CharField(label=form_label.label_assessor_perf_assessment,widget=forms.TextInput(attrs={'class':'autocompletefrmwfc'}))
        else:
            selected_wfc = ''
            selected_wfc_id = ''
            if user.reg_person:
                selected_wfc = "%s \t %s \t %s"% (user.reg_person.workforce_id,user.reg_person.national_id,user.reg_person.full_name)
                selected_wfc_id = '%s'%(user.reg_person.pk)
            if selected_wfc:
                self.fields['wfc_assessing_wfc'] = forms.CharField(required = False,label=form_label.label_assessor_perf_assessment,widget=forms.TextInput(attrs={'class':'autocompletefrmwfc','disabled':'disabled','value':selected_wfc}))
            self.fields['wfc_id_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc_id}))
            self.fields['wfc_assessing_wfc_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc}))
            self.fields['hidden_wfc_id_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc_id}))
        
        org_options = None
        
        if args[0] and 'org_options' in args[0]:
            org_opt = get_list_from_dic_or_querydict(args[0], 'org_options')
            org_options = org_opt
            submitButtonText = form_label.label_button_update
            
        if not org_options:
            org_options = db_fieldchoices.get_list_of_cwacs(all_orgs=True)
            
        self.fields['org_assessing_wfc'] = forms.ChoiceField(label=form_label.label_org_unit,choices=org_options)
        
        self.helper = FormHelper()
                            
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = '/capture/add_form/'
        self.helper.layout = Layout(
            Fieldset(
                        '',
                        Div(
                            Div(HTML("""
                                        <div class="panel-heading">"""+form_label.label_head_child_details+"""</div>
                                        <div class="panel-body pan mandatory" ><p/> 
                                                <div class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_id+"""</dt>
                                                      <dd>{{subject.workforce_id}}</dd>
                                                     </dl>
                                                     <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_first_name+"""</dt>
                                                      <dd>{{subject.first_name}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_sex+"""</dt>
                                                      <dd>{{subject.sex}}</dd>
                                                    </dl>
                                                    
                                                     
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.nrc+"""</dt>
                                                      <dd>{{subject.national_id}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_surname+"""</dt>
                                                      <dd>{{subject.surname}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.label_dob+"""</dt>
                                                      <dd>{{subject.date_of_birth}}</dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <div  class='col-md-6' align="right">
                                                    <span><strong>"""+form_label.label_current_primary_org_unit+"""</strong></span>
                                                    </div>
                                                    <div  class='col-md-6' align="left">
                                                    <span>{{subject.primary_org_unit_name}}</span>
                                                    </div>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <div  class='col-md-6' align="right">
                                                    <span><strong>"""+form_label.label_other_parent_org_unit+"""</strong></span>
                                                    </div>
                                                    <div  class='col-md-6' align="left">
                                                    <span>{{subject.org_units_string}}</span>
                                                    </div>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <div  class='col-md-6' align="right">
                                                    <span><strong></strong></span>
                                                    </div>
                                                    <div  class='col-md-6' align="left">
                                                    <span>.</span>
                                                    </div>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <div  class='col-md-6' align="right">
                                                    <span><strong>"""+form_label.label_communities_where_working+"""</strong></span>
                                                    </div>
                                                    <div  class='col-md-6' align="left">
                                                    <span>{{subject.communities_string}}</span>
                                                    </div>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <div  class='col-md-6' align="right">
                                                    <span><strong>"""+form_label.label_geo_areas_where_working+"""</strong></span>
                                                    </div>
                                                    <div  class='col-md-6' align="left">
                                                    <span>{{subject.wards_string}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        """
                                         ),
                                         HTML('<p/>'),
                                    css_class='panel panel-primary',
                                ),
                            ),
                        Div(
                                Div(HTML(form_label.label_sec_A),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'wfc_assessing_wfc',
                                    'org_assessing_wfc',
                                    'date_assessment_comp',
                                    'Q067',
                                    'form_type',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                            Div(HTML(form_label.label_sec_B),
                                css_class = 'panel-heading'
                                ),
                            Div(
                                    HTML('<p/>'),
                                    'Q068',
                                    'Q069',
                                    'Q070',
                                    Div('Q071', css_class='criminal'),
                                    'form_id',
                                    css_class = 'panel-body pan',
                                ),
                                css_class='panel panel-primary',
                            ), 
                       Div(
                                Div(HTML(form_label.label_sec_C),
                                    css_class = 'panel-heading',
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    'Q072',
                                    Div('Q073_control', css_class='literacy'),
                                    'Q074',
                                    'Q075',
                                    'Q076',
                                    'Q073',
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary'
                            ),
                        Div(
                                Div(HTML(form_label.label_sec_D),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Div('wfc_assessing_wfc_hidden',css_class='hide'),
                                    'Q077',
                                    'Q078',
                                    HTML(""" 
                                    <input id="id_subject" name="subject" type="hidden" value="{{subject.id_int}}">
                                    """),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                                Div(HTML(form_label.label_sec_E),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Div('hidden_wfc_id_hidden',css_class='hide'),
                                    'Q079',
                                    'Q080',
                                    Div('wfc_id_hidden',css_class='hide'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                       Div(
                        HTML("<Button id='submit_wfc_assessment_form' onclick=\"customValidate(event)\" type=\"submit\" style=\"margin-right:4px\" class=\"btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+ submitButtonText +"</Button>"),
                        HTML("<a type=\"button\" href=\"/capture/forms_home/\"class=\"btn btn-success\"><i class=\"fa fa-undo\"></i> "+form_label.label_button_cancel+"</a><br><br>"),
                        style='margin-left:38%'
                        ),
            )
        )
        
    def clean(self):
        if self._errors:
            self._errors["__all__"] = self.error_class([u"Workforce Assessment not saved - please correct the errors in red below"])
            
        return self.cleaned_data
