''' Headings '''
HEADING_ORGANISATION_DETAILS =  u"Institution details"
HEADING_SECTION_A = u"Section A"
HEADING_SECTION_B = u"Section B"
HEADING_SECTION_C = u"Section C - Children in institution"
HEADING_SECTION_D = u"Section D - Workforce"
HEADING_SECTION_E = u"Section E - Infrastructure and services"
HEADING_SECTION_F = u"Section F - Administration and income"
HEADING_SECTION_G = u"Section G"

LABEL_BUTTON_SUBMIT = u"Save and finish"
LABEL_BUTTON_CANCEL = u"Cancel"
LABEL_BUTTON_REMOVE = "Remove"

''' Section A '''
DATE_OF_FORM = u"Date of form:"
REPORTED_BY =   u"Reported by:"
WORKFORCE_OR_USER_FILLING_FORM = u"Workforce member / user filling form:"

''' Section B '''
RESIDENTIAL_INSTITUTION_TYPE = u"Residential institution type:"
WHO_OWNS_THE_BUILDING = u"Who owns the building?"
MISSION_OF_INSTITUTUION = u"Mission of institution:"
MINIMUM_AGE_OF_ENROLLMENT = u"Minimum age of enrollment:"
MAXIMUM_AGE_OF_ENROLLMENT = u"Maximum age of enrollment:"
INSTITUTUIONAL_CAPACITY = u"Institutional capacity:"
LABEL_BUTTON_NEXT_TAB_SEC_B = u"Continue to Section C - children"

''' Section C '''
CHILD = u"Child:"
NEW_ADVERSE_CONDITION = u"New adverse condition:"
CURRENT_RESIDENTIAL_STATUS = u"Current residential status:"
FAMILY_STATUS = u"Family status:"
HAS_COURT_COMMITTAL_ORDER = u"Has court committal order:"
DATE_FIRST_ADMITTED = u"Date first admitted:"
DATE_LEFT = u"Date left:"
LABEL_BUTTON_ADD = u"Add"

BENEFICIARY_ID_BEN_TBL_HEADER = u"Beneficiary ID"
NAME_BEN_TBL_HEADER = u"Name"
NEW_ADVERSE_CONDITION_BEN_TBL_HEADER = u"New adverse condition"
CHILD_STATUS_DETAILS_BEN_TBL_HEADER = u"Details"
DATE_FIRST_ADMITTED_BEN_TBL_HEADER = u"Date first admitted"
DATE_LEFT_BEN_TBL_HEADER = u"Date left"
LABEL_BUTTON_NEXT_TAB_SEC_C = u"Continue to Section D - workforce"

''' Section D '''
WORKFORCE_ID_WORKFORCE_TBL_HEADER = u"Workforce ID or NRC"
NAME_WORKFORCE_TBL_HEADER = u"Name"
POSITION_OR_ROLE_TBL_HEADER = u"Role or position"
EMPLOYMENT_STATUS_TBL_HEADER = u"Employment status"
LABEL_BUTTON_NEXT_TAB_SEC_D = u"Continue to Section E, F, G"

''' Section E '''
WATER_AND_HYGENE = u"Water & hygiene services:"
HYGIENE_STANDARD_IN_INSTITUTION = u"What is the hygiene standard in the institution?"
IS_WATER_SOURCE_IN_INSTITUTION = u"Is the water source within the institution?"
PRIMARY_WATER_SOURCE = u"What is the primary water source?"
HOW_FAR_IS_WATER_SOURCE = u"Approximately how far away is the water source (in metres):"
DOES_WATER_SOURCE_SERVICE_INSTITUTION_DAILY = u"Does the primary water source service the institution on a daily basis?"

TOILET_FACILITIES = u"Toilet facilities:"
HOW_MANY_TOILETS_FOR_BOYS = u"How many toilets specifically for boys?"
HOW_MANY_TOILETS_FOR_GIRLS = u"How many toilets specifically for girls?"
HOW_MANY_TOILETS_TOTAL = u"How many toilets in total?"
DO_BOYS_AND_GIRLS_SHARE_SAME_TOILETS = u"Do boys and girls share the same toilets?"
TYPES_OF_TOILETS_CHILDREN_USE = u"What types of toilets do children in the institution use?"
HOW_MANY_BATHROOMS_FOR_BOYS = u"How many bathrooms specifically for boys?"
HOW_MANY_BATHROOMS_FOR_GIRLS = u"How many bathrooms specifically for girls?"
HOW_MANY_BATHROOMS_TOTAL = u"How many bathrooms in total?"
DO_BOYS_AND_GIRLS_SHARE_SAME_BATHROOMS = u"Do boys and girls share the same bathrooms?"
TYPES_OF_BATHROOMS_CHILDREN_USE = u"What types of bathrooms do children in the institution use?"
NOTES_ON_WATER_HYGIENE_SANITATION = u"Notes on water, hygiene and sanitation:"

COOKING_AND_DINING_FACILITIES = u"Cooking & dining facilities:"
DIETARY_CHART = u"Dietary chart:"
VARIETY_IN_DIET = u"Variety in diet (cereals, fruits, eggs etc):"
UNDER_FIVE_CARDS_FOR_CHILDREN_5_AND_BELOW = u"Under five cards for children aged 5 & below:"
NUMBER_OF_MEALS_PER_DAY = u"Number of meals provided per day:"

SLEEPING_FACILITIES = u"Sleeping facilities:"
SEPARATE_SLEEPING_FACILITIES_FOR_BOYS_AND_GIRLS = u"Are there separate sleeping facilities for boys and girls?"
NUMBER_OF_CHILDREN_PER_BED = u"How many children per bed on average?"
HOW_MANY_SLEEP_ON_FLOOR_MATS = u"How many children sleep on the floor on mats?"
MOSQUITO_NETS_ON_BEDS_AND_WINDOWS = u"Are there mosquito nets on beds and windows?"
NOTES_ON_SLEEPING_FACILITIES = u"Notes on sleeping facilities:"

HOME_SECUTITY = u"Home security:"
FIRE_SAFETY = u"Fire safety:"
VENTILATION = u"Ventilation:"
CHILD_PROTECTION_POLICY = u"Child protection policy:"
FIRST_AID_BOX_AVAILABLE = u"Is there a first aid box?"
QUALITY_OF_FIRST_AID_BOX_CONTENTS = u"Quality of contents of first aid box:"
SICK_BAY_PRESENT = u"Is there a sick bay?"
TRAINED_PERSONNEL_IN_SICK_BAY = u"Trained medical personnel in sick bay?"
SICK_BAY_CONDITION = u"Condition of sick bay:"
CHILDREN_IN_NEED_OF_MEDICAL_TX_TAKEN_TO = u"Where are the children taken when they are in need of medical treatment?"
DISTANCE_TO_NEAREST_MEDICAL_FACILITY = u"Approximate distance to nearest medical facility in metres:"
HOW_ARE_CHILDREN_TRANSPORTED_TO_MED_FACILITY = u"How are children transported to medical facility?"
NOTES_ON_SLEEPING_SAFETY = u"Notes on safety:"

RECREATION_FACILITIES = u"Recreation facilities:"
MENTAL_GAMES = u"Mental games:"
PHYSICAL_GAMES = u"Physical games:"
INDOOR_GAMES = u"Indoor games:"
OUTDOOR_GAMES = u"Outdoor games:"
ARE_GAMES_AGE_APPROPRIATE = u"Are games and toys age appropriate?"
NOTES_ON_RECREATIONAL_ACTIVITIES_FACILITIES = u"Notes on recreational activities / facilities:"

FORMAL_EDUCATION_OFFERED = u"Formal educational programmes offered:"
HIGHEST_GRADE_OFFERED = u"Highest grade offered:"
EDUCATIONAL_REHABILITATION_PROGRAMES_OFFERED = u"Skills (non-formal) offered:"
NOTES_ON_EDUCATIONAL_REHABILITATION_PROGRAMMES = u"Notes on educational/rehabilitation programmes offered:"
'''
NOTES_ON_INFRASTRUCTURE = u"Notes on infrastructure:"
'''

''' Section F '''
WORK_PLAN = u"Work plan:"
ADMINISTRATIVE_RECORDS = u"Administrative records:"
BANK_ACCOUNT = u"Bank account:"
NOTES_ON_ADMINISTRATION = u"Notes of Administration:"
SOURCES_OF_INCOME = u"Sources of income:"
NOTES_ON_SOURCES_OF_INCOME = u"Notes of sources of income:"
''' Section G '''
GENERAL_REMARKS = u"General remarks on institution:"
INSPECTOR_RECOMMENDATIONS = u"Recommendations by inspector:"
ADHERENCE_TO_MINIMUM_STANDARDS = u"Is the institution adhering to minimum standards?"

''' Errors '''
ERROR_MESSAGE_SAVE_NOT_SUCCESSFUL = 'Save not successful - please make corrections below'
ERROR_MESSAGE_DATE_LESS_THAN_DATE_OPERATIONAL = u'The date you entered is less than the date operational for this institution'
ERROR_MESSAGE_DATE_GREATER_THAN_DATE_CLOSED = u'The date you entered is greater than the date closed for this institution'
ERROR_MESSAGE_MINIMUM_AGE_RANGE = u'The age must be between 0 and 17'
ERROR_MESSAGE_MAXIMUM_AGE_RANGE = u'The age must be between 0 and 80'
ERROR_MESSAGE_INSTITUTIONAL_CAPACITY = u'The value must be greater than 0'
ERROR_MESSAGE_DATE_GREATER_THAN_FORM_DATE = u'The date cannot be after the date of this Form'
ERROR_MESSAGE_DATE_LESS_THAN_CHILD_BDAY = u'The date cannot be before the child\'s date of birth'
ERROR_MESSAGE_DATE_GREATER_THAN_CHILD_DEATH_DATE = u'The date cannot be after the child\'s recorded date of death'
ERROR_MESSAGE_DATE_GREATER_THAN_DATE_LEFT = u'The first date admitted cannot be after the date left'
ERROR_MESSAGE_PERSON_FILLED_PAPER_REQUIRED = u'Please select the workforce member/user who filled out this form'
ERROR_MESSAGE_MINIMUM_AGE_GT_MAXIMUM_AGE = u'The minimum age of enrollment cannot be greater the maximum age of enrollment'
ERROR_MESSAGE_ENTER_NUMBER = u'Please enter a number greater than 0'