   
'''
Created on Sep 29, 2014

@author: PKaumba
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import fields_list_provider as db_fieldchoices, validators as validate_helper
from ovc_main.utils.general import convert_to_int_array, get_list_from_dic_or_querydict
from functools import partial
from django.conf import settings
from ovc_main.utils.auth_access_control_util import del_form_field, filter_fields_for_display
import datetime
import child_in_conflict_with_law_labels as form_label
from ovc_main.beneficiary_registration import load_ben_from_id as reg
from django.core.validators import MinValueValidator, MaxValueValidator
from ovc_main.utils.custom_template_tags import CustomDiv
#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

answer_set_ids_add_empty_list = {22:False, 14:True, 23:True, 24:True, }
form_db_lookups = db_fieldchoices.get_answer_set(answer_set_ids = answer_set_ids_add_empty_list)

class ChildInConflictLawForm(forms.Form):
    ovc_subject = None
    form_type = forms.CharField(required = False,widget=forms.HiddenInput)
    subject = forms.CharField(required = False,widget=forms.HiddenInput)
    org_options = None
    wfc_id = "String there"
    wfc_string = None
    
    form_id = forms.CharField(required = False,widget=forms.HiddenInput)
    wfc_id_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    social_welfare_officer_managing_case_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    hidden_wfc_id_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    social_wfc_id_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    court_wfc_id_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    court_data_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    social_data_hidden = forms.CharField(required = False,widget=forms.HiddenInput)
    org_assessing_child = forms.CharField(required = False,widget=forms.HiddenInput)
    
    #Section A
    social_welfare_officer_managing_case = forms.CharField(label=form_label.SOCIAL_WELFARE_MANAGING,widget=forms.TextInput(attrs={'class':'autocompletewfc'}))
    Q098 = forms.CharField(max_length=4, required = False, label=form_label.EDU_LEVEL_OF_CHILD)
    Q099 = forms.DateField(widget=DateInput('%d-%B-%Y'),label=form_label.DATE_OF_ARREST, input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    Q100 = forms.CharField(max_length=255,required = False,label=form_label.REFFERING_POLICE_STATION)
    Q101 = forms.IntegerField(min_value=1, max_value=999, required = False, label=form_label.STATION_CALL_SIGN_NUMBER)
    Q102 = forms.RegexField(required = False, label=form_label.POLICE_CASE_DOCKET_NUM, regex=r'^\b\d{1,}\/\b\d{2}\/\d{4}$',error_message = (form_label.REGEX_ERROR))
    Q103 = forms.DateField(widget=DateInput('%d-%B-%Y'),label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    Q104 = forms.DateField(widget=DateInput('%d-%B-%Y'),label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    district = forms.ChoiceField(label=form_label.LOCATION_OF_OFFENSE_DIST, choices=db_fieldchoices.get_list_of_districts())
    ward = forms.ChoiceField(label=form_label.LOCATION_OF_OFFENSE_WARD, choices=db_fieldchoices.get_list_of_wards())
    Q105 = forms.MultipleChoiceField(label=form_label.TYPE_OF_OFFENSE, choices = form_db_lookups[22], widget=forms.SelectMultiple(attrs={'class':'multiselect-searchable'}))#forms.ChoiceField(required = False, label=form_label.TYPE_OF_OFFENSE, choices = form_db_lookups[22])#
    Q106 = forms.CharField(max_length=255,required = False, label=form_label.NOTES_ON_OFFENSE)
    Q107 = forms.ChoiceField(required = False, label=form_label.RECEIVED_BAIL, choices = form_db_lookups[14])
    Q108 = forms.CharField(max_length=255,required = False, label=form_label.NOTES_ON_DETENTION_BAIL)
    Q109 = forms.ChoiceField(required = False, label=form_label.METHOD_OF_DISPOSAL_BY_POLICE, choices = form_db_lookups[23])
    
    #Section B
    social_date = forms.DateField(required = False,widget=DateInput('%d-%B-%Y'),label=form_label.SOCIAL_DATE, input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    social_welfare_officer = forms.CharField(required = False,label=form_label.SOCIAL_WELFARE_OFFICER,widget=forms.TextInput(attrs={'class':'autocompletefrmwfc'}))
    social_who_was_interviewed = forms.CharField(max_length=255,required = False, label=form_label.SOCIAL_WHO_WAS_INTERVIEWED)
    social_notes = forms.CharField(max_length=255,required = False, label=form_label.SOCIAL_NOTES)
    social_recommendations = forms.CharField(max_length=255,required = False, label=form_label.SOCIAL_RECOMMENDATIONS)
    
    court_date = forms.DateField(required = False,widget=DateInput('%d-%B-%Y'),label=form_label.COURT_DATE, input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    court_welfare_officer = forms.CharField(required = False,label=form_label.COURT_WELFARE_OFFICER,widget=forms.TextInput(attrs={'class':'autocompletefrmwfc'}))
    court = forms.ChoiceField(required = False, label=form_label.COURT, choices = db_fieldchoices.get_item_id_and_description_for_cat(True, 'Encounter type with notes - court',True))
    name_of_judge = forms.CharField(max_length=255,required = False, label=form_label.NAME_OF_JUDGE)
    investigations_ordered = forms.CharField(max_length=255,required = False, label=form_label.INVESTIGATIONS_ORDERED)
    notes = forms.CharField(max_length=255,required = False, label=form_label.COURT_NOTES)
    Q110 = forms.CharField(max_length=255,required = False, label=form_label.COURT_CASE_NUM)
    
    #Section C
    Q111 = forms.ChoiceField(required = False, label=form_label.OUTCOME, choices = form_db_lookups[24])
    Q112 = forms.CharField(max_length=255,required = False, label=form_label.NOTES_ON_OUTCOME)
    Q113 = forms.DateField(required = False, widget=DateInput('%d-%B-%Y'),label=form_label.DATE_CASE_DISPOSED, input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    
    def __init__(self, user,*args, **kwargs):
        self.user = user
        submitButtonText = form_label.SAVE
        super(ChildInConflictLawForm, self).__init__(*args, **kwargs)
        
        org_options = None
        
        if user.is_superuser or user.has_perm('auth.enter frm high sensitive'):
            self.fields['social_welfare_officer_managing_case'] = forms.CharField(label=form_label.SOCIAL_WELFARE_MANAGING,widget=forms.TextInput(attrs={'class':'autocompletewfc'}))
        else:
            selected_wfc = ''
            selected_wfc_id = ''
            if user.reg_person:
                selected_wfc = "%s \t %s \t %s"% (user.reg_person.workforce_id,user.reg_person.national_id,user.reg_person.full_name)
                selected_wfc_id = '%s'%(user.reg_person.pk)
            self.fields['social_welfare_officer_managing_case'] = forms.CharField(required = False,label=form_label.SOCIAL_WELFARE_MANAGING,widget=forms.TextInput(attrs={'class':'autocompletewfc','disabled':'disabled','value':selected_wfc}))
            self.fields['wfc_id_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc_id}))
            self.fields['social_welfare_officer_managing_case_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc}))
            self.fields['hidden_wfc_id_hidden'] = forms.CharField(required = False,widget=forms.TextInput(attrs={'value':selected_wfc_id}))
        
        if args[0] and 'org_options' in args[0]:
            org_opt = get_list_from_dic_or_querydict(args[0], 'org_options')
            org_options = org_opt
            submitButtonText = form_label.UPDATE
            
        if not org_options:
            org_options = db_fieldchoices.get_list_of_cwacs(all_orgs=True)
            
        self.fields['district'] = forms.ChoiceField(label=form_label.LOCATION_OF_OFFENSE_DIST, choices=db_fieldchoices.get_list_of_districts())
        self.fields['ward'] = forms.ChoiceField(label=form_label.LOCATION_OF_OFFENSE_WARD, choices=db_fieldchoices.get_list_of_wards())
        
        self.helper = FormHelper()
                            
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = '/capture/add_form/'
        self.helper.layout = Layout(
            Fieldset(
                        '',
                        Div(
                            Div(HTML("""
                                        <div class="panel-heading">"""+form_label.LABEL_HEAD_CHILD_DETAILS+"""</div>
                                        <div class="panel-body pan mandatory" ><p/> 
                                                <div class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.LABEL_ID+"""</dt>
                                                      <dd>{{subject.beneficiary_id}}</dd>
                                                     </dl>
                                                     <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.LABEL_FIRST_NAME+"""</dt>
                                                      <dd>{{subject.first_name}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.LABEL_SEX+"""</dt>
                                                      <dd>{{subject.sex}}</dd>
                                                    </dl>
                                                    
                                                     
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.LABEL_BIRTH_CERT+"""</dt>
                                                      <dd>{{subject.birth_cert_num}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.LABEL_SURNAME+"""</dt>
                                                      <dd>{{subject.last_name}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.LABEL_DOB+"""</dt>
                                                      <dd>{{subject.date_of_birth}}</dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt>"""+form_label.LABEL_COMMUNITY+"""</dt>
                                                      <dd>{{subject.residence.community}}</dd>
                                                      <dt>"""+form_label.LABEL_WARD+"""</dt>
                                                      <dd>{{subject.residence.ward}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-4'>
                                                      <dt> </dt>
                                                      <dd> </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                        """
                                         ),
                                    css_class='panel panel-primary',
                                ),
                            ),
                        Div(
                                Div(HTML(form_label.LABEL_HEAD_SEC_A),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'social_welfare_officer_managing_case',
                                    'Q098',
                                    HTML(""" 
                                    <input id="id_subject" name="subject" type="hidden" value="{{subject.id_int}}">
                                    """),
                                    'Q099',
                                    'Q100',
                                    'form_type',
                                    'Q101',
                                    'Q102',
                                    'district',
                                    'ward',
                                    Div(
                                        Div(
                                            HTML('<div class="col-md-5"><span class="pull-right">'+form_label.START_DATE+'*'+'</span></div>'),
                                            Field('Q103', css_class='col-md-1'),
                                            css_class = 'col-md-6',
                                        ),
                                        Div(
                                            HTML('<div class="col-md-2"><span>'+form_label.END_DATE+'*'+'</span></div>'),
                                            Field('Q104', css_class='col-md-2'),
                                            css_class = 'col-md-4',
                                        ),
                                        style='margin-left:7%',
                                        css_class = 'col-md-10 form-inline',
                                    ),
                                    Div('wfc_id_hidden',css_class='hide'),
                                    'Q105',
                                    'Q106',
                                    'Q107',
                                    'Q108',
                                    'Q109',
                                    'org_assessing_child',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                       
                        Div(
                            Div(HTML(form_label.LABEL_HEAD_SEC_B),
                                css_class = 'panel-heading'
                                ),
                            Div(
                                    HTML('<p/>'),
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Social welfare</b></u></font></span>'),
                                    'social_date',
                                    HTML("<label id=\"id_error_social_date\" style=\"color:red;margin-left:300px;display:None\"><strong>  </strong></label>"),
                                    'social_welfare_officer',
                                    HTML("<label id=\"id_error_social_welfare_officer\" style=\"color:red;margin-left:300px;display:None\"><strong>  </strong></label>"),
                                    'social_who_was_interviewed',
                                    'social_notes',
                                    'social_data_hidden',
                                    'social_recommendations',
                                    'social_wfc_id_hidden',
                                    Div(
                                    HTML("<br><a type=\"button\" id=\"id_welfare_add\" class=\"btn btn-success\">"+form_label.TH_SOCIAL_ADD_BUTTON+"</a><br><br>"),
                                    style='margin-left:45%',
                                    ),
                                    Div(
                                        HTML("""
                                            <table id="id_socialTable" border="1" style="table-layout:fixed;overflow-y: hidden;" class="table table-hover table-striped table-bordered table-advanced tablesorter socialTable">
                                                <thead>
                                                <tr>
                                                    <th style="visibility: hidden; width: 1%;"></th>
                                                    <th>"""+form_label.TH_SOCIAL_DATE+"""</th>
                                                    <th>"""+form_label.TH_SOCIAL_WELFARE_OFFICER+"""</th>
                                                    <th>"""+form_label.TH_SOCIAL_WHO_WAS_INTERVIEWED+"""</th>
                                                    <th>"""+form_label.TH_SOCIAL_NOTES+"""</th>
                                                    <th>"""+form_label.TH_SOCIAL_RECOMMENDATIONS+"""</th>
                                                    <th>"""+form_label.TH_SOCIAL_EDIT+"""</th>
                                                    <th>"""+form_label.TH_SOCIAL_REMOVE+"""</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {% if beneficiary.guardian  %}
                                                {% for guardn in beneficiary.guardian %}
                                                    <tr>
                                                        <td id = "1"></td>
                                                        <td id = "2"></td>
                                                        <td id = "3"></td>
                                                        <td id = "4"> </td>
                                                        <td id = "5"> </td>
                                                        <td id = "6"> </td>
                                                        <td id = "7"> <button onclick='myEditFunction()' class='editbutton'><i class='fa fa-trash fa-2'></i></button></td>
                                                        <td id = "8"> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>
                                                    </tr>
                                                {% endfor %}
                                                {% endif %}
                                                </tbody>
                                            </table>
                                            
                                        """),
                                    css_class = 'col-md-11 panel-body pan',
                                    style = 'margin-left:50px',
                                    ),
                                
                                    HTML('<p/>'),
                                    HTML('<span class="col-md-12""><font size="3"><u><b>Court appearances</b></u></font></span>'),
                                    'court_date',
                                    HTML("<label id=\"id_error_court_date\" style=\"color:red;margin-left:300px;display:None\"><strong>  </strong></label>"),
                                    'court_welfare_officer',
                                    HTML("<label id=\"id_error_court_welfare_officer\" style=\"color:red;margin-left:300px;display:None\"><strong>  </strong></label>"),
                                    'court',
                                    'name_of_judge',
                                    'investigations_ordered',
                                    'court_data_hidden',
                                    'notes',
                                    'court_wfc_id_hidden',
                                    Div(
                                    HTML("<br><a type=\"button\" id=\"id_court_add\" class=\"btn btn-success\">"+form_label.TH_COURT_ADD_BUTTON+"</a><br><br>"),
                                    style='margin-left:45%',
                                    ),
                                    Div(
                                        HTML("""
                                            <table id="id_courtTable" border="1" style="table-layout:fixed;overflow-y: hidden;"  class="table table-hover table-striped table-bordered table-advanced tablesorter socialTable">
                                                <thead>
                                                <tr>
                                                    <th style="visibility: hidden; width: 1%;"></th>
                                                    <th style="visibility: hidden; width: 1%;"></th>
                                                    <th>"""+form_label.TH_COURT_DATE+"""</th>
                                                    <th>"""+form_label.TH_COURT_WELFARE_OFFICER+"""</th>
                                                    <th>"""+form_label.TH_COURT+"""</th>
                                                    <th>"""+form_label.TH_NAME_OF_JUDGE+"""</th>
                                                    <th>"""+form_label.TH_INVESTIGATIONS_ORDERED+"""</th>
                                                    <th>"""+form_label.TH_COURT_NOTES+"""</th>
                                                    <th>"""+form_label.TH_COURT_EDIT+"""</th>
                                                    <th>"""+form_label.TH_COURT_REMOVE+"""</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {% if beneficiary.guardian  %}
                                                {% for guardn in beneficiary.guardian %}
                                                    <tr>
                                                        <td id = "1"></td>
                                                        <td id = "2"></td>
                                                        <td id = "3"></td>
                                                        <td id = "4"> </td>
                                                        <td id = "5"> </td>
                                                        <td id = "6"> </td>
                                                        <td id = "7"> </td>
                                                        <td id = "8"> </td>
                                                        <td id = "9"> <button onclick='myEditFunction()' class='editbutton'><i class='fa fa-trash fa-2'></i></button></td>
                                                        <td id = "10"> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>
                                                    </tr>
                                                {% endfor %}
                                                {% endif %}
                                                </tbody>
                                            </table>
                                            
                                            <!-- Button trigger modal -->
                                            <button id="id_docket_confirm_dialog_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                              Launch modal
                                            </button>
                                            
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                              <div class="modal-dialog">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Invalid Docket Number</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    The entered date month and month and year for the case docket number is different from the current month and year, are you sure you want to continue?
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                    <button class="btn btn-success" id="submit">Yes</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            
                                        """),
                                    css_class = 'col-md-11 panel-body pan',
                                    style = 'margin-left:50px',
                                    ),
                                    Div('hidden_wfc_id_hidden',css_class='hide'),
                                    'Q110',
                                    css_class = 'panel-body pan',
                                ),
                                css_class='panel panel-primary',
                            ), 
                       Div(
                                Div(HTML(form_label.LABEL_HEAD_SEC_C),
                                    css_class = 'panel-heading',
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Div('social_welfare_officer_managing_case_hidden',css_class='hide'),
                                    'Q111',
                                    'Q112',
                                    'Q113',
                                    'form_id',
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary'
                            ),
                       Div(
                        HTML("<Button id='submit_child_conflict_form' onclick=\"customValidate(event)\" type=\"submit\" style=\"margin-right:4px\" class=\"btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+ submitButtonText +"</Button>"),
                        HTML("<a type=\"button\" href=\"/capture/forms_home/\"class=\"btn btn-success\"><i class=\"fa fa-undo\"></i> "+form_label.CANCEL+"</a><br><br>"),
                        style='margin-left:38%'
                        ),
            )
        )
        
    def clean(self):
        if self._errors:
            self._errors["__all__"] = self.error_class([u"Child in conflict with the law form not saved - please correct the errors in red below"])
            
        return self.cleaned_data
