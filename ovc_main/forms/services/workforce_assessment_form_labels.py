from django.conf import settings

label_head_child_details = 'Workforce member details'
label_id = '%s ID: ' % settings.APP_NAME
nrc = 'NRC:'
label_first_name = 'First name:'
label_surname = 'Surname:'
label_sex = 'Sex:'
label_dob = 'Date of birth:'
label_current_primary_org_unit = 'Current primary parent organisational unit:'
label_other_parent_org_unit = 'Other parent organisational unit(s):'
label_communities_where_working = 'Communities where working:'
label_geo_areas_where_working = 'Geographical areas where working:'

label_sec_A = "Section A"
label_assessor_perf_assessment = 'Assessor performing assessment'
label_org_unit = 'Organisational unit'
label_date_assess_comp = 'Date assessment completed'
label_wfc_job_title = "Workforce member's current job title / role"

label_sec_B = "Section B - Child protection"
label_read_signed_org_policy_child_protection = "Has read and signed their organisation's policy on child protection"
label_read_signed_confience_policy = 'Has read and signed confidentiality policy'
label_crime_clearance = 'Criminal record clearance'
label_crime_details = 'Criminal record details'

label_sec_C = "Section C - Education and training"
label_literacy_level = 'Literacy level'
label_low_literacy_worker = 'If low literacy - mentor/paired worker'
label_high_school_level_attained = 'Highest school education level attained (school grade)'
label_further_qualification = 'Further qualifications attained / tertiary education'
label_childcare_training_received = 'Childcare specific training received'

label_sec_D = "Section D - Work experience"
label_num_of_years_experience = "Number of years' experience in social work / working with children or vulnerable families:"
label_details_experience = "Details of experience in social work / working with children or vulnerable families"

label_sec_E = "Section E - Training needs"
label_training_needs_assessment = 'Training needs assessment'
label_priority_areas = 'Priority areas for training needs'

label_button_cancel = 'Cancel'
label_button_save = 'Save'
label_button_update = 'Update'


