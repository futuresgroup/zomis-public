   
'''
Created on Sep 29, 2014

@author: PKaumba
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import fields_list_provider as db_fieldchoices, validators as validate_helper
from functools import partial
from ovc_main.models import RegPerson
from django.conf import settings
from ovc_main.utils.auth_access_control_util import del_form_field, filter_fields_for_display
import datetime
import guardianforms_labels as form_label
from ovc_main.utils.general import convert_to_int_array, get_list_from_dic_or_querydict
#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

permission_type_fields = {  'rel_org':
                                {        
                                'auth.update ben contact by org':['physical_address','mobile_phone_number','email_address', 'edit_mode_hidden', 'pk_id'],
                                'auth.update ben general by org':['ben_district','ben_ward','community',
                                                                  'date_of_death','location_change_date','prev_loc_change_date','edit_mode_hidden', 'pk_id']
                                },
        
                            'other':
                                {
                                 'auth.update ben special':['first_name', 'last_name', 'other_name', 'sex', 'date_of_birth','dontskip','birth_cert_num','nrc','edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled','guardians_hidden'],
                                 'auth.update ben contact by org':['physical_address','mobile_phone_number','email_address','dontskip', 'edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled','guardians_hidden'],
                                'auth.update ben general by org':['ben_district','ben_ward','community',
                                                                  'date_of_death','location_change_date','prev_loc_change_date','dontskip', 'edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled','guardians_hidden']
                                }
                          }

class GuardianRegistrationForm(forms.Form):
    is_update_page = False
    ben_id = ''
    is_update = forms.CharField(required=False, widget=forms.HiddenInput())
    beneficiary_id = forms.CharField(label=form_label.label_id, required=False, initial = ben_id, widget=forms.HiddenInput())
    pk_id =  forms.CharField(label=form_label.label_person_id, required=False, widget=forms.HiddenInput())
    
    #Identification information
    first_name = forms.CharField(label=form_label.label_first_name)
    last_name = forms.CharField(label=form_label.label_surname)
    other_name = forms.CharField(required=False, label=form_label.label_other_names)
    sex = forms.ChoiceField(label=form_label.label_sex,choices=db_fieldchoices.get_sex_list())
    date_of_birth = forms.DateField(widget=DateInput('%d-%B-%Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_guardian_date_of_birth])
    nrc = forms.RegexField(regex=r'^\b\d{6}\/\b\d{2}\/\b[1-3]{1}$',error_message = (form_label.error_message_nrc),label=form_label.label_nrc, required=False)
    
    #Where the guardian lives
    ben_district = forms.ChoiceField(label=form_label.label_district, choices=db_fieldchoices.get_list_of_districts())
    ben_ward = forms.ChoiceField(label=form_label.label_ward, choices=db_fieldchoices.get_list_of_wards())
    community = forms.ChoiceField(label=form_label.label_community, choices=db_fieldchoices.get_list_of_cwacs())
    
    #Guardians contact details
    mobile_phone_number = forms.RegexField(required=False,label=form_label.label_mobile_phone,regex=validate_helper.mobile_phone_number_regex, error_message = (form_label.error_message_mobile_phone_number))
    email_address = forms.EmailField(required=False, label=form_label.label_email_address)
    physical_address = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 4}), label=form_label.label_physical_address)
    
    #Paper Trail
    workforce_member = forms.CharField(required=False, label=form_label.label_workforce_member)
    date_paper_form_filled = forms.DateField(required=False, widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    location_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    date_of_death = forms.DateField(required=False, widget=DateInput('%d-%B-%Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    edit_mode_hidden = forms.CharField(required=False)
    
    res_id_hidden = forms.CharField(required=False)
    
    prev_loc_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
        
    def __init__(self, user, *args, **kwargs):
        self.user = user        
        submitButtonText = form_label.label_button_save
        do_display_status = 'display:None'
        dont_display_status = ''
        actionurl = '/beneficiary/guardian/'
        header_text = """<div class="note note-info"><h4><strong>OVC Guardian Registration (ChildTrack form 3a)</strong>
        </h4><h5><p>This form is for registering guardians (ie primary caregivers) of orphans and vulnerable children. A guardian or primary caregiver is a 
        parent, a relative who is responsible for taking care of the basic needs of the child, or an adoptive or foster parent</h5></div>"""
        is_update_page = False
        if 'is_update_page' in kwargs:
            do_display_status = ''
            self.is_update_page = kwargs.pop('is_update_page')
            if self.is_update_page:
                header_text = """<div class="note note-info"><h4><strong>File Update Guardian Registration (ZOMIS form 3a)</strong></h4></div>"""
                actionurl = '/beneficiary/update_ben/'
                submitButtonText = form_label.label_button_update
                dont_display_status = 'display:None'
                
                if args and 'ben_district' in args[0]:
                    districtid = args[0]['ben_district']
                
                wardsoptionlist = ''
                if args and 'ben_ward' in args[0]:
                    wardid = get_list_from_dic_or_querydict(args[0], 'ben_ward')
                    
                    if args and 'community' in args[0]:
                        communities = get_list_from_dic_or_querydict(args[0], 'community')
                
        #if self.is_update_page:
            #self.fields['date_paper_form_filled'] = forms.DateField(widget=DateInput('%d %B, %Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
        #else:
            #self.fields['date_paper_form_filled'] = forms.DateField(required=False, widget=DateInput('%d %B, %Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
        
        super(GuardianRegistrationForm, self).__init__(*args, **kwargs)
        
        if self.is_update_page:
            self.fields['ben_district'] = forms.ChoiceField(label=form_label.label_district, choices=db_fieldchoices.get_list_of_districts())
            self.fields['ben_ward'] = forms.ChoiceField(label=form_label.label_ward, choices=db_fieldchoices.get_list_of_wards(districtid))
            wards = db_fieldchoices.get_list_of_wards(districtid)
            self.fields['community'] = forms.ChoiceField(label=form_label.label_community, choices=db_fieldchoices.get_list_of_cwacs(wards))
        else:
            self.fields['ben_district'] = forms.ChoiceField(label=form_label.label_district, choices=db_fieldchoices.get_list_of_districts())
            self.fields['ben_ward'] = forms.ChoiceField(label=form_label.label_ward, choices=db_fieldchoices.get_list_of_wards())
            self.fields['community'] = forms.ChoiceField(label=form_label.label_community, choices=db_fieldchoices.get_list_of_cwacs())
        
        
        today = datetime.datetime.now().date()
        dpff = datetime.datetime.strptime(str(today), '%Y-%m-%d')
        date_paper_form = dpff.strftime('%d-%B-%Y')
        
        self.fields['location_change_date'].initial = date_paper_form
        self.fields['date_paper_form_filled'].initial = date_paper_form
        #self.fields['date_of_death'].initial = date_paper_form
        #self.fields['location_change_date'].widget.attrs['readonly'] = True
        #self.fields['date_paper_form_filled'].widget.attrs['readonly'] = True
        #self.fields['date_of_death'].widget.attrs['readonly'] = True
        #self.fields['date_of_birth'].widget.attrs['readonly'] = True
        
        self.helper = FormHelper()
        
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = actionurl
        self.helper.layout = Layout(
            MultiField(

                        '',
                        
                        Div(
                                Div(
                                    HTML('<p/>'),
                                    HTML("""<div id="div_id_beneficiary_id" class="form-group" style=\""""+do_display_status+"""\">
                                                <label for="id_benficiary_id" class="control-label col-md-3">"""+form_label.label_heading_beneficairy_id+"""</label>
                                                <div>
                                                  <p class="form-control-static col-md-6"><strong>{{beneficiary.beneficiary_id}}</strong></p>
                                                </div>
                                            </div>"""),
                                    'beneficiary_id',
                                    'pk_id',
                                    'nrc',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),  
                        Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="1" checked="checked"/> %s' % form_label.label_heading_update_guardian_details
                                    ),
                                    css_class = 'panel-heading clearfix'
                                    ),
                                css_class='panel panel-primary',
                                style = do_display_status,
                            ),                      
                        Div(
                                Div(HTML(form_label.label_heading_guardian_identification),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'first_name',
                                    'last_name',
                                    AppendedText('other_name','<span data-toggle="tooltip" title="'+form_label.label_tooltip_other_name+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'sex',
                                    AppendedText('date_of_birth','<span data-toggle="tooltip" title="'+form_label.label_tooltip_date_of_birth+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                            Div(HTML('<span class="col-md-3">%s</span>' %form_label.label_heading_where_guardian_lives),
                                    Div(
                                            Div(
                                                Div(
                                                    HTML('<div class="col-md-3"><a href=\"/organisation/new/\" target=\"_blank\"><u>'+form_label.label_register_org_unit+'</u>.</a></div>'),
                                                    Field('location_change_date', template='ovc_main/custom_date_field.html', style = do_display_status)
                                                )
                                            )
                                        ),
                                    css_class = 'panel-heading col-md-12'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    Field(Div('edit_mode_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    'ben_district',
                                    'ben_ward',
                                    'community',
                                    'physical_address',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                                Div(HTML(form_label.label_heading_guadian_contact_details),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Field(Div('prev_loc_change_date', style='visibility:hidden'), css_class='form-inline'),
                                    'mobile_phone_number',
                                    'email_address',
                                    Field(Div('res_id_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary',
                        
                            ),
                            Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="2"/> '+form_label.label_option_died),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Div('date_of_death', css_class='dead_control'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary',
                                style = do_display_status,
                            ),
                            Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="3"/> %s' % form_label.label_option_guardian_never_existed),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                    ),
                                css_class='panel panel-primary',
                            ),
                            Div(
                                Div(HTML(form_label.label_heading_paper_trail),
                                    Div(
                                        Div(
                                            HTML('<a href=\"/workforce/new_workforce/\" target=\"_blank\">'+form_label.label_register_workforce_member+'</u></a>'),
                                            css_class = 'form-inline'
                                            ),
                                        css_class= 'toolbars'
                                    ),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    'is_update',
                                    Field('workforce_member', style="display:inline", css_class="autocompletewfc form-inline", id="id_workforce_member"),
                                    'date_paper_form_filled',
                                    Field(Div('res_id_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    css_class = 'panel-body pan'
                                    ),
                                Div(
                                    Div(
                                        HTML("""
                                                <table id="paperTrailTable"  class="table table-hover table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>"""+form_label.label_person_recorded+"""</th>
                                                        <th>"""+form_label.label_paper_date+"""</th>
                                                        <th>"""+form_label.label_person_recorded_electronically+"""</th>
                                                        <th>"""+form_label.label_electronic_time+"""</th>
                                                        <th>"""+form_label.label_interface+"""</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {% if beneficiary.audit_trail  %}
                                                    {% for audit in beneficiary.audit_trail %}
                                                        <tr>
                                                            <td id = "1">{{ audit.Person_recorded_on_paper }}</td>
                                                            <td id = "2">{{ audit.Paper_date }}</td>
                                                            <td id = "3">{{ audit.Person_recorded_electronically }}</td>
                                                            <td id = "4">{{ audit.Electronic_date_time }}</td>
                                                            <td id = "5">{{ audit.Interface }}</td>
                                                        </tr>
                                                    {% endfor %}
                                                    {% endif %}
                                                    </tbody>
                                                </table>
                                                
                                                <!-- Button trigger modal -->
                                                <button id="confirm_dialog_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                                  Launch demo modal
                                                </button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Duplicate Person</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        """+form_label.error_message_duplicate_name+"""
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button id="id_cancel_submit" type="button" class="btn btn-default" data-dismiss="modal">"""+form_label.label_button_make_changes+"""</button>
                                                        <button class="btn btn-success" id="submit">"""+form_label.label_button_save_anyway+"""</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                
                                                <!-- Button trigger modal -->
                                                <button id="duplicate_dialog_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#dupModal">
                                                  Launch demo modal
                                                </button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="dupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Duplicate Person</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                        """+form_label.error_message_sure_void+"""
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button id="id_cancel_submit" type="button" class="btn btn-default" data-dismiss="modal">"""+form_label.label_button_no+"""</button>
                                                        <button class="btn btn-success" id="submit">"""+form_label.label_button_yes+"""</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                
                                                <input id="ben_wfc_name_hidden" type="hidden">
                                                <input id="ben_wfc_id_hidden" type="hidden">
                                                <input id="ben_wfc_name_national_id" type="hidden">
                                      """),
                                    )
                                ),
                                css_class='mandatory panel panel-primary',
                                ),
                       Div(
                            HTML("<Button id='submit_child_reg' onclick=\"updateFinaleGuardian(event)\" type=\"submit\" style=\"margin-right:4px\" class=\"mandatory btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+submitButtonText+"</Button>"),
                            HTML("<a type=\"button\" href=\"/beneficiary/ben_search/\"class=\"mandatory btn btn-success\"><i class=\"fa fa-undo\"></i> "+form_label.label_button_cancel+"</a><br><br>"),
                            style='margin-left:38%'
                        ),
                        
            )
        )
        
        if self.is_update_page:
            layout = self.helper.layout
            searchitems = filter_fields_for_display(self.user, permission_type_fields)
            print searchitems, 'search items'
            if searchitems:
                del_form_field(layout, searchitems)
                self.helper.layout = layout
        
    def clean(self):
        if (self.cleaned_data.get('location_change_date') != None):
            entered_value = self.cleaned_data.get('location_change_date')
            if (self.cleaned_data.get('prev_loc_change_date') != None):
                date_linked_value = self.cleaned_data.get('prev_loc_change_date')
                print 'date_linked_value', date_linked_value
                print 'entered_value', entered_value
                if entered_value < date_linked_value:
                    self._errors["location_change_date"] = self.error_class([u""+form_label.error_message_location_change_date])
                
        if self.is_update_page:
                if (self.cleaned_data.get('date_of_birth') != None):
                    dob = self.cleaned_data.get('date_of_birth')
                    if (self.cleaned_data.get('date_of_death') != None):
                        dod = self.cleaned_data.get('date_of_death')
                        pk_pers = self.cleaned_data.get('pk_id')
                        most_recent_change_date = None
                        mrcd=None
                        if pk_pers:
                            most_recent_change_date = db_fieldchoices.get_most_recent_record_date_for_person_id(pk_pers)
                            print 'most_recent_change_date',most_recent_change_date
                            if most_recent_change_date:
                                mrcd = most_recent_change_date.date()
                        print 'pk_pers',pk_pers
                        print 'mrcd',mrcd   
                        if dod:
                            if dod < dob:
                                self._errors["date_of_death"] = self.error_class([u""+form_label.error_message_death_before_birth])
                            elif mrcd:
                                if dod < mrcd:
                                    self._errors["date_of_death"] = self.error_class([u""+form_label.error_message_death_before_last_beneficiary_date])
                                        
        if (self.cleaned_data.get('nrc') != None or self.cleaned_data.get('first_name') != None or self.cleaned_data.get('last_name') != None):
            
            nrc_id = self.cleaned_data.get('nrc')
            f_name = self.cleaned_data.get('first_name')
            l_name = self.cleaned_data.get('last_name')
            
            if nrc_id or (f_name and l_name):
                if(RegPerson.objects.filter(national_id=nrc_id,is_void=False,date_of_death = None).count() > 0):
                    person = RegPerson.objects.filter(national_id=nrc_id,is_void=False,date_of_death = None)
                    m_person = None
                    for m_per in person:
                        m_person = m_per
                    
                    pk_db = int(m_person.pk)
                    pk_entered = None
                    if (self.cleaned_data.get('pk_id') != None):
                        if self.cleaned_data.get('pk_id'):
                            pk_entered = int(self.cleaned_data.get('pk_id'))
                    
                    if (self.is_update_page and (pk_db == pk_entered)):
                        do_nothing = None
                    else:
                        if m_person.national_id:
                            msg = "[%s],[%s],[%s, %s, %s] %s"% (m_person.national_id,m_person.beneficiary_id,m_person.first_name,m_person.surname,m_person.other_names,form_label.error_message_duplicate_last_part)
                        else:
                            msg = form_label.error_message_duplicate_nrc
                        if m_person.national_id:
                            self._errors["nrc"] = self.error_class([u""+msg])
                                
                    '''
                    if(RegPerson.objects.filter(first_name=f_name,surname=l_name,is_void=False,date_of_death = None).count() > 0):
                        person = RegPerson.objects.filter(first_name=f_name,surname=l_name,is_void=False,date_of_death = None)
                        m_person = None
                        for m_per in person:
                            m_person = m_per
                        
                        if m_person:
                            msg = "["+m_person.national_id+"],["+m_person.beneficiary_id+"], ["+m_person.first_name+", "+m_person.surname+", "+m_person.other_names+"] is already in this registry."# Do you want to save?"
                        else:
                            msg = "This person is already in the system."
                        print ValidationError
                        self._errors["nrc"] = self.error_class([u""+msg])
                    '''
                            
        if self._errors:
            self._errors["__all__"] = self.error_class([u""+form_label.error_message_page_errors])
            
                            
        return self.cleaned_data
