from django.db import models
from datetime import datetime
# Create your models here.
class RegOrgUnit(models.Model):
    org_unit_id_vis = models.CharField(max_length=12)
    org_unit_name = models.CharField(max_length=255)
    org_unit_type_id = models.CharField(max_length=4)
    is_gdclsu  = models.BooleanField()
    date_operational = models.DateField(null=True)
    date_closed = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    parent_org_unit_id = models.CharField(max_length=12, null=True, blank=True)


    def _is_active(self):
        if self.date_closed:
            return False
        else:
            return True

    is_active = property(_is_active)

    class Meta:
        db_table = 'tbl_reg_org_units'

    def make_void(self, date_closed=None):
        self.is_void = True
        if date_closed:
            self.date_closed = date_closed
        super(RegOrgUnit, self).save()



class RegOrgUnitContact(models.Model):
    #organisation_unit_id = models.CharField(max_length=7)
    org_unit = models.ForeignKey(RegOrgUnit)
    contact_detail_type_id = models.CharField(max_length=20)
    contact_detail = models.CharField(max_length=255)
    is_void = models.BooleanField(default=False)
    class Meta:
        db_table = 'tbl_reg_org_units_contact'

class RegOrgUnitExternalID(models.Model):
    #organisation_unit_id = models.CharField(max_length=7)
    org_unit = models.ForeignKey(RegOrgUnit)
    identifier_type_id = models.CharField(max_length=4)
    identifier_value = models.CharField(max_length=255, null=True)
    is_void = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_reg_org_units_external_ids'

class RegOrgUnitGdclsu(models.Model):
    #organisation_unit_id = models.CharField(max_length=7)
    org_unit = models.ForeignKey(RegOrgUnit)
    gdclsu_id = models.CharField(max_length=12)
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_reg_org_units_gdclsu'

class RegOrgUnitGeography(models.Model):
    #org_unit_id = models.CharField(max_length=7)
    org_unit = models.ForeignKey(RegOrgUnit)
    area_id = models.IntegerField()
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_reg_org_units_geo'

    def make_void(self, date_delinked=None):
        self.is_void = True
        if date_delinked:
            self.date_delinked = date_delinked
        elif not self.date_delinked:
            self.date_delinked = datetime.now().date()
        super(RegOrgUnitGeography, self).save()

class RegPerson(models.Model):
    #person_id = models.CharField(primary_key=True,max_length=32)
    beneficiary_id = models.CharField(max_length=10, null=True, blank=True)
    workforce_id = models.CharField(max_length=8, null=True, blank=True)
    birth_reg_id = models.CharField(max_length=15, null=True, blank=True)
    national_id = models.CharField(max_length=15, null=True, blank=True)
    first_name = models.CharField(max_length=255)
    other_names = models.CharField(max_length=255, null=True, blank=True)
    surname = models.CharField(max_length=255)
    date_of_birth = models.DateField()
    date_of_death = models.DateField(null=True, blank=True)
    sex_id = models.CharField(max_length=4)
    is_void = models.BooleanField(default=False)

    def _get_full_name(self):
        return  '%s %s %s' % (self.first_name, self.other_names, self.surname)

    def make_void(self):
        self.is_void = True
        super(RegPerson,self).save()

    def record_death(self,date_of_death=None):
        if date_of_death:
            self.date_of_death = date_of_death
        super(RegPerson,self).save()

    full_name  = property(_get_full_name)
    class Meta:
        db_table = 'tbl_reg_persons'

class SetupGeorgraphy(models.Model):
    area_id = models.IntegerField()
    area_type_id = models.CharField(max_length=50)
    area_name = models.CharField(max_length=100)
    parent_area_id = models.IntegerField(null=True)
    area_name_abbr = models.CharField(max_length=5, null=True)
    timestamp_created = models.DateTimeField(auto_now=True, default=datetime.now())
    timestamp_updated = models.DateTimeField(auto_now=True, default=datetime.now())
    is_void = models.BooleanField(default=False)
    class Meta:
        db_table = 'tbl_list_geo'

class SetupList(models.Model):
    item_id = models.CharField(max_length=4)
    item_description = models.CharField(max_length=255)
    item_description_short = models.CharField(max_length=26, null=True)
    item_category = models.CharField(max_length=255, null=True, blank=True)
    the_order = models.IntegerField(null=True)
    user_configurable = models.BooleanField(default=False)
    sms_keyword = models.BooleanField(default=False)
    is_void = models.BooleanField(default=False)
    field_name = models.CharField(max_length=200, null=True, blank=True)
    timestamp_modified = models.DateTimeField(auto_now=True, default=datetime.now())
    class Meta:
        db_table = 'tbl_list_general'

class AdminPreferences(models.Model):
    person = models.ForeignKey(RegPerson)
    preference_id = models.CharField(max_length=4)

    class Meta:
        db_table = 'tbl_admin_preferences'


class RegPersonsGuardians(models.Model):
    child_person = models.ForeignKey(RegPerson)
    guardian_person_id = models.CharField(max_length=10)
    relationship_notes = models.CharField(max_length=255)
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_reg_persons_guardians'

class RegPersonsTypes(models.Model):
    person = models.ForeignKey(RegPerson)
    person_type_id = models.CharField(max_length=4)
    date_began = models.DateField(null=True)
    date_ended = models.DateField(null=True)
    is_void = models.BooleanField(default=False)

    def make_void(self,person_type_change_date=None):
        self.is_void = True
        if person_type_change_date:
            self.date_ended = person_type_change_date
        super(RegPersonsTypes,self).save()

    class Meta:
        db_table = 'tbl_reg_persons_types'

class RegPersonsGeo(models.Model):
    person = models.ForeignKey(RegPerson)
    area_id = models.IntegerField()
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)

    def make_void(self,date_delinked, is_void):
        if date_delinked:
            self.date_delinked=date_delinked
        self.is_void=True
        super(RegPersonsGeo,self).save()

    class Meta:
        db_table = 'tbl_reg_persons_geo'

class RegPersonsGdclsu(models.Model):
    person = models.ForeignKey(RegPerson)
    gdclsu = models.ForeignKey(RegOrgUnit)
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)

    def make_void(self,date_delinked, is_void=True):
        if date_delinked:
            self.date_delinked=date_delinked
        self.is_void=True
        super(RegPersonsGdclsu,self).save()

    class Meta:
        db_table = 'tbl_reg_persons_gdclsu'

class RegPersonsExternalIds(models.Model):
    person = models.ForeignKey(RegPerson)
    identifier_type_id = models.CharField(max_length=4)
    identifier = models.CharField(max_length=255)
    is_void = models.BooleanField(default=False)

    def make_void(self):
        self.is_void = True
        super(RegPersonsExternalIds,self).save()

    class Meta:
        db_table = 'tbl_reg_persons_external_ids'

class RegPersonsContact(models.Model):
    person = models.ForeignKey(RegPerson)
    contact_detail_type_id = models.CharField(max_length=4)
    contact_detail = models.CharField(max_length=255)
    is_void = models.BooleanField(default=False)

    def make_void(self):
        self.is_void = True
        super(RegPersonsContact,self).save()

    class Meta:
        db_table = 'tbl_reg_persons_contact'

class RegPersonsOrgUnits(models.Model):
    person = models.ForeignKey(RegPerson)
    parent_org_unit = models.ForeignKey(RegOrgUnit)
    primary = models.BooleanField(default=False)
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)

    def make_void(self,parent_org_change_date=None):
        self.is_void = True
        if parent_org_change_date:
            self.date_delinked = parent_org_change_date
        super(RegPersonsOrgUnits,self).save()

    class Meta:
        db_table = 'tbl_reg_persons_org_units'

class CoreAdverseConditions(models.Model):
    beneficiary = models.ForeignKey(RegPerson)
    adverse_condition_id = models.CharField(max_length=4)
    is_void = models.BooleanField(default=False)
    sms_id = models.IntegerField(null=True)
    form_id = models.IntegerField(null=True)

    class Meta:
        db_table = 'tbl_core_adverse_conditions'

class CoreServices(models.Model):
    workforce = models.ForeignKey(RegPerson, related_name='service_workforce')
    beneficiary = models.ForeignKey(RegPerson, related_name='service_beneficiary')
    encounter_date = models.DateField()
    core_item_id = models.CharField(max_length=4)

    class Meta:
        db_table = 'tbl_core_services'

class CoreEncounters(models.Model):
    workforce = models.ForeignKey(RegPerson, related_name='encounter_workforce')
    beneficiary = models.ForeignKey(RegPerson, related_name='encouner_beneficiary')
    encounter_date = models.DateField()
    org_unit_id = models.CharField(max_length=8)
    area_id = models.IntegerField()
    encounter_type_id = models.CharField(max_length=4)
    sms_id = models.IntegerField(null=True)
    form_id = models.IntegerField(null=True)

    class Meta:
        db_table = 'tbl_core_encounters'

class AdminCaptureSites(models.Model):
    #id = models.IntegerField(primary_key=True)
    org_unit_id = models.CharField(max_length=7, null=True, blank=True)
    capture_site_name = models.CharField(max_length=255, null=True, blank=True)
    date_installed = models.DateField(null=True, blank=True)
    approved = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_admin_capture_sites'

class Forms(models.Model):
    form_guid = models.CharField(max_length=64)
    form_type_id = models.CharField(max_length=4, null=True)
    form_subject_id = models.CharField(max_length=15, null=True)
    form_area_id = models.IntegerField(null=True)
    date_began = models.DateField(null=True)
    date_ended = models.DateField(null=True)
    date_filled_paper = models.DateField(null=True)
    person_id_filled_paper = models.IntegerField(null=True)
    org_unit_id_filled_paper = models.CharField(max_length=8, null=True)
    capture_site_id = models.IntegerField(null=True, blank=True)
    timestamp_created = models.DateTimeField(null=True)
    user_id_created = models.CharField(max_length=9, null=True)
    timestamp_updated = models.DateTimeField(null=True)
    user_id_updated = models.CharField(max_length=9, null=True)
    is_void = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_forms'

class AdminDownload(models.Model):
    capture_site_id = models.IntegerField(null=True, blank=True)
    section_id = models.CharField(max_length=4, null=True)
    timestamp_started = models.DateTimeField(null=True)
    timestamp_completed = models.DateTimeField(null=True)
    number_records = models.IntegerField(null=True)
    request_id = models.CharField(max_length=64, null=True)
    success = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_admin_download'

class AdminUploadForms(models.Model):
    form = models.ForeignKey(Forms)
    timestamp_uploaded = models.DateTimeField(null=True)

    class Meta:
        db_table = 'tbl_admin_upload_forms'

class ListQuestions(models.Model):
    question_text = models.CharField(max_length=255, null=True, blank=True)
    question_code = models.CharField(max_length=50)
    form_type_id = models.CharField(max_length=4, null=True, blank=True)
    answer_type_id = models.CharField(max_length=4, null=True, blank=True)
    answer_set_id = models.IntegerField(db_index=True, null=True)
    the_order = models.IntegerField(db_index=True, null=True)
    timestamp_modified = models.DateTimeField(auto_now=True, null=True)
    is_void = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_list_questions'

class ListAnswers(models.Model):
    answer_set_id = models.IntegerField(db_index=True, null=True)
    answer = models.CharField(max_length=255, null=True, blank=True)
    the_order = models.IntegerField(db_index=True, null=True)
    timestamp_modified = models.DateTimeField(auto_now=True, null=True)
    is_void = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_list_answers'

class FormGenAnswers(models.Model):
    form = models.ForeignKey(Forms)
    question = models.ForeignKey(ListQuestions)
    answer_id = models.ForeignKey(ListAnswers, null=True)

    class Meta:
        db_table = 'tbl_form_gen_answers'

class FormGenText(models.Model):
    form = models.ForeignKey(Forms)
    question = models.ForeignKey(ListQuestions)
    answer_text = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = 'tbl_form_gen_text'

class FormGenNumeric(models.Model):
    form = models.ForeignKey(Forms)
    question = models.ForeignKey(ListQuestions)
    answer = models.IntegerField(null=True)

    class Meta:
        db_table = 'tbl_form_gen_numeric'

class FormCsi(models.Model):
    form = models.ForeignKey(Forms)
    domain_id = models.CharField(max_length=4, null=True, blank=True) #TODO part of composite key
    score = models.IntegerField(null=True, blank=True)
    observations = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = 'tbl_form_csi'

class FormPersonParticipation(models.Model):
    form = models.ForeignKey(Forms)
    workforce_or_beneficiary_id = models.CharField(max_length=15)
    participation_level_id = models.CharField(max_length=4, null=True, blank=True)

    class Meta:
        db_table = 'tbl_form_person_participation'

class FormCommInterventions(models.Model):
    form = models.ForeignKey(Forms)
    intervention_id = models.CharField(max_length=4, null=True, blank=True)
    org_unit_id = models.CharField(max_length=7, null=True, blank=True)
    status_id = models.CharField(max_length=4, null=True, blank=True)
    activity_report = models.CharField(max_length=255, null=True, blank=True)
    no_beneficiariies = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'tbl_form_comm_interventions'

class FormOrgUnitContributions(models.Model):
    form = models.ForeignKey(Forms)
    org_unit_id = models.CharField(max_length=7)#TODO part of composite key
    contribution_id = models.CharField(max_length=4) #TODO part of composite key

    class Meta:
        db_table = 'tbl_form_org_unit_contributions'

class FormFinance(models.Model):
    form = models.ForeignKey(Forms)
    org_unit_id_received = models.CharField(max_length=7, null=True, blank=True)
    amount = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'tbl_form_finance'

class FormResChildren(models.Model):
    form = models.ForeignKey(Forms, null=True)
    child_person_id = models.CharField(max_length=10, null=True, blank=True)
    institution_id = models.CharField(max_length=7, null=True, blank=True)
    residential_status_id = models.CharField(max_length=4, null=True, blank=True)
    date_admitted = models.DateField(null=True, blank=True)
    date_left = models.DateField(null=True, blank=True)
    sms_id = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'tbl_form_res_children'

class FormResWorkforce(models.Model):
    form = models.ForeignKey(Forms)
    workforce_id = models.CharField(max_length=8, null=True, blank=True)
    institution_id = models.CharField(max_length=7, null=True, blank=True)
    position_id = models.CharField(max_length=4, null=True, blank=True)
    full_part_time_id = models.CharField(max_length=4, null=True, blank=True)

    class Meta:
        db_table = 'tbl_form_res_workforce'

class RegPersonsWorkforceIds(models.Model):
    person = models.ForeignKey(RegPerson)
    workforce_id = models.CharField(max_length=8, null=True)

    class Meta:
        db_table = 'tbl_reg_persons_workforce_ids'
        
class RegPersonsBeneficiaryIds(models.Model):
    person = models.ForeignKey(RegPerson)
    beneficiary_id = models.CharField(max_length=10, null=True)

    class Meta:
        db_table = 'tbl_reg_persons_beneficiary_ids'

class CaptureTaskTracker(models.Model):
    id = models.AutoField(primary_key=True)
    task_id = models.CharField(max_length=64, null=True)
    operation = models.CharField(max_length=8, null=True)
    timestamp_started = models.DateTimeField(auto_now=True, default=datetime.now())
    timestamp_completed = models.DateTimeField(null=True)
    completed = models.BooleanField(default=False)

    class Meta:
        db_table = 'tbl_capture_task_tracker'