from django.conf import settings


label_heading_child_ident_info = 'Child identification information'
label_heading_where_child_lives = 'Where the child lives'
label_heading_adverse_conditions = 'Adverse conditions (Choose as many as apply, either now or in the past)'
label_heading_child_contact_details = 'Child contact details'
label_heading_register_workforce = 'Register workforce member'
label_heading_paper_trail = 'Paper trail'


label_option_update_beneficiary_details = 'Update beneficiary details'
label_option_died = 'Deceased'
label_option_ovc_never_existed = 'OVC never existed (record is a registration mistake or a duplicate)'


label_beneficiary_id = '%s beneficiary ID' % settings.APP_NAME
label_birth_registration_number = 'Birth certificate number '
label_nrc = 'NRC number '
label_guardian = 'Guardian '
label_register_org_unit = 'Register an organizational unit'
label_childs_guardian = "Child's guardians"
label_register_guardian = 'Register a guardian'
label_ben_identification = 'NRC or %s beneficiary ID' % settings.APP_NAME
label_name = 'Name'
label_notes_on_relationship = "Notes on guardian's relationship with child"
label_use_contact_details = 'Use contact details'
# label_steps_ovc_number = 'STEPS OVC ID' label change for political reasons
label_steps_ovc_number = 'Other ID'

label_person_recorded = 'Person recorded on paper'
label_paper_date = 'Date recorded on paper'
label_person_recorded_electronically = 'Person recorded electronically'
label_electronic_time = 'Date and time recorded electronically'
label_interface = 'Interface'


label_workforce_member = 'Workforce member recorded on paper '
label_district = 'District '
label_ward = 'Ward '
label_community = 'Community (CWAC or GDCLSU) '
label_mobile_number = 'Mobile phone number '
label_email_address = 'Email address '
label_residential_institution = 'Residential institution '
label_first_name = 'First name '
label_last_name = 'Surname '
label_other_names = 'Other names '
label_sex = 'Sex '
label_physical_address = 'Physical address / Description of where living '


label_tooltip_other_name = 'Any other names which are either in official documents or are commonly used to refer to the person'
label_tooltip_date_of_birth = 'If unknown, put an estimate'

error_message_page_errors = "OVC not saved - please correct the errors in red below"
error_message_nrc = "NRC number must be in the format 987654/32/1"
error_message_birth_cert_num = "Birth certificate number must be in the format PRO/123456/YYYY"
error_message_duplicate_nrc = "This NRC has already been associated with another user, workforce member or beneficiary in this system. You cannot register this NRC again."
error_message_duplicate_last_part = "is already in this registry."
error_message_duplicate_name = "There is already a person with the first name and surname you entered, are you sure you want to proceed?"
error_message_death_before_birth = "Date of death cannot be before the date of birth"
error_message_death_before_last_beneficiary_date = "Date of death cannot be before the last record date of the beneficiary"
error_message_location_change_date = "Location change date cannot be before the previous time the location was changed"
error_message_guardian_change_date = "Guardian change date cannot be before the previous time the guardian was changed"
error_message_mobile_phone = "Phone number must be entered in the format: '+260977123456'. Up to 10 digits allowed."
error_message_no_guardians = "At least one guardian must be chosen unless the tick box \"no adult guardians\" is chosen"
error_message_no_adverse_condition = "Please select at least one adverse condition for this child"
error_message_sure_void = "Are you sure this person never existed (ie this record is a mistake or a duplicate record)?"
label_tooltip_guardian = "Guardians are parent(s), the relative(s) who are taking care of the child, or the child's adoptive or foster parent(s). <br/> If the child's primary caregiver(s) or guardians(s) are not already registered in ZOMIS, register the guardian first and then register the child.</p>"

label_button_add = 'Add'
label_button_remove = 'Remove'
label_button_cancel = 'Cancel'
label_button_save = 'Save' 
label_button_update = 'Update'
label_button_make_changes = 'Make changes'
label_button_save_anyway = 'Save anyway'
label_button_yes = 'Yes'
label_button_no = 'No'