from django.conf import settings
from ovc_main.utils.fields_list_provider import get_capture_site_name, get_capture_site_id

def site_info(request):
    return {'is_capture_site': settings.IS_CAPTURE_SITE,
            'app_name': settings.APP_NAME,
            'capture_id': get_capture_site_id(),
            'capture_name': get_capture_site_name(),
            'debug': settings.DEBUG}
            
#CAPTURE_SITE_ID = get_capture_site_id()
#CAPTURE_SITE_NAME = get_capture_site_name()

