(function ($) {
	//http://stackoverflow.com/questions/24353748/jquery-set-min-max-input-in-option-type-number
    $.fn.restrict = function () {
        return this.each(function(){
            if (this.type && 'number' === this.type.toLowerCase()) {
                $(this).on('change', function(){
                    var _self = this,
                        v = parseFloat(_self.value),
                        min = parseFloat(_self.min),
                        max = parseFloat(_self.max);
                    if (v >= min && v <= max){
                        _self.value = v;
                    }
                    else {
                        _self.value = v < min ? min : max;
                    }
                });
            }
        });
    };
})(jQuery);