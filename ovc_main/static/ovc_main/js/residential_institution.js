$(document).ready(function () {
    get_reported_by_options = function ()
    {
        $.ajax({
            dataType: "json",
            url: "/forms/get_reported_by_options?org_id_int=" + $('#id_subject').val(),
            success: function (results) {
                if (results.length === 0) {
                    return;
                }
                else {
                    $("#id_Q032").empty();
                    $.each(results, function (index) {
                        $("#id_Q032").append(
                                "<option value='" + results[index].value + "'>" + results[index].text + "</option>"
                                );
                    });
                }
            },
            error: function () {
            }
        });
    };
    if ($("#id_Q032").find('option:selected').val() === '') {
        get_reported_by_options();
    }

    get_institution_workforce = function ()
    {
        $.ajax({
            dataType: "json",
            url: "/workforce/get_workforce_for_res_institution?date=" + $("#id_date_of_form").val()
                    + "&org_id=" + $("#id_org_system").val() + "&form_id=" + $("#id_form_id").val(),
            success: function (results) {
                if (results.length === 0) {
                    $('#workforceStatusTable tbody').html("<tr><td colspan='4'>No workforce members found</td></tr>");
                    return;
                }
                else {
                    $.each(results, function (index) {
                        $("#workforceStatusTable > tbody").append(
                                "<tr>" +
                                "<td id = 'workforce_id'>" + results[index].workforce_id + "</td> " +
                                "<td id = 'workforce_name'>" + results[index].workforce_name + "</td> " +
                                "<td id = 'workforce_role'><select id = 'roles_select'>" +
                                get_role_html(results[index].role_id) +
                                "</select></td>" +
                                "<td id = 'workforce_employment_status'><select id = 'employment_status_select'>" +
                                get_employment_html(results[index].employment_status_id) +
                                "</select></td> " +
                                "</tr>"
                                );

                    });
                }
            },
            error: function () {
                $('#workforceStatusTable tbody').html("<tr><td colspan='4'>An error ocurred why loading the data, please refresh the page</td></tr>");
            }
        });
    };

    function get_role_html(role_id) {
        var roles_html = '';
        $('#id_workforce_roles option').each(function () {
            var item_html = '';
            if (role_id === $(this).val()) {
                item_html = "<option value='" + $(this).attr('value') + "' selected>" + $(this).text() + "</option>"
            } else {
                item_html = "<option value='" + $(this).attr('value') + "'>" + $(this).text() + "</option>"
            }
            roles_html = roles_html + item_html;
        });
        return roles_html;
    }

    function get_employment_html(employment_status_id) {
        var employment_status = '';
        $('#id_workforce_employment_status option').each(function () {
            var item_html = '';
            if (employment_status_id === $(this).val()) {
                item_html = "<option value='" + $(this).attr('value') + "' selected>" + $(this).text() + "</option>"
            } else {
                item_html = "<option value='" + $(this).attr('value') + "'>" + $(this).text() + "</option>"
            }
            employment_status = employment_status + item_html;
        });
        return employment_status;
    }

    //TODO place this code in a 'date of form' change event
    if ($('#id_workforce_data').val() === '') {
        get_institution_workforce();
    } else {
        populateWorkforceTable();
    }

    function populateWorkforceTable() {
        var workforce_data = $("#id_workforce_data").val();
        if (workforce_data === '') {
            get_institution_workforce();
            return;
        }
        var workforce_data_items = JSON.parse(workforce_data);
        $("#workforceStatusTable tbody").html('');
        $.each(workforce_data_items, function (index) {
            var workforce_id = workforce_data_items[index].workforce_id;
            var workforce_name = workforce_data_items[index].workforce_name;
            var workforce_role = workforce_data_items[index].workforce_role;
            var workforce_employment_status = workforce_data_items[index].workforce_employment_status;

            populateWorkforceRow(workforce_id, workforce_name, workforce_role, workforce_employment_status);
        });
    }

    function populateWorkforceRow(workforce_id, workforce_name, workforce_role, workforce_employment_status) {
        $("#workforceStatusTable").find('tbody')
                .append($('<tr>')
                        .append($('<td id="workforce_id" class="workforcedata">')
                                .append($('<label>')
                                        .text(workforce_id)
                                        )
                                )
                        .append($('<td id="workforce_name" class="workforcedata">')
                                .append($('<label>')
                                        .text(workforce_name)
                                        )
                                )
                        .append($('<td id="workforce_role" class="workforcedata">')
                                .append($('<select id = \'roles_select\'>')
                                        .html(get_role_html(workforce_role))
                                        )
                                )
                        .append($('<td id="workforce_employment_status" class="workforcedata">')
                                .append($('<select id = \'employment_status_select\'>')
                                        .html(get_employment_html(workforce_employment_status))
                                        )
                                )
                        );
    }

    /*
     $('.iCheck-helper').click(function(e){
     console.warn(e);
     console.warn(e.currentTarget.previousSiblingElement);
     $('input[id='+e.id+']').iCheck('uncheck');
     }); */

    get_force_user_filled_form = function ()
    {
        $.ajax({
            dataType: "json",
            url: "/workforce/res_wfc_autocomplete?wfc_name=",
            success: function (results) {
                if (results.length === 0) {
                    return;
                }
                else {

                    $.each(results, function (index) {
                        if (results[0][4] === true) {
                            $('#id_user_workforce_filled_form').val(concatenatearray(results[0], 1, 3, '\t'));
                            $('#id_user_workforce_filled_form').attr('disabled', 'disabled');
                            $('#id_person_filled_form_text').val(concatenatearray(results[0], 1, 3, '\t'));
                            $('#id_user_workforce_filled_form_hidden').val(results[0][0]);                            
                        }
                    });
                }
            },
            error: function () {
            }
        });
    };
    
    if ($('#id_person_filled_form_text').val() === '') {
        get_force_user_filled_form();
    }else{
        $('#id_user_workforce_filled_form').val($('#id_person_filled_form_text').val());
        $('#id_user_workforce_filled_form').attr('disabled', 'disabled');
    }

    $(".residential_institute_autocompletewfc").autocomplete({
        source: function (request, response) {
            $.getJSON("/workforce/res_wfc_autocomplete?wfc_name=" + request.term, function (data) {
                response($.map(data, function (value, key) {
                    return {
                        label: concatenatearray(value, 1, 3, '\t'),
                        value: value
                    };
                }));
            });
        },
        minLength: 1,
        delay: 100,
        select: function (event, ui) {
            event.preventDefault();
            var result = concatenatearray(ui.item.value, 1, 3, '\t');
            $("#id_user_workforce_filled_form").val(result);
            $('#id_user_workforce_filled_form_hidden').val(ui.item.value[0]);
        },
    });

    $("#id_user_workforce_filled_form").on("autocompleteselect", function (event, ui) {
        var result = concatenatearray(ui.item.value);
        $("#id_user_workforce_filled_form").val(result);
    });

    $(".residential_institute_autocompletechild").autocomplete({
        source: function (request, response) {
            $.getJSON("/beneficiary/res_child_autocomplete?child_name=" + request.term, function (data) {
                response($.map(data, function (value, key) {
                    return {
                        label: concatenatearray(value, 1, 3, '\t'),
                        value: value
                    };

                }));
            });
        },
        minLength: 1,
        delay: 100,
        select: function (event, ui) {
            event.preventDefault();
            $('#id_child_id').val(ui.item.value[0]);
            $('#id_beneficiary_id').val(ui.item.value[1]);
            $('#id_child_name').val(ui.item.value[3]);
            $('#id_child_date_of_birth').val(ui.item.value[4]);
            $('#id_child_date_of_death').val(ui.item.value[5]);
            $('#id_recent_date_admitted').val(ui.item.value[6]);
            $('#id_recent_date_left').val(ui.item.value[7]);
            $('#id_date_first_admitted').val($('#id_recent_date_admitted').val());
        },
    });

    $("#id_child_autocomplete").on("autocompleteselect", function (event, ui) {
        var result = concatenatearray(ui.item.value, 1, 3, '\t');
        $("#id_child_autocomplete").val(result);
    });

    concatenatearray = function (tmparray, startindex, lastindex, separator)
    {
        var charseparator = separator ? separator : '\t';
        var sindex = startindex ? startindex : 1;
        var eindex = lastindex < tmparray.length ? lastindex : tmparray.length - 1;
        var result = [];
        for (i = sindex; i <= eindex; i++) {
            if (tmparray[i] === null || tmparray[i] === '')
                continue;
            result.push(tmparray[i]);
        }

        return result.join(charseparator)
    }

    //1.    If "resident in institution"(RSRS) is chosen, make "family status" and "court 
    //      committal" fields visible
    //2.    Date first admitted is invisible if residential status is "Receiving services from residential 
    //      institution, but was never resident"(RSNV). 
    //3.    If any previous records of the child in this institution in tbl_forms_res_children 
    //      have a date admitted, copy that value in here by default (if more than 
    //      one, take the one from the most recent form)
    $('#id_current_residential_status').change(function () {
        if ($('#id_current_residential_status').val() === "RSRS") {
            $('#div_id_family_status').show();
            $('#div_id_has_court_committal_order').show();
            $('#div_id_date_first_admitted').show();
            $('#id_date_first_admitted').val($('#id_recent_date_admitted').val());
            $('#div_id_date_left').hide();
            $('#id_date_left').val('');
        }
        else if ($('#id_current_residential_status').val() === "RSNV") {
            $('#div_id_date_first_admitted').hide();
            $('#id_date_first_admitted').val('');
            $('#div_id_date_left').hide();
            $('#div_id_date_left').val('');
            $('#div_id_family_status').hide();
            $('#div_id_has_court_committal_order').hide();
        }
        else if ($('#id_current_residential_status').val() === "RSRI" ||
                $('#id_current_residential_status').val() === "RSTR" ||
                $('#id_current_residential_status').val() === "RSLF") {
            $('#div_id_date_left').show();
            $('#id_date_left').val($('#id_recent_date_left').val());
            $('#div_id_date_first_admitted').show();
            $('#div_id_family_status').hide();
            $('#div_id_has_court_committal_order').hide();
        } 

        clearControls();
    });

    function clearControls() {
        $("#div_id_family_status select").val("");
        $("#div_id_has_court_committal_order select").val("");
    }

    if ($('#id_workforce_type').val() === "RSRS") {
        $('#div_id_family_status').show();
        $('#div_id_has_court_committal_order').show();
    } else {
        $('#div_id_family_status').hide();
        $('#div_id_has_court_committal_order').hide();
    }

    // Inspector recommendations and Adherence to minimum standards: 
    // Only visible if "Inspection report by external inspector"
    $('#id_Q032').change(function () {
        if ($('#id_Q032').val() === "42") {
            $('#div_id_Q065_066').show();
        } else {
            $('#div_id_Q065_066').hide();
        }
    });
    if ($('#id_Q032').val() === "42") {
        $('#div_id_Q065_066').show();
    } else {
        $('#div_id_Q065_066').hide();
    }



});

