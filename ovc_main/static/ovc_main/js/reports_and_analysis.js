$(document).ready(function () {
	var runningReport = null;
	$("#id_report_type").on( "change", function(){
		$("#id_run").removeAttr("hide");
		resetAllControls();
		var selectedReport = $("#id_report_type").val();
		$.ajax({
            dataType: "json",
            url: "/reports/report_params?report_id=" + selectedReport,
            contentType: "application/json; charset=utf-8",
            success: function (report_params) {
                $.each(report_params, function (key, value) {
                	switch(key)
                    {
                        case "item_id":
                        	get_set_control_attributes(value,"id_item_id");
                        	break;
                        case "question_id":
	                    	get_set_control_attributes(value,"id_question");
	                    	break;
                        case "answer_id":
	                    	get_set_control_attributes(value,"id_answer");
	                    	break;
                        case "area_id":
                        	$("#id_control_to_visual").val('visible');
	                    	get_set_control_attributes(value,"id_geo_area");
	                    	break;
                        case "org_unit_id":
	                    	get_set_control_attributes(value,"id_org_unit");
	                    	break;
	                    case "set_id":
	                    	get_set_control_attributes(value,"id_sub_units");
	                    	break;
	                    case "from_date":
	                    	get_set_control_attributes(value,"id_from");
	                    	break;
	                    case "to_date":
	                    	get_set_control_attributes(value,"id_to");
	                    	break;
	                    case "from_date_comp":
	                    	get_set_control_attributes(value,"id_from_c");
	                    	break;
	                    case "to_date_comp":
	                    	get_set_control_attributes(value,"id_to_c");
	                    	break;
                        default:
                                break;
                    } 
                });
            }
        });
	});
	
	resetAllControls();
	
	function resetAllControls(){
		$( "#div_id_item_id").hide();
		$( "#div_id_question").hide();
		$( "#div_id_answer").hide();
		$( "#div_id_geo_area").hide();
		$( "#div_id_org_unit").hide();
		$( "#div_id_sub_units").hide();
		$( "#div_id_from").hide();
		$( "#div_id_to").hide();
		$( "#div_id_from_c").hide();
		$( "#div_id_to_c").hide();
		$("#id_control_to_visual").val('');
		$("#id_item_id").prop('required', false);
		$("#id_question").prop('required', false);
		$("#id_answer").prop('required', false);
		$("#id_geo_area").prop('required', false);
		$("#id_org_unit").prop('required', false);
		$("#id_sub_units").prop('required', false);
		$("#id_from").prop('required', false);
		$("#id_to").prop('required', false);
		$("#id_to_c").prop('required', false);
		$("#id_from_c").prop('required', false);
		
		$("#id_item_id").removeAttr("disabled");
		$("#id_question").removeAttr("disabled");
		$("#id_answer").removeAttr("disabled");
		$("#id_geo_area").removeAttr("disabled");
		$("#id_org_unit").removeAttr("disabled");
		$("#id_sub_units").removeAttr("disabled");
		$("#id_from").removeAttr("disabled");
		$("#id_to").removeAttr("disabled");
		$("#id_to_c").removeAttr("disabled");
		$("#id_from_c").removeAttr("disabled");
		
		$("#id_item_id").val('');
		$("#id_question").val('');
		$("#id_answer").val('');
		$("#id_geo_area").val('');
		$("#id_org_unit").val('');
		$("#id_sub_units").val('');
		$("#id_from").val('');
		$("#id_to").val('');
		$("#id_to_c").val('');
		$("#id_from_c").val('');
	}
	
	
	function get_set_control_attributes(value, control_name){
		$.each(value, function (key1, value1) {
    		switch(key1)
    		{
    		case 'filter':
    			if(value1 != null){
	    			$("#"+control_name).empty();
	    			$('<option>').val("").text("-----").appendTo("#"+control_name);
	    			$.each(value1, function (key2,value2) {
	    				$('<option>').val(key2).text(value2).appendTo("#"+control_name);		
	        		});
	        		$("#"+control_name).selectpicker('refresh');
    			}
    			break;
    			
    		case 'initially_visible':
        		if(value1)
        		{
        			$( "#div_"+control_name).show();
        		}else{
        			$( "#div_"+control_name).hide();
        		}
    			break;
    			
    		case 'required':
    			if(value1)
        		{
        			$("#"+control_name).prop('required', true);
        		}else{
        			$("#"+control_name).prop('required', false);
        		}
    			$("#div_id_"+control_name).removeClass("has-error");
    			$("#div_id_"+control_name+"_error >span").remove();
    			break;
    			
    		case 'label':
    			if(value1 != null && value1 != "")
    			{
    				$("#"+control_name + "_label").text(value1);
    			}
    			break;
    			
    		case 'default':
    			$("#"+control_name).val(value1);
    			break;
    			
    		default:
    			break;
    		}
    	});
    }
	
    function clearResults(){
    	$("#reportResults > thead").html("");
    	$("#reportResults > tbody").html("");
    	$("#reportResults > tbody").html("<tr><td colspan='1'>no results</td></tr>");
    }
    
	
    function cannotSelectTwoControls(selectedControl,firstControlToHide,secondControlToHide)
    {
    	if($("#"+selectedControl).val() != "" && $("#"+selectedControl).val() != null)
		{
			$("#"+firstControlToHide).prop("disabled",true);
			$("#"+secondControlToHide).prop("disabled",true);
			$("#"+firstControlToHide).val('');
			$("#"+secondControlToHide).val('');
		}
		else
		{
			$("#"+firstControlToHide).val('');
			$("#"+secondControlToHide).val('');
			$("#"+firstControlToHide).removeAttr("disabled");
			$("#"+secondControlToHide).removeAttr("disabled");
		}	
    }
    
    $("#id_report_type").on('change', function(){
    	clearResults();
    	clearFormValid();
    	var changedControl = "";
    	var firstControlToHide = "";
    	var secondControlToHide = "";
    	var selectedReport = $("#id_report_type").val();
    	switch(selectedReport)
    	{
    	case "1":
    		changedControl = "id_item_id";
			firstControlToHide = "id_org_unit";
			secondControlToHide = "id_sub_units";
			onChangeMethod(changedControl,firstControlToHide,secondControlToHide);
			break;
    	case "2":
    	case "4":
    	case "5":
    	case "7":
    	case "8":
    	case "9":
    	case "12":
    	case "13":
    	case "14":
    	case "28":
    	case "30":
    		changedControl = "id_geo_area";
			firstControlToHide = "id_org_unit";
			secondControlToHide = "id_sub_units";
			onChangeMethod(changedControl,firstControlToHide,secondControlToHide);
			break;
    	case "27":
    		
    		changedControl = "id_org_unit";
			firstControlToHide = "id_sub_units";
			onChangeWorkforce(changedControl,firstControlToHide)
			onChangeWorkforce(firstControlToHide,changedControl)
    		break;
    	default:
    		break;
    	}
    });
    
    function onChangeWorkforce(changedControl,firstControlToHide){
    	$('.'+changedControl).live('change',function(){
			if($("#"+changedControl).val() != "" && $("#"+changedControl).val() != null)
			{
				$("#"+firstControlToHide).prop("disabled",true);
				$("#"+firstControlToHide).val('');
			}
			else
			{
				$("#"+firstControlToHide).val('');
				$("#"+firstControlToHide).removeAttr("disabled");
			}
		});
    }
    
    
    function onChangeMethod(firstControlToHide,secondControlToHide,thirdControlToHide)
    {
    	wireChangeMethod(firstControlToHide,secondControlToHide,thirdControlToHide);
    	wireChangeMethod(secondControlToHide,firstControlToHide,thirdControlToHide);
    	wireChangeMethod(thirdControlToHide,firstControlToHide,secondControlToHide);
    };
    
    function wireChangeMethod(firstControlToHide,secondControlToHide,thirdControlToHide)
    {
    	$('.'+firstControlToHide).live('change',function(){
	    	cannotSelectTwoControls(firstControlToHide,secondControlToHide,thirdControlToHide);
	    });
    }
    
    $("#id_question").on('change',function(){
    	var selectedReport = $("#id_report_type").val();
    	if (selectedReport != null && selectedReport != "")
		{
			var reps = ["30",];
    		if(reps.indexOf(selectedReport) > -1)
    		{
	    		$("#div_id_answer").show();
	    		var filterItem = $("#id_question").val();
	    		$("#id_answer").empty();
	    		$.ajax({
	                dataType: "json",
	                url: "/reports/report_params?question_id=" + filterItem,
	                contentType: "application/json; charset=utf-8",
	                success: function (report_params) {
		    			$('<option>').val("").text("-----").appendTo("#id_answer");
	                    $.each(report_params, function (key, value) {
	                    	$('<option>').val(key).text(value).appendTo("#id_answer");
	                    });
	                    $('#id_answer').selectpicker('refresh');
	                }
	            });
    		}
		}
    });
    
    
    $("#id_item_id").on('change',function(){
    	var selectedReport = $("#id_report_type").val();
    	if($("#id_control_to_visual").val() === "visible" && $("#id_item_id").val() != null && $("#id_item_id").val() != "")
    	{
    		var reps = ["9","5","10","13","14","26"];
    		if (selectedReport != null && selectedReport != "")
    		{
	    		if(!(reps.indexOf(selectedReport) > -1))
	    		{
		    		$("#div_id_geo_area").show();
		    		var filterItem = $("#id_item_id").val();
		    		$.ajax({
		                dataType: "json",
		                url: "/reports/report_params?filter=" + filterItem +"&rep_id="+ selectedReport,
		                contentType: "application/json; charset=utf-8",
		                success: function (report_params) {
		                	$("#id_geo_area").empty();
			    			$('<option>').val("").text("-----").appendTo("#id_geo_area");
			    			//$("#div_id_geo_area").removeClass('hide');
			    			//$("#div_id_geo_area").show();
		                    $.each(report_params, function (key, value) {
		                    	$('<option>').val(key).text(value).appendTo("#id_geo_area");
		                    });
		                    $('#id_geo_area').selectpicker('refresh');
		                }
		            });
	    		}
    		}
    	}
    	else
    	{
    		//$("#id_geo_area").val('');
    		//$("#div_id_geo_area").addClass('hide');
    		//$("#div_id_geo_area").hide();
    	}
    	
    	
    	var selectedItemId = $("#id_item_id").val();
    	var selectedControl = "";
    	var controlToHide = "";
    	switch(selectedReport)
    	{
    	case "1":
    		break;
    	case "2":
    		break;
    	case "3":
			if(selectedItemId == "GPRV")
			{
				$("#div_id_geo_area").hide();
			}
			else
			{
				$("#div_id_geo_area").show();
			}
			if(selectedItemId == "GWRD" || selectedItemId == "GCON")
			{
				$("#id_geo_area").prop('required',true);
			}
			else
			{
				$("#id_geo_area").prop('required',false);
			}
    		break;
    	case "4":
    		break;
    	case "5":
    		break;
    	case "6":
    		if(selectedItemId == "GWRD")
    		{
    			$("#div_id_geo_area").show();
    			$("#id_geo_area").prop('required',true);
    		}
    		else
    		{
    			$("#div_id_geo_area").hide();
    			$("#id_geo_area").prop('required',false);
    		}
    		break;
    	default:
    		break;
    	}
    });
    
    $(".autocompleteorgunit").autocomplete({
        source: function (request, response) {
            $.getJSON("/reports/report_params?org_name=" + request.term, function (data) {
                response($.map(data, function (value, key) {
                    return {
                        label: concatenatearray(value),
                        value: value
                    };

                }));
            });
        },
        minLength: 1,
        delay: 100,
        select: function (event, ui) {
            event.preventDefault();
            $('#id_org_id_hidden').val(ui.item.value[0]);
        }
    });
    
    $( "#id_org_unit" ).on( "autocompleteselect", function( event, ui ) {
        var result = concatenatearray(ui.item.value);
        $( "#id_org_unit" ).val(result);
    });
    
    
    $( "#id_org_unit" ).keyup(function() {
    	$('#id_org_id_hidden').val("");
    	$('#id_org_unit').trigger("change");	
    });
    
    function concatenatearray(tmparray, startindex, separator)
    {
        var charseparator = separator? separator:'\t';
        var sindex = startindex? startindex:1;
        var result = [];
        for(i=sindex; i < tmparray.length; i++){
            if(tmparray[i] == null || tmparray[i] == '')
                continue;
           result.push(tmparray[i]);
        }
        
        return result.join(charseparator);
    }
    
    $("#id_run").click(function (e) {
    		clearResults();
    		$("#reportResults > tbody").html("<tr><td colspan='1'>Processing...</td></tr>");
		    if(formIsValid())
		    {
		    	e.preventDefault();
                $.ajax({
                    dataType: "html",
                    url: "/reports/run_report?report_type="+ $( "#id_report_type").val() +"&item_id=" + $( "#id_item_id").val() + 
                	"&question=" + $( "#id_question").val() +
                	"&geo_area=" + $( "#id_geo_area").val() +
                	"&org_unit=" + $( "#id_org_id_hidden").val() +
                	"&sub_units=" + $( "#id_sub_units").val() +
                	"&from_date=" + $( "#id_from").val() +
                	"&to_date=" + $("#id_to").val() +
                	"&from_date_c=" + $( "#id_from_c").val() +
                	"&to_date_c=" + $( "#id_to_c").val() +
                	"&answer=" + $( "#id_answer").val(),
                    contentType: "application/json; charset=utf-8",
                    success: function (results) {
                    	runningReport = results;
                        if (results.length == 0) {
                            $('#reportResults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
                        }
                        else {
                        	var headerRow = "";
                        	var addedheader = false;
                        	var bodyRow = "";
                        	var obj = $.parseJSON(results);
                        	$.each(obj, function (key,value) {
                        		if(!addedheader)
                    			{
                        		headerRow += " <tr>";
                        		}
                        		
                        		bodyRow += " <tr>";
                        		$.each(value, function (key1,value1) {
                        			if(!addedheader)
                        			{
                        				if (key1 != "" && key1 != null)
                        				{
                        					headerRow += " <th>" + key1 +  "</th>";
                        				}
                        			}
                        			
                        			bodyRow += " <td>" + value1 + "</td> ";
                        		});
                        		if(!addedheader)
                    			{
                        		headerRow += "</tr>";
                        		}
                        		
                        		bodyRow += "</tr>";
                        		addedheader = true;
                        	});
                        	$("#reportResults > thead").html("");
                        	$("#reportResults > tbody").html("");
                        	$("#reportResults > thead").append(headerRow);
                        	$("#reportResults > tbody").append(bodyRow);
                        }
                    },
                    error: function () {
                        $('#reportResults tbody').html("<tr><td span='5'>No results for search term supplied</td></tr>");
                    }
                });
     		}
     		else
		    {
     			toastr.error('Please enter valid parameters in the fields', 'Reports',{ timeOut: 10000 }); 
		    }
     });
     
     function formIsValid()
     {
    	 var valid = true;
    	 
    	 if(!isFieldValid("item_id"))
    	 {
    		 valid = false;
    	 }
    	 if(!isFieldValid("to_c"))
    	 {
    		 valid = false;
    	 }
    	 if(!isFieldValid("from_c"))
    	 {
    		 valid = false;
    	 }
    	 if(!isFieldValid("to"))
    	 {
    		 valid = false;
    	 }
    	 if(!isFieldValid("from"))
    	 {
    		 valid = false;
    	 }
    	 if(!isFieldValid("sub_units"))
    	 {
    		 valid = false;
    	 }
    	 if(!isFieldValid("org_unit"))
    	 {
    		 valid = false;
    	 }
    	 if(!isFieldValid("geo_area"))
    	 {
    		 valid = false;
    	 }
    	 if(!isFieldValid("question"))
    	 {
    		 valid = false;
    	 }
    	 
    	 return valid;
     }
     function isFieldValid(fName)
     {
    	 var fieldName = fName;
    	 var valid = true;
    	 if($("#id_"+fName).prop('required')===true && ($("#id_"+fName).val()==="" || $("#id_"+fName).val()===null))
    	 {
    		 if($("#div_id_"+fName).hasClass("has-error"))
    		 {
    			 valid = false;
    		 }
    		 else{
    			 $("#div_id_"+fName).addClass("has-error");
    			 $("#div_id_"+fName+"_error").append("<span id='error_1_id_"+fName+"' class='help-block'>This field is required</span>");
        		 valid = false;
    		 }
    	 }
    	 else
    	 {
    		 $("#div_id_"+fName).removeClass("has-error");
    		 $("#div_id_"+fName+"_error >span").remove();
    		 valid = true;
    	 }
    	 return valid;
     }
     
     $("#id_setup_sets").on('click',function(e){
		 $(".setup-set").removeClass("hide");	
		 $(".setup-set").show();
		 $(".reports-home").addClass("hide");
		 $(".reports-home").hide();
		 $(".reports-home").addClass("hide");
		 $(".reports-home").hide();
	 });
     
	 $("#id_return").click(function(e){
		 $(".reports-home").removeClass("hide");	
		 $(".reports-home").show();
		 $(".setup-set").addClass("hide");
		 $(".setup-set").hide();
		 reloadSetNames('#id_sub_units');
	 });
	 
	 $("#id_add").click(function(e){
		 var add_text = $(".add-ico").text();
		 if(add_text == ' Add ')
		 {
			 $(".s-text").removeClass("hide");	
			 $(".s-text").show();
			 $(".s-sel").addClass("disabled");
			 $(".s-sel").hide();
			 $(".add-ico").text(' Save ');
			 $(".add-ico").removeClass('fa fa-plus-square');
			 $(".add-ico").addClass('fa fa-floppy-o');
			 $("#id_add").removeClass('btn btn-success');
			 $("#id_add").addClass('btn btn-primary');
			 $("#id_delete").removeClass('btn btn-danger');
			 $("#id_delete").addClass('btn btn-blue btn-square');
			 $(".delete-ico").removeClass('fa fa-trash-o');
			 $(".delete-ico").addClass('fa fa-undo');
			 $(".delete-ico").text(' Cancel ');
		 }
		 else
		 {
			 if($("#id_set_name_text").val() != null && $("#id_set_name_text").val() != "")
			 {
				 $.ajax({
	                dataType: "json",
	                url: "/reports/rep_utils?set_name=" + $("#id_set_name_text").val(),
	                contentType: "application/json; charset=utf-8",
	                async: false,
	                success: function (set_id_obj) {
	                	var s_id = -1;
	                	if(set_id_obj != null)
	                	{
	                		$.each(set_id_obj, function (key, value) {
	                    		s_id = value;
	                    	});
	                    	reloadSetNames('#id_set_name');
	                    	$('#id_set_name').val(s_id);
	                    	$('#id_set_name').selectpicker('refresh');
	                	}
	                }
	             });
				 
	             $('#id_search_org').multiSelect('deselect_all');
	             $(".s-sel").removeClass("hide");	
				 $(".s-sel").show();
				 $(".s-text").addClass("disabled");
				 $(".s-text").hide();
				 $(".add-ico").text(' Add ');
				 $(".add-ico").removeClass('fa fa-floppy-o');
				 $(".add-ico").addClass('fa fa-plus-square');
				 $("#id_add").removeClass('btn btn-primary');
				 $("#id_add").addClass('btn btn-success');
				 $("#id_delete").removeClass('btn btn-blue btn-square');
				 $("#id_delete").addClass('btn btn-danger');
				 $(".delete-ico").removeClass('fa fa-undo');
				 $(".delete-ico").addClass('fa fa-trash-o');
				 $(".delete-ico").text(' Delete ');
			 }
			 else
			 {
				 toastr.error('You have not entered the name of the set. Please enter the name or click Cancel', 'Error',{ timeOut: 5000 });
			 }
		 }
	 });
	 
	 function reloadSetNames(controlRefreshed)
	 {
		 $.ajax({
             dataType: "json",
             url: "/reports/rep_utils?refresh_set_names='True'",
             contentType: "application/json; charset=utf-8",
             async: false,
             success: function (set_id_obj) {
             	var sel_set_opts = null;
             	$.each(set_id_obj, function (key, value) {
             		if(key=="sets")
             		sel_set_opts = value;
             	});
             	
             	$(controlRefreshed).empty();
             	$(controlRefreshed).append(sel_set_opts);
             	$(controlRefreshed).selectpicker('refresh');
             	$(controlRefreshed+'_text').val('');             	
             }
         });
	 }
	 
	 $("#id_save_set").click(function(e){
		 if($("#id_set_name").val() == null || $("#id_set_name").val() == "")
		 {
			 toastr.error('Please select a set to save to first', 'Error',{ timeOut: 5000 });
			 return;
		 }
		 
		 if($("#id_search_org").val() == null || $("#id_search_org").val() == "") 
		 {
			 toastr.error('Please choose organisational units to save to first', 'Error',{ timeOut: 5000 });
			 return;
		 }
		 
		 var orgs_selected = $("#id_search_org").val();
		 $.ajax({
                dataType: "json",
                url: "/reports/rep_utils?orgs=" + orgs_selected.join(',') +"&s_id="+ $("#id_set_name").val(),
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (set_id_obj) {
                	if(set_id_obj != null)
                	{
                		toastr.success('Set saved successfully', 'Report sets',{ timeOut: 1000 });
                	}
                }
	      });
	 });
	 
	 $("#id_confirm_delete").click(function(e){
		 var orgs_selected = $("#id_set_name").val();
		 $.ajax({
	                dataType: "json",
	                url: "/reports/rep_utils?set_delete=" + $("#id_set_name").val(),
	                contentType: "application/json; charset=utf-8",
	                async: false,
	                success: function (set_id_obj) {
	                	if(set_id_obj != null)
	                	{
	                		toastr.success('Set successfully deleted', 'Report sets',{ timeOut: 1000 });
	                		reloadSetNames('#id_set_name');
	                	}
	                }
	      });
	 });
	 
	 
	 $("#id_delete").click(function(e){
		 var add_text = $(".delete-ico").text();
		 if(add_text == ' Delete ')
		 {
			 if($("#id_set_name").val() == null || $("#id_set_name").val() == "")
			 {
				 toastr.error('Please select a set to delete first', 'Error',{ timeOut: 1000 });
				 return;
			 }
			 $("#reports_dialog_box").click();
		 }
		 else
		 {
			 $(".s-sel").removeClass("hide");	
			 $(".s-sel").show();
			 $(".s-text").addClass("disabled");
			 $(".s-text").hide();
			 $(".add-ico").text(' Add ');
			 $(".add-ico").removeClass('fa fa-floppy-o');
			 $(".add-ico").addClass('fa fa-plus-square');
			 $("#id_add").removeClass('btn btn-primary');
			 $("#id_add").addClass('btn btn-success');
			 $(".delete-ico").removeClass('fa fa-undo');
			 $(".delete-ico").addClass('fa fa-trash-o');
			 $("#id_delete").removeClass('btn btn-blue btn-square');
			 $("#id_delete").addClass('btn btn-danger');
			 $(".delete-ico").text(' Delete ');
		 }
	 });
	 
	 $('#id_set_name').on('change', function(){
		 if($('#id_set_name').val() != "" && $('#id_set_name').val() != null)
		 {
			 loadOrgsForSet($('#id_set_name').val());
		 }
		 else{
			 $('#id_search_org').multiSelect('deselect_all');
		 }
		 
	 });

	 function loadOrgsForSet(s_id)
	 {
		 $.ajax({
             dataType: "json",
             url: "/reports/rep_utils?set_id=" + s_id,
             contentType: "application/json; charset=utf-8",
             async: false,
             success: function (set_id_obj) {
             	var sel_org_opts = null;
             	$.each(set_id_obj, function (key, value) {
             		if(key=="sel_orgs")
             		sel_org_opts = value;
             	});
             	
             	var tmparray = [];
	            $('#id_search_org').multiSelect('deselect_all');
             	var str_val = sel_org_opts.join(',');
             	var sp_vals = str_val.split(',');
             	$('#id_search_org').multiSelect('select', sp_vals);
             	$('#id_search_org').multiSelect('refresh');
             }
         });
	 }
	 
	 function clearFormValid()
     {
    	 clearFieldValid("item_id");
    	 clearFieldValid("to_c");
    	 clearFieldValid("from_c");
    	 clearFieldValid("to");
    	 clearFieldValid("from");
    	 clearFieldValid("sub_units");
    	 clearFieldValid("org_unit");
    	 clearFieldValid("geo_area");
    	 clearFieldValid("question");
     }
     function clearFieldValid(fName)
     {
    	 if($("#div_id_"+fName).hasClass("has-error"))
    	 {
	    	 $("#div_id_"+fName).removeClass("has-error");
			 $("#div_id_"+fName+"_error >span").remove();
    	 }
     }
     $("#id_convert_excel").click(function(){
    	 alert($("#download_export").prop("href"));
    	 alert("last here");
     });
     
     
     $("#id_convert_excel").click(function(){
    	 if(runningReport==null)
    	 {
    		 toastr.error('Please run a report first before you can export', 'Reports',{ timeOut: 10000 }); 
    		 return;
    	 }
    	 $.when(get_export_url).then(function(a1){
    		 alert(a1);
    		 $("#download_export").click();
    		 alert($("#download_export").prop("href"));
    	 });
    	 
    	 
    	 
         
         //return false;
    	 //alert(doc_url);
         /*
         $.ajax({
             dataType: "json",
             url: doc_url,//"/reports/rep_utils?report_to_export=" + runningReport +"&sel_report="+ selectedReport,
             contentType: "application/json; charset=utf-8",
             async: false,
             success: function (exported) {
            	
             }
         });
         */
     });
     function get_export_url(){
    	 var selectedReport = $("#id_report_type").val();
    	 var doc_url = "";
    	 $.ajax({
             dataType: "json",
             url: "/reports/rep_utils?report_to_export=" + runningReport +"&sel_report="+ selectedReport,
             contentType: "application/json; charset=utf-8",
             async: true,
             success: function (exported) {
            	//toastr.success('Report successfully exported', 'Reports',{ timeOut: 10000 });
    		 	//var doc_url = "";
	        	 $.each(exported, function (key, value) {
	        		 doc_url = value;
	          	 });
	        	 
	          	
	          	alert("here first");
	          	alert("then here");
	            $("#download_export").prop("href",doc_url);
	            //$("#download_export").refresh();
	            console.log(doc_url);
	            //if(doc_url != "")
	            //{
	            	//$("#download_export").attr("href",doc_url);
	            	//console.log("clicked");
	            	
	            //}
             }
         });
    	 return doc_url;
     }
     
	 
});