$(document).ready(function () {
    setInterval(refreshPage, 2000);
    if ($('#task_processing_id').val() != null && $('#task_processing_id').val() != '') {
        $('#downloadbtn').addClass('disabled');
        $('#uploadformbtn').addClass('disabled');
    }

    function refreshPage()
    {
        $.ajax({
            dataType: "json",
            url: "/api/refresh_capture_site?task=" + $('#task_processing_id').val(),
            success: function (results) {

                if (results.length == 0) {
                    $('#downloadstbl tbody').empty().html("");
                }
                else {
                    $('#downloadstbl tbody').empty().html("");
                    var obj = $.parseJSON(results);
                    var downloads = $.parseJSON(obj['downloads']);
                    $.each(downloads, function (index) {
                        $("#downloadstbl > tbody").append(
                                "<tr>" +
                                "<td>" + downloads[index].section + "</td> " +
                                "<td>" + downloads[index].started + "</td> " +
                                "<td>" + downloads[index].ended + "</td> " +
                                "<td>" + downloads[index].num_of_recs + "</td> " +
                                "<td>" + downloads[index].success + "</td> " +
                                "</tr>"
                                );
                    });

                    var uploads = $.parseJSON(obj['uploads']);
                    $('#formtbl tbody').empty().html("");
                    $.each(uploads, function (index) {
                        $("#formtbl > tbody").append(
                                "<tr>" +
                                "<td>" + uploads[index].formtype + "</td> " +
                                "<td>" + uploads[index].count + "</td> " +
                                "</tr>"
                                );
                    });

                    var recent_upload_date = $.parseJSON(obj['recent_upload_date']);
                    $('#recent_upload_date').html(recent_upload_date);

                    var total_forms = $.parseJSON(obj['total_forms']);
                    $('#form_count').html(total_forms);

                    var pageinfo = $.parseJSON(obj['pageinfo']);
                    $('#pagescount').html(pageinfo.pagescount);

                    var task_operation = $.parseJSON(obj['task_operation']);
                    var task_running = $.parseJSON(obj['task_running']);
                    var task_completed_success = $.parseJSON(obj['task_completed_success']);
                    var task_id = $.parseJSON(obj['task_id']);

                    //Update download status if active task is a download operation
                    if (task_running == true && task_operation == 'download') {
                        show_download_progress();
                        $('#id_btn_stop_download').removeClass('disabled');
                        if (task_id != null) {
                            $('#task_processing_id').val(task_id);
                        }
                    }

                    //Update upload status if active task is a upload operation
                    if (task_running == true && task_operation == 'upload') {
                        show_upload_progress();
                        $('#id_btn_stop_upload').removeClass('disabled');
                        if (task_id != null) {
                            $('#task_processing_id').val(task_id);
                        }
                    }
                    /**console.warn(task_running);
                     console.warn(task_completed_success);
                     console.warn(task_operation);*/
                    if (task_running == false && task_completed_success == true) {
                        if (task_operation == 'download') {
                            $(".downloadspinner").hide();
                            $("#id_download_message").html('The download completed successfully! ');

                            $(".downloading").delay(4000).fadeOut('slow', function () {
                                $('#task_processing_id').val('');
                            });
                            $('#id_btn_stop_download').addClass('disabled');
                        }
                        if (task_operation == 'upload') {
                            $(".uploadspinner").hide();
                            $("#id_upload_message").html('The upload completed successfully! ');

                            $(".uploading").delay(4000).fadeOut('slow', function () {
                                $('#task_processing_id').val('');
                            });
                            $('#id_btn_stop_upload').addClass('disabled');

                        }
                        enable_upload_download();
                    }
                    else if (task_running == false && task_completed_success == false) {
                        if (task_operation === 'download') {
                            $(".downloadspinner").hide();
                            $("#id_download_message").html('The download failed to run successfully. Please retry the download!');

                            $(".downloading").delay(4000).fadeOut('slow', function () {
                                $('#task_processing_id').val('');
                            });
                            $('#id_btn_stop_download').addClass('disabled');
                        }
                        if (task_operation === 'upload') {
                            $(".uploadspinner").hide();
                            $("#id_upload_message").html('The upload failed to run successfully. Please retry the upload!');

                            $(".uploading").delay(4000).fadeOut('slow', function () {
                                $('#task_processing_id').val('');
                            });
                            $('#id_btn_stop_upload').addClass('disabled');
                        }
                        enable_upload_download();
                    }
                    else if (task_id === '') {
                        enable_upload_download();
                        $('#id_btn_stop_download').addClass('disabled');
                        $('#id_btn_stop_upload').addClass('disabled');

                        if ($(".uploading").is(":visible")) {
                            if (task_completed_success === false && task_operation == 'upload') {
                                $(".uploadspinner").hide();
                                $("#id_upload_message").html('The download failed to run successfully. Please retry the download!');
                            } else if (task_completed_success === true && task_operation == 'upload') {
                                $(".uploadspinner").hide();
                                $("#id_upload_message").html('The upload completed successfully! ');
                            } else if (task_completed_success === null && task_operation == null) {
                                $(".uploadspinner").hide();
                                $("#id_upload_message").html('The upload completed successfully! ');
                            }

                            $(".uploading").delay(4000).fadeOut('slow', function () {
                                $('#task_processing_id').val('');
                            });
                        }
                        if ($(".downloading").is(":visible")) {
                            if (task_completed_success === false && task_operation == 'download') {
                                $(".downloadspinner").hide();
                                $("#id_download_message").html('The download failed to run successfully. Please retry the download!');
                            } else if (task_completed_success === false && task_operation == 'download') {
                                $(".downloadspinner").hide();
                                $("#id_download_message").html('The download completed successfully!');
                            } else if (task_completed_success === null && task_operation == null) {
                                $(".downloadspinner").hide();
                                $("#id_download_message").html('The download completed successfully!');
                            }

                            $(".downloading").delay(4000).fadeOut('slow', function () {
                                $('#task_processing_id').val('');
                            });
                        }
                    }
                }
            },
            error: function () {
                //alert('A connection error occured. Please ensure that you are connected');;
            }
        });
    }

    $("#uploadformbtn").click(function () {
        begin_upload();
    });

    begin_upload = function ()
    {
        show_upload_progress();
        $('#id_btn_stop_upload').removeClass('disabled');
        $.ajax({
            dataType: "json",
            url: "/api/begin_upload",
            success: function (results) {
                if (results.length == 0) {
                    $('#formtbl tbody').empty().html("");
                    $("#formtbl > tbody").append(
                            "<tr>" +
                            "<td colspan=\"2\"> There are currently no forms in the upload queue</td> " +
                            "</tr>"
                            );
                }
                else {
                    $('#formtbl tbody').empty().html("");
                    var obj = $.parseJSON(results);
                    var uploads = $.parseJSON(obj['uploads']);

                    $.each(uploads, function (index) {
                        $("#formtbl > tbody").append(
                                "<tr>" +
                                "<td>" + uploads[index].formtype + "</td> " +
                                "<td>" + uploads[index].count + "</td> " +
                                "</tr>"
                                );
                    });

                    var task_id = $.parseJSON(obj['task_id']);
                    $('#task_processing_id').val(task_id);
                }
            },
            error: function () {
                $('#formtbl tbody').empty().html("");
                $("#formtbl > tbody").append(
                        "<tr>" +
                        "<td colspan=\"2\"> An error occured. Please try again. </td> " +
                        "</tr>"
                        );
            }
        });
    };

    $("#downloadbtn").click(function () {
        begin_download();
    });

    begin_download = function ()
    {
        var password = get_password();
        if (password === '') {
            return;
        }
        show_download_progress();
        $('#id_btn_stop_download').removeClass('disabled');
        $.ajax({
            dataType: "json",
            url: "/api/begin_download?section=" + $("#id_download_sections").val() + '&password=' + password,
            success: function (results) {
                if (results.length == 0) {
                    $('#downloadstbl tbody').empty().html("");
                }
                else {
                    $('#downloadstbl tbody').empty().html("");
                    var obj = $.parseJSON(results);
                    var downloads = $.parseJSON(obj['downloads']);

                    $.each(downloads, function (index) {
                        $("#downloadstbl > tbody").append(
                                "<tr>" +
                                "<td>" + downloads[index].section + "</td> " +
                                "<td>" + downloads[index].started + "</td> " +
                                "<td>" + downloads[index].ended + "</td> " +
                                "<td>" + downloads[index].num_of_recs + "</td> " +
                                "<td>" + downloads[index].success + "</td> " +
                                "</tr>"
                                );
                    });

                    var task_id = $.parseJSON(obj['task_id']);
                    $('#task_processing_id').val(task_id);
                }
            },
            error: function () {
                $('#downloadstbl tbody').empty().html("");
            }
        });
    };

    get_password = function () {
        var p_word = '';
        var password = prompt("Please enter your password", "");
        if (password !== null) {
            p_word = password;
        }
        return p_word;
    }

    disable_upload_download = function () {
        $('#downloadbtn').addClass('disabled');
        $('#uploadformbtn').addClass('disabled');
    };

    enable_upload_download = function () {
        $('#downloadbtn').removeClass('disabled');
        $('#uploadformbtn').removeClass('disabled');
    };

    show_download_progress = function () {
        $(".downloadspinner").show();
        $("#id_download_message").html('Currently downloading. Please wait ...  ');
        disable_upload_download();

        //$(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
        $(".downloading").show();
    };

    show_upload_progress = function () {
        $(".uploadspinner").show();
        $("#id_upload_message").html('Currently uploading. Please wait ...  ');
        disable_upload_download();
        $(".uploading").show();
    };

    $(".canceldownloadupload").click(function () {
        cancel_current_task();
    });

    cancel_current_task = function ()
    {
        $.ajax({
            dataType: "json",
            url: "/capture/stop_download_upload?task_id=" + $('#task_processing_id').val(),
            success: function (results) {
                var obj = $.parseJSON(results);
                var cancelled = $.parseJSON(obj['cancelled']);
                var task_operation = $.parseJSON(obj['task_operation']);

                if (task_operation == 'download') {
                    $(".downloadspinner").hide();
                    $("#id_download_message").html('The download has been cancelled!');

                    $(".downloading").delay(4000).fadeOut('slow', function () {
                        $('#task_processing_id').val('');
                    });
                    $('#id_btn_stop_download').addClass('disabled');
                }
                if (task_operation == 'upload') {
                    $(".uploadspinner").hide();
                    $("#id_upload_message").html('The upload has been cancelled!');

                    $(".uploading").delay(4000).fadeOut('slow', function () {
                        $('#task_processing_id').val('');
                    });
                    $('#id_btn_stop_upload').addClass('disabled');
                }
                enable_upload_download();
            },
            error: function () {
            }
        });
    };

});