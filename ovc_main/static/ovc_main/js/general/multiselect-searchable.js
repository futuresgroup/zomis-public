$(document).ready(function () {
	$('.multiselect-searchable').multiSelect({
		 selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search items...' style='margin-bottom:5px;'>",
		 selectionHeader: "<input type='text' class='form-control search-input2' autocomplete='off' style='visibility:hidden;margin-bottom:5px;'>",
		  afterInit: function(ms){
		    var that = this,
		        $selectableSearch = that.$selectableUl.prev(),
		        $selectionSearch = that.$selectionUl.prev(),
		        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
		        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
		
		    
		    that.qs1 = $selectableSearch.quicksearch(selectableSearchString).on('keydown', function(e){
		      if (e.which === 40){
		        that.$selectableUl.focus();
		        return false;
		      }
		   });
		    
		  },
		  afterSelect: function(){
			    this.qs1.cache();
			  },
			  afterDeselect: function(){
			    this.qs1.cache();
			  }
	});
});
	