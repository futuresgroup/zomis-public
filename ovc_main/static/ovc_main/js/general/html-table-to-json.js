$(document).ready(function () {
    /**
     * This function converts an html table into a json string. Set the id of the 
     * 'td' you require to a meaningful value and the ones you don't need to 'not_saving'
     * @param {String} table_name
     * @param {Integer} startindex
     * @returns {String} The json form of the table.
     */
    function getTableDataAsJsonString(table_name, startindex) {
        var data = {};
        var sindex = startindex ? startindex : 1;
        $("#" + table_name + " tr").each(function (i, v) {
            if (i < sindex) {
                return;
            }
            data[i] = {};
            $(this).children('td').each(function (ii, vv) {
                if (this.id !== 'not_saving') {
                    data[i][this.id] = $(this).text();
                }
            });
        });
        return JSON.stringify(data);
    }
});