$(document).ready(function () {
	function updateDuration(){
		if ($('#id_duration_units').val() === '1') {
			$('#id_Q086').val('');
			$('#id_Q087').val($('#id_duration_of_event').val());
		} else if ($('#id_duration_units').val() === '2') {
			$('#id_Q086').val($('#id_duration_of_event').val());
			$('#id_Q087').val('');
		}else{
			$('#id_Q086').val('');
			$('#id_Q087').val('');
		}

	}

	$('#id_duration_units').change(function () {
		updateDuration();
		});

	$(function () {
	  var $male_adults = $('#id_Q093');
	  var $female_adults = $('#id_Q094');
	  var $male_children = $('#id_Q095');
	  var $female_children = $('#id_Q096');
	  var $total = $('#id_Q097');
	  
	  $male_adults.on('keydown', function () {
		setTimeout(function () {
			var total = getTotal($male_adults.val(), $female_adults.val(), $male_children.val(), $female_children.val());
		  $total.val(total);
		}, 0);
	  });
	  
	  $female_adults.on('keydown', function () {
		setTimeout(function () {
			var total = getTotal($male_adults.val(), $female_adults.val(), $male_children.val(), $female_children.val());
		  $total.val(total);
		}, 0);
	  });
	  
	  $male_children.on('keydown', function () {
		setTimeout(function () {
			var total = getTotal($male_adults.val(), $female_adults.val(), $male_children.val(), $female_children.val());
		  $total.val(total);
		}, 0);
	  });
	  
	  $female_children.on('keydown', function () {
		setTimeout(function () {
			var total = getTotal($male_adults.val(), $female_adults.val(), $male_children.val(), $female_children.val());
		  $total.val(total);
		}, 0);
	  });
	});

	$('.counts').change(function () {
		var male_adults = $('#id_Q093').val();
		var female_adults = $('#id_Q094').val();
		var male_children = $('#id_Q095').val();
		var female_children = $('#id_Q096').val();

		var total = getTotal(male_adults, female_adults, male_children, female_children);

		$('#id_Q097').val(total);
		});
		
	function getTotal(male_adults, female_adults, male_children, female_children){
		var w = 0;
		var x = 0;
		var y = 0;
		var z = 0;
		if(male_adults !== ''){
			w = parseInt(male_adults);
			if(w < 0){
				w = 0;
			}
		}
		if(female_adults !== ''){
			x = parseInt(female_adults);
			if(x < 0){
				x = 0;
			}
		}
		if(male_children !== ''){
			y = parseInt(male_children);
			if(y < 0){
				y = 0;
			}
		}
		if(female_children !== ''){
			z = parseInt(female_children);
			if(z < 0){
				z = 0;
			}
		}
		return w + x + y + z;
	}

	function showGeos(){
		if ($('#id_Q089').val() === '123') {
			$('#div_id_district_parent').show();
			$('#div_id_ward_parent').hide();
		} else if ($('#id_Q089').val() === '124') {
			$('#div_id_district_parent').show();
			$('#div_id_ward_parent').show();
		}else{
			$('#div_id_district_parent').hide();
			$('#div_id_ward_parent').hide();
		}
	}

	function updateGeos(){
		showGeos();
		$('#id_district').val('');
		$('#id_ward').val('');
	}

	showGeos();

	$('#id_Q089').change(function () {
		updateGeos();
		});

    handledistrictchange = function () {
				var district_id = $("#id_district").val();
                $.ajax({
                    dataType: "json",
                    url: "/beneficiary/wards?district_id=" + district_id,
                    contentType: "application/json; charset=utf-8",
                    success: function (wards) {
                        $("#id_ward").empty();
                        $('<option>').val(0).text('-----').appendTo("#id_ward");
                        $.each(wards, function (key, val) {
                            $('<option>').val(key).text(val).appendTo("#id_ward");   
                        });
                    }
                });
            };

	$('#id_district').change(function () {
		handledistrictchange();
		});

	
	$(".organisation_autocomplete").autocomplete({
        source: function (request, response) {
            $.getJSON("/workforce/orgautocomp?org_name=" + request.term,  function (data) {
                response($.map(data, function (value, key) {
                    return {
                        label: concatenatearray(value, 0, 1, '\t'),
                        value: value
                    };
                }));
            });
        },
        minLength: 1,
        delay: 100,
        select: function (event, ui) {
            event.preventDefault();
            var result = concatenatearray(ui.item.value, 0, 1, '\t');
            $("#id_organisational_unit").val(result);
			$("#id_org_unit_name").val(ui.item.value[1]);
			$("#id_org_unit_id").val(ui.item.value[0]);
        },
    });

	$(".beneficiary_autocomplete").autocomplete({
        source: function (request, response) {
            $.getJSON("/beneficiary/public_sens_ben_autocomp?ben_name=" + request.term, function (data) {
                response($.map(data, function (value, key) {
                    return {
                        label: concatenatearray(value, 1, 4, '\t'),
                        value: value
                    };

                }));
            });
        },
        minLength: 1,
        delay: 100,
        select: function (event, ui) {
            event.preventDefault();
            $('#person_id').val(ui.item.value[0]);
            $('#beneficiary_id').val(ui.item.value[1]);
			$('#birth_reg_id').val(ui.item.value[2]);
			$('#national_id').val(ui.item.value[3]);
            $('#beneficiary_name').val(ui.item.value[4]);
            $('#sex').val(ui.item.value[5]);
			$('#age').val(ui.item.value[6]);
        },
    });

    $("#id_beneficiary").on("autocompleteselect", function (event, ui) {
        var result = concatenatearray(ui.item.value, 1, 4, '\t');
        $("#id_beneficiary").val(result);
    });


	$( "#id_date_event_started" ).change(function() {
		var val = $( "#id_date_event_started" ).val();
		$( "#id_date_event_ended" ).val(val);
	});
});