$(document).ready(function () {
	function checkUpdateMode(e){
		  var edit_mode = $('input:radio[name=edit_mode]:checked').val();
		  $( "#id_edit_mode_hidden" ).val(edit_mode);
		  if(edit_mode == 3)
		  {
			  e.preventDefault();
			  $("#duplicate_dialog_box").click();
			  showingDuplicate = true;
		  }
		  else
		  {
			  var person = checkNames();
	            if(person != undefined)
	            {
	            	e.preventDefault();
					$("#confirm_dialog_box").click();
	            }
		  }
	  }  

	updateFinaleGuardian = function (e){
			checkUpdateMode(e);
			checkDateofDeath();
			validateWfcField();
			}
			
	function checkDateofDeath(){
            	if(!$("#id_date_of_death").prop('required'))
            	{
            		$("#id_date_of_death").val('');
            	}
            }
            
    function invalidWorkforce(){
    	var enteredWorkforce = $('input[name="workforce_member"]').val();
        var spVals = enteredWorkforce.split(',');
        var len = spVals.length;
        var re = /^W\d{7}$/;
        var toReturn;
        
    	if(len == 4)
        {
            if(spVals[1]!="" || spVals[1]=="")
            {
                if(spVals[1]!="" && re.test(spVals[1]))
                {
                	toReturn = false;
                }
                else{
                	toReturn = true;
                }
            }
            else if(spVals[2]!=""||spVals[2]=="")
            {
                if(spVals[2]!="" && testNrc(spVals[2]))
                {
                	toReturn = false;
                }
                else{
                	toReturn = true;
                }
            }
            else if(spVals[3]=="")
            {
            	toReturn = true;
            }
            
            if(spVals[0]){
            	$.ajax({
                    dataType: "json",
                    url: "/beneficiary/get_wfc_object?wfc_id=" + spVals[0] + "&wfc_obj='Yes'",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (person) {
            			if(person == null || person == undefined)
            			{
            				toReturn = true;
            			}
            			else
            			{
            				toReturn = false;
            			}
                    }
                });
            }
            return toReturn;
        }
        else if($('input[name="workforce_member"]').val() != null && $('input[name="workforce_member"]').val() != "")
        {
        	return true;
        }
    }
    
    
    function validateWfcField()
    {
    	if (invalidWorkforce()) {
    		$('input[name="workforce_member"]')[0].setCustomValidity('Enter a valid workforce');
	    } 
    	else {
    		$('input[name="workforce_member"]')[0].setCustomValidity('');
	    }
    }
            
  function checkNames(){
    	var tmpperson;
        $.ajax({
                dataType: "json",
                url: "/beneficiary/check_names?first_name=" + $( "#id_first_name" ).val() + "&last_name=" + $( "#id_last_name" ).val() + "&pk_id=" + $( "#id_pk_id" ).val(),
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (person) {
                	tmpperson = person;
                }
            });
        return tmpperson;
    }
  
  $("#id_cancel_submit").on('click',function(){
  	toggleSubmitButton();
  });
  
  function toggleSubmitButton(){
  	if($(":submit").attr('disabled') == 'disabled'){
  		$(":submit").removeAttr('disabled');
  	}
  	else{
  		$(":submit").attr('disabled', 'disabled');
  	}
  }
  
  $("form").submit(function(){
  	toggleSubmitButton();
      return true;
  });
});
