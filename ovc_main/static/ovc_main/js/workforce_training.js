$(document).ready(function () {
	function updateDuration(){
		if ($('#id_duration_units').val() === '1') {
			$('#id_Q081').val('');
			$('#id_Q082').val($('#id_duration_of_training').val());
		} else if ($('#id_duration_units').val() === '2') {
			$('#id_Q081').val($('#id_duration_of_training').val());
			$('#id_Q082').val('');
		}else{
			$('#id_Q081').val('');
			$('#id_Q082').val('');
		}

	}

	$('#id_duration_units').change(function () {
		updateDuration();
		});

	$(".workforce_training_autocompletewfc").autocomplete({
        source: function (request, response) {
            $.getJSON("/workforce/workforce_training_autocomplete?wfc_name=" + request.term, function (data) {
                response($.map(data, function (value, key) {
                    return {
                        label: concatenatearray(value, 1, 3, '\t'),
                        value: value
                    };
                }));
            });
        },
        minLength: 1,
        delay: 100,
        select: function (event, ui) {
            event.preventDefault();
            var result = concatenatearray(ui.item.value, 1, 3, '\t');
            $("#id_workforce_member").val(result);
            $('#id_workforce_member_id').val(ui.item.value[1]);
			$('#id_workforce_member_nrc').val(ui.item.value[2]);
			$('#id_workforce_member_name').val(ui.item.value[3]);
        }
    });

	$(".organisation_autocomplete").autocomplete({
        source: function (request, response) {
            $.getJSON("/workforce/orgautocomp?org_name=" + request.term,  function (data) {
                response($.map(data, function (value, key) {
                    return {
                        label: concatenatearray(value, 0, 1, '\t'),
                        value: value
                    };
                }));
            });
        },
        minLength: 1,
        delay: 100,
        select: function (event, ui) {
            event.preventDefault();
            var result = concatenatearray(ui.item.value, 0, 1, '\t');
            $("#id_organisational_unit").val(result);
			$("#id_org_unit_name").val(ui.item.value[1]);
			$("#id_org_unit_id").val(ui.item.value[0]);
        }
    });

	$('#id_where_training_took_place_district').change(function () {
		handledistrictchange('');
		$("#id_where_training_took_place_ward").show();
		});
	
      handledistrictchange = function (ward) {
                $.ajax({
                    dataType: "json",
                    url: "/beneficiary/wards?district_id=" + $("#id_where_training_took_place_district").val(),
                    contentType: "application/json; charset=utf-8",
                    success: function (wards) {
                        $("#id_where_training_took_place_ward").empty();
                        $('<option>').val(0).text('-----').appendTo("#id_where_training_took_place_ward");
                        $.each(wards, function (key, val) {
                            $('<option>').val(key).text(val).appendTo("#id_where_training_took_place_ward");   
                        });
						$("#id_where_training_took_place_ward").val(ward)
                    }
                });
            };

	if($("#id_where_training_took_place_district").val() !== ''){
		var ward = $("#id_where_training_took_place_ward").val();
		handledistrictchange(ward);
	}

	$( "#id_date_training_started" ).change(function() {
		var val = $( "#id_date_training_started" ).val();
		$( "#id_date_training_ended" ).val(val);
	});

});
