$(document).ready(function () {    
//==================Search Beneficiary start
            /*
             $("#searchbtnben").click(function(){
             //alert($("#searchorgtxt").val());
             $.ajax({
             dataType: "html",
             url: "/organisation/benlists?searchterm="+$("#searchbentxt").val(),
             contentType: "application/json; charset=utf-8",
             //data: {"districtid" : values},
             success: function( results ) {
             if (results.length == 0){
             $('#benresults tbody').html("<tr><td colspan='8'>No results for search term supplied</td></tr>");
             }
             else{
             $('#benresults tbody').empty().append(results);
             }
             },
             error: function () {
             $('#benresults tbody').html("<tr><td span='8'>No results for search term supplied</td></tr>");
             //alert($("#searchorgtxt").val())
             }
             });
             });
             */
            $(".gw-nextben").click(function () {
                var pagenumber = $(".gw-page").val();
                benpagination(++pagenumber);
            });
            $(".gw-prevben").click(function () {
                var pagenumber = $(".gw-page").val();
                benpagination(--pagenumber);
            });
            benpagination = function (pagenumber)
            {
                $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
                $(".searching").show();
                $(".totalrecords").hide();

                $.ajax({
                    dataType: "json",
                    url: "/beneficiary/benlists?searchterm=" + $("#searchbentxt").val() + "&pagenumber=" + pagenumber + "&itemsperpage=" + $(".gw-pageSizeben").val() + "&bentype=" + $("#bentype").val(),
                    success: function (results) {
                        if (results.length == 0) {
                            //$('#benresults tbody').html("<tr><td colspan='9'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#benresults tbody').empty().html("");
                            $.each(results, function (index) {

                                if (results[index].pageinfo == 'pageinfo')
                                {
                                    $(".countpages").empty().html(results[index].pagescount);
                                    $(".totalrecords").empty().append("Found " + results[index].total_records + " matching beneficiary(s)");

                                    //set the control number
                                    $(".gw-page").val(results[index].pagenumber);
                                    if (results[index].pagenumber >= results[index].pagescount)
                                    {
                                        if (!$(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").addClass("disabled");
                                        }
                                        if ($(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").removeClass("disabled");
                                        }

                                    }
                                    if (results[index].pagescount == 1) {
                                        if (!$(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").addClass("disabled");
                                        }
                                        if (!$(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber <= 1) {
                                        if (!$(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount) {
                                        if ($(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").removeClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                        if ($(".gw-prevbent").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").removeClass("disabled");
                                        }
                                    }

                                }
                                else {
                                    var action_html = "";
                                    var row_html = "<tr>";
                                    
                                    if (!results[index].is_capture_app) {
                                        row_html = "<tr class='table_row_clickable' " +
                                            "href='/beneficiary/view_ben?beneficiary_id=" + results[index].pk_id + "' style='cursor:pointer'> ";
                                    }
                                    if (!results[index].is_capture_app) {
                                        action_html = "<td> <span> <a href='/beneficiary/update_ben?beneficiary_id=" + results[index].pk_id + "' class='btn btn-primary btn-xs'> <i class='fa fa-edit'></i>&nbsp;Update</a></span></td></tr>";
                                    }
                                    
                                    $("#benresults > tbody").append(row_html +
                                            "<td>" + results[index].beneficiary_id + "</td> " +
                                            "<td>" + results[index].name + "</td> " +
                                            "<td>" + results[index].date_of_birth + "</td> " +
                                            "<td>" + results[index].sex + "</td> " +
                                            "<td>" + results[index].person_type + "</td> " +
                                            "<td>" + results[index].community + "</td> " +
                                            "<td>" + results[index].ward + "</td> " +
                                            "<td>" + results[index].district + "</td> " +
                                            action_html
                                            );
                                }
                            });
                        }
                    },
                    error: function () {
                        $('#benresults tbody').html("<tr><td span='9'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });
                //alert("success");
                $(".searching").fadeOut('slow', function () {
                    $(".totalrecords").fadeIn('slow');
                });

                //$(".totalrecords").show();
            };
            $(".gw-pageSizeben").change(function () {
                $(".gw-pageSizeben").val($(".gw-pageSizeben").val());
                benpagination(1);
            });
            $("#searchbtnben").click(function () {
                benpagination(1);
            });

            $("#searchbentxt").keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    benpagination(1);
                    return false;
                }
            });
            //==================Search Beneficiary End
            
});
            

