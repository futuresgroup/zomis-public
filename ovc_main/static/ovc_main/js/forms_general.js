$(document).ready(function () {
	
  window.query_bens_source = function (request, response) {
        $.getJSON("/beneficiary/res_child_autocomplete?child_name=" + request.term, function (data) {
            response($.map(data, function (value, key) {
            	//value: benid, bendisplayid,'birth-reg-id','firstname + surname', dateofbirth, dateofdeath, dateadmitted, dateleft,
            	//		ben-geo-location-id, ben-geo-location-display
                return {
                    label: concatenatearray(value, 1, 3, '\t')+'\t'+value[9],
                    value: value
                };

            }));
        });
    };
    
	window.query_all_bens_source = function (request, response) {
	    $.getJSON("/beneficiary/all_ben_autocomplete?search_token=" + request.term, function (data) {
	        response($.map(data, function (value, key) {
	        	//value: benid, bendisplayid,'birth-reg-id','firstname + surname', dateofbirth, dateofdeath, dateadmitted, dateleft,
	        	//		ben-geo-location-id, ben-geo-location-display
	            return {
	                label: concatenatearray(value, 1, 3, '\t')+'\t'+value[9],
	                value: value
	            };
	
	        }));
	    });
	};

	var concatenatearray = function (tmparray, startindex, lastindex, separator)
    {
        var charseparator = separator ? separator : '\t';
        var sindex = startindex ? startindex : 1;
        var eindex = lastindex < tmparray.length ? lastindex : tmparray.length - 1;
        var result = [];
        for (i = sindex; i <= eindex; i++) {
            if (tmparray[i] === null || tmparray[i] === '')
                continue;
            result.push(tmparray[i]);
        }

        return result.join(charseparator);
    }

});
