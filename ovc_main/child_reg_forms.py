   
'''
Created on Sep 29, 2014

@author: PKaumba
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import fields_list_provider as db_fieldchoices, validators as validate_helper
from ovc_main.utils.general import convert_to_int_array, get_list_from_dic_or_querydict
from functools import partial
from ovc_main.models import RegPerson
from django.conf import settings
from ovc_main.utils.auth_access_control_util import del_form_field, filter_fields_for_display
import datetime
import child_reg_forms_labels as form_label

#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

permission_type_fields = {  'rel_org':
                                {        
                                'auth.update ben contact by org':['beneficiary_id','physical_address','mobile_phone_number','email_address', 'edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled','err_msg'],
                                'auth.update ben general by org':['beneficiary_id','STEPS_OVC_number','residential_institution','ben_district','ben_ward','community','date_of_death',
                                                                  'location_change_date','prev_loc_change_date','no_adult_guardians','guardian_change_date',
                                                                  'prev_guard_change_date','guardian','notes_relationship','guardians_hidden','dontskip', 'edit_mode_hidden', 
                                                                  'pk_id','workforce_member','date_paper_form_filled','err_msg']
                                },
        
                            'other':
                                {
                                 'auth.update ben special':['beneficiary_id','first_name', 'last_name', 'other_name', 'sex', 'date_of_birth','birth_cert_num','nrc','edit_mode_hidden', 
                                                            'pk_id','workforce_member','date_paper_form_filled','err_msg'],
                                 'auth.update ben contact':['beneficiary_id','physical_address','mobile_phone_number','email_address', 'edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled','err_msg'],
                                'auth.update ben general':['beneficiary_id','STEPS_OVC_number','residential_institution','ben_district','ben_ward','community','date_of_death',
                                                                  'location_change_date','prev_loc_change_date','no_adult_guardians','guardian_change_date',
                                                                  'prev_guard_change_date','guardian','notes_relationship','guardians_hidden','dontskip', 'edit_mode_hidden', 
                                                                  'pk_id','workforce_member','date_paper_form_filled','err_msg']
                                }
                          }


class ChildRegistrationForm(forms.Form):
    is_update_page = False
    is_update = forms.CharField(required=False, widget=forms.HiddenInput())
    err_msg = forms.CharField(required=False, widget=forms.HiddenInput())
    
    #Child Identification information
    first_name = forms.CharField(label=form_label.label_first_name)
    last_name = forms.CharField(label=form_label.label_last_name)
    other_name = forms.CharField(required=False, label=form_label.label_other_names)
    sex = forms.ChoiceField(label=form_label.label_sex,choices=db_fieldchoices.get_sex_list())
    #steps_ovc_number = forms.CharField(required=False)
    date_of_birth = forms.DateField(required=True, widget=DateInput('%d %B, %Y'), input_formats=('%d %B %Y', '%d %B, %Y',), validators=[validate_helper.validate_child_date_of_birth])
    birth_cert_num = forms.RegexField(regex=r'^\b\w{3}\/\b\d{6}\/\d{4}$',required=False, label=form_label.label_birth_registration_number,error_message = (form_label.error_message_birth_cert_num))
    nrc = forms.RegexField(regex=r'^\b\d{6}\/\b\d{2}\/\b[1-3]{1}$',error_message = (form_label.error_message_nrc),label=form_label.label_nrc, required=False)
    pk_id = forms.CharField(label='pk_id: ', required=False, widget=forms.HiddenInput())
    STEPS_OVC_number = forms.CharField(required=False, label=form_label.label_steps_ovc_number)
    
    #Where the child lives
    residential_institution = forms.CharField(required=False, label=form_label.label_residential_institution)
    ben_district = forms.ChoiceField(label=form_label.label_district, choices=db_fieldchoices.get_list_of_districts())
    ben_ward = forms.ChoiceField(label=form_label.label_ward, choices=db_fieldchoices.get_list_of_wards())
    community = forms.ChoiceField(label=form_label.label_community, choices=db_fieldchoices.get_list_of_cwacs())
    physical_address = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 4}), label=form_label.label_physical_address)
    
    #Child's guardians/primary caregivers
    no_adult_guardians = forms.BooleanField(required = False)
    guardian = forms.CharField(required=False, label=form_label.label_guardian, widget=forms.TextInput(attrs={'class':'autocompleteben'}))
    notes_relationship = forms.CharField(required=False, label=form_label.label_notes_on_relationship)
    guardians_hidden = forms.CharField(required=False)
    
    #Adverse conditions (Click as many as apply, either now or in the past)
    #adverse_cond = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=db_fieldchoices.get_list_adverse_cond(), label='.')
    
    #Child's contact details
    mobile_phone_number = forms.RegexField(required=False,label=form_label.label_mobile_number,regex=validate_helper.mobile_phone_number_regex, error_message = (form_label.error_message_mobile_phone))
    email_address = forms.EmailField(required=False, label=form_label.label_email_address)
    res_id_hidden = forms.CharField(required=False)
    
    #Died
    date_of_death = forms.DateField(required=False, widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date]) #format='%d-%B-%Y'
    
    #Paper Trail
    workforce_member = forms.CharField(required=False, label=form_label.label_workforce_member)
    date_paper_form_filled = forms.DateField(required=False, widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    guardian_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    location_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    
    prev_guard_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    prev_loc_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    adverse_cond =  forms.MultipleChoiceField(required = False, choices=db_fieldchoices.get_list_adverse_cond(), label='.')
    
    edit_mode_hidden = forms.CharField(label='',required=False)
    
    def __init__(self, user,*args, **kwargs):
        self.user = user
        is_update_page = False
        do_display_status = 'display:None'
        hide_status = 'text'
        dont_display_status = ''
        submitButtonText = form_label.label_button_save
        actionurl = '/beneficiary/child/'
        display_date_closed_control = 'visible'
        header_text = """<div class="note note-info"><h4><strong>OVC Registration ("""+settings.APP_NAME+""" form 3b)</strong>
                        </h4><h5><p>This form is for registering orphans and vulnerable children</h5></div>"""
        
        districtid = None
        wardid = None
        if 'is_update_page' in kwargs:
            self.is_update_page = kwargs.pop('is_update_page')
            if self.is_update_page:
                do_display_status = ''
                dont_display_status = 'display:None'
                header_text = """<div class="note note-info"><h4><strong>OVC Registration ("""+settings.APP_NAME+""" form 3b)</strong></div>"""
                actionurl = '/beneficiary/update_ben/'
                submitButtonText = form_label.label_button_update
                hide_status = 'visibility:hidden'
                
                if args and 'ben_district' in args[0]:
                    districtid = args[0]['ben_district']
                
                wardsoptionlist = ''
                if args and 'ben_ward' in args[0]:
                    wardid = get_list_from_dic_or_querydict(args[0], 'ben_ward')
                    
                    if args and 'community' in args[0]:
                        communities = get_list_from_dic_or_querydict(args[0], 'community')
        
        super(ChildRegistrationForm, self).__init__(*args, **kwargs)
        
        if self.is_update_page:
            self.fields['adverse_cond'] =  forms.MultipleChoiceField(required = False, widget=forms.HiddenInput(), choices=db_fieldchoices.get_list_adverse_cond(), label='.')
            self.fields['date_paper_form_filled'] = forms.DateField(widget=DateInput('%d %B, %Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            self.fields['guardian_change_date'] = forms.DateField(required=False, widget=DateInput('%d %B, %Y'),label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            self.fields['ben_ward'] = forms.ChoiceField(label=form_label.label_ward, choices=db_fieldchoices.get_list_of_wards(districtid))
            wards = db_fieldchoices.get_list_of_wards(districtid)
            self.fields['community'] = forms.ChoiceField(label=form_label.label_community, choices=db_fieldchoices.get_list_of_cwacs(wards))
            self.fields['location_change_date'] = forms.DateField(widget=DateInput('%d %B, %Y'),required=True,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            
        else:
            self.fields['adverse_cond'] =  forms.MultipleChoiceField(widget= forms.CheckboxSelectMultiple, choices=db_fieldchoices.get_list_adverse_cond(), label='')
            self.fields['date_paper_form_filled'] = forms.DateField(required=False, widget=DateInput('%d %B, %Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            self.fields['guardian_change_date'] = forms.DateField(required=False, widget=DateInput('%d %B, %Y'),label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            self.fields['ben_ward'] = forms.ChoiceField(label=form_label.label_ward, choices=db_fieldchoices.get_list_of_wards())
            self.fields['community'] = forms.ChoiceField(label=form_label.label_community, choices=db_fieldchoices.get_list_of_cwacs())
            self.fields['location_change_date'] = forms.DateField(widget=forms.HiddenInput(),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            
        self.fields['ben_district'] = forms.ChoiceField(label=form_label.label_district, choices=db_fieldchoices.get_list_of_districts())
        #self.fields['ben_ward'] = forms.ChoiceField(label='Ward: ', choices=db_fieldchoices.get_list_of_wards(districtid))
        #self.fields['community'] = forms.ChoiceField(label='Community (CWAC or GDCLSU): ', choices=db_fieldchoices.get_list_of_cwacs(wardid))
        self.fields['date_of_birth'] = forms.DateField(required=True, widget=DateInput('%d %B, %Y'), input_formats=('%d %B %Y', '%d %B, %Y',), validators=[validate_helper.validate_child_date_of_birth])
        
        self.helper = FormHelper()
                            
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = actionurl
        self.helper.layout = Layout(
            MultiField(

                        '',
                        
                        Div(
                                Div(
                                    HTML('<p/>'),
                                    HTML("""<div id="div_id_beneficiary_id" class="form-group" style=\""""+do_display_status+"""\">
                                            <label for="id_beneficiary_id" class="control-label col-md-3">"""+settings.APP_NAME+""" """+form_label.label_beneficiary_id+""":</label>
                                            <div>
                                              <p class="form-control-static col-md-6"><strong>{{beneficiary.beneficiary_id}}</strong></p>
                                            </div>
                                        </div>"""),
                                    'birth_cert_num',
                                    Div('nrc', css_class='nrc_control'),
                                    css_class = 'panel-body pan',
                                    ),
                                Div(
                                Div(
                                    HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="1" checked="checked"/> %s' % form_label.label_option_update_beneficiary_details),
                                    css_class = 'panel-heading clearfix'
                                    ),
                                    css_class='panel panel-green',
                                    style = do_display_status,
                                    ),    
                                    
                                Div(HTML(form_label.label_heading_child_ident_info),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'first_name',
                                    'last_name',
                                    AppendedText('other_name','<span data-toggle="tooltip" title="'+form_label.label_tooltip_other_name+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'sex',
                                    AppendedText('date_of_birth','<span data-toggle="tooltip" title="'+form_label.label_tooltip_date_of_birth+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'STEPS_OVC_number',
                                    #'steps_ovc_number',
                                    'pk_id',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                                Div(HTML('<span class="col-md-4">'+form_label.label_heading_where_child_lives+'</span>'),
                                    Div(
                                            Div(
                                                Div(
                                                    HTML('<div class="col-md-3"><a href=\"/organisation/new/\" target=\"_blank\"><u>'+form_label.label_register_org_unit+'</u>.</a></div>'),
                                                    Field('location_change_date', template='ovc_main/custom_date_field.html', style = do_display_status)
                                                )
                                            )
                                        ),
                                    css_class = 'panel-heading col-md-12'
                                    ),
                            Div(
                                    HTML('<p/>'),
                                    Field(Div('edit_mode_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    #Field(AppendedText('residential_institution','<span data-toggle="tooltip" title="If the child is a full time resident of an orphanage, care home, correctional institution or other type of residential institution" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'), style="display:inline", css_class="autocompleteRes form-inline", id="id_guardian "),
                                    Div(Field('residential_institution', style="display:inline", css_class="autocompleteRes form-inline"),css_class='loc_controls'),
                                    css_class = 'panel-body pan',
                                    style = dont_display_status,
                                    ),
                            Div(
                                HTML('<p/><strong>Or</strong>'),
                                style='margin-left:15%'
                                ),
                                Div(
                                    HTML('<p/>'),
                                    Div('ben_district',css_class='loc_controls'),
                                    Div('ben_ward',css_class='loc_controls'),
                                    Div('community',css_class='loc_controls'),
                                    Div('physical_address', css_class='loc_controls'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                            ),       
                        Div(
                                Div(HTML('<span class="col-md-4">%s</span>' % form_label.label_childs_guardian),
                                    Div(
                                            Div(
                                                Div(
                                                    HTML('<div class="col-md-3"><a href=\"/beneficiary/guardian/\" target=\"_blank\"><u>'+form_label.label_register_guardian+'</u>.</a></div>'),
                                                    Field('guardian_change_date', template='ovc_main/custom_guardian_change_date.html', style = do_display_status)
                                                )
                                            )
                                        ),
                                    css_class = 'panel-heading col-md-12'
                                    ),
                            
                            
                                Div(
                                    Field(Div('guardians_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    Div('no_adult_guardians', style='margin-left:17%'),
                                    Div(
                                        AppendedText(
                                                     'guardian',
                                                     '<span data-toggle="tooltip" data-html="true" title="'+form_label.label_tooltip_guardian+'"><i class="fa fa-info-circle autocompleteben"></i></span>'
                                                     ), 
                                        css_class='guardian_controls', 
                                        ),
                                    Div('notes_relationship', css_class='guardian_controls'),
                                    HTML("<label id=\"guardian_error_display\" style=\"color:red;margin-left:26%;display:None\"><strong>  </strong></label>"),
                                    css_class = 'panel-body pan',
                                    ),
                                Div(
                                    HTML("<br><a type=\"button\" onclick=\"addGuardian()\" id=\"ben_add\" class=\"dontskip btn btn-success\">"+form_label.label_button_add+"</a><br><br>"),
                                    style='margin-left:41%',
                                    css_class='guardian_controls'
                                    ),
                                Div(
                                    HTML("""
                                    <table id="guardianTable"  class="table table-hover table-condensed">
                                        <thead>
                                        <tr>
                                            <th>"""+form_label.label_ben_identification+"""</th>
                                            <th>"""+form_label.label_name+"""</th>
                                            <th>"""+form_label.label_notes_on_relationship+"""</th>
                                            <th style=\""""+hide_status+"""\">"""+form_label.label_use_contact_details+"""</th>
                                            <th>"""+form_label.label_button_remove+"""</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {% if beneficiary.guardian  %}
                                        {% for guardn in beneficiary.guardian %}
                                            <tr>
                                                <td id = "1">{{ guardn.guardian }}</td>
                                                <td id = "2">{{ guardn.guard_name }}</td>
                                                <td id = "3">{{ guardn.notes_relationship }}</td>
                                                <td id = "3"> </td>
                                                <td id = '5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>
                                            </tr>
                                        {% endfor %}
                                        {% endif %}
                                        </tbody>
                                    </table>
                                    
                                    
                                    
                                    <input id="ben_id_hidden" type="hidden">
                                    <input id="ben_name_hidden" type="hidden">
                                    <input id="ben_wfc_name_national_id" type="hidden">
                                    
                                    <input id="ben_res_name_hidden" type="hidden">
                                    <input id="ben_res_id_hidden" type="hidden">
                                    
                                    
                                    
                                    
                                  """),
                                    css_class = 'panel-body pan guardian_controls'
                                    ),
                                css_class='panel panel-primary',
                            ),
                       Div(
                                Div(HTML(form_label.label_heading_adverse_conditions),
                                    css_class = 'panel-heading',
                                    style = dont_display_status,
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Field('adverse_cond', template='ovc_main/adverse_conditions.html'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary',
                                style = dont_display_status,
                            ),
                        Div(
                                Div(HTML(form_label.label_heading_child_contact_details),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Field(Div('prev_guard_change_date', style='visibility:hidden'), css_class='form-inline'),
                                    'mobile_phone_number',
                                    'err_msg',
                                    'email_address',
                                    Field(Div('prev_loc_change_date', style='visibility:hidden'), css_class='form-inline'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-primary',
                                
                            ),
                       Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="2"/> '+form_label.label_option_died),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Div(
                                        'date_of_death', css_class='dead_control'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-green',
                                style = do_display_status,
                            ),
                       Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="3"/> %s' %form_label.label_option_ovc_never_existed),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                    ),
                                css_class='panel panel-green',
                            ),
                        Div(
                                Div(HTML(form_label.label_heading_paper_trail),
                                    Div(
                                        Div(
                                            HTML('<a href=\"/workforce/new_workforce/\" target=\"_blank\">'+form_label.label_heading_register_workforce+'</u></a>'),
                                            css_class = 'form-inline'
                                            ),
                                        css_class= 'toolbars'
                                    ),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    'is_update',
                                    Field('workforce_member', style="display:inline", css_class="autocompletewfc form-inline", id="id_workforce_member "),
                                    'date_paper_form_filled',
                                    Field(Div('res_id_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    css_class = 'panel-body pan'
                                    ),
                                Div(
                                    Div(
                                    HTML("""
                                    <table id="paperTrailTable"  class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>"""+form_label.label_person_recorded+"""</th>
                                            <th>"""+form_label.label_paper_date+"""</th>
                                            <th>"""+form_label.label_person_recorded_electronically+"""</th>
                                            <th>"""+form_label.label_electronic_time+"""</th>
                                            <th>"""+form_label.label_interface+"""</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {% if beneficiary.audit_trail  %}
                                        {% for audit in beneficiary.audit_trail %}
                                            <tr>
                                                <td id = "1">{{ audit.Person_recorded_on_paper }}</td>
                                                <td id = "2">{{ audit.Paper_date }}</td>
                                                <td id = "3">{{ audit.Person_recorded_electronically }}</td>
                                                <td id = "4">{{ audit.Electronic_date_time }}</td>
                                                <td id = "5">{{ audit.Interface }}</td>
                                            </tr>
                                        {% endfor %}
                                        {% endif %}
                                        </tbody>
                                    </table>
                                    
                                    <!-- Button trigger modal -->
                                    <button id="confirm_dialog_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                      Launch demo modal
                                    </button>
                                    
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Duplicate Person</h4>
                                          </div>
                                          <div class="modal-body">
                                            """+form_label.error_message_duplicate_name+"""
                                          </div>
                                          <div class="modal-footer">
                                            <button id="id_cancel_submit" type="button" class="btn btn-default" data-dismiss="modal">"""+form_label.label_button_make_changes+"""</button>
                                            <button class="btn btn-success" id="submit">"""+form_label.label_button_save_anyway+"""</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                                    <!-- Button trigger modal -->
                                    <button id="duplicate_dialog_box" type="button" style="visibility:hidden" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#dupModal">
                                      Launch demo modal
                                    </button>
                                    
                                    <!-- Modal -->
                                    <div class="modal fade" id="dupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Duplicate Person</h4>
                                          </div>
                                          <div class="modal-body">
                                            """+form_label.error_message_sure_void+"""
                                          </div>
                                          <div class="modal-footer">
                                            <button id="id_cancel_submit" type="button" class="btn btn-default" data-dismiss="modal">"""+form_label.label_button_no+"""</button>
                                            <button class="btn btn-success" id="submit">"""+form_label.label_button_yes+"""</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                
                                                               
                                    <input id="ben_wfc_name_hidden" type="hidden">
                                    <input id="ben_wfc_id_hidden" type="hidden">
                                    <input id="ben_wfc_name_national_id" type="hidden">
                                  """),
                                    )
                                    ),
                                    css_class='mandatory panel panel-primary',
                          ),
                       Div(
                        HTML("<Button id='submit_child_reg' onclick=\"updateFinale(event)\" type=\"submit\" style=\"margin-right:4px\" class=\"mandatory btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+submitButtonText+"</Button>"),
                        HTML("<a type=\"button\" href=\"/beneficiary/ben_search/\"class=\"mandatory btn btn-success\"><i class=\"fa fa-undo\"></i> "+form_label.label_button_cancel+"</a><br><br>"),
                        style='margin-left:38%'
                        ),
            )
        )
        
        if self.is_update_page:
            layout = self.helper.layout
            searchitems = filter_fields_for_display(self.user, permission_type_fields)
            if searchitems:
                del_form_field(layout, searchitems)
                self.helper.layout = layout
    
    
      
    def clean(self):
        if (self.cleaned_data.get('guardian_change_date') != None):
            entered_value = self.cleaned_data.get('guardian_change_date')
            if (self.cleaned_data.get('prev_guard_change_date') != None):
                date_linked_value = self.cleaned_data.get('prev_guard_change_date')
                if entered_value < date_linked_value:
                    self._errors["location_change_date"] = self.error_class([u""+form_label.error_message_guardian_change_date])
        
        if (self.cleaned_data.get('err_msg') != None):
            error_message = self.cleaned_data.get('err_msg')
            if error_message:
                self._errors["last_name"] = self.error_class([u""+error_message])
        
        if (self.cleaned_data.get('location_change_date') != None):
            entered_value = self.cleaned_data.get('location_change_date')
            if (self.cleaned_data.get('prev_loc_change_date') != None):
                date_linked_value = self.cleaned_data.get('prev_loc_change_date')
                if entered_value < date_linked_value:
                    self._errors["location_change_date"] = self.error_class([u""+form_label.error_message_location_change_date])
                #elif entered_value < most_recent_change_date:
                #    self._errors["location_change_date"] = self.error_class([u"Location change date cannot be less than previous record dates for this OVC"])
        
        if (self.cleaned_data.get('guardians_hidden') != None):
            value = self.cleaned_data.get('guardians_hidden')
            no_guardian = self.cleaned_data.get('no_adult_guardians')
            if not no_guardian:
                if not value:
                    self._errors["no_adult_guardians"] = self.error_class([u""+form_label.error_message_no_guardians])
        
        if (self.cleaned_data.get('nrc') != None or self.cleaned_data.get('first_name') != None or self.cleaned_data.get('last_name') != None):
            
            nrc_id = self.cleaned_data.get('nrc')
            f_name = self.cleaned_data.get('first_name')
            l_name = self.cleaned_data.get('last_name')
            
            if nrc_id or (f_name and l_name):
                if(RegPerson.objects.filter(national_id=nrc_id,is_void=False,date_of_death = None).count() > 0):
                    person = RegPerson.objects.filter(national_id=nrc_id,is_void=False,date_of_death = None)
                    m_person = None
                    for m_per in person:
                        m_person = m_per
                    
                    pk_db = int(m_person.pk)
                    pk_entered = None
                    if (self.cleaned_data.get('pk_id') != None):
                        if self.cleaned_data.get('pk_id'):
                            pk_entered = int(self.cleaned_data.get('pk_id'))
                    
                    if (self.is_update_page and (pk_db == pk_entered)):
                        do_nothing = None
                    else:
                        if m_person.national_id:
                            msg = "[%s],[%s],[%s, %s, %s] %s"% (m_person.national_id,m_person.beneficiary_id,m_person.first_name,m_person.surname,m_person.other_names,form_label.error_message_duplicate_last_part)
                        else:
                            msg = form_label.error_message_duplicate_nrc
                        if m_person.national_id:
                            self._errors["nrc"] = self.error_class([u""+msg])
                
                '''
                if(RegPerson.objects.filter(first_name=f_name,surname=l_name,is_void=False,date_of_death = None).count() > 0):
                    person = RegPerson.objects.filter(first_name=f_name,surname=l_name,is_void=False,date_of_death = None)
                    m_person = None
                    for m_per in person:
                        m_person = m_per
                    
                    if m_person:
                        msg = "["+m_person.national_id+"],["+m_person.beneficiary_id+"], ["+m_person.first_name+", "+m_person.surname+", "+m_person.other_names+"] is already in this registry."# Do you want to save?"
                    else:
                        msg = "This person is already in the system."
                    self._errors["nrc"] = self.error_class([u""+msg])
                '''
                               
        if not self.is_update_page:
            adverse_cond = self.cleaned_data.get('adverse_cond')
            if not adverse_cond:
                self._errors["adverse_cond"] = self.error_class([u""+form_label.error_message_no_adverse_condition])
                
        if self.is_update_page:
            if (self.cleaned_data.get('date_of_birth') != None):
                dob = self.cleaned_data.get('date_of_birth')
                
                if (self.cleaned_data.get('date_of_death') != None):
                    dod = self.cleaned_data.get('date_of_death')
                    pk_pers = self.cleaned_data.get('pk_id')
                    most_recent_change_date = None
                    mrcd=None
                    if pk_pers:
                        most_recent_change_date = db_fieldchoices.get_most_recent_record_date_for_person_id(pk_pers)
                        if most_recent_change_date:
                            mrcd = most_recent_change_date.date()
                    if dod:
                        if dod < dob:
                            self._errors["date_of_death"] = self.error_class([u""+form_label.error_message_death_before_birth])
                        elif mrcd:
                            if dod < mrcd:
                                self._errors["date_of_death"] = self.error_class([u""+form_label.error_message_death_before_last_beneficiary_date])
        if self._errors:
            self._errors["__all__"] = self.error_class([u""+form_label.error_message_page_errors])
            
        return self.cleaned_data

    
    
