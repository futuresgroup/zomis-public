from django.db import models

# Create your models here.
class MessageText(models.Model):
    variable = models.CharField(max_length=100)
    text = models.CharField(max_length=255)
    description = models.CharField(max_length=255)

    class Meta:
        db_table = "tbl_sms_messages"
