import datetime
from simplesmsforms.smsform import SMSForm
from simplesmsforms.smsform_fields import PrefixField, SingleChoiceField, MultiChoiceField, DateField
from sms_fields import (OVCDateField, OVCIDField, OVCBeneficiaryIDField,
OVCOrgUnitField, OVCPasswordField, OVCBracketField, OVCIDField)
from db_lookups import (get_prefix_list, get_services_provided_list,
                        get_referrals_provided_list, get_adverse_conditions_list,
                        get_encounter_type_list)
from sms_fields import deny_date_beyond_three_months_ago, deny_future_dates, deny_longer_than_six_characters, deny_special_characters

class CHGPForm(SMSForm):
    """change password SMS form"""
    keyword = "CHGP"

    member_id = OVCIDField(prefixes=["wnrc", "w"], name="member_ID", required=False)
    password = OVCPasswordField(prefixes=["x"], name="password", required=True)
    new_password = OVCBracketField(
        name="New Password",
        validators=[deny_longer_than_six_characters,
                    deny_special_characters],
        required=True,)

    def get_fields(self):
        return [self.member_id, self.password, self.new_password]

class FINGForm(SMSForm):
    keyword = "FING"

    search_text = OVCBracketField(name="Search Text", required=True)

    def get_fields(self):
        return [self.search_text]

class FINKForm(SMSForm):
    keyword = "FINK"

    search_text = OVCBracketField(name="Search Text", required=True)

    def get_fields(self):
        return [self.search_text]

class FINUForm(SMSForm):
    keyword = "FINU"

    search_text = OVCBracketField(name="Search Text", required=True)

    def get_fields(self):
        return [self.search_text]

SEX_CHOICES = ("SFEM", "SMAL")
class FINWForm(SMSForm):
    keyword = "FINW"

    search_text = OVCBracketField(name="Search Text", required=True)
    member_id = OVCIDField(prefixes=["wnrc", "w"], name="member_ID",
                           required=False, check_duplicates=False)
    password = OVCPasswordField(prefixes=["x"], name="password", required=True)
    age = PrefixField(prefixes=["agey"], name="age", required=False)
    sex = MultiChoiceField(choices=SEX_CHOICES, name="sex",
                           required=False)

    def get_fields(self):
        return [self.member_id, self.password, self.search_text, self.age,
                self.sex]

CADRE_CHOICES = ("TBVC", "TBGR")
class FINBForm(SMSForm):
    keyword = "FINB"

    member_id = OVCIDField(prefixes=["wnrc"], name="member_ID", required=False)
    search_text = OVCBracketField(name="Search Text", required=True)
    member_id = OVCIDField(prefixes=["wnrc", "w"], name="member_ID", required=False)
    password = OVCPasswordField(prefixes=["x"], name="password", required=True)
    age = PrefixField(prefixes=["agey"], name="age", required=False)
    sex = MultiChoiceField(choices=SEX_CHOICES, name="sex",
                           required=False)

    def get_fields(self):
        return [self.member_id, self.password, self.search_text, self.age,
                self.sex]

class RECSForm(SMSForm):
    keyword = "RECS"

    member_id = OVCIDField(prefixes=["wnrc"], name="member_ID", required=False)
    password = OVCPasswordField(prefixes=["x"], name="password", required=True)
    beneficiary_id = OVCBeneficiaryIDField(
        prefixes=["bnrc", "bbcn"], name="beneficiary_ID", required=True)
    encounter = SingleChoiceField(
        name="encounter_type", choices=get_encounter_type_list(), required=True)
    org_id = OVCOrgUnitField(name="organisation_ID", required=False)
    ward = PrefixField(prefixes=["lgeo"], name="ward", required=False)
    service_date = OVCDateField(
        name="service_date",
        required=False,
        date_formats=[
            "%d%b%Y",
            "%d%b%y",
            "%d%B%y",
            "%d%B%Y",
            "%d/%m/%y",
            "%d/%m/%Y",
            "%d-%m-%y",
            "%d-%m-%Y",
        ],
        validators=[
            deny_future_dates,
            deny_date_beyond_three_months_ago
        ])
    services_provided = MultiChoiceField(
        name="services_provided",
        choices=get_services_provided_list(),
        required=False)
    referral_provided = MultiChoiceField(
        name="referral_provided",
        choices=get_referrals_provided_list(),
        required=False)
    adverse_conditions = MultiChoiceField(
        name="adverse_conditions",
        choices=get_adverse_conditions_list(),
        required=False)

    def get_fields(self):
        return [self.member_id, self.password, self.beneficiary_id, self.encounter,
                self.org_id, self.ward, self.service_date, self.services_provided,
                self.referral_provided, self.adverse_conditions]



