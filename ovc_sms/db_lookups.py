import datetime
from django.contrib.auth.hashers import check_password, make_password
from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Q
from ovc_main.models import (SetupList, SetupGeorgraphy, RegPerson, RegPersonsContact,
 RegPersonsOrgUnits, RegOrgUnit, RegPersonsGeo, CoreEncounters, CoreServices,
 CoreAdverseConditions)
from rapidsms.contrib.messagelog.models import Connection, Message
from simplesmsforms.smsform_exceptions import ChoiceException
import sms_exceptions
from ovc_sms.models import MessageText
from ovc_main.utils import lookup_field_dictionary as fielddictionary
from ovc_auth.models import AppUser
import const
from general_utils import generic_functions
from ovc_auth.user_manager import set_password

def lookup_messages_text(variable):
    try:
        message_text = MessageText.objects.get(variable=variable)
    except MessageText.DoesNotExist:
        return ""
    else:
        return unicode(message_text.text)


def get_prefix_list(prefix_description):
    SetupList.objects.filter(item_description=prefix_description)


def get_services_provided_list():
    service_types = SetupList.objects.filter(
        item_category="Service type")
    return [item.item_id for item in service_types]


def get_referrals_provided_list():
    referrals_types = SetupList.objects.filter(
        item_category__in=["Referral completed", "Referral made"])
    return [item.item_id for item in referrals_types]


def get_adverse_conditions_list():
    adverse_types = SetupList.objects.filter(
        item_category__in=["Adverse condition"])
    return [item.item_id for item in adverse_types]


def get_encounter_type_list():
    encounter_types = SetupList.objects.filter(
        item_category__in=["Encounter type"])
    return [item.item_id for item in encounter_types]


def lookup_db_values(value_list):
    regex = "|".join(value_list)
    items = SetupList.objects.filter(
        item_id__iregex="({regex})".format(regex=regex))

    return [item.item_description for item in items]


def get_ward_district_prefix():
    ward_prefix = SetupList.objects.filter(
        item_description__in=[
            "Ward or District ID prefix",
            ])
    return [item.item_id for item in ward_prefix]


def get_password_prefix():
    password_prefixes = SetupList.objects.filter(
        item_description__in=[
            "Password prefix",
            ])
    return [item.item_id for item in password_prefixes]


def get_org_prefix():
    org_prefixes = SetupList.objects.filter(
        item_description__in=[
            "Organisational unit ID prefix",
            ])
    return [item.item_id for item in org_prefixes]


def get_workforce_id_prefix():
    workforce_ids = SetupList.objects.filter(
        item_description__in=[
            "Workforce member ID prefix",
            "Workforce member NRC prefix"])
    return [item.item_id for item in workforce_ids]


def get_beneficiary_id_prefix():
    beneficiary_ids = SetupList.objects.filter(
        item_description__in=[
            "Beneficiary ID prefix",
            "Beneficiary birth certificate number prefix",
            "Beneficiary NRC prefix"])
    return [item.item_id for item in beneficiary_ids]


def get_district_constituencies(district_id):
    constituencies = SetupGeorgraphy.objects.filter(
        area_type_id=fielddictionary.geo_type_constituency_code,
        parent_area_id=district_id)
    return constituencies

def get_district_wards(district_id):
    constituencies = get_district_constituencies(district_id)
    wards = SetupGeorgraphy.objects.filter(
        area_type_id=fielddictionary.geo_type_ward_code,
        parent_area_id__in=constituencies.values_list("area_id"))
    return wards

def get_parent_area_by_type(area_id, parent_area_type_id):
    """Looks up in the parent geo trees to find the parent with a specific
    area_type_id"""
    area = get_area_from_id_or_exception(area_id)
    parent_area_id = area.parent_area_id
    if not parent_area_id:
        return False
    else:
        parent_area = get_area_from_id_or_exception(parent_area_id)
        if parent_area.area_type_id == parent_area_type_id:
            return parent_area
        else:
            return get_parent_area_by_type(parent_area.area_id, parent_area_type_id)

def get_all_districts():
    return SetupGeorgraphy.objects.filter(area_type_id=fielddictionary.geo_type_district_code)

def get_all_wards():
    return SetupGeorgraphy.objects.filter(area_type_id=fielddictionary.geo_type_ward_code)

def check_worker_area(person, area_ids=None, encounter_date=None):
    """
        If the workforce member has no geographical tags, all wards are allowed.
        If the workforce member has district tag or tags only, only wards within those district(s) are allowed
        If the workforce member has ward tags, only those wards are allowed
    """
    allowed_areas = []

    all_assigned_tags = RegPersonsGeo.objects.filter(person=person)

    #Check for delinked
    all_assigned_tags = all_assigned_tags.filter(Q(date_delinked__isnull=True)|Q(date_delinked__gt=encounter_date))

    all_wards = get_all_wards()
    all_assigned_wards = all_assigned_tags.filter(area_id__in=all_wards.values_list('area_id', flat=True))

    all_districts = get_all_districts()
    assigned_districts = all_assigned_tags.filter(area_id__in=all_districts.values_list('area_id', flat=True))

    if not area_ids:
        area_ids =  [area_id.area_id for area_id in all_assigned_tags]

    #condition one
    if not all_assigned_tags:#Has no tags assigned so we allow anything
        allowed_areas = all_wards

    #has district tags, so we allow only districts
    if assigned_districts and not allowed_areas:
        allowed_areas = []
        for assigned_district in assigned_districts:
            allowed_areas.extend(get_district_wards(assigned_district.area_id))


    if all_assigned_wards and not allowed_areas:
        allowed_areas = all_assigned_wards


    allowed_areas = [ward.area_id for ward in allowed_areas]
    if not set(area_ids).issubset(set(allowed_areas)):
        raise sms_exceptions.InvalidWorkerGeoException(field='')

    return True

def check_beneficiary_ward(user, ward_id, encounter_date):
    """Checks to ensure that the ward specified is part of the beneficiary's
    geographical data"""
    person_geos = RegPersonsGeo.objects.filter(person=user, area_id=ward_id)
    person_geos = person_geos.filter(Q(date_delinked__isnull=True)|Q(date_delinked__gt=encounter_date))
    if not ward_id in person_geos.values_list('area_id', flat=True):
        raise sms_exceptions.InvalidBeneficiaryWardException(field=ward_id)
    return True


def get_beneficiary_or_exception(beneficiary_id_prefix, beneficiary_id, encounter_date):
    """
    Figure out the ID that was passed in from beneficary_id_prefix and return
    the True, beneficiary or False, Error
    """
    try:
        if beneficiary_id_prefix.lower() == "bbcn":
            beneficiary = RegPerson.objects.get(birth_reg_id__iexact=beneficiary_id)
        elif beneficiary_id_prefix.lower() == "bnrc":
            beneficiary = RegPerson.objects.get(national_id=beneficiary_id)
        else:
            beneficiary = RegPerson.objects.get(beneficiary_id__iexact=beneficiary_id)
    except RegPerson.DoesNotExist:
        raise sms_exceptions.InvalidBeneficiaryIDException(field="{id_prefix}{id}".format(
            id_prefix=beneficiary_id_prefix.upper(), id=beneficiary_id))
    else:
        """When you are checking existence of workforce and beneficiary in the db in tbl_reg_org_persons filter
        on(void=false and (date_of_death is null or date_of_death > [sms encounter date]))"""
        if beneficiary.date_of_death and beneficiary.date_of_death < encounter_date or beneficiary.is_void:
            raise sms_exceptions.PersonIsVoidException(field=beneficiary_id)

        return  beneficiary

def get_worker_or_exception(worker_id_prefix, worker_id, encounter_date):
    try:
        if worker_id_prefix.lower() == "wnrc":
            worker = RegPerson.objects.get(national_id=worker_id)
        else:
            worker = RegPerson.objects.get(workforce_id__iexact=worker_id)
    except RegPerson.DoesNotExist:
        raise sms_exceptions.InvalidWorkerIDException(field="{id_prefix}={id}".format(
            id_prefix=worker_id_prefix, id=worker_id))
    else:
        if worker.is_void:
            raise sms_exceptions.PersonIsVoidException(field=worker_id)

        """When you are checking existence of workforce and beneficiary in the db in tbl_reg_org_persons filter
        on(void=false and (date_of_death is null or date_of_death > [sms encounter date]))"""
        if worker.date_of_death and worker.date_of_death < encounter_date or worker.is_void:
            raise sms_exceptions.PersonIsVoidException(field=worker_id)
        return  worker

def get_org_unit_or_exception(unit_id, encounter_date):
    try:
        org_unit = RegOrgUnit.objects.get(org_unit_id_vis__iexact=unit_id)
    except RegOrgUnit.DoesNotExist:
        raise sms_exceptions.NonExistantOrgUnitException(field=unit_id)
    return org_unit

def check_org_unit(user, unit_id, encounter_date):
    """checks that the org unit passed in is one of the parent or units of the
    person"""
    """When you are checking existence of org units in the db tbl_reg_org_units filter tbl_reg_org_units on
    (void=false and (date_closed is null or date_closed > [sms encounter date]))"""
    person_org_units = RegPersonsOrgUnits.objects.filter(person=user, parent_org_unit__is_void=False)
    person_org_units = person_org_units.filter(Q(parent_org_unit__date_closed__isnull=True)|Q(parent_org_unit__date_closed__gt=encounter_date))
    person_org_units = person_org_units.filter(Q(date_delinked__isnull=True)|Q(date_delinked__gt=encounter_date))
    if not str(unit_id) in person_org_units.values_list('parent_org_unit__org_unit_id_vis', flat=True):
        raise sms_exceptions.InvalidOrgUnitSelectionException(field=unit_id)
    return True

def get_org_id_from_person(person, encounter_date):
    try:
        reg_person_units = RegPersonsOrgUnits.objects.filter(
            person=person,
            primary=True,
            )
        reg_person_units = reg_person_units.filter(Q(date_delinked__isnull=True)|Q(date_delinked__gt=encounter_date))
        reg_person_unit = reg_person_units[0]
    except IndexError :
        raise sms_exceptions.NoPrimaryOrgUnit(field='')

    return reg_person_unit.parent_org_unit.org_unit_id_vis

def get_beneficiary_ward_or_exception(beneficiary, encounter_date):
    try:
        beneficiary_geos = RegPersonsGeo.objects.filter(person=beneficiary)
        beneficiary_geos = beneficiary_geos.filter(Q(date_delinked__isnull=True)|Q(date_delinked__gt=encounter_date))
        beneficiary_geo = beneficiary_geos[0]
    except IndexError:
        raise sms_exceptions.InvalidBeneficiaryWardException(field=beneficiary.beneficiary_id)
    return get_area_from_id_or_exception(beneficiary_geo.area_id)

def get_area_from_id_or_exception(area_id):
    try:
        geo = SetupGeorgraphy.objects.get(area_id=area_id)
    except SetupGeorgraphy.DoesNotExist:
        raise sms_exceptions.InvalidBeneficiaryWardException(field=area_id)
    return geo

def authenticate_sms_user(phone_number, member_id_prefix='', member_id='', password='', allow_different_phone_number=True):
    """
    Authenticates user and returns them or raises Authentication Exception
    If phone number is registered and password included, authenticate using that
    only
    If phone number is not registered, authenticate using workforce_id and password

    Returns an authenticated user, use authuser.reg_person to get to the workforce object
    """
    #The phone number given to us by rapidsms does not have a + but the number
    #in the user profile has the +
    if not phone_number.startswith('+'):
        phone_number = '+'+phone_number
    try:
        if member_id:
            reg_contact = RegPersonsContact.objects.get(
            contact_detail_type_id=fielddictionary.contact_designated_mobile_phone,
            contact_detail=phone_number,
            person__workforce_id__iexact=member_id)
        else:
            reg_contact = RegPersonsContact.objects.get(
            contact_detail_type_id=fielddictionary.contact_designated_mobile_phone,
            contact_detail=phone_number)
        user = AppUser.objects.get(reg_person=reg_contact.person)

        if not check_password(password, user.password):
            raise sms_exceptions.AuthenticationException(lookup_messages_text("AUTH_ERROR_INVALID_PASSWORD"))
    except RegPersonsContact.DoesNotExist:
        if not allow_different_phone_number:
            raise sms_exceptions.AuthenticationException(lookup_messages_text("AUTH_ERROR_INVALID_ID_OR_PASSWORD_UNREGISTERED_NUMBER"))
        #If not a registered number, workforce id needs to be present
        if not member_id:
            raise sms_exceptions.AuthenticationException(lookup_messages_text("AUTH_ERROR_MISSING_WORKFORCE_ID_UNREGISTERED_NUMBER"))

        try:
            if member_id_prefix == "wnrc":
                user = AppUser.objects.get(national_id=member_id)
            else:
                user = AppUser.objects.get(workforce_id__iexact=member_id)
        except AppUser.DoesNotExist:
            raise sms_exceptions.AuthenticationException(lookup_messages_text("AUTH_ERROR_INVALID_ID_OR_PASSWORD_UNREGISTERED_NUMBER"))
        else:
            if not check_password(password, user.password):
                raise sms_exceptions.AuthenticationException(lookup_messages_text("AUTH_ERROR_INVALID_ID_OR_PASSWORD_UNREGISTERED_NUMBER"))

    return user

def clean_fields(sms_fields):
    """Do any final cleaning of the fields, returns simple mapping of values and prefixes
    to actual db items where possible."""
    cleaned_fields = {}
    try:
        cleaned_fields['encounter_date'] = sms_fields['service_date']['value']
    except KeyError:
        cleaned_fields['encounter_date'] = datetime.date.today()

    cleaned_fields['password'] = sms_fields['password']['value']
    member_id = sms_fields['member_ID']['value']
    member_id_prefix = sms_fields['member_ID']['prefix']
    cleaned_fields['worker'] = get_worker_or_exception(member_id_prefix, member_id, cleaned_fields["encounter_date"])

    try:
        organisation_id = sms_fields['organisation_ID']['value']
    except KeyError:
        organisation_id = ''
    if organisation_id:
        cleaned_fields['organisation'] = get_org_unit_or_exception(organisation_id, cleaned_fields["encounter_date"])
    else:
        organisation_id = get_org_id_from_person(cleaned_fields['worker'], cleaned_fields["encounter_date"])
        cleaned_fields['organisation'] = get_org_unit_or_exception(organisation_id, cleaned_fields["encounter_date"])

    beneficiary_id = sms_fields['beneficiary_ID']['value']
    beneficiary_id_prefix = sms_fields['beneficiary_ID']['prefix']
    cleaned_fields['beneficiary'] = get_beneficiary_or_exception(beneficiary_id_prefix, beneficiary_id, cleaned_fields['encounter_date'])

    try:
        area_id = sms_fields['ward']['value']
    except KeyError:
        area_id = ''
    if area_id:
        cleaned_fields['area'] = get_area_from_id_or_exception(area_id)
    else:
        cleaned_fields['area'] = get_beneficiary_ward_or_exception(cleaned_fields['beneficiary'], cleaned_fields["encounter_date"])

    encounter_type_list = sms_fields['encounter_type']['value']
    cleaned_fields['encounter_type']  = [encounter.upper() for encounter in encounter_type_list]

    try:
        adverse_conditions_list = sms_fields['adverse_conditions']['value']
    except KeyError:
        adverse_conditions_list = []
    cleaned_fields['adverse_conditions'] = [condition.upper() for condition in adverse_conditions_list]

    try:
        referral_provided_list = sms_fields['referral_provided']['value']
    except KeyError:
        referral_provided_list = []
    cleaned_fields['referral_provided'] = [referral.upper() for referral in referral_provided_list]

    try:
        services_provided_list = sms_fields['services_provided']['value']
    except KeyError:
        services_provided_list = []
    cleaned_fields['services_provided'] = [service.upper() for service in services_provided_list]

    return cleaned_fields

from django.db import transaction
@transaction.atomic
def save(sms, phone_number, sms_fields):
    """
    Authenticate
    Save encounter
    Save adverse
    Save services

    return saved, errors
    """
    saved = False
    try:
        password = sms_fields['password']['value']
        member_id_prefix = sms_fields.get('member_ID', '') or {'value':'', 'prefix':''}

        member_id = member_id_prefix['value']
        member_id_prefix = member_id_prefix['prefix']
        auth_user = authenticate_sms_user(phone_number, member_id_prefix=member_id_prefix, member_id=member_id, password=password)
        worker = auth_user.reg_person
    except sms_exceptions.SMSFieldException as e:
        return saved, [e]
    else:
        #replace values
        sms_fields["member_ID"] = {}
        if not worker.workforce_id:
            return saved, [sms_exceptions.UserNotAuthorisedServiceProvider(field='')]
        sms_fields["member_ID"]['value'] = worker.workforce_id
        sms_fields["member_ID"]['prefix'] = "w"

    #clean fields
    try:
        cleaned_fields = clean_fields(sms_fields)
    except sms_exceptions.SMSFieldException as e:
        return saved, [e]

    organisation = cleaned_fields['organisation']
    beneficiary = cleaned_fields['beneficiary']
    area = cleaned_fields['area']
    referral_provided = cleaned_fields['referral_provided']
    services_provided = cleaned_fields['services_provided']
    adverse_conditions = cleaned_fields['adverse_conditions']
    encounter_date = cleaned_fields['encounter_date']
    encounter_type = cleaned_fields['encounter_type']


    #Run all validators
    try:
        from sms_fields import deny_future_dates, deny_date_beyond_three_months_ago
        deny_future_dates(encounter_date)
        deny_date_beyond_three_months_ago(encounter_date)
        check_org_unit(worker, organisation.org_unit_id_vis, encounter_date=encounter_date)
        check_worker_area(worker, [area.area_id], encounter_date)
        #TODO ADD tests for this
        if "EHOM" in encounter_type:
            check_beneficiary_ward(beneficiary, area.area_id, encounter_date)
        if not referral_provided and not services_provided and not adverse_conditions:
            raise sms_exceptions.NoServicesOrConditionsRecordedException(field='')
    except sms_exceptions.SMSFieldException as e:
        return saved, [e]

    encounter_type = encounter_type[0]

    try:
        with transaction.atomic():
            save_encounter(worker, beneficiary, encounter_date,
                           organisation.pk, area.area_id, encounter_type, sms.logger_msg.id)
            for adverse_condition in set(adverse_conditions):
                save_adverse_condition(beneficiary, adverse_condition, sms.logger_msg.id)

            for referral in set(referral_provided):
                save_services(worker, beneficiary, encounter_date, referral, sms.logger_msg.id)

            for service_provided in set(services_provided):
                save_services(worker, beneficiary, encounter_date, service_provided, sms.logger_msg.id)


            saved = True
    except sms_exceptions.SMSFieldException as e:
        return saved, [e]


    return saved, []


def save_encounter(workforce, beneficiary, encounter_date, org_unit_id, area_id, encounter_type_id, sms_id):
    """If an encounter is already in the tbl_core_encounters for the same
        workforce member, same beneficiary and same date, and it has a form_id,
        then do not make any changes. (Data entered via forms takes precedence
        over data entered via sms). If it has no form_id, delete the previous record
        and the associated records in tbl_core_services, and replace with new
        records.
        return True, encounter if this was a freshly created encounter
        or False, encounter if there was a previous encounter with form id that has been returned

    """
    new_encounter = CoreEncounters(
        workforce_person=workforce,
        beneficiary_person=beneficiary,
        encounter_date=encounter_date,
        org_unit_id=org_unit_id,
        area_id=area_id,
        encounter_type_id=encounter_type_id,
        sms_id=sms_id,
        )

    try:
        form_encounter = CoreEncounters.objects.get(
            workforce_person=workforce,
            beneficiary_person=beneficiary,
            encounter_date=encounter_date,
            form_id__isnull=False
            )
        return False, form_encounter
    except CoreEncounters.DoesNotExist:
        #If no encounter with a form id exists, we delete any existing encounters
        #and the coreservices
        CoreEncounters.objects.filter(
            workforce_person=workforce,
            beneficiary_person=beneficiary,
            encounter_date=encounter_date,
            form_id__isnull=True
            ).delete()
        CoreServices.objects.filter(
            workforce_person=workforce,
            beneficiary_person=beneficiary,
            encounter_date=encounter_date).delete()

        new_encounter.save()
        return True, new_encounter


def save_adverse_condition(beneficiary, adverse_condition_id, sms_id):
    """If an adverse condition is already in tbl_core_adverse_conditions for the
    same adverse condition and same beneficiary - if the sms_id is null, add
    the sms_id of this sms to the record. If the sms_id is not null, do not make
    any changes or additions.
    NEW BIZ RULES:if the sms app finds any adverse conditions with a form id and without an sms
    id, it adds the sms id.  And if it finds any adverse conditions with an sms
    id only, it does nothing to those records.
    """
    created = False
    #Business rule: Do not save adverse conditions for a guardian
    if beneficiary.regpersonstypes_set.filter(person_type_id="TBGR"):
        raise sms_exceptions.NoAdverseConditionOnGuardian(field='')

    #First get all the same adverse conditions
    core_adverse_conditions = CoreAdverseConditions.objects.filter(
        beneficiary_person=beneficiary,
        adverse_condition_id=adverse_condition_id,
        sms_id__isnull=True
    )
    if core_adverse_conditions:
        for core_adverse_condition in core_adverse_conditions:
            if not core_adverse_condition.sms_id:
                core_adverse_condition.sms_id = sms_id
                core_adverse_condition.save()
        core_adverse_condition = core_adverse_conditions[0]
    else:
        core_adverse_condition = CoreAdverseConditions.objects.create(
            beneficiary_person=beneficiary,
            adverse_condition_id=adverse_condition_id,
            sms_id=sms_id
        )
        created = True

    return created, core_adverse_condition

def save_services(workforce, beneficiary, encounter_date, item_id, sms_id):
    saved = CoreServices.objects.create(
        workforce_person=workforce,
        beneficiary_person=beneficiary,
        encounter_date=encounter_date,
        core_item_id=item_id,
        sms_id=sms_id)

def undo_last_action(phone_number):
    """Using the phone number, figures out the latest messages from a person withing
    a certain period of time and then deletes the corresponding data."""
    days_ago = datetime.timedelta(days=3)
    three_days_ago = datetime.date.today() - days_ago
    connection = Connection.objects.filter(identity=phone_number)

    all_msgs = Message.objects.filter(
        direction="I",
        connection=connection,
        date__gt=three_days_ago
        ).order_by('-date')

    errors = []
    undone_encounter_details = {}
    for msg in all_msgs:
        try:
            last_saved_encounter = CoreEncounters.objects.get(sms_id=msg.id)
        except CoreEncounters.DoesNotExist:
            pass
        else:
            undone_encounter_details['beneficiary_id'] = last_saved_encounter.beneficiary_person.beneficiary_id
            undone_encounter_details['encounter_date'] = last_saved_encounter.encounter_date
            #delete and break out of the loop
            saved_services = CoreServices.objects.filter(
                workforce_person=last_saved_encounter.workforce_person,
                beneficiary_person=last_saved_encounter.beneficiary_person,
                encounter_date=last_saved_encounter.encounter_date)
            saved_services.delete()
            last_saved_encounter.delete()
            break

    if not undone_encounter_details:
        errors.append(sms_exceptions.NoMessagesToUndoException(field=''))

    return undone_encounter_details, errors


def process_finw(sms, phone_number, sms_fields):
    #Authenticate

    #process the SMS

    #Return the SMS
    errors = []
    try:
        password = sms_fields['password']['value']
        member_id_prefix = sms_fields.get('member_ID', '') or {'value':'', 'prefix':''}

        member_id = member_id_prefix['value']
        member_id_prefix = member_id_prefix['prefix']
        auth_user = authenticate_sms_user(phone_number, member_id_prefix=member_id_prefix, member_id=member_id, password=password)
        worker = auth_user.reg_person
    except sms_exceptions.SMSFieldException as e:
        return False, [e]
    else:
        #replace values
        sms_fields["member_ID"] = {}
        if not worker.workforce_id:
            return saved, [sms_exceptions.UserNotAuthorisedServiceProvider(field='')]
        sms_fields["member_ID"]['value'] = worker.workforce_id
        sms_fields["member_ID"]['prefix'] = "w"

    try:
        search_string = sms_fields['Search Text']
        search_string = search_string['value']
    except KeyError:
        search_string = False

    try:
        age = sms_fields['age']
        age = age['value']
    except KeyError:
        age = False

    try:
        sex = sms_fields['sex']
        sex = sex['value']
        #This returns a list
        sex = sex[0]
    except KeyError:
        sex = False
    results = generic_functions.get_list_of_persons(search_string=search_string,
                                                    has_workforce_id=True,
                                                    age=age, in_person_types=["TWNE", "TWVL", "TWGE"], sex=sex)
    return results, errors

def process_finb(sms, phone_number, sms_fields):
    #Authenticate

    #process the SMS

    #Return the SMS
    errors = []
    try:
        password = sms_fields['password']['value']
        member_id_prefix = sms_fields.get('member_ID', '') or {'value':'', 'prefix':''}

        member_id = member_id_prefix['value']
        member_id_prefix = member_id_prefix['prefix']
        auth_user = authenticate_sms_user(phone_number, member_id_prefix=member_id_prefix, member_id=member_id, password=password)
        worker = auth_user.reg_person
    except sms_exceptions.SMSFieldException as e:
        return False, [e]
    else:
        #replace values
        sms_fields["member_ID"] = {}
        if not worker.workforce_id:
            return saved, [sms_exceptions.UserNotAuthorisedServiceProvider(field='')]
        sms_fields["member_ID"]['value'] = worker.workforce_id
        sms_fields["member_ID"]['prefix'] = "w"

    try:
        search_string = sms_fields['Search Text']
        search_string = search_string['value']
    except KeyError:
        search_string = False

    try:
        age = sms_fields['age']
        age = age['value']
    except KeyError:
        age = False

    try:
        sex = sms_fields['sex']
        sex = sex['value']
        #This returns a list
        sex = sex[0]
    except KeyError:
        sex = False

    results = generic_functions.get_list_of_persons(search_string=search_string, age=age, sex=sex, has_beneficiary_id=True)
    return results, errors

def process_finu(sms, phone_number, sms_fields):
    #Return the SMS
    errors = []
    try:
        search_string = sms_fields['Search Text']
        search_string = search_string['value']
    except KeyError:
        search_string = False

    results = generic_functions.get_list_of_org_units(search_string=search_string)
    return results, errors

def process_fing(sms, phone_number, sms_fields):
    #Return the SMS
    errors = []
    try:
        search_string = sms_fields['Search Text']
        search_string = search_string['value']
    except KeyError:
        search_string = False
    all_wards = get_all_wards()
    results = generic_functions.direct_field_search(all_wards,
                                                    field_names=['area_name'],
                                                    search_string=search_string)
    return results, errors

def process_fink(sms, phone_number, sms_fields):
    #Return the SMS
    errors = []
    try:
        search_string = sms_fields['Search Text']
        search_string = search_string['value']
    except KeyError:
        search_string = False
    all_sms_keywords = SetupList.objects.filter(item_category='SMS Command')
    results = generic_functions.direct_field_search(all_sms_keywords,
                                                    field_names=['item_description'],
                                                    search_string=search_string)
    return results, errors

def process_chgp(sms, phone_number, sms_fields):
    saved = False
    try:
        password = sms_fields['password']['value']
        member_id_prefix = sms_fields.get('member_ID', '') or {'value':'', 'prefix':''}

        member_id = member_id_prefix['value']
        member_id_prefix = member_id_prefix['prefix']
        auth_user = authenticate_sms_user(phone_number,
                                          member_id_prefix=member_id_prefix,
                                          member_id=member_id,
                                          password=password,
                                          allow_different_phone_number=False)
        worker = auth_user.reg_person
    except sms_exceptions.SMSFieldException as e:
        return saved, [e]
    else:
        #replace values
        sms_fields["member_ID"] = {}
        if not worker.workforce_id:
            return saved, [sms_exceptions.UserNotAuthorisedServiceProvider(field='')]
        sms_fields["member_ID"]['value'] = worker.workforce_id
        sms_fields["member_ID"]['prefix'] = "w"

    errors = []
    try:
        new_password = sms_fields['New Password']['value']
    except KeyError:
        return saved, [e]
    except RegPersonsContact.DoesNotExist:
        return saved, [e]
    else:
        auth_user.set_password(new_password)
        auth_user.save()
        saved = auth_user.check_password(new_password)
    return saved, errors


def construct_finu_results_response(results):
    list_of_all_result_strings = []
    for result in results:
        names = "{org_unit_id}-{org_unit_name}".format(org_unit_id=result.org_unit_id_vis,
                                org_unit_name=result.org_unit_name)
        result_string = names#not necessary, just for documentation
        list_of_all_result_strings.append(result_string)
    return ", ".join(list_of_all_result_strings)

def construct_fing_results_response(results):
    list_of_all_result_strings = []
    for result in results:
        district = get_parent_area_by_type(result.area_id, "GDIS")
        district_name = district.area_name
        names = "{ward_code}-{ward_name}-{district_name}".format(ward_code=result.area_id,
                                                         ward_name=result.area_name,
                                                         district_name=district_name)
        result_string = names#not necessary, just for documentation
        list_of_all_result_strings.append(result_string)
    return ", ".join(list_of_all_result_strings)

def construct_fink_results_response(results):
    list_of_all_result_strings = []
    for result in results:
        names = "{keyword}-{description}".format(keyword=result.item_id,
                                                 description=result.item_description)
        result_string = names#not necessary, just for documentation
        list_of_all_result_strings.append(result_string)
    return ", ".join(list_of_all_result_strings)

def construct_finw_results_response(results):
    list_of_all_result_strings = []
    for result in results:
        names = "{first_name} {last_name}".format(first_name=result.first_name,
                                                  last_name=result.surname)
        reg_parent_org_units = RegPersonsOrgUnits.objects.filter(person=result)
        parent_orgs = " ".join([reg_parent_org.parent_org_unit.org_unit_name for
                                reg_parent_org in reg_parent_org_units])
        result_string = "-".join([
            result.workforce_id,
            names,
            parent_orgs
        ])
        list_of_all_result_strings.append(result_string)
    return ", ".join(list_of_all_result_strings)


def construct_finb_results_response(results):
    list_of_all_result_strings = []
    for result in results:
        names = "{first_name} {last_name}".format(first_name=result.first_name,
                                                  last_name=result.surname)
        result_string = "-".join([
            result.beneficiary_id,
            names,
        ])
        list_of_all_result_strings.append(result_string)
    return ", ".join(list_of_all_result_strings)
