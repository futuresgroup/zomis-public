import unittest
import itertools
import datetime
import random
from django.core.exceptions import ValidationError
from rapidsms.tests.harness import RapidTest, TestScript
from django.contrib.auth.hashers import check_password
from ovc_auth.user_manager import set_password
from model_mommy import mommy
from ovc_main.models import (SetupList, SetupGeorgraphy, RegPerson, RegPersonsContact, RegOrgUnit,
    RegPersonsOrgUnits, RegPersonsGeo, RegPersonsTypes, RegPersonsGdclsu,
                             RegOrgUnitGeography, RegOrgUnitGdclsu, CoreEncounters, CoreServices,
                             CoreAdverseConditions, SetupGeorgraphy)
from ovc_auth.models import AppUser
from ovc_main.utils.idgenerator import beneficiary_id_generator
from ovc_main.utils import lookup_field_dictionary as fielddictionary

from rapidsms.contrib.messagelog.models import Connection
from rapidsms.messages.incoming import IncomingMessage as Message
import const
import app
import db_lookups
from ovc_sms.sms_forms import RECSForm
from ovc_sms.sms_forms import OVCDateField
from ovc_sms.sms_fields import deny_date_beyond_three_months_ago, deny_future_dates
from ovc_sms.sms_exceptions import (PastThreeMonthsException, FutureDateException,
    InvalidBeneficiaryIDException, AuthenticationException, NonExistantOrgUnitException,
    InvalidOrgUnitSelectionException, NonExistantOrgUnitException, InvalidBeneficiaryWardException,
    InvalidWorkerGeoException, NoPrimaryOrgUnit, InvalidWorkerIDException, NoServicesOrConditionsRecordedException,
    GenericSMSFieldException, PersonIsVoidException)
from general_utils import generic_functions



class TestChoiceLookups(RapidTest):
    fixtures = ["initial_data.json"]

    def test_get_services_provided_list(self):
        service_list = db_lookups.get_services_provided_list()
        # All services start with S
        none_services = itertools.ifilterfalse(
            lambda x: x.startswith("S"), service_list)
        self.assertFalse(list(none_services))

    def test_get_adverse_list(self):
        adverse_list = db_lookups.get_adverse_conditions_list()

        none_adverse = itertools.ifilterfalse(
            lambda x: x.startswith("P"), adverse_list)
        self.assertFalse(list(none_adverse))

    def test_get_referrals_provided_list(self):
        referrals_list = db_lookups.get_referrals_provided_list()

        none_referrals = itertools.ifilterfalse(
            lambda x: x.startswith("R"), referrals_list)
        self.assertFalse(list(none_referrals))

    def test_prefix(self):
        self.assertTrue(
            ["B", "BNRC", "BBRN"], db_lookups.get_beneficiary_id_prefix())
        self.assertTrue(["LGEO"], db_lookups.get_ward_district_prefix())
        self.assertTrue(["X"], db_lookups.get_password_prefix())
        self.assertTrue(["U"], db_lookups.get_org_prefix())


class TestRECSForm(RapidTest):
    fixtures = ["initial_data.json", "initial_data.json"]

    def setUp(self):
        self.form = RECSForm()
        #remove the validators from the datefield
        self.org_unit_id = "u6434"
        self.org_unit = mommy.make(RegOrgUnit, org_unit_id_vis=self.org_unit_id)
        self.beneficiary_ID = "B54000"
        self.beneficiary = mommy.make(RegPerson,
            beneficiary_id=self.beneficiary_ID,
            national_id="540231/61/2",
            birth_reg_id="lus/123987/2014")

        self.form.service_date.validators = []

    def testBlankForm(self):
        self.receive('RECS', self.lookup_connections('1112223333')[0])
        self.assertEqual(self.outbound[0].text, db_lookups.lookup_messages_text("RECS_EXPECTED_STRING"))


    def testDateFormats(self):
        expected_date = datetime.date(2015, 01, 10)

        date_strings = ["10jan2015", "10january2015", "10/01/15", "10/01/2015", "10-01-15", "10-01-2015"]

        for date_string in date_strings:
            text = "RECS wnrc123455/21/1 xpas123 b54000 ehom u6434 lgeo12345 {date_string}".format(
                date_string=date_string)
            _, python_fields, _ = self.form.process_form(
                original_text=text)

            form_dict = self.form.bound_fields_to_bound_dict(python_fields)
            self.assertEqual(form_dict['service_date']['value'], expected_date)

    def testDayt(self):
        service_date = OVCDateField(
            name="service_date",
            required=False,
            date_formats=[
                "%d%b%Y",
                "%d%b%y",
                "%d%B%y",
                "%d%B%Y",
                "%d/%m/%y",
                "%d/%m/%Y"
            ])

        recognised_date, _ = service_date.to_python("DAYT")
        self.assertEqual(datetime.date.today(), recognised_date)

    def testbound_fields_to_bound_dict(self):
        valid, python_fields, errors = self.form.process_form(
            original_text="RECS W123456 xpas123 B54000 EHOM u6434 lgeo12345")

        fields_dict = self.form.bound_fields_to_bound_dict(python_fields)
        expected_fields_dict = {
            'organisation_ID': {'prefix': '', 'value': self.org_unit_id},
            'beneficiary_ID': {'prefix': '', 'value': self.beneficiary_ID},
            'encounter_type': {'prefix': '', 'value': ['EHOM']},
            'member_ID': {'prefix': '', 'value': 'W123456'},
            'ward': {'prefix': 'lgeo', 'value': '12345'},
            'password': {'prefix': 'x', 'value': 'pas123'}
            }
        self.assertEqual(fields_dict, expected_fields_dict)

    def testDaytInForm(self):
        valid, python_fields, errors = self.form.process_form(
            original_text="RECS w123456 xpas123 b540 EHOM u6434 lgeo12345 DAYT")
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)

        self.assertEqual(datetime.date.today(), field_dict['service_date']['value'])

    def testLowerCaseSupport(self):
        valid, python_fields, errors = self.form.process_form(
            original_text="RECS W123456 xpas123 B54000 Ehom u6434 lgeo12345 phvP, rcpl")

        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        expected_fields_dict = {
            'organisation_ID': {'prefix': '', 'value': self.org_unit_id},
            'referral_provided': {'prefix': '', 'value': ['rcpl']},
            'beneficiary_ID': {'prefix': '', 'value': self.beneficiary_ID},
            'adverse_conditions': {'prefix': '', 'value': ['phvP']},
            'encounter_type': {'prefix': '', 'value': ['Ehom']},
            'member_ID': {'prefix': '', 'value': 'W123456'},
            'ward': {'prefix': 'lgeo', 'value': '12345'},
            'password': {'prefix': 'x', 'value': 'pas123'}
            }
        self.assertEqual(field_dict, expected_fields_dict)

    def testMemberID(self):
        _, python_fields, _ = self.form.process_form(
            original_text="RECS W113342 xpas123 B54000 EHOM u6434 lgeo12345 12jan2014")
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        self.assertEqual("W113342", field_dict['member_ID']['value'])

    def testMemberNRC(self):
        _, python_fields, _ = self.form.process_form(
            original_text="RECS wnrc113342/61/1 xpas123 b54000 EHOM u6434 lgeo12345 12jan2014")
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        self.assertEqual("113342/61/1", field_dict['member_ID']['value'])

    def testMemberNRCError(self):
        _, python_fields, _ = self.form.process_form(
            original_text="RECS wnrb113342/61/1 xpas123 b54000 EHOM u6434 lgeo12345 12jan2014")
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        with self.assertRaises(KeyError):
            field_dict['member_ID']

    def testBeneficiary_ID(self):
        _, python_fields, _ = self.form.process_form(
            original_text="RECS w113342 xpas123 B54000 EHOM u6434 lgeo12345 12jan2014")
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        self.assertEqual(self.beneficiary.beneficiary_id, field_dict['beneficiary_ID']['value'])

    """
    def testNonExistantOrgUnitValidation(self):
        non_existant_org_unit_id = 2345
        _, python_fields, errors = self.form.process_form(
            original_text="RECS w113342 xpas123 b540 EHOM u{org_unit_id} lgeo12345 12jan2014".format(
                org_unit_id=non_existant_org_unit_id))
        self.assertIsInstance(errors[0], NonExistantOrgUnitException)
    """

    def testExistingOrgUnitValidation(self):
        _, python_fields, errors = self.form.process_form(
            original_text="RECS w113342 xpas123 b540 EHOM {org_unit_id} lgeo12345 12jan2014".format(
                org_unit_id=self.org_unit.org_unit_id_vis))
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        self.assertEqual(self.org_unit.org_unit_id_vis, field_dict['organisation_ID']['value'])

    def testBeneficiaryNRC(self):
        _, python_fields, _ = self.form.process_form(
            original_text="RECS wnrc113342/61/1 xpas123 bnrc540231/61/2 EHOM u6434 lgeo12345 12jan2014")
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        self.assertEqual(self.beneficiary.national_id, field_dict['beneficiary_ID']['value'])

    def testBeneficiaryBirthCertificate(self):
        _, python_fields, _ = self.form.process_form(
            original_text="RECS wnrc113342/61/1 xpas123 bbcnlus/123987/2014 EHOM u6434 lgeo12345 12jan2014")
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        self.assertEqual(self.beneficiary.birth_reg_id, field_dict['beneficiary_ID']['value'])

    def testBeneficiaryNRCError(self):
        _, python_fields, _ = self.form.process_form(
            original_text="RECS wnrb113342/61/1 xpas123 bnrcn540/61/3 EHOM u6434 lgeo12345 12jan2014")
        field_dict = self.form.bound_fields_to_bound_dict(python_fields)
        with self.assertRaises(KeyError):
            field_dict['beneficiary_ID']

    def testNoUserOrgUnitError(self):
        reg_persons_org_unit = mommy.make(RegPersonsOrgUnits, primary=False)
        with self.assertRaises(NoPrimaryOrgUnit):
            encounter_date = datetime.date.today()
            org_unit_id = db_lookups.get_org_id_from_person(reg_persons_org_unit.person, encounter_date)

    def testGetUserOrgUnit(self):
        reg_persons_org_unit = mommy.make(RegPersonsOrgUnits, primary=True)
        encounter_date = datetime.date.today()
        org_unit_id = db_lookups.get_org_id_from_person(reg_persons_org_unit.person, encounter_date)
        self.assertEqual(reg_persons_org_unit.parent_org_unit.org_unit_id_vis, org_unit_id)

        reg_persons_org_unit.date_delinked = datetime.date.today()
        reg_persons_org_unit.save()
        with self.assertRaises(NoPrimaryOrgUnit):
            org_unit_id = db_lookups.get_org_id_from_person(reg_persons_org_unit.person, encounter_date + datetime.timedelta(days=5))

        #Should not raise and error.
        org_unit_id = db_lookups.get_org_id_from_person(reg_persons_org_unit.person, encounter_date - datetime.timedelta(days=5))

    def testMissingPasswordError(self):
        valid, python_fields, _ = self.form.process_form(
            original_text="RECS wnrb113342/61/1 bnrcn540/61/3 EHOM u6434 lgeo12345 12jan2014")
        #import ipdb;ipdb.set_trace()


class TestActualSMS(RapidTest):
    fixtures = ["initial_data.json", "initial_data.json"]

    def setUp(self):
        self.form = RECSForm()
        self.org_unit = mommy.make(RegOrgUnit, org_unit_id_vis="U6434")
        self.beneficiary_ID = "B54000"
        self.beneficiary = mommy.make(RegPerson, beneficiary_id=self.beneficiary_ID)

    def get_text_response(self, text):
        from app import construct_verbose_response
        _, python_fields, _ = self.form.process_form(
            original_text=text)
        return construct_verbose_response(python_fields)

    """"
    def testSMSDayT(self):
        text = "RECS w123456 xpas123 b54000 EHOM u6434 lgeo12345 DAYT"
        self.receive(text, self.lookup_connections('1112223333')[0])
        self.assertEqual(self.outbound[0].text, self.get_text_response(text))

    def testSMSReferral(self):
        text = "RECS w123456 xpas123 b54000 EHOM u6434 lgeo12345 DAYT RCPL"
        self.receive(text, self.lookup_connections('1112223333')[0])
        self.assertEqual(self.outbound[0].text, self.get_text_response(text))

    def testSMSAdverseReport(self):
        text = "RECS w123456 xpas123 b54000 EHOM u6434 lgeo12345 DAYT SGFD"
        self.receive(text, self.lookup_connections('1112223333')[0])
        self.assertEqual(self.outbound[0].text, self.get_text_response(text))
    """

    def testErrorMissingMemberPassword(self):
        text = "recs wnrc123456/61/1 b54000 ehoM u6434 lgeo12345 DAYT SgFD rcpl"
        self.receive(text, self.lookup_connections('1112223333')[0])
        expected_text = db_lookups.lookup_messages_text('PASSWORD_IS_MISSING_ERROR')
        self.assertEqual(self.outbound[0].text, expected_text)

    #test dates
    def testerrorwrongdate(self):
        text = "recs wnrc123456/61/1 xpas123 b54000 ehom u6434 lgeo12345 12janfeb2015 sgfd rcpl"
        self.receive(text, self.lookup_connections('1112223333')[0])
        expected_text = const.INVALID_DATE
        self.assertRegexpMatches(self.outbound[0].text, expected_text)

    def testerrorfuturedate(self):
        #put a future date validator on the date field
        future_date = datetime.date.today() + datetime.timedelta(days=1)
        formated_future_date = future_date.strftime("%d%b%y")
        text = "recs wnrc123456/61/1 xpas123 b54000 ehom u6434 lgeo12345 {future_date} sgfd rcpl"
        self.receive(
            text.format(future_date=formated_future_date),
            self.lookup_connections('1112223333')[0]
            )
        expected_text = db_lookups.lookup_messages_text("REJECT_FUTURE_DATES")
        self.assertRegexpMatches(self.outbound[0].text, expected_text)

    def testdeny_date_beyond_three_months_ago(self):
        #put a future date validator on the date field
        self.form.service_date.validators = [deny_date_beyond_three_months_ago]

        past_date = datetime.date.today() - datetime.timedelta(days=200)
        formated_past_date = past_date.strftime("%d%b%y")
        text = "recs wnrc123456/61/1 xpas123 b54000 ehom u6434 lgeo12345 {past_date} sgfd rcpl"
        self.receive(
            text.format(past_date=formated_past_date),
            self.lookup_connections('1112223333')[0]
            )
        expected_text = db_lookups.lookup_messages_text("DATE_TOO_FAR_BACK_ERROR")
        self.assertRegexpMatches(self.outbound[0].text, expected_text)

    def testErrorTooManyChoices(self):
        text = "recs wnrc123456/61/1 xpas123 b54000 ehoM, EWRK u6434 lgeo12345 12JAN2015 SgFD rcpl"
        self.receive(text, self.lookup_connections('1112223333')[0])
        expected_text = "Please select only one value out of EELS, EHOM, EWRK"
        self.assertRegexpMatches(self.outbound[0].text, expected_text)

class TestGeoValidations(RapidTest):
    fixtures = ["initial_data.json", "geo_static_data.json"]

    def testNoGeoTags(self):
        person = mommy.make(RegPerson)
        area_id = 266 #ward chilenje

        encounter_date = datetime.date.today()
        validation = db_lookups.check_worker_area(person, [area_id], encounter_date)
        #All wards allowed
        self.assertTrue(validation)

    def testValidOnlyDistrictTags(self):
        district_id = 57 #district lusaka
        person_geo = mommy.make(RegPersonsGeo, area_id=district_id)

        ward_in_district = 286 #ward kamwala
        encounter_date = datetime.date.today()
        validation = db_lookups.check_worker_area(person_geo.person, [ward_in_district], encounter_date)
        self.assertTrue(validation)

    def testInvalidDistrictTag(self):
        #test error when person uses a ward not in their district.
        district_id = 57 #district lusaka
        person_geo = mommy.make(RegPersonsGeo, area_id=district_id)

        ward_in_district = 268 #ward chitongo...NOT IN LUSAKA DISTRICT
        with self.assertRaises(InvalidWorkerGeoException):
            encounter_date = datetime.date.today()
            validation = db_lookups.check_worker_area(person_geo.person, [ward_in_district], encounter_date)

    def testValidWardTag(self):
        area_id = 266 #ward muswishi
        person_geo = mommy.make(RegPersonsGeo, area_id=area_id)
        encounter_date = datetime.date.today()
        validation = db_lookups.check_worker_area(person_geo.person, [area_id], encounter_date)
        self.assertTrue(validation)

    def testMultipleDistrictTags(self):
        ndola_district_id = 88
        lusaka_district_id = 57

        ward_in_lusaka = 280#kabwata
        ward_in_ndola = 289  #kansenshi

        person_geo = mommy.make(RegPersonsGeo, area_id=ndola_district_id)
        encounter_date = datetime.date.today()
        self.assertTrue(db_lookups.check_worker_area(person_geo.person, [ward_in_ndola], encounter_date))

        with self.assertRaises(InvalidWorkerGeoException):
            encounter_date = datetime.date.today()
            validation = db_lookups.check_worker_area(person_geo.person, [ward_in_lusaka], encounter_date)

        mommy.make(RegPersonsGeo, area_id=lusaka_district_id, person=person_geo.person)
        encounter_date = datetime.date.today()
        self.assertTrue(db_lookups.check_worker_area(person_geo.person, [ward_in_lusaka], encounter_date))


class TestValidators(RapidTest):
    fixtures = ["initial_data.json"]
    def setUp(self):
        self.beneficiary_id = "B00000182"
        self.national_id = "123211/61/1"
        self.birth_reg_id = "lus/123987/2014"

        self.workforce_id = "idforwo"
        self.password = "personpass"
        self.phone_number = "0973203144"

        worker_contact = mommy.make(RegPersonsContact,
            contact_detail_type_id=fielddictionary.contact_designated_mobile_phone,
            contact_detail=self.phone_number,
            person__workforce_id=self.workforce_id
            )

        self.encounter_date = datetime.date.today()
        self.auth_user = AppUser.objects.create_user(workforce_id=self.workforce_id, password=self.password)
        self.auth_user.reg_person = worker_contact.person
        self.auth_user.save()

    def testfuture_dates_denied(self):
        with self.assertRaises(FutureDateException):
            two_days_ahead = datetime.timedelta(days=2)
            deny_future_dates(datetime.date.today()+two_days_ahead)

    def testDenyLaterThanPastThreeMonths(self):
        with self.assertRaises(PastThreeMonthsException):
            #re-implement with reletivedelta to have exact 3 months
            three_months_ago = datetime.timedelta(days=95)
            deny_date_beyond_three_months_ago(datetime.date.today()-three_months_ago)


    def testInvalidIDBeneficiaryID(self):
        false_beneficiary_id = "B10023"
        false_beneficiary_nrc = "false/61/1"
        false_beneficiary_birth_cert = "false/000112/2014"

        with self.assertRaises(InvalidBeneficiaryIDException):
            encounter_date = datetime.date.today()
            db_lookups.get_beneficiary_or_exception("b", false_beneficiary_id, encounter_date)

        with self.assertRaises(InvalidBeneficiaryIDException):
            encounter_date = datetime.date.today()
            db_lookups.get_beneficiary_or_exception("bnrc", false_beneficiary_nrc, encounter_date)

        with self.assertRaises(InvalidBeneficiaryIDException):
            encounter_date = datetime.date.today()
            db_lookups.get_beneficiary_or_exception("bbcn", false_beneficiary_birth_cert, encounter_date)


    def testValidBeneficiaryID(self):
        beneficiary_id = 'B00000216'
        beneficiary = mommy.make(RegPerson, beneficiary_id=beneficiary_id)
        encounter_date = datetime.date.today()
        looked_up_beneficiary = db_lookups.get_beneficiary_or_exception("b", beneficiary_id, encounter_date)
        self.assertEqual(beneficiary_id, looked_up_beneficiary.beneficiary_id)

    def testValidBeneficiaryNRC(self):
        beneficiary_nrc = "213422/61/1"
        beneficiary = mommy.make(RegPerson, national_id=beneficiary_nrc)
        encounter_date = datetime.date.today()
        looked_up_beneficiary = db_lookups.get_beneficiary_or_exception("bnrc", beneficiary_nrc, encounter_date)
        self.assertEqual(beneficiary_nrc, looked_up_beneficiary.national_id)

    def testValidBeneficiaryBirthCert(self):
        beneficiary_birth_cert = "ndl/0000001/15"
        beneficiary = mommy.make(RegPerson, birth_reg_id=beneficiary_birth_cert)
        encounter_date = datetime.date.today()
        looked_up_beneficiary = db_lookups.get_beneficiary_or_exception("bbcn", beneficiary_birth_cert, encounter_date)
        self.assertEqual(beneficiary_birth_cert, looked_up_beneficiary.birth_reg_id)

    def testVoidedBeneficiary(self):
        beneficiary_birth_cert = "ndl/0000001/15"
        beneficiary = mommy.make(RegPerson, birth_reg_id=beneficiary_birth_cert)
        encounter_date = datetime.date.today()
        looked_up_beneficiary = db_lookups.get_beneficiary_or_exception("bbcn", beneficiary_birth_cert, encounter_date)
        self.assertEqual(beneficiary_birth_cert, looked_up_beneficiary.birth_reg_id)

        old_encounter_date = encounter_date - datetime.timedelta(days=5)
        beneficiary.date_of_death = datetime.datetime.now()
        beneficiary.save()
        looked_up_beneficiary = db_lookups.get_beneficiary_or_exception("bbcn", beneficiary_birth_cert, old_encounter_date)

        #Encounter can not be after date of death.
        after_death_encounter_date = datetime.date.today() + datetime.timedelta(days=5)
        beneficiary.date_of_death = datetime.datetime.now()
        beneficiary.save()
        with self.assertRaises(PersonIsVoidException):
            looked_up_beneficiary = db_lookups.get_beneficiary_or_exception("bbcn", beneficiary_birth_cert, after_death_encounter_date)

        beneficiary.make_void()
        with self.assertRaises(PersonIsVoidException):
            looked_up_beneficiary = db_lookups.get_beneficiary_or_exception("bbcn", beneficiary_birth_cert, encounter_date)

    def testVoidedWorker(self):
        workforce_id = "W2342245"
        worker  = mommy.make(RegPerson, workforce_id=workforce_id)
        encounter_date = datetime.date.today()
        looked_up_worker = db_lookups.get_worker_or_exception("w", workforce_id, encounter_date)
        self.assertEqual(workforce_id, worker.workforce_id)

        old_encounter_date = encounter_date - datetime.timedelta(days=5)
        worker.date_of_death = datetime.datetime.now()
        worker.save()
        looked_up_worker = db_lookups.get_worker_or_exception("w", workforce_id, old_encounter_date)

        #Encounter can not be after date of death.
        after_death_encounter_date = datetime.date.today() + datetime.timedelta(days=5)
        worker.date_of_death = datetime.datetime.now()
        worker.save()
        with self.assertRaises(PersonIsVoidException):
            looked_up_worker = db_lookups.get_worker_or_exception("w", workforce_id, after_death_encounter_date)

        worker.make_void()
        with self.assertRaises(PersonIsVoidException):
            looked_up_worker = db_lookups.get_worker_or_exception("w", workforce_id, encounter_date)


    def testInvalidWorkerID(self):
        false_workforce_id = "W10023"
        false_worker_nrc = "false/61/1"

        with self.assertRaises(InvalidWorkerIDException):
            encounter_date = datetime.date.today()
            db_lookups.get_worker_or_exception("w", false_workforce_id, encounter_date)

        with self.assertRaises(InvalidWorkerIDException):
            encounter_date = datetime.date.today()
            db_lookups.get_worker_or_exception("wnrc", false_worker_nrc, encounter_date)


    def testValidWorkerID(self):
        workerforce_id = "W3234332"
        worker = mommy.make(RegPerson, workforce_id=workerforce_id)
        encounter_date = datetime.date.today()
        looked_up_worker = db_lookups.get_worker_or_exception("w", workerforce_id, encounter_date)
        self.assertEqual(workerforce_id, looked_up_worker.workforce_id)

    def testValidBeneficiaryNRC(self):
        worker_nrc = "213422/61/1"
        worker = mommy.make(RegPerson, national_id=worker_nrc)
        encounter_date = datetime.date.today()
        looked_up_worker = db_lookups.get_worker_or_exception("wnrc", worker_nrc, encounter_date)
        self.assertEqual(worker_nrc, looked_up_worker.national_id)


    def testAuthInvalidPassword(self):
        invalid_password = "falsepass"
        with self.assertRaisesRegexp(AuthenticationException, db_lookups.lookup_messages_text("AUTH_ERROR_INVALID_PASSWORD")):
            db_lookups.authenticate_sms_user(self.phone_number, member_id=self.workforce_id, password=invalid_password)

    def testInvalidWorkforceIDWithUnregisteredNumber(self):
        invalid_workforce_id = "falsew"
        unregistered_number = "123456"
        with self.assertRaisesRegexp(AuthenticationException, db_lookups.lookup_messages_text("AUTH_ERROR_INVALID_ID_OR_PASSWORD_UNREGISTERED_NUMBER")):
            db_lookups.authenticate_sms_user(unregistered_number, member_id=invalid_workforce_id, password=self.password)

    def testMissingWorkForceIdWithUnregisteredNumber(self):
        unregistered_number = "123456"
        with self.assertRaisesRegexp(AuthenticationException, db_lookups.lookup_messages_text("AUTH_ERROR_MISSING_WORKFORCE_ID_UNREGISTERED_NUMBER")):
            db_lookups.authenticate_sms_user(unregistered_number, member_id='', password=self.password)

    def testValidLoginWithRegistedNumber(self):
        authenticated_user = db_lookups.authenticate_sms_user(self.phone_number, member_id=self.workforce_id, password=self.password)
        self.assertEqual(self.auth_user, authenticated_user)

    def testValidLoginWithMissingWorkForceID(self):
        authenticated_user = db_lookups.authenticate_sms_user(self.phone_number, member_id='', password=self.password)
        self.assertEqual(self.auth_user, authenticated_user)

    def testValidLoginUnregisteredNumber(self):
        unregistered_number = "123456"
        authenticated_user = db_lookups.authenticate_sms_user(unregistered_number, member_id=self.workforce_id, password=self.password)
        self.assertEqual(self.auth_user, authenticated_user)

    def testValidLoginUsingNRC(self):
        unregistered_number = "123456"
        self.auth_user.national_id = self.national_id
        self.auth_user.save()
        authenticated_user = db_lookups.authenticate_sms_user(unregistered_number, member_id_prefix="wnrc", member_id=self.national_id, password=self.password)
        self.assertEqual(self.auth_user, authenticated_user)

    def testExistingButInvalidOrgUnitSelection(self):
        assigned_unit_id = 45355
        person = mommy.make(RegPerson, parent_org_unit__org_unit_id_vis=assigned_unit_id)

        unassigned_org_unit_id = 65321
        unassigned_org_unit = mommy.make(RegOrgUnit, org_unit_id_vis=unassigned_org_unit_id)
        with self.assertRaises(InvalidOrgUnitSelectionException):
            db_lookups.check_org_unit(person, unassigned_org_unit_id, encounter_date=datetime.datetime(2015, 02, 20))

    def testValidOrgUnitSelection(self):
        assigned_unit_id = "U45355"
        person_org_unit = mommy.make(RegPersonsOrgUnits, parent_org_unit__org_unit_id_vis=assigned_unit_id)
        validation = db_lookups.check_org_unit(person_org_unit.person, assigned_unit_id, encounter_date=datetime.datetime(2015, 02, 01))
        self.assertTrue(validation)

    def testVoidedOrg(self):
        assigned_unit_id = "U45355"
        person_org_unit = mommy.make(RegPersonsOrgUnits, parent_org_unit__org_unit_id_vis=assigned_unit_id)
        validation = db_lookups.check_org_unit(person_org_unit.person, assigned_unit_id, encounter_date=datetime.datetime(2015, 02, 01))
        self.assertTrue(validation)
        """When you are checking existence of org units in the db tbl_reg_org_units filter tbl_reg_org_units on
        (void=false and (date_closed is null or date_closed > [sms encounter date]))"""
        person_org_unit.parent_org_unit.date_closed = datetime.datetime.now()
        person_org_unit.parent_org_unit.save()
        encounter_date = datetime.datetime.now() - datetime.timedelta(days=3)
        validation = db_lookups.check_org_unit(person_org_unit.person, assigned_unit_id, encounter_date=encounter_date)
        self.assertTrue(validation)

        encounter_date = datetime.datetime.now() + datetime.timedelta(days=3)
        with self.assertRaises(InvalidOrgUnitSelectionException):
            validation = db_lookups.check_org_unit(person_org_unit.person, assigned_unit_id, encounter_date=encounter_date)

        person_org_unit.parent_org_unit.make_void()
        with self.assertRaises(InvalidOrgUnitSelectionException):
            validation = db_lookups.check_org_unit(person_org_unit.person, assigned_unit_id, encounter_date=datetime.datetime(2015, 02, 01))


    def testInvalidBeneficiaryGeoError(self):
        assigned_ward_id = 6545335
        assigned_ward_person = mommy.make(RegPersonsGeo, area_id=assigned_ward_id)
        person = assigned_ward_person.person

        unassigned_ward_id = 34343
        with self.assertRaises(InvalidBeneficiaryWardException):
            encounter_date = datetime.date.today()
            db_lookups.check_beneficiary_ward(person, unassigned_ward_id, encounter_date)

    def testValidBeneficiaryGeo(self):
        assigned_ward_id = 6545335
        assigned_ward_person = mommy.make(RegPersonsGeo, area_id=assigned_ward_id)
        person = assigned_ward_person.person

        encounter_date = datetime.date.today()
        validation = db_lookups.check_beneficiary_ward(person, assigned_ward_id, encounter_date)
        self.assertTrue(validation)


class TestDbSave(RapidTest):
    #fixtures = ["initial_data.json", "geo_static_data.json"]

    fixtures = ["initial_data.json"]
    def setUp(self):
        self.workforce_id = "W34567"
        self.beneficiary_id = "B00000133"
        self.encounter_date = datetime.date.today()

        self.beneficiary = mommy.make(RegPerson, beneficiary_id=self.beneficiary_id)
        self.ward_id = 501
        try:
            self.ward = SetupGeorgraphy.objects.get(area_id=self.ward_id)
        except SetupGeorgraphy.DoesNotExist:
            self.ward = mommy.make(SetupGeorgraphy, area_id=self.ward_id, area_type_id=fielddictionary.geo_type_ward_code)

        self.org_unit_id = "2334"
        self.organisation = mommy.make(RegOrgUnit, org_unit_id_vis=self.org_unit_id)
        self.designated_number = '0973203144'
        self.worker = mommy.make(RegPerson, workforce_id=self.workforce_id)
        self.reg_persons_org_unit = mommy.make(RegPersonsOrgUnits, person=self.worker, parent_org_unit=self.organisation, primary=True)
        self.worker_geo = mommy.make(RegPersonsGeo, person=self.worker, area_id=self.ward_id)

        self.password = "thepassword"
        self.auth_user = AppUser.objects.create_user(workforce_id=self.workforce_id, password=self.password)
        self.auth_user.reg_person = self.worker
        self.auth_user.save()

        self.other_area = 9586
        self.other_worker_workforce_id = "W0000091"
        self.other_worker_designated_number = '0973332238'
        self.other_worker = mommy.make(RegPerson, workforce_id=self.other_worker_workforce_id)
        self.reg_persons_org_unit = mommy.make(RegPersonsOrgUnits, person=self.other_worker, parent_org_unit=self.organisation, primary=True)
        self.worker_geo = mommy.make(RegPersonsGeo, person=self.other_worker, area_id=self.other_area)

        self.auth_user = AppUser.objects.create_user(workforce_id=self.other_worker_workforce_id, password=self.password)
        self.auth_user.reg_person = self.other_worker
        self.auth_user.save()

        self.form_saved_encounter = mommy.make(CoreEncounters,
            form_id=123,
            workforce_person=self.worker,
            beneficiary_person=self.beneficiary,
            encounter_date=self.encounter_date
            )

        self.adverse_condition_id = 324
        self.form_saved_adverse_condition = mommy.make(CoreAdverseConditions,
            beneficiary_person=self.beneficiary,
            adverse_condition_id=self.adverse_condition_id
            )

        self.beneficiary_ward_person = mommy.make(RegPersonsGeo, area_id=self.ward_id, person=self.beneficiary)

        self.fields_dict = {
            'organisation_ID': {'prefix': '', 'value': self.org_unit_id},
            'referral_provided': {'prefix': '', 'value': ['rcpl']},
            'beneficiary_ID': {'prefix': 'b', 'value': self.beneficiary_id},
            'adverse_conditions': {'prefix': '', 'value': ['phvP']},
            'services_provided': {'prefix': '', 'value':['SGFD']},
            'encounter_type': {'prefix': '', 'value': ['Ehom']},
            'member_ID': {'prefix': 'w', 'value': self.workforce_id},
            'ward': {'prefix': 'lgeo', 'value': self.ward_id},
            'service_date':{'prefix':'', 'value': datetime.date.today()},
            'password': {'prefix': 'x', 'value': self.password}
            }
        connection = mommy.make(Connection)

        self.sms = self.receive('RECS ....', self.lookup_connections('1112223333')[0])

        self.encounter_type = "EHOM"

    def testSaveEncounterWithExistingFormId(self):
        created, encounter = db_lookups.save_encounter(
            workforce=self.worker,
            beneficiary=self.beneficiary,
            encounter_date=self.encounter_date,
            org_unit_id=112,
            area_id=3234,
            encounter_type_id=self.encounter_type,
            sms_id=2
            )
        self.assertFalse(created)
        self.assertEqual(self.form_saved_encounter, encounter)

    def testSaveEncounterDeletePreviousRecords(self):
        #remove the form_id from the last
        self.form_saved_encounter.form_id = None
        self.form_saved_encounter.save()

        core_service = mommy.make(CoreServices,
            workforce_person=self.worker,
            beneficiary_person=self.beneficiary,
            encounter_date=self.encounter_date)
        self.assertEqual(CoreServices.objects.all().count(), 1)


        created, encounter = db_lookups.save_encounter(
            workforce=self.worker,
            beneficiary=self.beneficiary,
            encounter_date=self.encounter_date,
            org_unit_id=112,
            area_id=3234,
            encounter_type_id=self.encounter_type,
            sms_id=2
            )
        self.assertNotEqual(self.form_saved_encounter, encounter)
        self.assertTrue(created)
        self.assertEqual(CoreServices.objects.all().count(), 0)

    def testNewEncounter(self):
        beneficiary=mommy.make(RegPerson, beneficiary_id="B123778")
        worker=mommy.make(RegPerson, workforce_id="W0000018")
        created, encounter = db_lookups.save_encounter(
            workforce=worker,
            beneficiary=beneficiary,
            encounter_date=self.encounter_date,
            org_unit_id=112,
            area_id=3234,
            encounter_type_id=self.encounter_type,
            sms_id=2
            )
        self.assertTrue(created)
        self.assertNotEqual(self.form_saved_encounter, encounter)

    def testAddAdverseConditionWithExistingFormCreated(self):
        created, adverse_condition = db_lookups.save_adverse_condition(
            beneficiary=self.beneficiary,
            adverse_condition_id=self.adverse_condition_id,
            sms_id=987)

        self.assertEqual(adverse_condition, self.form_saved_adverse_condition)
        self.assertEqual(adverse_condition.sms_id, 987)

    def testAddAdverseConditonNew(self):
        beneficiary = mommy.make(RegPerson, beneficiary_id="B00000182")
        created, adverse_condition = db_lookups.save_adverse_condition(
            beneficiary=beneficiary,
            adverse_condition_id=self.adverse_condition_id,
            sms_id=989)

        self.assertNotEqual(adverse_condition, self.form_saved_adverse_condition)
        self.assertEqual(adverse_condition.sms_id, 989)

    def testAdverseConditionNoUpdate(self):
        sms_id = 1000
        self.form_saved_adverse_condition.sms_id = sms_id
        self.form_saved_adverse_condition.save()

        created, adverse_condition = db_lookups.save_adverse_condition(
            beneficiary=self.beneficiary,
            adverse_condition_id=self.adverse_condition_id,
            sms_id=sms_id)

        self.assertFalse(created)
        self.assertEqual(adverse_condition, self.form_saved_adverse_condition)

    def testCleanFieldsFilledValues(self):
        clean_fields = db_lookups.clean_fields(self.fields_dict)

        expected_fields_dict = {
            'beneficiary': self.beneficiary,
            'referral_provided': ['RCPL'],
            'area': self.ward,
            'organisation': self.organisation,
            'services_provided': ['SGFD'],
            'worker': self.worker,
            'encounter_date': datetime.date.today(),
            'adverse_conditions': ['PHVP'],
            'encounter_type': [self.encounter_type],
            'password': self.password
            }

        self.assertEqual(clean_fields, expected_fields_dict)

    def testCleanFieldsMissingValues(self):
        fields_dict = {
            'organisation_ID': {'prefix': 'u', 'value': ''},
            'referral_provided': {'prefix': '', 'value': ['rcpl']},
            'beneficiary_ID': {'prefix': 'b', 'value': self.beneficiary_id},
            'adverse_conditions': {'prefix': '', 'value': ['phvP']},
            'services_provided': {'prefix': '', 'value':['SGFD']},
            'encounter_type': {'prefix': '', 'value': ['Ehom']},
            'member_ID': {'prefix': 'w', 'value': self.workforce_id},
            'ward': {'prefix': 'lgeo', 'value': ''},
            'service_date':{'prefix':'', 'value': datetime.date.today()},
            'password': {'prefix': 'x', 'value': self.password}
            }
        clean_fields = db_lookups.clean_fields(fields_dict)

        expected_fields_dict = {
            'beneficiary': self.beneficiary,
            'referral_provided': ['RCPL'],
            'area': self.ward,
            'organisation': self.organisation,
            'services_provided': ['SGFD'],
            'worker': self.worker,
            'encounter_date': datetime.date.today(),
            'adverse_conditions': ['PHVP'],
            'encounter_type': [self.encounter_type],
            'password': self.password
            }

        self.assertEqual(clean_fields, expected_fields_dict)


    def testAuthErrorWhenSaving(self):
        fields_dict = {
            'organisation_ID': {'prefix': 'u', 'value': ''},
            'referral_provided': {'prefix': '', 'value': ['rcpl']},
            'beneficiary_ID': {'prefix': 'b', 'value': self.beneficiary_id},
            'adverse_conditions': {'prefix': '', 'value': ['phvP']},
            'services_provided': {'prefix': '', 'value':['SGFD']},
            'encounter_type': {'prefix': '', 'value': ['Ehom']},
            'member_ID': {'prefix': 'w', 'value': self.workforce_id},
            'ward': {'prefix': 'lgeo', 'value': ''},
            'service_date':{'prefix':'', 'value': datetime.date.today()},
            'password': {'prefix': 'x', 'value': "falsepassword"}
            }
        saved, errors = db_lookups.save(self.sms, self.designated_number, fields_dict)
        for error in errors:
            self.assertIsInstance(error, AuthenticationException)

    def testValidAuthWhenSaving(self):
        fields_dict = {
            'organisation_ID': {'prefix': 'u', 'value': ''},
            'referral_provided': {'prefix': '', 'value': ['rcpl']},
            'beneficiary_ID': {'prefix': 'b', 'value': self.beneficiary_id},
            'adverse_conditions': {'prefix': '', 'value': ['phvP']},
            'services_provided': {'prefix': '', 'value':['SGFD']},
            'encounter_type': {'prefix': '', 'value': ['Ehom']},
            'member_ID': {'prefix': 'w', 'value': self.workforce_id},
            'ward': {'prefix': 'lgeo', 'value': ''},
            'service_date':{'prefix':'', 'value': datetime.date.today()},
            'password': {'prefix': 'x', 'value': self.password}
            }
        saved, errors = db_lookups.save(self.sms, self.designated_number, fields_dict)

    def testInvalidBeneficiaryWard(self):
        """Another ward for worker where the beneficiary isn't"""
        second_worker_ward_id = 1602 #dilika
        mommy.make(RegPersonsGeo, person=self.worker, area_id=second_worker_ward_id)

        fields_dict = self.fields_dict
        fields_dict['ward']['value'] = second_worker_ward_id

        saved, errors = db_lookups.save(self.sms, self.designated_number, fields_dict)
        with self.assertRaisesRegexp(InvalidBeneficiaryWardException,
                                     db_lookups.lookup_messages_text("GEO_ERROR_INVALID_WARD")):
            for error in errors:
                raise error

    def testInvalidWorkerArea(self):
        fields_dict = self.fields_dict
        fields_dict['member_ID']['value'] = self.other_worker_workforce_id
        saved, errors = db_lookups.save(self.sms, self.other_worker_designated_number, fields_dict)

        with self.assertRaisesRegexp(InvalidWorkerGeoException, db_lookups.lookup_messages_text("GEO_ERROR_INVALID_GEO_WORKER")):
            for error in errors:
                raise error

    def testMissingServicesReferralAdverse(self):
        fields_dict = self.fields_dict
        fields_dict['referral_provided']['value'] = ''
        fields_dict['services_provided']['value'] = ''
        fields_dict['adverse_conditions']['value'] = ''
        saved, errors = db_lookups.save(self.sms, self.designated_number, fields_dict)
        with self.assertRaisesRegexp(NoServicesOrConditionsRecordedException, db_lookups.lookup_messages_text("NO_SERVICES_PROVIDED")):
            for error in errors:
                raise error

    def testSuccesfullSave(self):
        fields_dict = {
            'organisation_ID': {'prefix': '', 'value': ''},
            'referral_provided': {'prefix': '', 'value': ['rcpl']},
            'beneficiary_ID': {'prefix': 'b', 'value': self.beneficiary_id},
            'adverse_conditions': {'prefix': '', 'value': ['phvP']},
            'services_provided': {'prefix': '', 'value':['SGFD']},
            'encounter_type': {'prefix': '', 'value': ['Ehom']},
            'member_ID': {'prefix': 'w', 'value': self.workforce_id},
            'ward': {'prefix': 'lgeo', 'value': ''},
            'service_date':{'prefix':'', 'value': datetime.date.today()},
            'password': {'prefix': 'x', 'value': self.password}
            }
        CoreServices.objects.all().delete()
        CoreEncounters.objects.all().delete()
        CoreAdverseConditions.objects.all().delete()

        saved, errors = db_lookups.save(self.sms, self.designated_number, fields_dict)
        self.assertTrue(saved)
        self.assertEqual(2, CoreServices.objects.count())
        self.assertEqual(1, CoreEncounters.objects.count())
        self.assertEqual(1, CoreAdverseConditions.objects.count())

    def testSuccesfullSaveMultipleAdverse(self):
        fields_dict = {
            'organisation_ID': {'prefix': '', 'value': ''},
            'referral_provided': {'prefix': '', 'value': ['rcpl']},
            'beneficiary_ID': {'prefix': '', 'value': self.beneficiary_id},
            'adverse_conditions': {'prefix': '', 'value': ['phvP', 'pill']},
            'services_provided': {'prefix': '', 'value':['SGFD']},
            'encounter_type': {'prefix': '', 'value': ['Ehom']},
            'member_ID': {'prefix': 'w', 'value': self.workforce_id},
            'ward': {'prefix': 'lgeo', 'value': ''},
            'service_date':{'prefix':'', 'value': datetime.date.today()},
            'password': {'prefix': 'x', 'value': self.password}
            }
        CoreServices.objects.all().delete()
        CoreEncounters.objects.all().delete()
        CoreAdverseConditions.objects.all().delete()

        saved, errors = db_lookups.save(self.sms, self.designated_number, fields_dict)
        self.assertTrue(saved)
        self.assertEqual(2, CoreServices.objects.count())
        self.assertEqual(1, CoreEncounters.objects.count())
        self.assertEqual(2, CoreAdverseConditions.objects.count())

    def testSkipExistingAdverseAdd(self):
        CoreServices.objects.all().delete()
        CoreEncounters.objects.all().delete()
        CoreAdverseConditions.objects.all().delete()
        #add adverse condition
        mommy.make(CoreAdverseConditions, beneficiary_person=self.beneficiary, adverse_condition_id="PILL", sms_id=None)

        fields_dict = {
            'organisation_ID': {'prefix': '', 'value': ''},
            'referral_provided': {'prefix': '', 'value': ['rcpl']},
            'beneficiary_ID': {'prefix': '', 'value': self.beneficiary_id},
            'adverse_conditions': {'prefix': '', 'value': ['pill']},
            'services_provided': {'prefix': '', 'value':['SGFD']},
            'encounter_type': {'prefix': '', 'value': ['Ehom']},
            'member_ID': {'prefix': 'w', 'value': self.workforce_id},
            'ward': {'prefix': 'lgeo', 'value': ''},
            'service_date':{'prefix':'', 'value': datetime.date.today()},
            'password': {'prefix': 'x', 'value': self.password}
            }
        saved, errors = db_lookups.save(self.sms, self.designated_number, fields_dict)
        self.assertTrue(saved)
        self.assertEqual(2, CoreServices.objects.count())
        self.assertEqual(1, CoreEncounters.objects.count())
        self.assertEqual(1, CoreAdverseConditions.objects.count())


class testFullSMSIntegration(TestScript):
    fixtures = ["initial_data.json"]
    def setUp(self):
        self.designated_number = '260973203144'

    def testBugFixIDNotFound(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)
        text = "RECS W0000778 SCBN SGFD SGFS SSRP B00000786 EELS LGEO9586 U000018 xomUkh4 {encounter_date}".format(
            encounter_date=encounter_date.strftime("%d/%m/%y"))
        self.create_valid_workforce_person(workforce_id="W0000778",
                                           org_unit_id="U000018", password="omUkh4", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000786", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)


    def create_valid_workforce_person(self, workforce_id="W0000083", national_id="000001/15/1", org_unit_id="233", password='', ward_id=''):

        self.organisation = mommy.make(RegOrgUnit, org_unit_id_vis=org_unit_id)
        self.worker = mommy.make(RegPerson, workforce_id=workforce_id, national_id=national_id)
        mommy.make(RegPersonsContact,
            person=self.worker,
            contact_detail_type_id=fielddictionary.contact_designated_mobile_phone,
            contact_detail=self.designated_number)
        self.reg_persons_org_unit = mommy.make(RegPersonsOrgUnits, person=self.worker, parent_org_unit=self.organisation, primary=True)
        self.worker_geo = mommy.make(RegPersonsGeo, person=self.worker, area_id=ward_id)
        mommy.make(SetupGeorgraphy, area_id=ward_id, area_type_id=fielddictionary.geo_type_ward_code)

        self.auth_user = AppUser.objects.create_user(workforce_id=workforce_id, national_id=national_id, password=password)
        self.auth_user.reg_person = self.worker
        self.auth_user.save()
        return self.worker

    def create_valid_beneficiary(self, beneficiary_id="B00000182", national_id="000003/12/3", bbcn='23', ward_id="9856"):
        beneficiary = mommy.make(RegPersonsGeo, person__beneficiary_id=beneficiary_id, person__national_id=national_id, person__birth_reg_id=bbcn, area_id=ward_id)
        return beneficiary


    def testNoOrgUnit(self):
        #create user
        self.create_valid_workforce_person(workforce_id="W0000083", password="itMXhZ", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000133", ward_id="9586")

        text = "RECS W0000083 xitMXhZ B00000133 EHOM LGEO DAYT STNT RMFP PHVP"

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)
        self.assertEqual(1, CoreServices.objects.filter(core_item_id="STNT").count())
        self.assertEqual(1, CoreAdverseConditions.objects.filter(beneficiary_person__beneficiary_id="B00000133").count())
        self.assertEqual(1, CoreEncounters.objects.filter(workforce_person__workforce_id="W0000083").count())

    def testNoOrgUnitServices(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)
        text = "RECS xitMXhZ WNRC000001/12/1 EWRK B00000182 {encounter_date}".format(encounter_date=encounter_date.strftime("%d/%m/%y"))

        self.create_valid_workforce_person(national_id="000001/12/1", password="itMXhZ", ward_id="9586")
        self.create_valid_beneficiary("B00000182", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("NO_SERVICES_PROVIDED"))
        self.runScript(script)

    def testNoReferralAdverseCondition(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)
        text = "RECS w0000091 SCBN SGFD SGFS SSRP b00000141 EELS LGEO u000091 xIoQFTG {encounter_date}".format(encounter_date=encounter_date.strftime("%d/%m/%y"))

        self.create_valid_workforce_person(workforce_id="W0000091",
                                           org_unit_id="U000091", password="IoQFTG", ward_id="9586")
        self.create_valid_beneficiary("B00000141", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)
        self.assertEqual(4, CoreServices.objects.filter(core_item_id__in=["SCBN", "SGFD", "SGFS", "SSRP"]).count())

    def testNoBBCNReferralAdverseCondition(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)
        text = "RECS xpfNjBj BBCNPRO/000002/1999 WNRC000003/12/1 EHOM {encounter_date} RMPL RMSG RMVS RMLS RMCT".format(encounter_date=encounter_date.strftime("%d/%m/%y"))
        self.create_valid_workforce_person(national_id="000003/12/1",
                                           org_unit_id="U000091", password="pfNjBj", ward_id="9586")
        self.create_valid_beneficiary("B00000141", bbcn="PRO/000002/1999", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)
        self.assertEqual(4, CoreServices.objects.filter(core_item_id__in=["RMPL", "RMSG", "RMLS", "RMCT"]).count())
        self.assertEqual(encounter_date, CoreEncounters.objects.all()[0].encounter_date)

    def testNoServiceReferral(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)
        text = "RECS PCIL PCDS PCOL PCCH PPOR WNRC000004/12/1 xrhPvIi B00000158 EHOM U000109 {encounter_date}".format(encounter_date=encounter_date.strftime("%d/%m/%y"))

        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000109", password="rhPvIi", ward_id="9586")
        self.create_valid_beneficiary("B00000158", bbcn="PRO/000002/1999", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)
        self.assertEqual(5, CoreAdverseConditions.objects.filter(adverse_condition_id__in=["PCIL", "PCDS", "PCOL", "PCCH", "PPOR"]).count())
        self.assertEqual(encounter_date, CoreEncounters.objects.all()[0].encounter_date)

    def testNoServiceAdverseReferralWardDate(self):
        text = "RECS xrhPvIi WNRC000004/12/1 EELS BNRC000003/12/3 RMHF"
        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000109", password="rhPvIi", ward_id="9586")
        self.create_valid_beneficiary(national_id="000003/12/3", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)
        self.assertEqual(1, CoreServices.objects.filter(core_item_id__in=["RMHF"]).count())
        self.assertEqual(datetime.date.today(), CoreEncounters.objects.all()[0].encounter_date)

    def testNoServiceSeperators(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)
        text = "recs WNRC000005/12/1 XFxsUNl B00000166 EWrK u000091 LgEO {encounter_date} SCBC/SCBF;SCBU".format(encounter_date=encounter_date.strftime("%d%b%y"))

        self.create_valid_workforce_person(national_id="000005/12/1",
                                           org_unit_id="U000091", password="FxsUNl", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000166", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)
        self.assertEqual(3, CoreServices.objects.filter(core_item_id__in=["SCBC","SCBF","SCBU"]).count())
        self.assertEqual(encounter_date, CoreEncounters.objects.all()[0].encounter_date)

    def testMultipleNoServiceSeperators(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)#valid date of less than 2 months away.
        text = "Recs SPAD; XitMXhZ/ W0000083 : EHOM BBCNpro/000002/2005 RMHF lgeo| {encounter_date} PPOR".format(encounter_date=encounter_date.strftime("%d%b%y"))
        self.create_valid_workforce_person(workforce_id="W0000083",
                                           org_unit_id="U000091", password="itMXhZ", ward_id="9586")
        self.create_valid_beneficiary(bbcn="pro/000002/2005", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)
        self.assertEqual(2, CoreServices.objects.filter(core_item_id__in=["SPAD", "RMHF"]).count())
        self.assertEqual(1, CoreAdverseConditions.objects.filter(adverse_condition_id__in=["PPOR"]).count())
        self.assertEqual(encounter_date, CoreEncounters.objects.all()[0].encounter_date)

    def testCorrectMessage(self):
        text = "reCs U000158, xrhPvIi WNRC000004/12/1 bnrc000004/12/3 EHOM daYT PORM, RMHT , SCBU"
        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000158", password="rhPvIi", ward_id="9586")
        self.create_valid_beneficiary(national_id="000004/12/3", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)
        self.assertEqual(2, CoreServices.objects.filter(core_item_id__in=["RMHT", "SCBU"]).count())
        self.assertEqual(1, CoreAdverseConditions.objects.filter(adverse_condition_id__in=["PORM"]).count())
        self.assertEqual(datetime.date.today(), CoreEncounters.objects.all()[0].encounter_date)

    def testAdverseConditionOnBeneficiary(self):
        text = "reCs U000158, xrhPvIi WNRC000004/12/1 bnrc000004/12/3 EHOM daYT PORM, RMHT , SCBU"
        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000158", password="rhPvIi", ward_id="9586")
        beneficiary = self.create_valid_beneficiary(national_id="000004/12/3", ward_id="9586")
        #Link the beneficiary to a RegPersons type
        mommy.make(RegPersonsTypes, person=beneficiary.person, person_type_id="TBGR")
        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("NO_ADVERSE_ON_GUARDIAN_ERROR"))
        self.runScript(script)

    def testAnotherMissingReferralServiceAdverse(self):
        text = "recs B00000166 EWRK xpfNjBj WNRC000003/12/1"
        self.create_valid_workforce_person(national_id="000003/12/1",
                                           org_unit_id="U000158", password="pfNjBj", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000166", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("NO_SERVICES_PROVIDED"))
        self.runScript(script)

    def testDuplicateRecords(self):
        text = "recs W0000232 x4qWtHx B00000216 EELS U000158 PORF RMHT SPAD SPAD SPFM RMHT RMPP PORF PORF"
        self.create_valid_workforce_person(workforce_id="W0000232",
                                           org_unit_id="U000158", password="4qWtHx", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000216", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=", ".join([const.DUP_FIELD_SERVICES, const.DUP_FIELD_REFERRAL, const.DUP_FIELD_ADVERSE]))
        self.runScript(script)

    def testNoOrgUnitNoPassword(self):
        text = "RECS W0000083 B00000133 EHOM LGEO DAYT STNT RMFP PHVP"
        self.create_valid_workforce_person(workforce_id="W0000083",
                                           org_unit_id="U000158", password="pfNjBj", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000133", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("PASSWORD_IS_MISSING_ERROR"))
        self.runScript(script)

    @unittest.skip("Skipped until we figure out how to do unicode comparisons")
    def testNoOrgUnitNoPasswordNOBeneficiaryBadDate(self):
        text = "RECS xitMXhZ EWRK B00000182 1jan14 SCBN"
        self.create_valid_workforce_person(workforce_id="W0000083",
                                           org_unit_id="U000158", password="itMXhZ", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000182", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} < '{response}'
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("DATE_TOO_FAR_BACK_ERROR"))
        self.runScript(script)


    def testNoLocation(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)
        text = "RECS W0000091 SCBN SGFD SGFS SSRP B00000141 LGEO U000091 XIoQFTG {encounter_date}".format(encounter_date=encounter_date.strftime("%d/%m/%y"))

        self.create_valid_workforce_person(workforce_id="W0000091", org_unit_id="U000091", password="IoQFTG", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000141", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=", ".join([db_lookups.lookup_messages_text("ENCOUNTER_TYPE_MISSING_ERROR")])
            )
        self.runScript(script)


    def testNoKeyword(self):
        text = "pfNjBj BBCNPRO/000002/1999 WNCR000003/12/1 EHOM 19/12/2014 RMPL RMSG RMVS RMLS RMCT"
        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("UNRECOGNISED_KEYWORD_ERROR")
            )
        self.runScript(script)

    def testNoKeywordAgain(self):
        text = "REsc PCIL PCDS PCOL PCCH PPOR WNCR000004/12/1 rhPvIi B00000158 EHOM U000109 13/12/2014"
        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("UNRECOGNISED_KEYWORD_ERROR")
            )
        self.runScript(script)

    def testNoPassword(self):
        text = "RECS rhPvi WNRC000004/12/1 EELS BNRC000003/12/3 RMHF"
        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=", ".join([db_lookups.lookup_messages_text("PASSWORD_IS_MISSING_ERROR"),
            const.UNRECOGNISED_DATA_PASSWORD])
            )
        self.runScript(script)


    def testInvalidGuardian(self):
        encounter_date = datetime.datetime.now().date() - datetime.timedelta(days=60)
        text = "recs WNRC000005/12/1 xFxsUNl B00000166 EWRK u000091 LgEO {encounter_date} SCBC/SCBF;SCBU".format(encounter_date=encounter_date.strftime("%d/%m/%y"))

        self.create_valid_workforce_person(national_id="000005/12/1",
                                           org_unit_id="U000091", password="FxsUNl", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000182", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} < {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("INVALID_BENEFICIARY_ID_ERROR").format(field="B00000166"))
        self.runScript(script)

    @unittest.skip("Skipped until we figure out how to do unicode comparisons")
    def testInvalidDate(self):
        text = u"Recs SPAD; xitMXhZ/ W0000083 : EHOM BBCNpro/000002/2005 RMHF 6August14 PPOR"
        self.create_valid_workforce_person(workforce_id="W0000083",
                                           org_unit_id="U000091", password="itMXhZ", ward_id="9586")
        self.create_valid_beneficiary(bbcn="pro/000002/2005", ward_id="9586")

        script = u"""
            {designated_number} > {recs_text}
            {designated_number} < '{response}'
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("DATE_TOO_FAR_BACK_ERROR"))
        self.runScript(script)

    def testInvalidDateBadOrg(self):
        text = "recS U0001580 xrhPvIi WNRC000004/12/1 bnrc000004/12/3 EHOM daYT PORM, RMHT , SCBU"
        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000091", password="rhPvIi", ward_id="9586")
        self.create_valid_beneficiary(national_id="000004/12/3", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} < {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("SELECTION_ERROR_NON_EXISTANT_ORG_UNIT").format(field="U0001580"))
        self.runScript(script)

    def testIncorrectWorkforceNRC(self):
        text = "recs B00000166 EWRK xpfNjBj WNRC000003/12/4 PORM"
        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000091", password="pfNjBj", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000166", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} < {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("AUTH_ERROR_INVALID_ID_OR_PASSWORD_UNREGISTERED_NUMBER"))
        self.runScript(script)

    def testServiceKeys(self):
        text = "recs W0000232 x4qWtHx B00000216 EELS U000158 PORF RMHT SPAD SPFM RMPP"
        self.create_valid_workforce_person(workforce_id="W0000232",
                                           org_unit_id="U000158", password="4qWtHx", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000216", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} < {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)

    def testUserNotRegisteredForServiceProvision(self):
        text = "recs W0000232 x4qWtHx B00000216 EELS U000158 RMHT SPAD SPFM RMPP PORF"
        self.create_valid_workforce_person(workforce_id="W0000232",
                                           org_unit_id="U000158", password="4qWtHx", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B00000216", ward_id="9586")

        #blank the workforce ID for the user
        self.worker.workforce_id = None
        self.worker.save()

        script = """
            {designated_number} > {recs_text}
            {designated_number} < {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("MISSING_WORKFORCE_ID_ERROR"))
        self.runScript(script)

    def testDoubleOrgUnitID(self):
        text = "recs U000083 W0000067 xQZxRv1 B00000125 EELS U0000117 SCBN"
        self.create_valid_workforce_person(workforce_id="W0000067",
                                           org_unit_id="U0000117", password="QZxRv1", ward_id=9586)
        self.create_valid_beneficiary(beneficiary_id="B00000125")

        script = """
            {designated_number} > {recs_text}
            {designated_number} < {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=const.DUP_FIELD_ORG_ID
        )
        self.runScript(script)

    def testCommaDelimeters(self):
        text = "reCs,x4NtsJw ,W0000174,B00000158,EHOM SCBU"
        self.create_valid_workforce_person(workforce_id="W0000174",
                                           org_unit_id="U0000117", password="4NtsJw", ward_id=9586)
        self.create_valid_beneficiary(beneficiary_id="B00000158", ward_id=9586)

        script = """
            {designated_number} > {recs_text}
            {designated_number} < {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE")
        )
        self.runScript(script)

    def testLGEO(self):
        #testing with ward set to 280(Kabwata)
        text = "reCs x4NtsJw  W0000174 B00000158 LGEO501 EHOM SCBU"
        self.create_valid_workforce_person(workforce_id="W0000174",
                                           org_unit_id="U0000117", password="4NtsJw", ward_id=501)
        self.create_valid_beneficiary(beneficiary_id="B00000158", ward_id=501)

        script = """
            {designated_number} > {recs_text}
            {designated_number} < {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE")
        )
        self.runScript(script)




class testUndo(TestScript):
    fixtures = ["initial_data.json"]
    def setUp(self):
        self.designated_number = '260973203144'

    def create_valid_workforce_person(self, workforce_id="W0000083", national_id="000001/15/1", org_unit_id="U233", password='', ward_id=''):

        self.organisation = mommy.make(RegOrgUnit, org_unit_id_vis=org_unit_id)
        self.worker = mommy.make(RegPerson, workforce_id=workforce_id, national_id=national_id)
        mommy.make(RegPersonsContact,
            person=self.worker,
            contact_detail_type_id=fielddictionary.contact_designated_mobile_phone,
            contact_detail=self.designated_number)
        self.reg_persons_org_unit = mommy.make(RegPersonsOrgUnits, person=self.worker, parent_org_unit=self.organisation, primary=True)
        self.worker_geo = mommy.make(RegPersonsGeo, person=self.worker, area_id=ward_id)
        mommy.make(SetupGeorgraphy, area_id=ward_id, area_type_id=fielddictionary.geo_type_ward_code)

        self.auth_user = AppUser.objects.create_user(workforce_id=workforce_id, national_id=national_id, password=password)
        self.auth_user.reg_person = self.worker
        self.auth_user.save()
        return self.worker

    def create_valid_beneficiary(self, beneficiary_id="B00000182", national_id="000003/12/3", bbcn='23', ward_id="9586"):
        beneficiary = mommy.make(RegPersonsGeo, person__beneficiary_id=beneficiary_id, person__national_id=national_id, person__birth_reg_id=bbcn, area_id=ward_id)
        return beneficiary

    def sendCorrectMessage(self):
        text = "reCs U000158, xrhPvIi WNRC000004/12/1 bnrc000004/12/3 EHOM daYT PORM, RMHT , SCBU"
        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000158", password="rhPvIi", ward_id="9586")
        self.create_valid_beneficiary(national_id="000004/12/3", ward_id="9586")

        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
        self.runScript(script)

    def testSuccesfulUndo(self):
        self.sendCorrectMessage()
        self.assertEqual(2, CoreServices.objects.filter(core_item_id__in=["RMHT", "SCBU"]).count())
        self.assertEqual(1, CoreAdverseConditions.objects.filter(adverse_condition_id__in=["PORM"]).count())
        self.assertEqual(datetime.date.today(), CoreEncounters.objects.all()[0].encounter_date)

        text = "undo"
        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("UNDO_SUCCESS_MSG").format(beneficiary_id="B00000182", encounter_date=datetime.date.today().strftime("%d %b %Y")))
        self.runScript(script)
        self.assertEqual(0, CoreServices.objects.filter(core_item_id__in=["RMHT", "SCBU"]).count())
        self.assertEqual(1, CoreAdverseConditions.objects.filter(adverse_condition_id__in=["PORM"]).count())
        self.assertEqual(0, CoreEncounters.objects.count())

    def sendTwoCorrectMessage(self):
        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000158", password="rhPvIi", ward_id="9586")

        text_1 = "reCs U000158, xrhPvIi WNRC000004/12/1 bnrc000002/11/3 EHOM daYT PORM, RMHT , SCBU, SCBF"
        text_2 = "reCs U000158, xrhPvIi WNRC000004/12/1 bnrc000004/12/3 EHOM daYT PORM, RMHT , SCBU"

        self.create_valid_beneficiary(beneficiary_id="B3244222", national_id="000002/11/3", ward_id="9586")
        self.create_valid_beneficiary(beneficiary_id="B122345", national_id="000004/12/3", ward_id="9586")

        for text in [text_1, text_2]:
            script = """
                {designated_number} > {recs_text}
                {designated_number} <  {response}
            """.format(
                designated_number=self.designated_number,
                recs_text=text,
                response=db_lookups.lookup_messages_text("RECS_SUCCESS_MESSAGE"))
            self.runScript(script)

    def testSuccesfulUndoOnlyLatestMessage(self):
        self.sendTwoCorrectMessage()
        self.assertEqual(5, CoreServices.objects.count())
        self.assertEqual(2, CoreAdverseConditions.objects.filter(adverse_condition_id__in=["PORM"]).count())
        self.assertEqual(2, CoreEncounters.objects.count())

        text = "undo"
        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("UNDO_SUCCESS_MSG").format(beneficiary_id="B122345", encounter_date=datetime.date.today().strftime("%d %b %Y")))
        self.runScript(script)
        self.assertEqual(3, CoreServices.objects.count())
        self.assertEqual(2, CoreAdverseConditions.objects.count())
        #only the second encounter should be deleted
        self.assertEqual(1, CoreEncounters.objects.count())
        self.assertEqual('B3244222', CoreEncounters.objects.all()[0].beneficiary_person.beneficiary_id)

    def testNoMessagesToUndo(self):
        self.create_valid_workforce_person(national_id="000004/12/1",
                                           org_unit_id="U000158", password="rhPvIi", ward_id="9586")
        self.create_valid_beneficiary(national_id="000004/12/3", ward_id="9586")
        text = "undo"
        script = """
            {designated_number} > {recs_text}
            {designated_number} <  {response}
        """.format(
            designated_number=self.designated_number,
            recs_text=text,
            response=db_lookups.lookup_messages_text("NO_MESSAGES_TO_UNDO_ERROR"))
        self.runScript(script)

class TestGenericFunctions(TestScript):
    def setUp(self):
        self.regpersons_queryset = RegPerson.objects.all()
    """
    def test_core_id_filter_name(self):
        person = mommy.make(RegPerson, first_name="john", surname="guy")
        regpersons_queryset = RegPerson.objects.all()

        search_string = "john"
        results = generic_functions.search_core_ids(regpersons_queryset, search_string)
        self.assertEqual(person, results[0])
    """

    def test_core_id_filter_nrc(self):
        person = mommy.make(RegPerson, national_id='234521/12/1')
        regpersons_queryset = self.regpersons_queryset

        search_string = "234521/12/1"
        result = generic_functions.search_core_ids(regpersons_queryset, search_string)
        self.assertEqual(person, result[0])

    def test_core_id_filter_birth_certificate(self):
        person = mommy.make(RegPerson, birth_reg_id='lus/123987/2014')
        regpersons_queryset = self.regpersons_queryset

        search_string = "lus/123987/2014"
        result = generic_functions.search_core_ids(regpersons_queryset, search_string)
        self.assertEqual(person, result[0])


    def test_core_id_filter_workforce_id(self):
        person = mommy.make(RegPerson, workforce_id='W2361249')
        regpersons_queryset = self.regpersons_queryset

        search_string = "W2361249"
        result = generic_functions.search_core_ids(regpersons_queryset, search_string)
        self.assertEqual(person, result[0])

    def test_core_id_filter_beneficiary_id(self):
        person = mommy.make(RegPerson, beneficiary_id='B9842754')
        regpersons_queryset = self.regpersons_queryset

        search_string = "B9842754"
        result = generic_functions.search_core_ids(regpersons_queryset, search_string)
        self.assertEqual(person, result[0])

    def test_gdclsu_filter(self):
        regpersons_queryset = self.regpersons_queryset

        person = mommy.make(RegPerson)
        gdclsu_unit = mommy.make(RegPersonsGdclsu, person=person, gdclsu__org_unit_name="Main org")
        #Also make a second person who we can not find
        second_gdclsu_unit = mommy.make(RegPersonsGdclsu, person=person,
                                        gdclsu__org_unit_name="Other org")

        search_string = "Main org"
        result = generic_functions.search_gdclsu_tags(regpersons_queryset, search_string)
        self.assertEqual(person, result[0])

    def test_geo_tags(self):
        regpersons_queryset = self.regpersons_queryset

        geo_area = mommy.make(SetupGeorgraphy, area_name="test town")
        reg_persons_geo = mommy.make(RegPersonsGeo, area_id=geo_area.area_id)
        person = reg_persons_geo.person

        search_string = "test town"
        result = generic_functions.search_geo_tags(regpersons_queryset, search_string)
        self.assertEqual(person, result[0])

    def test_parent_org_units(self):
        regpersons_queryset = self.regpersons_queryset
        reg_persons_unit = mommy.make(RegPersonsOrgUnits,
                                     parent_org_unit__org_unit_name="best organisation")
        person = reg_persons_unit.person

        search_string = "best organisation"
        result = generic_functions.search_parent_orgs(regpersons_queryset, search_string)
        self.assertEqual(person, result[0])
    """
    def testResultsRanking(self):
        name_results = mommy.make(RegPerson)
        results_dict = {
            "names":name_results,
            "core_ids":core_id_results,
            "gdclsu": gdclsu_results,
            "geo_tag": geo_tag_results,
            "parent_orgs":parent_orgs_results,
        }
    """

    def test_simple_name_lookup(self):
        person = mommy.make(RegPerson, first_name="Andre", surname="Lesa")
        wrong_person = mommy.make(RegPerson, first_name="John", surname="Doe")
        search_string = "Andre Lesa"
        results = generic_functions.get_list_of_persons(search_string)
        self.assertEqual(len(results), 1)
        self.assertEqual(person, results[0])
        return results

    def test_as_of_date_not_specifiedlookup(self):
        """A date, or not specified. If not specified we assume we want
        current data (date_delinked is null)."""
        person = mommy.make(RegPerson, first_name="Andre", surname="Lesa")
        search_string = "Andre Lesa"
        results = generic_functions.get_list_of_persons(search_string)
        result = results[0]
        self.assertFalse(person.date_of_death)

    def test_as_of_date_specifiedlookup(self):
        """If specified, when looking at date_delinked, date_of_death etc
        we regard them as still linked, still alive etc if the date_delinked
        or date_of_death occurs after this parameter date"""
        person = mommy.make(RegPerson, first_name="John", surname="Doe")
        search_string = "John"
        as_of_date = datetime.datetime.now() - datetime.timedelta(10)
        results = generic_functions.get_list_of_persons(search_string,
                                                        as_of_date=as_of_date)
        self.assertEqual(person, results[0])
        person.date_of_death = datetime.datetime.now() - datetime.timedelta(days=20)#Date of death before the as of date
        person.save()
        results = generic_functions.get_list_of_persons(search_string,
                                                        as_of_date=as_of_date)
        self.assertEqual(0, len(results))

    def test_in_person_types(self):
        person = mommy.make(RegPerson, first_name="John", surname="Doe")
        person_types = mommy.make(RegPersonsTypes, person=person, person_type_id="TBVC")

        search_string = "John"
        results = generic_functions.get_list_of_persons(search_string)
        self.assertEqual(person, results[0])

        #Specify types that the person john doesn't have
        results = generic_functions.get_list_of_persons(search_string,
                                                        in_person_types=["TWNE","TBGR"])
        self.assertEqual(0, len(results))

        #Specify types that the person has
        results = generic_functions.get_list_of_persons(search_string,
                                                        in_person_types=["TBVC",
                                                                         "TBGR"])
        self.assertEqual(person, results[0])

    def test_age(self):
        """Specified in order to search for persons with a specific age plus or
        minus one year. If not specified, do not filter by age. If as_of_date
        provided, calculate age as of that date. If as_of_date not
        provided, calculate age on current date."""
        date_of_birth = datetime.datetime(1990, 11, 8)
        person = mommy.make(RegPerson, first_name="John", surname="Doe",
                            date_of_birth=date_of_birth)
        search_string = "John"
        #First test with age that will return a result
        age_delta = datetime.datetime.now() - date_of_birth
        age = age_delta.days/365
        results = generic_functions.get_list_of_persons(search_string, age=age)
        self.assertEqual(person, results[0])

        #Use a year of birth that will not return a result
        age += 5
        results = generic_functions.get_list_of_persons(search_string, age=age)
        self.assertEqual(0, len(results))

        #Use an as of date that agrees with the new age being used
        as_of_date_delta = datetime.timedelta(365*5)
        as_of_date = datetime.datetime.now() + as_of_date_delta
        results = generic_functions.get_list_of_persons(search_string, age=age,
                                                        as_of_date=as_of_date)
        self.assertEqual(person, results[0])

    def test_has_beneficiary_id(self):
        person = mommy.make(RegPerson, first_name="John", surname="Doe")
        search_string = "john"
        #Test with beneficiary id parameter
        results = generic_functions.get_list_of_persons(search_string,
                                                        has_beneficiary_id=True)
        self.assertEqual(0, len(results))

        #now test with the ID on person
        person.beneficiary_id = True
        person.save()
        results = generic_functions.get_list_of_persons(search_string,
                                                        has_beneficiary_id=True)
        self.assertEqual(person, results[0])

    def test_has_workforce_id(self):
        person = mommy.make(RegPerson, first_name="John", surname="Doe")
        search_string = "john"
        #Test with workforce id parameter
        results = generic_functions.get_list_of_persons(search_string,
                                                        has_workforce_id=True)
        self.assertEqual(0, len(results))

        #now test with the ID on person
        person.workforce_id = True
        person.save()
        results = generic_functions.get_list_of_persons(search_string,
                                                        has_workforce_id=True)
        self.assertEqual(person, results[0])

    def test_include_void(self):
        person = mommy.make(RegPerson, first_name="John", surname="Doe")
        search_string = "john"
        #Test with workforce id parameter
        results = generic_functions.get_list_of_persons(search_string,
                                                        include_void=True)
        self.assertEqual(0, len(results))

        #now test with the ID on person
        person.is_void = True
        person.save()
        results = generic_functions.get_list_of_persons(search_string,
                                                        include_void=True)
        self.assertEqual(person, results[0])

    def test_include_died(self):
        """True or false. If unspecified we assume true. Whether to include
        persons who have died or not. Note if as_of_date provided and
        include_died is false, look at records where (date_of_death is null or
        date_of_death > as_of_date )
        """
        days_ago = datetime.datetime.now() - datetime.timedelta(days=30)
        person = mommy.make(RegPerson, first_name="John", surname="Doe", date_of_death=days_ago)
        search_string = "John"
        results = generic_functions.get_list_of_persons(search_string)
        self.assertEqual(person, results[0])

        results = generic_functions.get_list_of_persons(search_string,
                                                        include_died=False)
        self.assertEqual(0, len(results))

class TestLookupSMS(TestScript):
    fixtures = ["initial_data.json"]
    def setUp(self):
        self.workforce_id = "W34567"
        self.beneficiary_id = "B00000133"
        self.encounter_date = datetime.date.today()

        self.beneficiary = mommy.make(RegPerson,
                                      beneficiary_id=self.beneficiary_id,
                                      sex_id="SMAL")
        self.ward_id = 501
        try:
            self.ward = SetupGeorgraphy.objects.get(area_id=self.ward_id)
        except SetupGeorgraphy.DoesNotExist:
            self.ward = mommy.make(SetupGeorgraphy, area_id=self.ward_id, area_type_id=fielddictionary.geo_type_ward_code)

        self.org_unit_id = "2334"
        self.organisation = mommy.make(RegOrgUnit,
                                       org_unit_id_vis=self.org_unit_id,
                                       org_unit_name="ABC")
        self.designated_number = '0973203144'
        self.worker = mommy.make(RegPerson, workforce_id=self.workforce_id)
        self.reg_persons_org_unit = mommy.make(RegPersonsOrgUnits, person=self.worker, parent_org_unit=self.organisation, primary=True)
        self.worker_geo = mommy.make(RegPersonsGeo, person=self.worker, area_id=self.ward_id)

        person_types = mommy.make(RegPersonsTypes, person=self.worker,
                                  person_type_id="TWNE")

        self.password = "thepassword"
        self.auth_user = AppUser.objects.create_user(workforce_id=self.workforce_id, password=self.password)
        self.auth_user.reg_person = self.worker
        self.auth_user.save()

        self.other_area = 9586
        self.other_worker_workforce_id = "W0000091"
        self.other_worker_designated_number = '0973332238'
        self.other_worker = mommy.make(RegPerson, workforce_id=self.other_worker_workforce_id)
        self.reg_persons_org_unit = mommy.make(RegPersonsOrgUnits, person=self.other_worker, parent_org_unit=self.organisation, primary=True)
        self.worker_geo = mommy.make(RegPersonsGeo, person=self.other_worker, area_id=self.other_area)

        self.auth_user = AppUser.objects.create_user(workforce_id=self.other_worker_workforce_id, password=self.password)
        self.auth_user.reg_person = self.other_worker
        self.auth_user.save()

    def test_wrong_password_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        second_org = mommy.make(RegPersonsOrgUnits, person=self.worker,
                                parent_org_unit__org_unit_name="XYZ")
        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (Jenifer Hamisi Mulungishi ABC org) AGEY30 sfem"%(self.workforce_id, "wrong_password")
        script = """
            {designated_number} > {text}
            {designated_number} < {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response=db_lookups.lookup_messages_text("AUTH_ERROR_INVALID_ID_OR_PASSWORD_UNREGISTERED_NUMBER")
        )
        self.runScript(script)

    def test_no_results_lookup(self):
        text = "FINW %s x%s (Jenifer Hamisi Mulungishi ABC org) AGEY30 sfem"%(self.workforce_id, self.password)
        expected_response = "WOOOO83-Jennifer Hamisi"
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="No workforce members match this search"
        )
        self.runScript(script)

    def test_name_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        second_org = mommy.make(RegPersonsOrgUnits, person=self.worker,
                                parent_org_unit__org_unit_name="XYZ")
        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (Jenifer Hamisi Mulungishi ABC org) AGEY30 sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC XYZ"
        )
        self.runScript(script)

    def test_parent_organisation_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (Mulungishi ABC org) AGEY30 sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC"
        )
        self.runScript(script)

    def test_geo_location_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        ndola_district_id = 88  #kansenshi
        try:
            self.ward = SetupGeorgraphy.objects.get(area_id=ndola_district_id)
        except SetupGeorgraphy.DoesNotExist:
            self.ward = mommy.make(SetupGeorgraphy, area_name="ndola", area_id=ndola_district_id, area_type_id=fielddictionary.geo_type_ward_code)
        person_geo = mommy.make(RegPersonsGeo, person=self.worker, area_id=ndola_district_id)

        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (ndola) AGEY30 sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC"
        )
        self.runScript(script)


    def test_gdclsu_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        person_gdclsu = mommy.make(RegPersonsGdclsu, person=self.worker,
                                   gdclsu__org_unit_name="the org")

        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (org) AGEY30 sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC"
        )
        self.runScript(script)

    def test_workforce_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (34567 Good org) AGEY30 sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC"
        )
        self.runScript(script)

    def test_national_id_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.national_id = "413943/61/1"
        self.worker.sex_id = "SFEM"

        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (413943 Good org) AGEY30 sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC"
        )
        self.runScript(script)


    def test_birth_reg_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.birth_reg_id = "blus/19/2014"
        self.worker.sex_id = "SFEM"

        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (blus/19 Good org) AGEY30 sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC"
        )
        self.runScript(script)


    def test_beneficiary_id_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.beneficiary_id = "b1245"
        self.worker.sex_id = "SFEM"

        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (b1245 Good org) AGEY30 sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC"
        )
        self.runScript(script)


    def test_no_age_specified_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        second_org = mommy.make(RegPersonsOrgUnits, person=self.worker,
                                parent_org_unit__org_unit_name="XYZ")
        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (Jenifer Hamisi Mulungishi ABC org) sfem"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC XYZ"
        )
        self.runScript(script)


    def test_no_sex_specified_lookup(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        second_org = mommy.make(RegPersonsOrgUnits, person=self.worker,
                                parent_org_unit__org_unit_name="XYZ")
        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s (Jenifer Hamisi Mulungishi ABC org) AGEY30"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="W34567-Jenifer Hamisi-ABC XYZ"
        )
        self.runScript(script)


    def test_no_search_text_error(self):
        self.worker.first_name = "Jenifer"
        self.worker.surname = "Hamisi"
        self.worker.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.worker.sex_id = "SFEM"

        second_org = mommy.make(RegPersonsOrgUnits, person=self.worker,
                                parent_org_unit__org_unit_name="XYZ")
        self.password = "thepassword"
        self.worker.save()

        text = "FINW %s x%s"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response=db_lookups.lookup_messages_text("MISSING_SEARCH_TEXT_ERROR")
        )
        self.runScript(script)

    def test_beneficiary_name_lookup_wrong_password(self):
        self.beneficiary.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.beneficiary.first_name = "Ben"
        self.beneficiary.surname = "Jean"
        self.beneficiary.save()

        text = "FINB %s x%s (B00000133) AGEY30 smal"%(self.workforce_id, "wrong_password")
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response=db_lookups.lookup_messages_text("AUTH_ERROR_INVALID_ID_OR_PASSWORD_UNREGISTERED_NUMBER")
        )
        self.runScript(script)

    def test_beneficiary_name_lookup(self):
        self.beneficiary.date_of_birth = datetime.datetime.now() - datetime.timedelta(365*30)
        self.beneficiary.first_name = "Ben"
        self.beneficiary.surname = "Jean"
        self.beneficiary.save()

        text = "FINB %s x%s (B00000133) AGEY30 smal"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="B00000133-Ben Jean"
        )
        self.runScript(script)


    def test_multiple_beneficiaries_in_search(self):
        self.beneficiary.first_name = "Ben"
        self.beneficiary.surname = "Jean"
        self.beneficiary.save()

        self.beneficiary2 = mommy.make(RegPerson,
                                      beneficiary_id="B3456",
                                      sex_id="SMAL",
                                      first_name="John",
                                      surname="Ivy")

        text = "FINB %s x%s (B00000133 John) smal"%(self.workforce_id, self.password)
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="B3456-John Ivy, B00000133-Ben Jean"
        )
        self.runScript(script)

class TestGenericOrgLookup(TestScript):
    fixtures = ["initial_data.json", "geo_static_data.json"]
    def setUp(self):
        self.ndola_district_id = 88
        self.main_org = mommy.make(RegOrgUnit, org_unit_name="The mech org",
                                   is_void=False)
        self.org_unit_queryset = RegOrgUnit.objects.all()
        self.field_names = ["org_unit_id_vis", "org_unit_name"]

    def test_name_search(self):
        results = generic_functions.direct_field_search(self.org_unit_queryset,
                                              search_string="mech",
                                              field_names=self.field_names)
        self.assertEqual(self.main_org, results[0])

    def test_search_geo_tags(self):
        results = generic_functions.search_geo_tags(self.org_unit_queryset,
                                                    search_string="ndola")
        self.assertFalse(results)

        mommy.make(RegOrgUnitGeography, org_unit=self.main_org,
                   area_id=self.ndola_district_id)
        results = generic_functions.search_geo_org_tags(self.org_unit_queryset,
                                                    search_string="ndola")
        self.assertEqual(self.main_org, results[0])

    def test_search_org_unit_gdclsu_tags(self):
        results = generic_functions.search_org_unit_gdclsu_tags(self.org_unit_queryset,
                                                                search_string="TAG")

        self.assertFalse(results)
        mommy.make(RegOrgUnitGdclsu, org_unit=self.main_org,
                   gdclsu_id="TheGDCLSUTag")

        results = generic_functions.search_org_unit_gdclsu_tags(self.org_unit_queryset,
                                                                search_string="TAG")
        self.assertEqual(self.main_org, results[0])

    def test_full_search_function(self):
        results = generic_functions.get_list_of_org_units(search_string="mech")
        self.assertEqual(self.main_org, results[0])


class TestLookupORGSMS(TestScript):
    fixtures = ["initial_data.json"]
    def setUp(self):
        self.designated_number = "4"
        self.ward_id = 501
        try:
            self.ward = SetupGeorgraphy.objects.get(area_id=self.ward_id)
        except SetupGeorgraphy.DoesNotExist:
            self.ward = mommy.make(SetupGeorgraphy, area_id=self.ward_id, area_type_id=fielddictionary.geo_type_ward_code)

        self.org_unit_id = "2334"
        self.org_unit_name = "Mech Org"
        self.org_unit = mommy.make(RegOrgUnit,
                                       org_unit_id_vis=self.org_unit_id,
                                       org_unit_name=self.org_unit_name,
                                       is_gdclsu=False)

    def test_no_results_lookup(self):
        text = "FINU (none existant)"
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="No organizational units match this search"
        )
        self.runScript(script)

    def test_name_lookup(self):
        second_org = mommy.make(RegOrgUnit, org_unit_name="XYZ")
        text = "FINU (mech org)"
        script = """
            {designated_number} > {text}
            {designated_number} < {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="%s-%s"%(self.org_unit.org_unit_id_vis,
                                       self.org_unit.org_unit_name)
        )
        self.runScript(script)

    def test_geo_location_lookup(self):
        ndola_district_id = 88  #kansenshi
        try:
            self.ward = SetupGeorgraphy.objects.get(area_id=ndola_district_id)
        except SetupGeorgraphy.DoesNotExist:
            self.ward = mommy.make(SetupGeorgraphy, area_name="ndola", area_id=ndola_district_id, area_type_id=fielddictionary.geo_type_ward_code)
        mommy.make(RegOrgUnitGeography, area_id=ndola_district_id, org_unit=self.org_unit)

        text = "FINU (ndola)"
        script = """
            {designated_number} > {text}
            {designated_number} < {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="%s-%s"%(self.org_unit_id, self.org_unit_name)
        )
        self.runScript(script)

    def test_gdclsu_lookup(self):
        person_gdclsu = mommy.make(RegOrgUnitGdclsu, org_unit=self.org_unit, gdclsu_id="org gdclsu")

        text = "FINU (org gdclsu)"
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="%s-%s"%(self.org_unit_id, self.org_unit_name)
        )
        self.runScript(script)

    def test_no_search_text_error_org(self):
        text = "FINU ()"
        script = """
            {designated_number} > {text}
            {designated_number} < {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response=db_lookups.lookup_messages_text("MISSING_SEARCH_TEXT_ERROR")
        )
        self.runScript(script)

    def test_multiple_orgs_in_search(self):
        second_org = mommy.make(RegOrgUnit, org_unit_id_vis="97842",
                                org_unit_name="The real mech organisation",
                                is_gdclsu=False)

        text = "FINU (mech)"
        script = """
            {designated_number} > {text}
            {designated_number} <  {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response="%s-%s, %s-%s"%(self.org_unit_id,
                                              self.org_unit_name,
                                              second_org.org_unit_id_vis,
                                              second_org.org_unit_name)
        )
        self.runScript(script)

class TestChangePassword(TestScript):
    fixtures = ["initial_data.json"]
    def setUp(self):
        self.workforce_id = "W34567"
        self.designated_number = '0973203144'
        self.worker = mommy.make(RegPerson,
                                 first_name='John',
                                 surname='Doe',
                                 workforce_id=self.workforce_id)
        self.reg_contact = mommy.make(RegPersonsContact,
                                      person=self.worker,
                                      contact_detail_type_id=fielddictionary.contact_designated_mobile_phone,
                                      contact_detail=self.designated_number)

        self.password = "thepassword"
        self.auth_user = AppUser.objects.create_user(reg_person=self.worker, workforce_id=self.workforce_id, password=self.password)
        self.auth_user.save()

    def test_password_change(self):
        new_password = 'changed_password'
        self.auth_user.set_password(new_password)
        self.auth_user.save()
        self.assertTrue(self.auth_user.check_password(new_password))

    def test_change_password_sms(self):
        new_password = "newpa2"
        text = "CHGP %s x%s (%s)"%(self.workforce_id, self.password, new_password)
        script ="""
            {designated_number} > {text}
            {designated_number} < {expected_response}
            """.format(
                designated_number=self.designated_number,
                text=text,
                expected_response=db_lookups.lookup_messages_text("PASSWORD_CHANGE_SUCCESSFUL"))
        self.runScript(script)
        self.assertTrue(self.auth_user.check_password(new_password))

    def test_deny_special_characters(self):
        new_password = "newpa!"
        text = "CHGP %s x%s (%s)"%(self.workforce_id, self.password, new_password)
        script ="""
            {designated_number} > {text}
            {designated_number} < {expected_response}
            """.format(
                designated_number=self.designated_number,
                text=text,
                expected_response=db_lookups.lookup_messages_text("INCORRECT_PASSWORD_FORMAT"))
        self.runScript(script)
        self.assertFalse(self.auth_user.check_password(new_password))

    def test_long_password(self):
        new_password = "verylongpassword"
        text = "CHGP %s x%s (%s)"%(self.workforce_id, self.password, new_password)
        script ="""
            {designated_number} > {text}
            {designated_number} < {expected_response}
            """.format(
                designated_number=self.designated_number,
                text=text,
                expected_response=db_lookups.lookup_messages_text("INCORRECT_PASSWORD_FORMAT"))
        self.runScript(script)
        self.assertFalse(self.auth_user.check_password(new_password))

class TestLookupOrdering(TestScript):
    def setUp(self):
        self.workforce_id = "W34567"
        self.designated_number = '0973203144'
        self.worker = mommy.make(RegPerson, workforce_id=self.workforce_id,
                                 first_name='John',
                                 surname='Doe')
        person_types = mommy.make(RegPersonsTypes, person=self.worker,
                                      person_type_id="TWGE")

        self.password = "thepassword"
        self.auth_user = AppUser.objects.create_user(workforce_id=self.workforce_id, password=self.password)
        self.auth_user.reg_person = self.worker
        self.auth_user.save()

        self.user1_id = "W0000786"
        self.user1_name = "User surround"
        self.user2_id = "W0000802"
        self.user2_name = "User 27"
        self.user3_id = "W0000794"
        self.user3_name = "User Supervisor"
        self.organisation = "Ministry of Community Development Mother and Child Health HQ"

        users_list = [(self.user1_id, self.user1_name),
                      (self.user3_id, self.user3_name),
                      (self.user2_id, self.user2_name),
                      ]


        for user_id, username in users_list:
            first_name, last_name = username.split()
            person_object = mommy.make(RegPerson,
                    workforce_id=user_id,
                    first_name=first_name,
                    surname=last_name,
                    sex_id="SFEM")
            person_types = mommy.make(RegPersonsTypes, person=person_object,
                                      person_type_id="TWGE")
            person_org_units = mommy.make(RegPersonsOrgUnits,
                                          person=person_object,
                                          parent_org_unit__org_unit_name=self.organisation)
            setattr(self, user_id, person_object)

    def testOrdering(self):
        expected_text = """{user3_id}-{user3_username}-{organisation}, {user1_id}-{user1_username}-{organisation}, {user2_id}-{user2_username}-{organisation}""".format(
                                            user1_id=self.user1_id,
                                            user1_username=self.user1_name,
                                            user2_id=self.user2_id,
                                            user2_username=self.user2_name,
                                            user3_id=self.user3_id,
                                            user3_username=self.user3_name,
                                            organisation=self.organisation)

        text = "FINW %s x%s (User sup)"%(self.workforce_id, self.password)

        script = """
        {designated_number} > {text}
        {designated_number} < {expected_response}
        """.format(
            designated_number=self.designated_number,
            text=text,
            expected_response=expected_text
        )
        self.runScript(script)

class TestRegressionFix():
    fixtures = ["initial_data.json"]
    def setUp(self):
        self.workforce_id = "W34567"
        self.designated_number = '0973203144'
        self.worker = mommy.make(RegPerson,
                                 first_name='John',
                                 surname='Doe',
                                 workforce_id=self.workforce_id)
        self.reg_contact = mommy.make(RegPersonsContact,
                                      person=self.worker,
                                      contact_detail_type_id=fielddictionary.contact_designated_mobile_phone,
                                      contact_detail=self.designated_number)

        self.password = "thepassword"
        self.auth_user = AppUser.objects.create_user(reg_person=self.worker, workforce_id=self.workforce_id, password=self.password)
        self.auth_user.save()

