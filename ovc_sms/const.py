#These are kept here for tests only, the actual text is done in the simplesmsform application
DUP_FIELD_SERVICES = ("You have included more than one value for Services Provided - please include only one within each message")
DUP_FIELD_REFERRAL = ("You have included more than one value for Referral Provided - please include only one within each message")
DUP_FIELD_ADVERSE = ("You have included more than one value for Adverse Conditions - please include only one within each message")
UNRECOGNISED_DATA_PASSWORD = "'rhPvi' is not recognised or is not in a standard format. Please remove it or adjust it so that it follows the format in your job aid."
DUP_FIELD_ORG_ID = "You have included more than one value for Organisation Id - please include only one within each message"
INVALID_DATE = ("Date not recognised - for dates please write the date, then the month, then the year, with no spaces between them.")
