import datetime
import string
import itertools
from simplesmsforms.smsform_fields import PrefixField, SingleChoiceField, MultiChoiceField, DateField
from sms_exceptions import FutureDateException, PastThreeMonthsException, IncorrectPasswordFormatException
import db_lookups
import const

def deny_date_beyond_three_months_ago(value):
    three_months = datetime.timedelta(days=90)
    three_months_ago = datetime.date.today() - three_months
    #Using days may be inaccurate, best to use reletivedelta later

    if value < three_months_ago:
        raise PastThreeMonthsException('')

def deny_future_dates(value):
    #print value,'value'
    #print date.today(),'date.today()'
    #print date.today() < value,'date.today() < value'
    if value:
        if datetime.date.today() < value:
            raise FutureDateException("Future date not allowed, please select today or a date in the past.")

def deny_longer_than_six_characters(value):
    if value:
        if len(value) > 6:
            raise  IncorrectPasswordFormatException(field="")

def deny_special_characters(value):
    special_characters_in_string = set(string.punctuation).intersection(value)
    if special_characters_in_string:
        raise SpecialCharactersInPasswordExcepton(field="")

class OVCDateField(DateField):
    def get_field_regex(self):
        """We will accept 2 formats for the dates: dayMonthYear, day/Month/Year
            with the month acceptable as a word or digits
        """
        regex_strings = [
            r"\b\d{1,2}[-/]\d{1,2}[-/]\d{1,4}\b",
            r"\b\d{1,2}[a-z]{3,14}\d{1,4}\b",
            r"DAYT"
        ]
        return [
            {
                "prefix": "", "regex": "{regex_strings}".format(regex_strings="|".join(regex_strings), name=self.name)
            }
        ]

    def to_python(self, text, accepted_prefix=""):
        if text.lower() == "dayt":
            return datetime.date.today(), accepted_prefix
        else:
            return super(OVCDateField, self).to_python(text, accepted_prefix)

class OVCIDField(PrefixField):
    def get_field_regex(self):
        nrc_regex = r"\bwnrc(?P<id_field>\d{6}/\d{,2}/\d{,2}\b)"
        id_regex = r"\b(?P<id_field>w\d{,9})\b"
        return [
            {
                "prefix": "wnrc", "regex": "{regex_string}".format(regex_string=nrc_regex)
            },
            {
                "prefix": "", "regex": "{regex_string}".format(regex_string=id_regex)
            },
        ]

class OVCOrgUnitField(PrefixField):

    def get_field_regex(self):
        id_regex = r"\b(?P<id_field>u\d{1,7})\b"
        return [
            {
                "prefix": "", "regex": "{regex_string}".format(regex_string=id_regex)
            },
        ]

class OVCBeneficiaryIDField(PrefixField):
    def get_field_regex(self):
        nrc_regex = r"\bbnrc(?P<id_field>\d{6}/\d{,2}/\d{,2})\b"
        id_regex = r"\b(?P<id_field>b\d{5,10})\b"
        bbcn_regex = r"\bbbcn(?P<id_field>[a-z]{,4}/\d{,6}/\d{,4})\b"
        return [
            {
                "prefix": "bnrc", "regex": "{regex_string}".format(regex_string=nrc_regex)
            },
            {
                "prefix": "", "regex": "{regex_string}".format(regex_string=id_regex)
            },
            {
                "prefix": "bbcn", "regex": "{regex_string}".format(regex_string=bbcn_regex)
            },
        ]

class OVCPasswordField(PrefixField):
    """Customize the error message for missing password"""
    def validate(self, value):
        if self.required and value in self.empty_values:
            raise MissingPasswordException()
        return super(OVCPasswordField, self).validate(value)

class OVCBracketField(PrefixField):
    def get_field_regex(self):
        bracket_regex = r"\((?P<search_string>.+?)\)"
        return [
            {
                "prefix": "", "regex":"{regex_string}".format(regex_string=bracket_regex)
            },
        ]

    def validate(self, value):
        if self.required and value in self.empty_values:
            raise MissingSearchTextException()
        return super(OVCBracketField, self).validate(value)

