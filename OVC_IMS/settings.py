"""
Django settings for OVC_IMS project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '2!kva$a9rm8lup8v841d27kh1wisf1d*128qx(hf2@*=2h^7km'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
IS_CAPTURE_SITE = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['41.218.84.26','zomis.mcdmch.gov.zm', 'www.zomis.mcdmch.gov.zm', '127.0.0.1', 'localhost', '192.168.0.5']
if IS_CAPTURE_SITE:
    #http://stackoverflow.com/a/30990617
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    localipaddress = s.getsockname()[0] 
    
    ALLOWED_HOSTS = ['127.0.0.1', 'localhost', localipaddress]

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'crispy_forms',
    'ovc_home',
    'ovc_main',
    'ovc_auth',
    #RapidSMS
    "django_nose",
    #"djtables",  # required by rapidsms.contrib.locations
    "django_tables2",
    "selectable",
    "south",
    "ovc_sms",
    'ovc_library',
)
if IS_CAPTURE_SITE == False:
    INSTALLED_APPS+=(
                     "rapidsms",
                    "rapidsms.router.blocking",
                    "rapidsms.backends.database",
                    "rapidsms.contrib.handlers",
                    "rapidsms.contrib.httptester",
                    "rapidsms.contrib.messagelog",
                    "rapidsms.contrib.messaging",
                    "rapidsms.contrib.registration",
                     )
ovc_db_name = 'ovc_ims'


if IS_CAPTURE_SITE:
    import capture_settings

split_database = True
if split_database:
    if IS_CAPTURE_SITE:
        ovc_db_name = 'ovc_ims_capture'

password = 'z0mi5_C0nstella!'
username = 'postgres'

if not DEBUG:
    OVC_IMS = os.path.dirname(os.path.abspath(__file__))#OVC_IMS/OVC_IMS/settings.py
    parent_OVC_IMS = os.path.dirname(OVC_IMS)#OVC_IMS
    workingdir = os.path.dirname(parent_OVC_IMS)

    passwordlocation =  os.path.join(workingdir, 'produtils', 'password.txt')
    with open(passwordlocation,'r') as f:
        password = f.readline()
        tmpusername = f.readline()
        username = tmpusername.strip() if tmpusername else username
        if not IS_CAPTURE_SITE:
            rsusername = f.readline()
            rspassword = f.readline()
    if not IS_CAPTURE_SITE:
        ovc_db_name = 'ovc_prod_db'

    if not IS_CAPTURE_SITE:
        rapid_prod_sms_backend = {
                             "airtel" : {
                                    "ENGINE": "rapidsms.backends.kannel.KannelBackend",
                                    "sendsms_url": "http://127.0.0.1:13013/cgi-bin/sendsms",
                                    "sendsms_params": {"smsc": "All services",
                                                       "from": "7080",
                                                       "username": rsusername.strip(),
                                                       "password": rspassword.strip()}, # set in locals$
                                    "coding": 0,
                                    "charset": "ascii",
                                    "encode_errors": "ignore", # strip out unknown (unicode) characters
                                }
                             }

RAPIDSMS_ROUTER = "rapidsms.router.blocking.BlockingRouter"

INSTALLED_BACKENDS = {
    "message_tester": {
        "ENGINE": "rapidsms.backends.database.DatabaseBackend",
    }
}

if not DEBUG and not IS_CAPTURE_SITE:
    INSTALLED_BACKENDS.update(rapid_prod_sms_backend)


remote_server_url = 'zomis.mcdmch.gov.zm'

CAPTURE_SITE_URLS = {
    'upload_server_url':'http://%s/api/upload/' % remote_server_url,
    'download_server_url':'http://%s/api/download' % remote_server_url,
    'server_url':'http://%s/server_admin/' % remote_server_url,
}

USERNAME = 'test'
PASSWORD = 'test'

import djcelery
djcelery.setup_loader()

CELERY_RESULT_BACKEND = 'redis://localhost'

CELERY_ACCEPT_CONTENT = ['json']
#CELERY_ACCEPT_CONTENT = ['json']

CELERY_TASKS_SERIALIZER = ['json']

CELERY_RESULT_SERIALIZER = 'json'

CELERY_EVENT_SERIALIZER = 'json'

BROKER_URL = 'redis://localhost:6379/0'

CELERY_IMPORTS= ('')
if IS_CAPTURE_SITE:
    CELERY_IMPORTS= ('rest_client')
else:
    CELERY_IMPORTS= ('rapidsms.router.celery.tasks')


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'OVC_IMS.urls'

WSGI_APPLICATION = 'OVC_IMS.wsgi.application'


SESSION_EXPIRE_AT_BROWSER_CLOSE = True
# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': ovc_db_name,
        'USER': username,
        'PASSWORD': password.strip(),
        'HOST': 'localhost',
        'PORT': '5432',
        }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_ROOT = '/var/www/zomisprod/static/'
if IS_CAPTURE_SITE:
    STATIC_ROOT = 'C:/Program Files (x86)/ZOMIS/Apache24/htdocs/zomis/static'

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = ("django.contrib.auth.context_processors.auth",
                               "ovc_main.context_processors.site_info",
                               "django.core.context_processors.request")
LOGIN_URL = 'ovc_home.views.ovc_login'
CRISPY_TEMPLATE_PACK = 'bootstrap3'

AUTH_USER_MODEL = 'ovc_auth.AppUser'
AUTHENTICATION_BACKENDS = ('ovc_auth.backends.OVCAuthenticationBackend',)


EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'zomisinfo@gmail.com'
EMAIL_HOST_PASSWORD = '!3mEr41D>'

APP_NAME = 'ZOMIS'

LOGGING_CONFIG = None

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'basic': {
            'format': '%(asctime)s %(name)-20s %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'basic',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'basic',
            'filename':  'rapidsms.log',
        },
    },
    'loggers': {
        'rapidsms': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'rapidsms.router.celery': {
            'handlers': ['file'],
            'level': 'DEBUG'
        }
    }
}
# Digital library file upload folder
MEDIA_ROOT = os.path.join(BASE_DIR, 'ovc_library/upload')
MEDIA_URL='/library/upload/'
# Digital library allowed files (doc, docx, xls, xlsx, pdf, zip, ppt, pptx)
ALLOWED_FILE_TYPES = ['doc', 'docx', 'xls', 'xlsx', 'pdf', 'zip', 'ppt', 'pptx']
# Digital library file size limit 5MB (5242880 bytes)
MAX_UPLOAD_SIZE = 5242880
EXPORT_ROOT = os.path.join(BASE_DIR, 'ExportedReports')
