from django.conf.urls import patterns, include, url

from django.contrib import admin
from ovc_main.views import dashboard_views
from rapidsms.backends.kannel.views import KannelBackendView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'ovc_home.views.ovc_login', name='ovc_login'),
    url(r'^change_password/', 'ovc_home.views.change_password', name='change_password'),
    url(r'^logout/', 'ovc_home.views.ovc_logout', name='ovc_logout'),
    url(r'^allocate_role/', 'ovc_home.views.allocate_role', name='allocate_role'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^home/', include('ovc_home.urls')),
    url(r'^organisation/', include('ovc_main.urls')),
    url(r'^workforce/', include('ovc_main.urls')),
    url(r'^beneficiary/', include('ovc_main.urls')),
    url(r'^capture/', include('ovc_main.urls')),
    url(r'^api/', include('ovc_main.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^dashboard/', dashboard_views.report_counts, name='report_counts'),
    url(r'^dashboard_children/', dashboard_views.dashboard_children, name='dashboard_children'),
    url(r'^test/', dashboard_views.test, name='test'),
    #intervention, activities, service, forms
    url(r'^forms/', include('ovc_main.urls')),
    url(r'^reports/', include('ovc_main.urls')),
    #RapidSMS
    url(r'^sms/', include('ovc_sms.urls')),
    url(r'^library/', include('ovc_library.urls')),
    url(r'^kannel/', KannelBackendView.as_view(backend_name="airtel")),

)
