from os.path import splitext
from django.db import models
from time import time


def get_extention(filename):
    ext = splitext(filename)[1][1:].lower()
    return ext


def get_upload_file_name(instance, filename):
    return 'documents/%s.%s' % (str(time()).replace('.', '_'), get_extention(filename))


class Document(models.Model):
    document_id = models.AutoField(primary_key=True)
    document_title = models.CharField(max_length=255, unique=True)
    document_keywords = models.CharField(max_length=255, null=True, blank=True, )
    extension = models.CharField(max_length=4)
    geographic_relevancy_id = models.CharField(max_length=4, null=True, blank=True, )
    stage_id = models.CharField(max_length=4, null=True, blank=True, )
    stage_notes = models.CharField(max_length=255, null=True, blank=True, )
    availability_id = models.CharField(max_length=4)
    document_file = models.FileField(upload_to=get_upload_file_name)
    timestamp_created = models.DateTimeField(auto_now_add=True)
    person_id_created = models.CharField(max_length=128)
    timestamp_updated = models.DateTimeField(auto_now=True, null=True, blank=True, )
    person_id_updated = models.CharField(max_length=128, null=True, blank=True, )
    is_void = models.BooleanField(default=False)


    def __unicode__(self):
        return self.document_title

    def delete(self, *args, **kwargs):
        # You have to prepare what you need before delete the model
        storage, path = self.document_file.storage, self.document_file.path
        # Delete the model before the file
        super(Document, self).delete(*args, **kwargs)
        # Delete the file after the model
        storage.delete(path)

    class Meta:
        db_table = 'tbl_documents'


class DocumentTag(models.Model):
    document = models.ForeignKey(Document)
    tag_id = models.CharField(max_length=4)

    class Meta:
        db_table = 'tbl_documents_tags'
        unique_together = ('document', 'tag_id',)


class DocumentGeo(models.Model):
    document = models.ForeignKey(Document)
    area_id = models.IntegerField()

    class Meta:
        db_table = 'tbl_documents_geo'
        unique_together = ('document', 'area_id',)


class DocumentOrgUnit(models.Model):
    document = models.ForeignKey(Document)
    document_role_id = models.CharField(max_length=4)
    # TODO DocumentOrgUnit - confirm field type
    org_unit_id = models.IntegerField(null=True, blank=True, )
    source_name = models.CharField(max_length=255, null=True, blank=True, )

    class Meta:
        db_table = 'tbl_documents_orgs'


