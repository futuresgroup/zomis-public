from django.conf.urls import url, patterns
from OVC_IMS import settings
from ovc_library import views
from django.conf.urls.static import static


urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^search', views.index_search, name='library_search'),
    url(r'^view', views.index_view, name='library_view'),
    url(r'^new', views.new_library_upload, name='upload'),
    url(r'^delete', views.index_delete, name='library_delete'),
    url(r'^confirm_delete', views.index_delete_confirm, name='library_delete_confirm'),
    url(r'^get_geographic_area', views.get_geographic_area, name='geographic_area_list'),
    url(r'^doc_org_search', views.search_org, name='doc_org_search'),
    url(r'^get_org', views.get_user_org, name='get_org_list'),
    url(r'^get_user_upload_orgs', views.get_upload_orgs, name='get_orgs_upload_role_list'),
    url(r'^upload/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)
