label_document_title = 'Title '
label_document_keywords = 'Keywords '
label_document_types = 'Types of Documents '
label_document_topics = 'Service area(s) '
label_geographical_level = 'Geographical level '
label_geographical_area = 'Geographical area '
label_document_stage = 'Document stage '
label_document_stage_notes = 'Stage notes '
label_document_availability = 'Document availability '
label_document_source = 'Source '
label_document_org_role = 'Organisational units and role in producing document'
label_document_role = 'Document role '
label_document_file = 'Document file '

label_tooltip_document_title = 'Enter a short title of the document '
label_tooltip_document_keywords = 'Enter a list of keywords which library users can use to search for this document '

label_table_heading_source = 'Source'
label_table_heading_role = 'Role'

warning_message_duplicate_dialog = 'A document with this title already exists in the library.  Please modify the document title to make it unique.'
warning_message_deactivation_dialog = 'Are you sure you want to remove this document from the library?'

label_button_add = 'Add'
label_button_cancel = 'Cancel'
label_button_save = 'Upload'

error_message_page_errors = "Document was not uploaded - please correct the errors in red below"
error_message_duplicate_document_title = "A document with this title already exists in the library.  Please modify the document title to make it unique."
error_message_no_document_types = "Please select at least one document type"
error_message_no_document_topics = "Please select at least one document topic"
error_message_no_document_org = "Please choose which organisational unit you are uploading this document on behalf of"
error_message_no_uploaded_org = "You do not have permission to upload documents on behalf of the selected organisational unit. Please choose an organisational unit in which you have a document upload permission."
error_message_no_document_availability = "Please select a document availability setting"
error_message_no_document_file = "Please choose a file to upload"
error_message_invalid_file_size = 'You may only upload documents which are under a file size of %s. This file has a size of %s'
error_message_invalid_file_type = 'The file you want to upload is not an allowable file type. The allowable file types are: %s'