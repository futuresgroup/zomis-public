import os

from rexec import FileWrapper
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from ovc_library import library_service
from ovc_library.library_forms import LibraryUploadForm
from ovc_main.models import SetupList
from models import Document
import json
from django.core import serializers

from django.core.paginator import Paginator
from OVC_IMS import settings
from django.views.static import serve


def index(request):
    # TODO use the methods provided in fields_list_provider.py

    document_type_list = library_service.get_search_document_type_list()
    service_area_list = library_service.get_search_service_area_list()

    documents_list = library_service.get_all_library(request.user)

    pages = Paginator(documents_list, 25)
    pageinfo = {'pageinfo': 'pageinfo',
                'pagescount': pages.num_pages,
                'total_records': pages.count,
                'pagenumber': 1}
    is_capture_app = settings.IS_CAPTURE_SITE

    return render(
        request,
        'library_home.html',
        {'documents_list': pages.page(1),
         'pageinfo': pageinfo,
         'document_type_list': document_type_list,
         'service_area_list': service_area_list,
         'is_capture_app': is_capture_app})


def index_search(request):
    is_capture_app = settings.IS_CAPTURE_SITE
    itemsperpage = 25
    pagenumber = 1

    librarySearch = request.GET['librarySearch']
    documentType = request.GET['documentType']
    serviceArea = request.GET['serviceArea']

    if 'itemsperpage' in request.GET:
        itemsperpage = request.GET['itemsperpage']

    if 'pagenumber' in request.GET:
        pagenumber = request.GET['pagenumber']

    document_type_list = library_service.get_search_document_type_list()
    service_area_list = library_service.get_search_service_area_list()
        
    documents_list = library_service.search_library_json(librarySearch, documentType, serviceArea, request.user)

    if int(itemsperpage) == -1:
        pages = Paginator(documents_list, 0)
        pageinfo = {'pageinfo': 'pageinfo',
                    'pagescount': 1,
                    'total_records': len(documents_list),
                    'pagenumber': 1}
        todisplay = documents_list
    else:
        pages = Paginator(documents_list, itemsperpage)
        pageinfo = {'pageinfo': 'pageinfo',
                    'pagescount': pages.num_pages,
                    'total_records': pages.count,
                    'pagenumber': pagenumber}
        todisplay = pages.page(pagenumber).object_list

    return HttpResponse(json.dumps((todisplay + [pageinfo] )))


def index_view(request):
    documentId = request.GET['document_id']
    document = library_service.get_document_view(documentId, request.user)
    return render(request, 'library_view.html', {'document': document}, )


@login_required(login_url='/')
def index_delete(request):
    documentId = request.GET['document_id']
    document = library_service.get_document_view(documentId, request.user)

    return render(request, 'library_delete.html', {'document': document}, )


@login_required(login_url='/')
def index_delete_confirm(request):
    documentId = request.POST['document_id']
    library_service.delete_document(documentId, request.user)

    return HttpResponseRedirect('/library/')


@login_required(login_url='/')
def new_library_upload(request):
    if not request.user.has_perm('auth.add doc library'):
        return HttpResponseRedirect('/library/')

    form = LibraryUploadForm(request.user)

    if request.POST:
        form = LibraryUploadForm(request.user, request.POST, request.FILES)

        if form.is_valid():
            library_service.save_document(form.cleaned_data, request.user)

            return HttpResponseRedirect('/library/')

    return render(request, 'new_document_upload.html', {'form': form})


def get_geographic_area(request):
    geographic_relevancy = request.GET['geographic_relevancy']
    geographic_area_list_json = library_service.get_area_json(geographic_relevancy)
    return HttpResponse(geographic_area_list_json, content_type="application/json")


def search_org(request):
    org_term = request.GET['term']
    org_list_json = library_service.search_org_json_all(org_term)
    return HttpResponse(org_list_json, content_type="application/json")


def get_user_org(request):
    org_list_json = library_service.get_org_json(request.user)
    return HttpResponse(org_list_json, content_type="application/json")


def get_upload_orgs(request):
    org_with_upload_list_json = library_service.get_orgs_with_upload_json(request.user)
    return HttpResponse(org_with_upload_list_json, content_type="application/json")