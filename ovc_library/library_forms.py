from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText, InlineCheckboxes
from django.template.defaultfilters import filesizeformat
from ovc_library import library_forms_text
from ovc_library import library_service
from OVC_IMS import settings


class LibraryUploadForm(forms.Form):
    document_title = forms.CharField(label=library_forms_text.label_document_title,max_length=255)
    document_keywords = forms.CharField(label=library_forms_text.label_document_keywords, required=False,max_length=255)
    document_types_tags = forms.MultipleChoiceField(choices=library_service.get_document_type_list(),
                                                    label='.')
    document_topics_tags = forms.MultipleChoiceField(choices=library_service.get_service_area_list(),
                                                     label='.')
    # extension

    geographic_relevancy = forms.ChoiceField(choices=library_service.get_geographical_level_list(),
                                             label=library_forms_text.label_geographical_level, required=False)
    geographic_area = forms.MultipleChoiceField(required=False, choices=library_service.get_geographic_area_list(),
                                                label=library_forms_text.label_geographical_area)
    stage = forms.ChoiceField(choices=library_service.get_document_stage_list(),
                              label=library_forms_text.label_document_stage, required=False)
    stage_notes = forms.CharField(label=library_forms_text.label_document_stage_notes,max_length=255, required=False)

    availability = forms.ChoiceField(choices=library_service.get_document_availability_list(),
                                     label=library_forms_text.label_document_availability)

    source = forms.CharField(label=library_forms_text.label_document_source, required=False,max_length=255)
    role = forms.ChoiceField(choices=library_service.get_document_role_list(),
                             label=library_forms_text.label_document_role, required=False)
    document_file = forms.FileField(label=library_forms_text.label_document_file)
    orgTable_data = forms.CharField(label='')


    def __init__(self, user, *args, **kwargs):
        super(LibraryUploadForm, self).__init__(*args, **kwargs)

        self.user = user
        self.helper = FormHelper()
        self.is_update_page = False

        do_display_status = 'display:None'
        dont_display_status = ''
        actionurl = '/library/new/'
        submitButtonText = library_forms_text.label_button_save

        if len(args) > 0:
            print 'ssssssssssssssss'

        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = actionurl

        self.helper.layout = Layout(
            MultiField(
                '',
                Div(
                    Div(
                        HTML('<p/>'),
                        AppendedText('document_title',
                                     '<span data-toggle="tooltip" title="' + library_forms_text.label_tooltip_document_title + '" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                        AppendedText('document_keywords',
                                     '<span data-toggle="tooltip" title="' + library_forms_text.label_document_keywords + '" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                        css_class='panel-body pan',
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    Div(HTML(library_forms_text.label_document_types),
                        css_class='panel-heading',
                        style=dont_display_status,
                    ),
                    Div(
                        HTML('<p/>'),
                        Field('document_types_tags', template='multiselect_checkboxes.html'),
                        css_class='panel-body pan'
                    ),
                    css_class='panel panel-primary',
                    style=dont_display_status,
                ),
                Div(
                    Div(HTML(library_forms_text.label_document_topics),
                        css_class='panel-heading',
                        style=dont_display_status,
                    ),
                    Div(
                        HTML('<p/>'),
                        Field('document_topics_tags', template='multiselect_checkboxes.html'),
                        css_class='panel-body pan'
                    ),
                    css_class='panel panel-primary',
                    style=dont_display_status,
                ),


                Div(
                    Div(HTML(library_forms_text.label_geographical_level),
                        css_class='panel-heading',
                        style=dont_display_status,
                    ),
                    Div(
                        HTML('<p/>'),
                        Field('geographic_relevancy'),
                        css_class='panel-body pan'
                    ),

                    Div(
                        HTML("""
                             <label class="control-label col-md-3"></label>
                             <div class="col-md-6">
                                <select id="geographic_area" multiple="multiple" name="geographic_area" ismultiselect="True" class="control">
                             </select>
                             </div>
                        """),
                        css_id='area',
                        style=do_display_status,
                        css_class='panel-body pan'
                    ),
                    css_class='panel panel-primary',
                    style=dont_display_status,
                ),
                Div(
                    Div(HTML(library_forms_text.label_document_stage),
                        css_class='panel-heading',
                        style=dont_display_status,
                    ),
                    Div(
                        HTML('<p/>'),
                        Field('stage'),
                        css_class='panel-body pan'
                    ),
                    Div(
                        HTML('<p/>'),
                        Field('stage_notes'),
                        css_class='panel-body pan'
                    ),
                    css_class='panel panel-primary',
                    style=dont_display_status,
                ),
                Div(
                    Div(HTML(library_forms_text.label_document_org_role),
                        css_class='panel-heading',
                        style=dont_display_status,
                    ),
                    Div(
                        Div(
                            HTML('<p/>'),
                            Field('source'),
                            HTML('<input type="hidden" name="source_id" id="source_id" />'),
                        ),
                        Div(
                            Field('role'),
                            css_class='panel-body pan'
                        ),
                        Div(
                            HTML("""<div  class="has-error">
  <div class="checkbox">
    <label>
      <strong>.</strong>
    </label>
  </div>
</div>"""),
                            css_id="orgrolerror",
                        ),
                        Div(
                            HTML(
                                "<br><a type=\"button\" id=\"add_orgunit\" class=\"btn btn-info org_data_hidden\"><i class=\"fa fa-plus\"></i>" + library_forms_text.label_button_add + "</a><br><br>"),
                            style='margin-left:26%',
                        ),

                        css_class="col-md-5",
                    ),

                    Div(
                        HTML("""
                                    <table id="orgTable"  class="table orgTable">
                                        <thead>
                                            <tr>
                                                <th style="width:1%"></th>
                                                <th style="width:1%"></th>
                                                <th>""" + library_forms_text.label_table_heading_source + """</th>
                                                <th>""" + library_forms_text.label_table_heading_role + """</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    """),
                        css_class='col-md-7'
                    ),
                    Div(
                        Field('orgTable_data', style=do_display_status),
                    ),
                    Div(
                        HTML('<p/>'),
                        Field('availability'),
                        css_class='panel-body pan'
                    ),
                    Div(
                        HTML('<p/>'),
                        Field('document_file'),

                        css_class='panel-body pan'
                    ),
                    css_class='panel panel-primary',
                    style=dont_display_status,
                ),


                Div(
                    HTML(
                        "<Button id='submit_library_upload' type='submit'  style=\"margin-right:4px\" class=\"mandatory btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> " + submitButtonText + "</Button>"),
                    HTML(
                        "<a type=\"button\" href=\"/library/\"class=\"mandatory btn btn-success\"><i class=\"fa fa-undo\"></i> " + library_forms_text.label_button_cancel + "</a><br><br>"),
                    style='margin-left:38%'
                ),
            )

        )

    def clean(self):

        document_title = self.cleaned_data.get('document_title')
        if library_service.check_duplicates(document_title):
            self._errors["document_title"] = self.error_class(
                [u"" + library_forms_text.error_message_duplicate_document_title])

        document_types_tags = self.cleaned_data.get('document_types_tags')
        document_topics_tags = self.cleaned_data.get('document_topics_tags')
        orgTable_data = self.cleaned_data.get('orgTable_data')
        document_file = self.cleaned_data.get('document_file')
        availability = self.cleaned_data.get('availability')

        if not document_types_tags:
            self._errors["document_types_tags"] = self.error_class(
                [u"" + library_forms_text.error_message_no_document_types])

        if not document_topics_tags:
            self._errors["document_topics_tags"] = self.error_class(
                [u"" + library_forms_text.error_message_no_document_topics])

        if not orgTable_data:
            self._errors["orgTable_data"] = self.error_class(
                [u"" + library_forms_text.error_message_no_document_org])

        if not document_file:
            self._errors["document_file"] = self.error_class(
                [u"" + library_forms_text.error_message_no_document_file])

        if not availability:
            self._errors["availability"] = self.error_class(
                [u"" + library_forms_text.error_message_no_document_availability])
        if availability:
            if availability == '-1':
                self._errors["availability"] = self.error_class(
                    [u"" + library_forms_text.error_message_no_document_availability])

        if document_file:
            file_type = library_service.get_file_extention_from_filename(document_file.name)
            if document_file._size > settings.MAX_UPLOAD_SIZE:
                self._errors["document_file"] = self.error_class(
                    [u"" + (library_forms_text.error_message_invalid_file_size) % (
                        filesizeformat(settings.MAX_UPLOAD_SIZE), filesizeformat(document_file._size))])
            else:
                if file_type not in settings.ALLOWED_FILE_TYPES:
                    self._errors["document_file"] = self.error_class(
                        [u"" + (library_forms_text.error_message_invalid_file_type) % (
                            ' '.join(settings.ALLOWED_FILE_TYPES))])

        if self._errors:
            self._errors["__all__"] = self.error_class([u"" + library_forms_text.error_message_page_errors])



        return self.cleaned_data






