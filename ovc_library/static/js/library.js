$(document).ready(function () {

    var itsonLoad = false;

    var currentOrgroles = $.cookie('srcorgrole');
    var myTableArray = [];

    var uploadableOrgs = [];
    var uploadError = '-1';

    getallowedOrgRole();

    setdefaultOrgRole();

    /*


     max-height: 100px;
     overflow-y: auto;

     */


    $("select[name='geographic_relevancy']").change(function () {
        var selectedValue = $("select[name='geographic_relevancy']").val();
        $.ajax({
            type: "GET",
            url: "/library/get_geographic_area",
            data: "geographic_relevancy=" + selectedValue,
            dataType: 'json',
            success: function (response) {
                var options = '';
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i].area_id + '">' + response[i].area_name + '</option>\n';
                }
                if (response.length == 0) {
                    $("#area").hide();
                } else {
                    $("#area").show();
                }
                $("select#geographic_area").html(options);
                $("#geographic_area").multiSelect('refresh');
            }
        });
    });

    $(document).on('click', '#del_orgunit', function () {
        $(this).closest('tr').remove();
        updateOrgRole();
        return false;
    });

    $("#id_source").autocomplete({
        source: function (request, response) {
            $("#source_id").val("-1"),
                $.get("/library/doc_org_search", {
                    term: request.term
                }, function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.name,
                            value: item.name,
                            sourceID: item.id
                        }
                    }))
                }, "json");
        },
        minLength: 1,
        dataType: "json",
        cache: false,
        select: function (event, ui) {
            $("#source_id").val(ui.item.sourceID ? ui.item.sourceID : "-1");
        },
        change: function (event, ui) {
            $("#source_id").val(ui.item != null ? ui.item.sourceID : "-1");
        }
    });

    $("head").append ("<style> .ui-autocomplete { max-height: 200px; overflow-y: auto; } </style>");

    function checkaddedorgunit(selectedOrg, selectedRole) {

        validorgrole = true;

        if (selectedRole == '283' & selectedOrg == '-1') {
            validorgrole = false;
            alert("Please choose one of your registered parent organisational units as the organisational unit which is uploading this document");
            return validorgrole;
        }

        var uploadedCount = 0;
        var authorCount = 0;
        var tb = $('#orgTable');
        tb.find('tbody tr').each(function (index, row) {

            if ($(this).find('td').eq(1).text() == '283') {
                uploadedCount++;
            }
            if ($(this).find('td').eq(1).text() == '284') {
                authorCount++;
            }
        });

        if (selectedRole == '283') {
            uploadedCount++;
        }
        if (selectedRole == '284') {
            authorCount++;
        }

        if (uploadedCount > 1 | authorCount > 1) {
            validorgrole = false;
            alert("Only one uploaded org unit and only one leading author org unit are allowed");
        }
        return validorgrole;
    }

    function checkValidRole() {

        validorgrole = true;

        var uploadedCount = 0;
        var authorCount = 0;
        var contributingCount = 0;
        var tb = $('#orgTable');

        tb.find('tbody tr').each(function (index, row) {

            if ($(this).find('td').eq(1).text() == '283') {
                uploadedCount++;
            }
            if ($(this).find('td').eq(1).text() == '284') {
                authorCount++;
            }
            /*
             if ($(this).find('td').eq(1).text() == '285') {
             contributingCount++;
             }

             $(this).closest('tr').remove();
             */

        });
        return validorgrole;
    }

    $("#add_orgunit").click(function () {
        itsonLoad = false;
        loaddefaultOrg();
    });


    function loaddefaultOrg() {

        if (uploadableOrgs.length == 0) {
            getallowedOrgRole();
        }

        var selectedOrg = $("#source_id").val();
        var freetextOrg = $("#id_source").val();
        var selectedRole = $("#id_role").val();

        if (!checkaddedorgunit(selectedOrg, selectedRole)) {
            return;
        }

        if (selectedRole == '-1') {
            return;
        }

        if (($.trim(selectedOrg).length == 0 | selectedOrg == '-1') && ($.trim(freetextOrg).length == 0)) {
            return;
        }

        if (selectedOrg !== '-1' && selectedRole == 283) {
            var found = false;
            for (var i = 0; i < uploadableOrgs.length; i++) {
                if (parseInt(uploadableOrgs[i].id) == parseInt(selectedOrg)) {
                    found = true;
                }
            }
            if (found == false && itsonLoad == false) {
                alert('You don’t have the role uploaded in the selected organisational unit');
                return;
            }
        }

        $("#orgTable").find('tbody').append($('<tr>')
                .append($('<td id="tbl_source_id">')
                    .append($('<label>')
                        .text($("#source_id").val())
                        .css('visibility', 'hidden')
                        .css('width', '1%')
                )
            )
                .append($('<td id="tbl_role_id">')
                    .append($('<label>')
                        .text($("select#id_role").val())
                        .css('visibility', 'hidden')
                        .css('width', '1%')
                )
            )
                .append($('<td id="tbl_source_name">')
                    .append($('<label>')
                        .text($("#id_source").val())
                )
            )
                .append($('<td id="tbl_role_name">')
                    .append($('<label>')
                        .text($("select#id_role option:selected").text())
                )
            )
                .append($("<td id='tbl_del_orgunit'> <button class='removebutton' id='del_orgunit'><i class='fa fa-trash fa-2'></i></button></td>")
            )
        );

        if (checkOrgRoleExists()) {
            return;
        }
        addOrgRole();
    }

    function addOrgRole() {

        if ($("#id_orgTable_data").text() != '') {
            return;
        }
        var data = {};
        var sindex = 1;
        $("#orgTable tr").each(function (i, v) {
            if (i < sindex) {
                return;
            }
            data[i] = {};
            $(this).children('td').each(function (ii, vv) {
                data[i][this.id] = $(this).text();
            });
        });
        var org_data = JSON.stringify(data);
        $("#id_orgTable_data").val(org_data);

        myTableArray = [];
        $("table#orgTable tr").each(function () {
            var arrayOfThisRow = [];
            var tableData = $(this).find('td');
            if (tableData.length > 0) {
                tableData.each(function () {
                    arrayOfThisRow.push($(this).text());
                });
                myTableArray.push(arrayOfThisRow);
            }
        });

        $.cookie('srcorgrole', JSON.stringify(myTableArray));


        resetOrgRoleEntry();
    }

    function resetOrgRoleEntry() {

        $("#id_source").val('');
        $("#source_id").val('');
        $("#id_role").val(-1);

    }

    function checkOrgRoleExists() {
        var seen = {};
        duplicates = false;
        $('#orgTable tbody tr').each(function () {

            var txt = $(this).text();
            if (seen[txt]) {
                duplicates = true;
                $(this).closest('tr').remove();
            }
            seen[txt] = true;
        });
        return duplicates;
    }

    function updateOrgRole() {

        var data = {};
        var sindex = 1;
        $("#orgTable tr").each(function (i, v) {
            if (i < sindex) {
                return;
            }
            data[i] = {};
            $(this).children('td').each(function (ii, vv) {
                data[i][this.id] = $(this).text();
            });
        });
        var org_data = JSON.stringify(data);
        if (org_data == '{}') {
            $("#id_orgTable_data").val('');
        } else {
            $("#id_orgTable_data").val(org_data);
        }


        $("table#orgTable tr").each(function () {
            var arrayOfThisRow = [];
            var tableData = $(this).find('td');
            if (tableData.length > 0) {
                tableData.each(function () {
                    arrayOfThisRow.push($(this).text());
                });
                myTableArray.push(arrayOfThisRow);
            }
        });
        $.cookie('srcorgrole', JSON.stringify(myTableArray));
    }

    function setdefaultOrgRole() {

        if (currentOrgroles == '-1') {
            itsonLoad = true;
            $.getJSON('/library/get_org', function (json) {
                    if (jQuery.isEmptyObject(json)) {
                        return;
                    }
                    $("#id_source").val(json[0].name);
                    $("#source_id").val(json[0].id);
                    $("#id_role").val(283);
                    //$("#add_orgunit").trigger("click");
                    loaddefaultOrg();

                }
            );
        } else {
            itsonLoad = false;
            $("#orgTable tbody").empty();
            var t = JSON.parse(currentOrgroles);
            for (index = 0; index < t.length; index++) {
                createFromMemory(t[index][0], t[index][1], t[index][2], t[index][3]);
            }
            if (t.length > 0) {
                var data = {};
                var sindex = 1;
                $("#orgTable tr").each(function (i, v) {
                    if (i < sindex) {
                        return;
                    }
                    data[i] = {};
                    $(this).children('td').each(function (ii, vv) {
                        data[i][this.id] = $(this).text();
                    });
                });
                var org_data = JSON.stringify(data);
                $("#id_orgTable_data").val(org_data);
            }
        }
    }

    function createFromMemory(tbl_source_id, tbl_role_id, tbl_source_name, tbl_role_name) {

        $("#orgTable").find('tbody').append($('<tr>')
                .append($('<td id="tbl_source_id">')
                    .append($('<label>')
                        .text(tbl_source_id)
                        .css('visibility', 'hidden')
                        .css('width', '1%')
                )
            )
                .append($('<td id="tbl_role_id">')
                    .append($('<label>')
                        .text(tbl_role_id)
                        .css('visibility', 'hidden')
                        .css('width', '1%')
                )
            )
                .append($('<td id="tbl_source_name">')
                    .append($('<label>')
                        .text(tbl_source_name)
                )
            )
                .append($('<td id="tbl_role_name">')
                    .append($('<label>')
                        .text(tbl_role_name)
                )
            )
                .append($("<td id='tbl_del_orgunit'> <button class='removebutton' id='del_orgunit'><i class='fa fa-trash fa-2'></i></button></td>")
            )
        );
    }

    function getallowedOrgRole() {

        $.getJSON("/library/get_user_upload_orgs", function (data) {
            $.each(data, function (key, val) {
                uploadableOrgs.push(val);
            });
        });

    }

    //file upload validation

    function formatBytes(bytes, decimals) {
        if (bytes == 0) return '0 Byte';
        var k = 1000;
        var dm = decimals + 1 || 3;
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        var i = Math.floor(Math.log(bytes) / Math.log(k));
        return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
    }

    $("#id_document_file").change(function () {
        //TODO read this form settings.py
        var limitSize = 5242880;
        var iSize = 0;
        uploadError = '-1';


        if ($.browser.msie) {
            var objFSO = new ActiveXObject("Scripting.FileSystemObject");
            var sPath = $("#id_document_file")[0].value;
            var objFile = objFSO.getFile(sPath);
            var iSize = objFile.size;
        }
        else {
            iSize = $("#id_document_file")[0].files[0].size;
        }
        if (iSize == 0) {
            uploadError = 'file uploaded is ' + formatBytes(iSize, 1) + ' ,you cannot upload an empty file ';
        }
        if (iSize > limitSize) {
            uploadError = 'file uploaded is ' + formatBytes(iSize, 1) + ' ,maximum allowed size is: ' + formatBytes(limitSize, 0);
        }
    });

    $('form').submit(function () {

        $('#submit_library_upload').prop("disabled", true);

        if (uploadError !== '-1') {
            alert(uploadError);
            $('#submit_library_upload').prop("disabled", false);
            event.preventDefault();
            return false;
        }
    })


});


