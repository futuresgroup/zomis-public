$(document).ready(function () {

    $.removeCookie('srcorgrole', {path: '/library/new'});
    $.cookie('srcorgrole', '-1', {path: '/library/new'});

    librarypagination = function (pagenumber) {
        $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
        $(".searching").show();
        $(".totalrecords").hide();

        var librarySearch = $("#searchlibrarytxt").val();
        var documentType = $("#document_type_list").val();
        var serviceArea = $("#service_area_list").val();
        var itemsperPage = $(".gw-pageSize").val();

        $.ajax({
            dataType: "json",
            url: "/library/search?librarySearch=" + librarySearch + "&documentType=" + documentType + "&serviceArea=" + serviceArea + "&pagenumber=" + pagenumber + "&itemsperpage=" + itemsperPage,
            success: function (results) {
                if (results.length == 0) {
                    $('#libraryresults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
                }
                else {
                    $('#libraryresults tbody').empty().html("");
                    $.each(results, function (index) {
                        if (results[index].pageinfo == 'pageinfo') {
                            $(".countpages").empty().html(results[index].pagescount);
                            $(".totalrecords").empty().append("Found " + results[index].total_records + " matching library document(s)");

                            //set the control number
                            $(".gw-page").val(results[index].pagenumber);
                            if (results[index].pagenumber >= results[index].pagescount) {
                                if (!$(".gw-next").hasClass("disabled")) {
                                    $(".gw-next").addClass("disabled");
                                }
                                if ($(".gw-prev").hasClass("disabled")) {
                                    $(".gw-prev").removeClass("disabled");
                                }
                            }
                            if (results[index].pagescount == 1) {
                                if (!$(".gw-next").hasClass("disabled")) {
                                    $(".gw-next").addClass("disabled");
                                }
                                if (!$(".gw-prev").hasClass("disabled")) {
                                    $(".gw-prev").addClass("disabled");
                                }
                            }
                            if (results[index].pagenumber <= 1) {
                                if (!$(".gw-prev").hasClass("disabled")) {
                                    $(".gw-prev").addClass("disabled");
                                }
                            }
                            if (results[index].pagenumber < results[index].pagescount) {
                                if ($(".gw-next").hasClass("disabled")) {
                                    $(".gw-next").removeClass("disabled");
                                }
                            }
                            if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                if ($(".gw-prevt").hasClass("disabled")) {
                                    $(".gw-prev").removeClass("disabled");
                                }
                            }
                        }
                        else {
                            <!-- (!results[index].is_capture_app) -->

                            var row_html = "<tr class='table_row_clickable' href='/library/view?document_id=" + results[index].document_id + "' style='cursor:pointer'>";
                            $("#libraryresults > tbody").append(
                                "<tr>" +
                                "<td>" + results[index].document_title + "</td> " +
                                "<td>" + results[index].document_types + "</td> " +
                                "<td>" + results[index].document_keywords + "</td> " +
                                "<td>" + results[index].document_service_area + "</td> " +
                                "<td>" + results[index].document_geo_links + "</td> " +
                                "<td> \
                                     <span> \
                                        <a href='/library/view?document_id=" + results[index].document_id + "' class='btn btn-primary btn-xs'> \
                                            <i class='fa fa-file-text-o'></i>&nbsp;Document info \
                                        </a> \
                                    </span> \
                                    <span> \
                                        <a href=" + results[index].document_url + " class='btn btn-success btn-xs'> \
                                            <i class='fa fa-download'></i>&nbsp;Download \
                                        </a> \
                                    </span> \
                                     <span> \
                                        <a href='/library/delete?document_id=" + results[index].document_id + "' class='btn btn-danger btn-xs'> \
                                            <i class='fa fa-trash-o'></i>&nbsp;Remove \
                                        </a> \
                                    </span> \
                                </td>" +
                                "</tr>"
                            );
                        }
                    });
                }
            },
            error: function () {
                $('#libraryresults tbody').html("<tr><td span='5'>No results for search term supplied</td></tr>");
                //alert($("#searchorgtxt").val())
            }
        });

        $(".searching").fadeOut('slow', function () {
            $(".totalrecords").fadeIn('slow');
        });
        //$(".totalrecords").show();
    };
    $("#searchlibrary").click(function () {
        librarypagination(1);
    });
    $(".gw-pageSize").change(function () {
        $(".gw-pageSize").val($(".gw-pageSize").val());
        librarypagination(1);
    });

    $(".gw-next").click(function () {
        var pagenumber = $(".gw-page").val();
        librarypagination(++pagenumber);
    });

    $(".gw-prev").click(function () {
        var pagenumber = $(".gw-page").val();
        librarypagination(--pagenumber);
    });
});



