from ovc_auth.models import OVCRole, OVCUserRoleGeoOrg, AppUser
from ovc_main.models import RegOrgUnit
from ovc_main.utils import notification_manager
from ovc_auth.user_manager import assign_role, get_ovc_user_roles, \
    remove_role, reset_ovc_user_role_geo_org, create_user_from_workforce_id, \
    reset_password, deactivate_user, activate_user

def process_allocate_values(data, user_logged_in, workforce_id):
    print data, 'in allocate roles data'
    
    
    workforce_id = int(workforce_id)
    usertoallocate_role = ''
    
    if AppUser.objects.filter(reg_person__pk=workforce_id).count() > 0:
        usertoallocate_role = AppUser.objects.get(reg_person__pk=workforce_id)
    else:
        usertoallocate_role = create_user_from_workforce_id(workforce_id)
    user = usertoallocate_role
    
    update_roles(workforce_id,data, user)
    process_other(user, data)

def process_other(user, data):
    for field, value in data:
        if field == 'resetpassword':
            password = reset_password(user)
            if user.reg_person:
                notification_manager.change_passord_notification(user.reg_person, user, password)
        elif field == 'userstate' and 'deactivate' in value:
            deactivate_user(user)
        elif field == 'userstate' and 'activate' in value:
            activate_user(user)
            
def update_roles(workforce_id, data, user):
    roles = {role.group_id:role for role in OVCRole.objects.all()}
    

    
    
    user_existing_roles = get_ovc_user_roles(user)
    roles_allocated = []    
    
    reset_ovc_user_role_geo_org(user)
    
    for request_role_id, request_role_values in data:
        if request_role_id in roles:
            role = roles[request_role_id]
            
            roles_allocated.append(role)
            
            if role.restricted_to_org_unit:
                for value in request_role_values:
                    value = int(value)
                    print value, 'in restricted to org unit'
                    print role, 'role, in restricted org unit'
                    if role in user_existing_roles and \
                        OVCUserRoleGeoOrg.objects.filter(user=user, 
                                                         group=role, 
                                                         org_unit__id=value).count() > 0:
                        continue
                    assign_role(user, role.group_name, organisationid=value)
                    
            elif role.restricted_to_geo:
                for value in request_role_values:
                    value = int(value)
                    print value, 'in restricted to geo'
                    print user,role.group_name, value,'in geo, to get assigned 1'
                    if role in user_existing_roles and OVCUserRoleGeoOrg.objects.filter(user=user, 
                                                         group=role, 
                                                         area__area_id=value).count() > 0:
                        continue
                    print user,role.group_name, value, 'in geo, to get assigned'
                    assign_role(user,role.group_name, area_id=value)
                    
            else:
                if role in user_existing_roles:
                    continue
                assign_role(user, role.group_name)
                
    rolestodelete = list(set(user_existing_roles) - set(roles_allocated))
    
    for roletodelete in rolestodelete:
        remove_role(user, roletodelete)

def prepare_form_fields(loggedin_user, workforce_id):
    from ovc_main.workforce_registration import load_wfc_from_id
    
    workforce_id = int(workforce_id)
    
    usertoallocate_role = ''
    
    if AppUser.objects.filter(reg_person__pk=workforce_id).count() > 0:
        usertoallocate_role = AppUser.objects.get(reg_person__pk=workforce_id)
    else:
        usertoallocate_role = create_user_from_workforce_id(workforce_id)

    user = usertoallocate_role
    
    
    workforce = load_wfc_from_id(workforce_id)
    districts_without_children = workforce_districts_without_wards(workforce)
    
    roles = OVCRole.objects.all()
    userroles = {}
    
    for geoorginfo in OVCUserRoleGeoOrg.objects.filter(user=user):
        
        if geoorginfo.group in userroles and geoorginfo.area:
            userroles[geoorginfo.group]['geo'].append(geoorginfo.area.area_id)
        elif geoorginfo.group not in userroles and geoorginfo.area:
            userroles[geoorginfo.group]={'geo':[geoorginfo.area.area_id]}
            
        if geoorginfo.group in userroles and geoorginfo.org_unit:
            userroles[geoorginfo.group]['org'].append(geoorginfo.org_unit.pk)
        elif geoorginfo.group not in userroles and geoorginfo.org_unit:
            userroles[geoorginfo.group]={'org':[geoorginfo.org_unit.pk]}
            
            
    fordisplay = {'non':[], 'geo':[], 'org':[], 'user_is_active':user.is_active}
    for districtid, districtname in districts_without_children:
        rowdic = {'dstid':districtid, 'dstname':districtname, 'columns':[]}
        for role in roles:
            if role.restricted_to_geo:
                checked = ''
                if role in userroles and districtid in userroles[role]['geo']:
                    checked = 'checked'
                rowdic['columns'].append({'group_id':role.group_id, 
                                          'group_name':role.group_name,
                                          'checked':checked})
        fordisplay['geo'].append(rowdic)
    

    orgsdict = {org.org_id:org.primary_org for org in workforce.org_units}
    orgs = [(org.pk, org.org_unit_name, orgsdict[org.org_unit_id_vis]) for org in RegOrgUnit.objects.filter(org_unit_id_vis__in=orgsdict.keys())]
    for org in RegOrgUnit.objects.filter(org_unit_id_vis__in=orgsdict.keys()):
        rowdic = {'orgid':org.pk,'orgdisplayid':org.org_unit_id_vis, 'orgname':org.org_unit_name, 'isprimaryorg':orgsdict[org.org_unit_id_vis], 'columns':[]}
        for role in roles:
            if role.restricted_to_org_unit:
                checked = ''
                if role in userroles and org.pk in userroles[role]['org']:
                    checked = 'checked'
                rowdic['columns'].append({'group_id':role.group_id, 
                                          'group_name':role.group_name,
                                         'checked':checked})
        
        fordisplay['org'].append(rowdic)
        print fordisplay
                
    existingroles = get_ovc_user_roles(user)
    for role in OVCRole.objects.all():
        if not role.restricted_to_geo and not role.restricted_to_org_unit:
            checked = ''
            if existingroles and role in existingroles:
                checked = 'checked'
            fordisplay['non'].append((role, checked))

    return fordisplay, workforce

def workforce_districts_without_wards(workforce):
    
    from ovc_main.utils import geo_location
    
    
    workforce_districts = workforce.geo_location['districts']
    workforce_wards = workforce.geo_location['wards']
    geoparents = []
    if workforce_wards:
        for ward in workforce_wards:
            geoparents = geoparents + geo_location.get_geo_ancestors_ids(ward)
    if workforce_districts:
        districts_without_children = [dstid for dstid in workforce_districts if dstid not in geoparents]
        districts_without_children = geo_location.get_geo_info(districts_without_children)
    
        return districts_without_children
    return []