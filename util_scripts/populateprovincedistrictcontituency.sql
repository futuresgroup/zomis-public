--check instruction on how to use script before the where statement
--this is a script off a 4 tables, province, district, constituency and ward. it can be adapted by changing the appropriate 
--foreign keys
with cte (prv_name, dst_name, cst_name, wrd_name, prv_id_row, dst_id_row,cst_id_row, wrd_id_row, rownum, prv_part, dst_part, cst_part) as
(
	select 
		prv_name, 
		dst_name, 
		cst_name, 
		wrd_name,
		
		dense_rank()over(order by p.prv_id) prv_id_row,
		(dense_rank()over(order by p.prv_id,d.dst_id) + 11) dst_id_row,
		(dense_rank()over(order by p.prv_id,d.dst_id, cst_id) + 200) cst_id_row,
		(dense_rank()over(order by p.prv_id,d.dst_id, cst_id, wrd_id) + 500) wrd_id_row,
		ROW_NUMBER() over(order by wrd_id) rownumber,
		dense_rank()over(partition by prv_id order by wrd_id) prv_part,
		dense_rank()over(partition by dst_id order by wrd_id) dst_part,
		dense_rank()over(partition by cst_id order by wrd_id) cst_part
		

	from tbl_province p
	join tbl_district d on p.prv_id = d.dst_parent_id
	join tbl_constituency c on d.dst_id = c.cst_parent_id
	join tbl_ward w on c.cst_id = w.wrd_parent_id

	--where dst_name in ('ndola','lusaka','namwala')
	
	
)
select 

'{
"model":"ovc_org.SetupGeorgraphy",
"pk":'+CAST(prv_id_row as varchar(5))+',
"fields":{
	"area_id":'+cast( prv_id_row as varchar(5))+',
	"area_type_id":"province",
	"area_name": "'+prv_name+'"
}},' as province
,'{
"model":"ovc_org.SetupGeorgraphy",
"pk":'+CAST(dst_id_row as varchar(5))+',
"fields":{
	"area_id":'+cast( dst_id_row as varchar(5))+',
	"area_type_id":"district",
	"area_name": "'+dst_name+'",
	"parent_area_id": '+cast(prv_id_row as varchar(5))+'
}},' as district,
'{
"model":"ovc_org.SetupGeorgraphy",
"pk":'+CAST(cst_id_row as varchar(5))+',
"fields":{
	"area_id":'+cast( cst_id_row as varchar(5))+',
	"area_type_id":"constituency",
	"area_name": "'+cst_name+'",
	"parent_area_id": '+cast(dst_id_row as varchar(5))+'
}},' as constituency,
'{
"model":"ovc_org.SetupGeorgraphy",
"pk":'+CAST(wrd_id_row as varchar(5))+',
"fields":{
	"area_id":'+cast( wrd_id_row as varchar(5))+',
	"area_type_id":"ward",
	"area_name": "'+wrd_name+'",
	"parent_area_id": '+cast(cst_id_row as varchar(5))+'
}},' as ward
,*

from cte
--when selecting province, use prv_part, for district use dst_part, for constituency use cst_part, for ward, remove the condition altogether.
--when selecting province, copy the province column's json output, for district, copy the district column, for constituency the constituency column
--and for ward the ward column
--Note, you will have to run this script 4 times to get your results for each level, in the process chaing the where condition below
where cst_part = 1

